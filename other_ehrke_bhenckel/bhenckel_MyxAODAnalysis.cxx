#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include "xAODRootAccess/tools/TFileAccessTracer.h"
#include <xAODEventInfo/EventInfo.h>
#include <xAODCore/ShallowCopy.h>
#include "xAODTruth/xAODTruthHelpers.h"
#include "METUtilities/METHelpers.h"
#include "xAODBase/IParticleHelpers.h"

#include "xAODMetaData/FileMetaData.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

MORE_DIAGNOSTICS()

float getPtAtFirstMeasurement( const xAOD::TrackParticle* tp ) {
    if ( tp == nullptr ) return 0.0f;
    unsigned int index;
    if ( tp->indexOfParameterAtPosition( index, xAOD::FirstMeasurement ) )
        return std::hypot(tp->parameterPX(index), tp->parameterPY(index)); // hypotenuse with overflow safety
    return tp->pt();
}

/// This ptconv is the old one used by MVACalib
float compute_ptconv(const xAOD::Photon* ph) {
    auto vx = ph->vertex();
    if (!vx) return 0.0;

    TLorentzVector sum;
    if (vx->trackParticle(0)) sum += vx->trackParticle(0)->p4();
    if (vx->trackParticle(1)) sum += vx->trackParticle(1)->p4();
    return sum.Perp();
}

float compute_pt1conv(const xAOD::Photon* ph) {  
    static const SG::AuxElement::Accessor<float> accPt1("pt1");
    const xAOD::Vertex* vx = ph->vertex();
  
    if (!vx) return 0.0;
    if (accPt1.isAvailable(*vx)) {
        return accPt1(*vx);
    } else {
        return getPtAtFirstMeasurement(vx->trackParticle(0));
    }
}

float compute_pt2conv(const xAOD::Photon* ph) {
    static const SG::AuxElement::Accessor<float> accPt2("pt2");
    const xAOD::Vertex* vx = ph->vertex();
      
    if (!vx) return 0.0;
    if (accPt2.isAvailable(*vx)) {
    return accPt2(*vx);
    } else {
    return getPtAtFirstMeasurement(vx->trackParticle(1)); 
    }
}

float compute_convEtOverPt(const xAOD::Photon* ph) {
    auto cl = ph->caloCluster();
    
    float Eacc = cl->energyBE(1) + cl->energyBE(2) + cl->energyBE(3);
    float cl_eta = cl->eta();
    
    float rv = 0.0;
    if (xAOD::EgammaHelpers::numberOfSiTracks(ph) == 2) {
        rv = std::max(0.0f, Eacc/(std::cosh(cl_eta)*compute_ptconv(ph)));
    }
    return std::min(rv, 2.0f);
}

float compute_convPtRatio(const xAOD::Photon* ph) {
    if (xAOD::EgammaHelpers::numberOfSiTracks(ph) == 2) {
        auto pt1 = compute_pt1conv(ph);
        auto pt2 = compute_pt2conv(ph);
        return std::max(pt1, pt2)/(pt1+pt2);
    } else {
        return 1.0f;
    }
}
    
MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
    m_grl ("GoodRunsListSelectionTool/grl", this),
    m_PileupReweighting ("CP::PileupReweightingTool/tool", this),
    m_MCShifterTool ( "ElectronPhotonShowerShapeFudgeTool", this),
    m_egammaCalibrationSmearingTool ( "CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool", this),
    m_photonSFReco ( "AsgPhotonEfficiencyCorrection", this),
    m_photonSFID ( "AsgPhotonEfficiencyCorrection", this),
    m_photonSFIso ( "AsgPhotonEfficiencyCorrection", this),
    // m_electronSFRecoFwd ( "AsgElectronEfficiencyCorrection", this),    
    // m_electronSFIDFwd ( "AsgElectronEfficiencyCorrection", this),
    m_muonCalibrationSmearingTool ( "CP::MuonCalibrationAndSmearingTool/MuonCalibrationAndSmearingTool", this),
    m_muonSelectionToolLoose ( "CP::MuonSelectionTool", this),
    m_muonSF ( "CP::MuonEfficiencyScaleFactors", this),
    m_muonSFIso ( "CP::MuonEfficiencyScaleFactors", this),
    m_isolationSelectionToolLoose ( "CP::IsolationSelectionTool", this),
    m_jetCalibrationTool ( "JetCalibrationTool", this),
    m_metutil ( "MetMaker", this),
    m_lgbm_pdfvars ( "LightGBMpredictor", this),
    m_lgbm_convvars ( "LightGBMpredictor", this),
    m_lgbm_binvars ( "LightGBMpredictor", this),
    m_lgbm_noconvvars ( "LightGBMpredictor", this),
    m_lgbm_nocellvars ( "LightGBMpredictor", this),
    m_lgbm_extvars ( "LightGBMpredictor", this),
    m_lgbm_iso ( "LightGBMpredictor", this),
    // m_lgbm_fwd_pid ( "LightGBMpredictor", this),
    // m_lgbm_fwd_er ( "LightGBMpredictor", this),
    // m_lgbm_fwd_iso ( "LightGBMpredictor", this),    
    m_images ( "ImageCreation", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  declareProperty ("grlTool", m_grl);
  declareProperty ("pileupReweighting", m_PileupReweighting);
  declareProperty ("egammaCalibrationAndSmearingTool", m_egammaCalibrationSmearingTool);
  declareProperty ("MCShifterTool", m_MCShifterTool);
  declareProperty ("photonSFReco", m_photonSFReco);
  declareProperty ("photonSFID", m_photonSFID);
  declareProperty ("photonSFIso", m_photonSFIso);
  // declareProperty ("electronSFRecoFwd", m_electronSFRecoFwd);  
  // declareProperty ("electronSFIDFwd", m_electronSFIDFwd);  
  declareProperty ("muonCalibrationSmearingTool", m_muonCalibrationSmearingTool);
  declareProperty ("muonSelectionToolLoose", m_muonSelectionToolLoose);
  declareProperty ("muonSF", m_muonSF);
  declareProperty ("muonSFIso", m_muonSFIso);
  declareProperty ("isolationSelectionToolLoose", m_isolationSelectionToolLoose);
  declareProperty ("jetCalibrationTool", m_jetCalibrationTool);
  declareProperty ("metutil", m_metutil);
  declareProperty ("lgbmPdfVar", m_lgbm_pdfvars);
  declareProperty ("lgbmPdfConvVar", m_lgbm_convvars);
  declareProperty ("lgbmPdfBinVar", m_lgbm_binvars);
  declareProperty ("lgbmPdfNoConvVar", m_lgbm_noconvvars);
  declareProperty ("lgbmPdfNoCellVar", m_lgbm_nocellvars);
  declareProperty ("lgbmPdfExtVar", m_lgbm_extvars);
  declareProperty ("lgbmIso", m_lgbm_iso);
  // declareProperty ("lgbmFwdPid", m_lgbm_fwd_pid);
  // declareProperty ("lgbmFwdEr", m_lgbm_fwd_er);
  // declareProperty ("lgbmFwdIso", m_lgbm_fwd_iso);
  declareProperty ("imageCreation", m_images);
}


StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.



  ANA_MSG_DEBUG("::initialize: 4x::enableFailure");
  CP::SystematicCode::enableFailure();
  StatusCode::enableFailure();
  xAOD::TReturnCode::enableFailure();
  CP::CorrectionCode::enableFailure();

  ANA_CHECK( requestFileExecute() );
  ANA_MSG_DEBUG("::initialize: xAOD::TFileAccessTracer::enableDataSubmission(false);");
  xAOD::TFileAccessTracer::enableDataSubmission(false);

  // Setup photon multiplicity cutflow histograms
  m_CutFlows["Init"] = new multiplicityCutFlow;
  m_CutFlows["OR"] = new multiplicityCutFlow;
  m_CutFlows["Pt"] = new multiplicityCutFlow;
  m_CutFlows["Eta"] = new multiplicityCutFlow;
  m_CutFlows["Iso"] = new multiplicityCutFlow;
  m_CutFlows["LHLoose"] = new multiplicityCutFlow;

  // // Setup fwd electron multiplicity cutflow histograms
  // m_FwdCutFlows["Fwd_Init"] = new multiplicityCutFlow;
  // m_FwdCutFlows["Fwd_Pt"] = new multiplicityCutFlow;
  // m_FwdCutFlows["Fwd_Eta"] = new multiplicityCutFlow;
  // m_FwdCutFlows["Fwd_OQ"] = new multiplicityCutFlow;
  // m_FwdCutFlows["Fwd_LHMedium"] = new multiplicityCutFlow;
  
  // Initialize cut flow histograms
  ANA_CHECK( createCutFlowHists(m_CutFlows) );
  // ANA_CHECK( createCutFlowHists(m_FwdCutFlows) );  

  // Initialize histograms for electron kinematics, with and without Iso/ID selection
  ANA_CHECK( book( TH1F ( "h_eta", "h_eta", 50, -2.47, 2.47) ) );
  ANA_CHECK( book( TH1F ( "h_phi", "h_phi", 50, -3.15, 3.15) ) );
  ANA_CHECK( book( TH1F ( "h_et", "h_et", 50, 10, 200) ) );
  ANA_CHECK( book( TH1F ( "h_eta_LHLoose", "h_eta_LHLoose", 50, -2.47, 2.47) ) );
  ANA_CHECK( book( TH1F ( "h_phi_LHLoose", "h_phi_LHLoose", 50, -3.15, 3.15) ) );
  ANA_CHECK( book( TH1F ( "h_et_LHLoose", "h_et_LHLoose", 50, 10, 200) ) ); 

  // // Initialize histograms for fwd electron kinematics, with and without ID selection
  // ANA_CHECK( book( TH1F ( "h_fwd_eta", "h_fwd_eta", 50, -4.9, 4.9) ) );
  // ANA_CHECK( book( TH1F ( "h_fwd_phi", "h_fwd_phi", 50, -3.15, 3.15) ) );
  // ANA_CHECK( book( TH1F ( "h_fwd_et", "h_fwd_et", 50, 20, 200) ) );
  // ANA_CHECK( book( TH1F ( "h_fwd_eta_LHMedium", "h_fwd_eta_LHMedium", 50, -4.9, 4.9) ) );
  // ANA_CHECK( book( TH1F ( "h_fwd_phi_LHMedium", "h_fwd_phi_LHMedium", 50, -3.15, 3.15) ) );
  // ANA_CHECK( book( TH1F ( "h_fwd_et_LHMedium", "h_fwd_et_LHMedium", 50, 20, 200) ) ); 
  // 
  // Initialize histograms for number of leptons saved and the sum of weights histogram
  ANA_CHECK( book( TH1I ( "h_nPhotons", "h_nPhotons", 5, 0, 5) ) );
  // ANA_CHECK( book( TH1I ( "h_nFwdElectrons", "h_nFwdElectrons", 5, 0, 5) ) );  
  ANA_CHECK( book( TH1F ( "sumOfWeights", "sumOfWeights", 6, 0, 6) ) );
  
  // Cutflow histograms for photons
  ANA_CHECK( book( TH1I ( "h_cutFlow", "h_cutFlow", 11, 0, 11) ) );
  ANA_CHECK( book( TH1I ( "h_indivCutFlow", "h_indivCutFlow", 11, 0, 11) ) );
  ANA_CHECK( book( TH1I ( "h_truthCutFlow", "h_truthCutFlow", 11, 0, 11) ) );
  ANA_CHECK( book( TH1I ( "h_indivTruthCutFlow", "h_indivTruthCutFlow", 11, 0, 11) ) ); 
  // 
  // // Cutflow histograms for fwd electrons  
  // ANA_CHECK( book( TH1I ( "h_FwdCutFlow", "h_FwdCutFlow", 5, 0, 5) ) );
  // ANA_CHECK( book( TH1I ( "h_FwdIndivCutFlow", "h_FwdIndivCutFlow", 5, 0, 5) ) );
  // ANA_CHECK( book( TH1I ( "h_FwdTruthCutFlow", "h_FwdTruthCutFlow", 5, 0, 5) ) );
  // ANA_CHECK( book( TH1I ( "h_FwdIndivTruthCutFlow", "h_FwdIndivTruthCutFlow", 5, 0, 5) ) );   
  
  // Set the bin labels to the corresponding selection for photon cutflows
  for ( const auto& hist : {hist( "h_cutFlow" ), hist( "h_indivCutFlow" ), hist( "h_truthCutFlow" ), hist( "h_indivTruthCutFlow" )}){
      hist->GetXaxis()->SetBinLabel(1, "Init");
      hist->GetXaxis()->SetBinLabel(2, "OR");
      hist->GetXaxis()->SetBinLabel(3, "Pt");
      hist->GetXaxis()->SetBinLabel(4, "Eta");
  }

  // // Set the bin labels to the corresponding selection for fwd electron cutflows
  // for ( const auto& hist : {hist( "h_FwdCutFlow" ), hist( "h_FwdIndivCutFlow" ), hist( "h_FwdTruthCutFlow" ), hist( "h_FwdIndivTruthCutFlow" )}){
  //     hist->GetXaxis()->SetBinLabel(1, "Init");
  //     hist->GetXaxis()->SetBinLabel(2, "Pt");
  //     hist->GetXaxis()->SetBinLabel(3, "Eta");
  //     hist->GetXaxis()->SetBinLabel(4, "OQ");
  //     hist->GetXaxis()->SetBinLabel(5, "LHMedium");
  // }
  
  // Initialize output tree
  ANA_CHECK( book( TTree ("analysis", "My analysis ntuple") ) );
  TTree* m_myTree = tree ("analysis");
  m_myTree->SetAutoFlush(500);
  
  // Event level variables
  m_myTree->Branch( "runNumber", &event_runNumber );
  m_myTree->Branch( "evtNumber", &event_evtNumber );
  m_myTree->Branch( "MCEventWeight", &event_MCWeight ); 
  m_myTree->Branch( "correctedScaledAverageMu", &event_correctedScaledAverageMu );
  m_myTree->Branch( "correctedScaledActualMu", &event_correctedScaledActualMu );
  m_myTree->Branch( "crossSectionFactor", &event_crossSection );
  m_myTree->Branch( "NvtxReco", &NvtxReco );
  m_myTree->Branch( "EventTrigger", &trig );
  m_myTree->Branch( "eventWeight", &eventWeight );
  m_myTree->Branch( "mcChannelNumber", &event_mcChannelNumber );
  m_myTree->Branch( "nPhotons", &nPhotons );
  m_myTree->Branch( "met_met", &met_met );
  m_myTree->Branch( "met_phi", &met_phi ); 
  
  // Photon ID outputs
  m_myTree->Branch( "pdf_score", &pdf_score );
  m_myTree->Branch( "pdfconv_score", &pdfconv_score );
  m_myTree->Branch( "pdfbin_score", &pdfbin_score );
  m_myTree->Branch( "pdfnoconv_score", &pdfnoconv_score );
  m_myTree->Branch( "pdfnocell_score", &pdfnocell_score );
  m_myTree->Branch( "pdfext_score", &pdfext_score );
  m_myTree->Branch( "pdf_logitscore", &pdf_logitscore );
  m_myTree->Branch( "pdfconv_logitscore", &pdfconv_logitscore );
  m_myTree->Branch( "pdfbin_logitscore", &pdfbin_logitscore );
  m_myTree->Branch( "pdfnoconv_logitscore", &pdfnoconv_logitscore );
  m_myTree->Branch( "pdfnocell_logitscore", &pdfnocell_logitscore );
  m_myTree->Branch( "pdfext_logitscore", &pdfext_logitscore );
  m_myTree->Branch( "LHLoose", &LHLoose );
  m_myTree->Branch( "LHTight", &LHTight );
  
  // Photon truth information
  m_myTree->Branch( "truthPdgId", &truthPdgId );
  m_myTree->Branch( "truthType", &truthType );
  m_myTree->Branch( "truthOrigin", &truthOrigin );
  m_myTree->Branch( "truthParticle", &truthParticle );
  m_myTree->Branch( "truth_eta", &truth_eta );
  m_myTree->Branch( "truth_phi", &truth_phi );
  m_myTree->Branch( "truth_m", &truth_m );
  m_myTree->Branch( "truth_px", &truth_px );
  m_myTree->Branch( "truth_py", &truth_py );
  m_myTree->Branch( "truth_pz", &truth_pz );
  m_myTree->Branch( "truth_E", &truth_E ); 
  
  // Photon kinematics
  m_myTree->Branch( "e", &e );
  m_myTree->Branch( "eta", &eta );
  m_myTree->Branch( "phi", &phi );
  m_myTree->Branch( "pt", &pt );
  m_myTree->Branch( "et", &et );
  
  // Photon scale factors
  m_myTree->Branch( "recoSF", &recoSF );
  m_myTree->Branch( "idSF", &idSF );
  m_myTree->Branch( "isoSF", &isoSF );
  m_myTree->Branch( "combSF", &combSF );
  
  // Photon LH variables
  m_myTree->Branch( "Rhad1", &Rhad1 ); // X
  m_myTree->Branch( "Rhad", &Rhad ); // X
  m_myTree->Branch( "weta2", &weta2 ); // X
  m_myTree->Branch( "Rphi", &Rphi ); // X
  m_myTree->Branch( "Reta", &Reta ); // X
  m_myTree->Branch( "Eratio", &Eratio ); // X
  m_myTree->Branch( "wtots1", &wtots1 ); // X
  m_myTree->Branch( "DeltaE", &DeltaE ); // X
  m_myTree->Branch( "weta1", &weta1 ); // X ws3
  m_myTree->Branch( "fracs1", &fracs1 ); // X fside
  m_myTree->Branch( "f1", &f1 );

  // Photon Conversion variable
  m_myTree->Branch( "nTrackPart", &nTrackPart );
  m_myTree->Branch( "photonConversionType", &photonConversionType );
  m_myTree->Branch( "photonConversionRadius", &photonConversionRadius );
  m_myTree->Branch( "photonVertexConvEtOverPt", &photonVertexConvEtOverPt );
  m_myTree->Branch( "photonVertexConvPtRatio", &photonVertexConvPtRatio );
  
  // Photon Extra Variables
  m_myTree->Branch( "maxEcell_time", &maxEcell_time );
  m_myTree->Branch( "maxEcell_energy", &maxEcell_energy );
  m_myTree->Branch( "core57cellsEnergyCorrection", &core57cellsEnergyCorrection );
  m_myTree->Branch( "r33over37allcalo", &r33over37allcalo );
  m_myTree->Branch( "phiModCalo", &phiModCalo );
  
  // Photon Isolation variables
  m_myTree->Branch( "GradientIso", &GradientIso );
  m_myTree->Branch( "topoetcone20", &topoetcone20 );
  m_myTree->Branch( "topoetcone30", &topoetcone30 );
  m_myTree->Branch( "topoetcone40", &topoetcone40 );
  m_myTree->Branch( "ptvarcone20", &ptvarcone20 );
  m_myTree->Branch( "ptvarcone30", &ptvarcone30 );
  m_myTree->Branch( "ptvarcone40", &ptvarcone40 );
  m_myTree->Branch( "LGBM_iso", &LGBM_iso );
  m_myTree->Branch( "LGBM_logitiso", &LGBM_logitiso );
  
  // Additional variables for ER with CNN
  m_myTree->Branch( "f0Cluster", &f0Cluster );
  m_myTree->Branch( "R12", &R12 );
  m_myTree->Branch( "fTG3", &fTG3 );
  m_myTree->Branch( "eAccCluster", &eAccCluster );
  m_myTree->Branch( "cellIndexCluster", &cellIndexCluster );
  m_myTree->Branch( "etaModCalo", &etaModCalo );
  m_myTree->Branch( "dPhiTH3", &dPhiTH3 );
  m_myTree->Branch( "poscs1", &poscs1 );
  m_myTree->Branch( "poscs2", &poscs2 );
  
  // Photon images 
  // em calo energy
  m_myTree->Branch( "em_calo0", &em_calo0 );
  m_myTree->Branch( "em_calo1", &em_calo1 );
  m_myTree->Branch( "em_calo2", &em_calo2 );
  m_myTree->Branch( "em_calo3", &em_calo3 );
  m_myTree->Branch( "em_calo4", &em_calo4 );
  m_myTree->Branch( "em_calo5", &em_calo5 );
  m_myTree->Branch( "em_calo6", &em_calo6 );
  m_myTree->Branch( "em_calo7", &em_calo7 );
  
  // h calo energy
  m_myTree->Branch( "h_calo0", &h_calo0 );
  m_myTree->Branch( "h_calo1", &h_calo1 );
  m_myTree->Branch( "h_calo2", &h_calo2 );
  m_myTree->Branch( "h_calo3", &h_calo3 );
  m_myTree->Branch( "h_calo4", &h_calo4 );
  m_myTree->Branch( "h_calo5", &h_calo5 );
  m_myTree->Branch( "h_calo6", &h_calo6 );
  m_myTree->Branch( "h_calo7", &h_calo7 );
  
  // // fwd electrons ID outputs
  // m_myTree->Branch( "fwd_LHLoose", &fwd_LHLoose );
  // m_myTree->Branch( "fwd_LHMedium", &fwd_LHMedium );
  // m_myTree->Branch( "fwd_LHTight", &fwd_LHTight );
  // m_myTree->Branch( "fwd_LGBM_pid", &fwd_LGBM_pid );
  // 
  // // fwd electrons truth information
  // m_myTree->Branch( "fwd_truthPdgId", &fwd_truthPdgId );
  // m_myTree->Branch( "fwd_truthType", &fwd_truthType );
  // m_myTree->Branch( "fwd_truthOrigin", &fwd_truthOrigin );
  // m_myTree->Branch( "fwd_truthParticle", &fwd_truthParticle );
  // m_myTree->Branch( "fwd_truth_eta", &fwd_truth_eta );
  // m_myTree->Branch( "fwd_truth_phi", &fwd_truth_phi );
  // m_myTree->Branch( "fwd_truth_E", &fwd_truth_E ); 
  // 
  // // fwd electrons kinematics
  // m_myTree->Branch( "fwd_eta", &fwd_eta );
  // m_myTree->Branch( "fwd_phi", &fwd_phi );
  // m_myTree->Branch( "fwd_pt", &fwd_pt );
  // m_myTree->Branch( "fwd_et", &fwd_et );
  // m_myTree->Branch( "fwd_LGBM_energy", &fwd_LGBM_energy );
  // m_myTree->Branch( "fwd_LGBM_et", &fwd_LGBM_et );
  // 
  // // fwd isolation vars
  // m_myTree->Branch( "fwd_topoetcone20", &fwd_topoetcone20 );
  // m_myTree->Branch( "fwd_topoetcone30", &fwd_topoetcone30 );
  // m_myTree->Branch( "fwd_topoetcone40", &fwd_topoetcone40 );
  // m_myTree->Branch( "fwd_topoetconecoreConeEnergyCorrection", &fwd_topoetconecoreConeEnergyCorrection );
  // m_myTree->Branch( "fwd_LGBM_iso", &fwd_LGBM_iso );
  // 
  // // fwd electrons kinematics
  // m_myTree->Branch( "fwd_recoSF", &fwd_recoSF );
  // m_myTree->Branch( "fwd_idSF", &fwd_idSF );
  // m_myTree->Branch( "fwd_combSF", &fwd_combSF );
  
  //============================================================================
  // Retrieving Tools
  //============================================================================
  ANA_MSG_DEBUG("::initialize: Retrieving Tools");
  
  // GRL tool
  ANA_CHECK( m_grl.retrieve() );

  // PRW ( change appropiately to year )
  ANA_CHECK( m_PileupReweighting.retrieve() );

  // Loose Isolation selection tool
  ANA_CHECK( m_isolationSelectionToolLoose.retrieve() );

   // Egamma shower shape fudge tool
  ANA_CHECK( m_MCShifterTool.retrieve() );
  
  // Photon calibration and smearing tool
  ANA_CHECK( m_egammaCalibrationSmearingTool.retrieve() );
  
  // Photon reconstruction scale factor tool ( one is needed for all different tpyes(Reco,ID,...) )
  ANA_CHECK( m_photonSFReco.retrieve() );
  ANA_CHECK( m_photonSFID.retrieve() );
  ANA_CHECK( m_photonSFIso.retrieve() );
  
  // Muon Tools
  ANA_CHECK( m_muonCalibrationSmearingTool.retrieve() );
  ANA_CHECK( m_muonSelectionToolLoose.retrieve() );
  ANA_CHECK( m_muonSF.retrieve() );
  ANA_CHECK( m_muonSFIso.retrieve() );
  // Jet Calibration
  ANA_CHECK( m_jetCalibrationTool.retrieve() );

  // met maker
  ANA_CHECK( m_metutil.retrieve() );
  
  // LGBM
  // ID/ISO for photons
  ANA_CHECK( m_lgbm_pdfvars.retrieve() );
  ANA_CHECK( m_lgbm_convvars.retrieve() );
  ANA_CHECK( m_lgbm_binvars.retrieve() );
  ANA_CHECK( m_lgbm_noconvvars.retrieve() );
  ANA_CHECK( m_lgbm_extvars.retrieve() );
  ANA_CHECK( m_lgbm_iso.retrieve() );
  // // ID/ER for fwd electrons
  // ANA_CHECK( m_lgbm_fwd_pid.retrieve() );
  // ANA_CHECK( m_lgbm_fwd_er.retrieve() );
  // ANA_CHECK( m_lgbm_fwd_iso.retrieve() );
    
  // Initialize OverlapRemovalTool
  m_orFlags = new ORUtils::ORFlags("OverlapRemovalTool", "selected", "overlaps");
  m_orFlags->doMuons = true;
  m_orFlags->doJets = true;
  m_orFlags->doTaus = false;
  m_orFlags->doPhotons = true;
  m_orFlags->doFatJets = false;
  m_orFlags->doEleEleOR = true;
  m_orToolbox = new ORUtils::ToolBox();
  ANA_CHECK( ORUtils::recommendedTools(*m_orFlags, *m_orToolbox) );
  ANA_CHECK( m_orToolbox->eleEleORT.setProperty("UseClusterMatch", true) );
  ANA_CHECK( m_orToolbox->setGlobalProperty("OutputLevel", 3) );
  ANA_CHECK( m_orToolbox->initialize() );
  m_orTool = m_orToolbox->masterTool;

  // Images
  ANA_CHECK( m_images.retrieve() );



  m_eventCounter = 0;
  Info( "initialize()", "Initialization done" ); 
  return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis :: fileExecute ()
{
    TString dirname = gSystem->Getenv("WorkDir_DIR"); 

        
    const xAOD::FileMetaData* fmld = new xAOD::FileMetaData();
    float mcProcID = -999;
    if( inputMetaStore()->retrieve(fmld, "FileMetaData").isSuccess()  ) {
        fmld->value( xAOD::FileMetaData::mcProcID, mcProcID);   
    }

    event_isMC = mcProcID > 1;
    Info( "fileExecute()", "mcProcID: %f", mcProcID );
    Info( "fileExecute()", "File is MC: %d", event_isMC);

    if ( event_isMC ){
        //check if file is from a DxAOD
        const std::string dummy = "StreamAOD";
        bool m_isDerivation = !( inputMetaStore()->contains<std::string>( dummy ) );
    
        if(m_isDerivation ){
            const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
            if(!inputMetaStore()->retrieve(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
                Error("initializeEvent()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
                return StatusCode::FAILURE;
            }
            // if ( incompleteCBC->size() != 0 ) {
            //   Error("initializeEvent()","Found incomplete Bookkeepers! Check file for corruption.");
            //   return StatusCode::FAILURE;
            // }
            // Now, let's find the actual information
            const xAOD::CutBookkeeperContainer* completeCBC = 0;
            if(!inputMetaStore()->retrieve(completeCBC, "CutBookkeepers").isSuccess()){
                Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
                return StatusCode::FAILURE;
            }
    
            // First, let's find the smallest cycle number,
            // i.e., the original first processing step/cycle
            int minCycle = 10000;
            for ( auto cbk : *completeCBC ) {
                if ( ! cbk->name().empty()  && minCycle > cbk->cycle() ){ minCycle = cbk->cycle(); }
            }
            // Now, let's actually find the right one that contains all the needed info...
            const xAOD::CutBookkeeper* allEventsCBK=0;
            const xAOD::CutBookkeeper* DxAODEventsCBK=0;
            std::string derivationName = "HIGG1D1Kernel"; //need to replace by appropriate name
            int maxCycle = -1;
            for (const auto& cbk: *completeCBC) {
                if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
                    allEventsCBK = cbk;
                    maxCycle = cbk->cycle();
                }
                
                if ( cbk->name() == derivationName){
                    DxAODEventsCBK = cbk;
                }
            }
            
            uint64_t nEventsProcessed  = allEventsCBK->nAcceptedEvents() + hist("sumOfWeights")->GetBinContent(1);
            double sumOfWeights        = allEventsCBK->sumOfEventWeights() + hist("sumOfWeights")->GetBinContent(2);
            double sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared() + hist("sumOfWeights")->GetBinContent(3);
    
            uint64_t nEventsDxAOD           = DxAODEventsCBK->nAcceptedEvents() + hist("sumOfWeights")->GetBinContent(4);
            double sumOfWeightsDxAOD        = DxAODEventsCBK->sumOfEventWeights() + hist("sumOfWeights")->GetBinContent(5);
            double sumOfWeightsSquaredDxAOD = DxAODEventsCBK->sumOfEventWeightsSquared() + hist("sumOfWeights")->GetBinContent(6);
    
            hist("sumOfWeights")->SetBinContent(1,nEventsProcessed);
            hist("sumOfWeights")->SetBinContent(2,sumOfWeights);
            hist("sumOfWeights")->SetBinContent(3,sumOfWeightsSquared);
            hist("sumOfWeights")->SetBinContent(4,nEventsDxAOD);
            hist("sumOfWeights")->SetBinContent(5,sumOfWeightsDxAOD);
            hist("sumOfWeights")->SetBinContent(6,sumOfWeightsSquaredDxAOD);
    
        }

        //Example of loading in the crossSections into a map
        TTree cross_t; cross_t.ReadFile(dirname + "/data/MyAnalysis/my.metadata.txt"); 
        uint32_t mcchannel=0; double crossSection=0; double kFactor = 0; double filterEff = 0;
        cross_t.SetBranchAddress("dataset_number",&mcchannel);
        cross_t.SetBranchAddress("crossSection",&crossSection);
        cross_t.SetBranchAddress("kFactor",&kFactor);
        cross_t.SetBranchAddress("genFiltEff",&filterEff);
        for(int i=0;i<cross_t.GetEntries();i++) {
            cross_t.GetEntry(i);
            m_crossSections[mcchannel] = crossSection;
            m_kFactors[mcchannel] = kFactor;
            m_filterEffs[mcchannel] = filterEff;
        }
    }

    return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.


  // Print processing rate at set interval, so we know where we are
  ++m_eventCounter;
  if ( m_eventCounter == 1) m_startTime = time(0);
  if ( m_eventCounter % m_progressInterval == 0 ) {
  	Info("execute()","%li events processed so far  <<<==", m_eventCounter );
	Info("execute()","Processing rate = %f Hz", float(m_eventCounter)/float(time(0)-m_startTime));
  }
  ANA_MSG_DEBUG("::execute: Event " << m_eventCounter);

  // retrieve the eventInfo object from the event store
  ANA_CHECK (evtStore()->retrieve (m_eventInfo, "EventInfo"));

  //============================================================================
  // Event cleaning
  //============================================================================
  ANA_MSG_DEBUG("::execute: Event Cleaning");

  // detector errors
  if (
  ( m_eventInfo->errorState( xAOD::EventInfo::LAr )  == xAOD::EventInfo::Error )
  || ( m_eventInfo->errorState( xAOD::EventInfo::Tile ) == xAOD::EventInfo::Error )
  || ( m_eventInfo->errorState( xAOD::EventInfo::SCT )  == xAOD::EventInfo::Error )
  || ( m_eventInfo->isEventFlagBitSet( xAOD::EventInfo::Core, 18 ) )
  )
  return StatusCode::SUCCESS;
  
  // if data check if event passes GRL
  if (!event_isMC) { 
      if (!m_grl->passRunLB(*m_eventInfo)) {
          return StatusCode::SUCCESS; 
      }
  } 

  // Jet cleaning
  if ( m_eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad") == 0 ){
      return StatusCode::SUCCESS;
  }


  //============================================================================
  // Retrieving containers
  //============================================================================
  ANA_MSG_DEBUG("::execute: Retrieving Containers");
  // retrieve photon container 
  ANA_MSG_DEBUG("::execute: Retrieving photons...");
  const xAOD::PhotonContainer* photons_org = 0;
  ANA_CHECK( evtStore()->retrieve( photons_org, "Photons" ) );
  std::pair< xAOD::PhotonContainer*, xAOD::ShallowAuxContainer* > photons_shallowCopy = xAOD::shallowCopyContainer( *photons_org );
  m_photons = photons_shallowCopy.first;
  
  // retrieve electron container 
  ANA_MSG_DEBUG("::execute: Retrieving electrons...");
  const xAOD::ElectronContainer* electrons_org = 0;
  ANA_CHECK( evtStore()->retrieve( electrons_org, "Electrons" ) );
  std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > electrons_shallowCopy = xAOD::shallowCopyContainer( *electrons_org );
  m_electrons = electrons_shallowCopy.first;
  
  // // retrieve fwd electron container 
  // ANA_MSG_DEBUG("::execute: Retrieving fwd electrons...");
  // const xAOD::ElectronContainer* fwdElectrons_org = 0;
  // ANA_CHECK( evtStore()->retrieve( fwdElectrons_org, "ForwardElectrons" ) );
  // std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > fwdElectrons_shallowCopy = xAOD::shallowCopyContainer( *fwdElectrons_org );
  // m_fwdElectrons = fwdElectrons_shallowCopy.first;  
  // std::unique_ptr< xAOD::ElectronContainer > fwdElectrons_p( fwdElectrons_shallowCopy.first );
  // std::unique_ptr< xAOD::ShallowAuxContainer > fwdElectrons_p2( fwdElectrons_shallowCopy.second );
   
  // retrieve vertices
  ANA_MSG_DEBUG("::execute: Retrieving PrimaryVertices...");
  const xAOD::VertexContainer* vertex_org = 0;
  ANA_CHECK( evtStore()->retrieve( vertex_org, "PrimaryVertices") );
  std::pair< xAOD::VertexContainer*, xAOD::ShallowAuxContainer* > vertex_shallowCopy = xAOD::shallowCopyContainer( *vertex_org );
  m_vertex = vertex_shallowCopy.first;
  std::unique_ptr< xAOD::VertexContainer > vertices_p( vertex_shallowCopy.first );
  std::unique_ptr< xAOD::ShallowAuxContainer > vertices_p2( vertex_shallowCopy.second );
  
  // retrieve Tracks
  ANA_MSG_DEBUG("::execute: Retrieving tracks...");
  const xAOD::TrackParticleContainer* indet_tracks_org = 0;
  ANA_CHECK( evtStore()->retrieve( indet_tracks_org, "InDetTrackParticles" ) );
  std::pair< xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer* > indet_tracks_shallowCopy = xAOD::shallowCopyContainer( *indet_tracks_org );
  m_indetTracks.reset( indet_tracks_shallowCopy.first );
  m_indetTracksAux.reset( indet_tracks_shallowCopy.second );
  
  // retrieve muon container 
  ANA_MSG_DEBUG("::execute: Retrieving muons...");
  const xAOD::MuonContainer* muons_org = 0;
  ANA_CHECK( evtStore()->retrieve( muons_org, "Muons" ) );
  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons_org );
  m_muons = muons_shallowCopy.first;
  
  // retrieve jet container
  ANA_MSG_DEBUG("::execute: Retrieving AntiKt4EMPFlowJets...");
  const xAOD::JetContainer* jets_org = 0;
  ANA_CHECK( evtStore()->retrieve( jets_org, "AntiKt4EMPFlowJets" ) );
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets_org );
  m_jets = jets_shallowCopy.first;
  
  if ( event_isMC ){
      ANA_MSG_DEBUG("::execute: Retrieving egammaTruthParticles...");
      ANA_CHECK( evtStore()->retrieve( m_egammaTruthContainer, "egammaTruthParticles" ) );
      
      ANA_MSG_DEBUG("::execute: Retrieving TruthParticles...");      
      ANA_CHECK( evtStore()->retrieve( m_truthParticles, "TruthParticles" ) );
  }
  
  // Missing et stuff
  met::addGhostMuonsToJets(*muons_org, *m_jets);
  
  
  for ( const auto& vtx : *m_vertex ){
      if ( vtx->vertexType() == xAOD::VxType::PriVtx) {
          m_primaryVertex = vtx;
      }
  }
  if ( m_primaryVertex->vertexType() != xAOD::VxType::PriVtx) {
      return StatusCode::SUCCESS;
  }

  //============================================================================
  // Calibrations and Corrections
  //============================================================================
  ANA_MSG_DEBUG("::execute: Calibrations and Corrections");
  for ( const auto& pho : *m_photons ) {
      if ( event_isMC ) {
          // Apply fudge factors to photons
          if ( m_MCShifterTool->applyCorrection( *pho ) != CP::CorrectionCode::Ok ) {
              ANA_MSG_WARNING( "execute(): Photon correction failed on an object.");
          }
      }
      // apply energy corrections to photons
      if( m_egammaCalibrationSmearingTool->applyCorrection( *pho ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Photon calibration failed" );
  }

  for ( const auto& el : *m_electrons ) {
      if ( event_isMC ) {
          // Apply fudge factors to electrons
          if ( m_MCShifterTool->applyCorrection( *el ) != CP::CorrectionCode::Ok ) {
              ANA_MSG_WARNING( "execute(): Electron correction failed on an object.");
          }
      }
      // apply energy corrections to electrons
      if( m_egammaCalibrationSmearingTool->applyCorrection( *el ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Electron calibration failed" );
  }

  // for ( const auto& fel : *m_fwdElectrons ){
  //     // apply energy corrections to fwd electrons      
  //     if( m_egammaCalibrationSmearingTool->applyCorrection( *fel ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Fwd Electron calibration failed" );
  // }
 
  for ( const auto& mu : *m_muons ) {
      // apply energy corrections to muons
      if( m_muonCalibrationSmearingTool->applyCorrection( *mu ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Muon calibration failed" );
  }
 
  for ( const auto& jet : *m_jets ) {
      // apply energy corrections to jets
      if( m_jetCalibrationTool->applyCorrection( *jet) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Jet calibration failed" );
  }
  
  
  ANA_MSG_DEBUG("::execute: Retrieving MET_Core_AntiKt4EMPFlow...");
  ANA_CHECK( evtStore()->retrieve(m_coreMet, "MET_Core_AntiKt4EMPFlow") );
  
  // needed for MET_Maker
  if(!xAOD::setOriginalObjectLink(*photons_org, *m_photons)){//tell calib container what old container it matches
      std::cout << "Failed to set the original object links" << std::endl;
      return 1;
  }
  if(!xAOD::setOriginalObjectLink(*jets_org, *m_jets)){//tell calib container what old container it matches
      std::cout << "Failed to set the original object links" << std::endl;
      return 1;
  }
  if(!xAOD::setOriginalObjectLink(*electrons_org, *m_electrons)){//tell calib container what old container it matches
      std::cout << "Failed to set the original object links" << std::endl;
      return 1;
  }
  if(!xAOD::setOriginalObjectLink(*muons_org, *m_muons)){//tell calib container what old container it matches
      std::cout << "Failed to set the original object links" << std::endl;
      return 1;
  }
  
  // Select objects which should be used in the overlap removal
  const ort::inputDecorator_t selectDec("selected");
  for ( auto pho : *m_photons ){
      if ( pho->pt() > 10000 && abs( pho->caloCluster()->etaBE(2) ) < 2.47){
          selectDec(*pho) = true;
      }
      else{
          selectDec(*pho) = false;
      }
  }
  
  for ( auto el : *m_electrons ){
      if ( el->pt() > 10000 && abs( el->caloCluster()->etaBE(2) ) < 2.47){
          selectDec(*el) = true;
      }
      else{
          selectDec(*el) = false;
      }
  }
  
  for ( auto mu : *m_muons ){
      if ( mu->pt() > 10000 && abs( mu->eta() ) < 2.7 ){
          selectDec(*mu) = true;
      }
      else{
          selectDec(*mu) = false;
      }
  }  
  
  for ( auto jet : *m_jets ){
      if ( jet->pt() > 20000 && abs( jet->eta() ) < 2.7 ){
          selectDec(*jet) = true;
      }
      else{
          selectDec(*jet) = false;
      }
  }  
  
  // record the calibrated objects and there shallow copies ( needed by MET_Maker )
  ANA_CHECK( evtStore()->record(m_photons, "CalibPhotons") );
  ANA_CHECK( evtStore()->record(photons_shallowCopy.second, "CalibPhotonsAux") );
  ANA_CHECK( evtStore()->record(m_jets, "CalibJets") );
  ANA_CHECK( evtStore()->record(jets_shallowCopy.second, "CalibJetsAux") );
  ANA_CHECK( evtStore()->record(m_muons, "CalibMuons") );
  ANA_CHECK( evtStore()->record(muons_shallowCopy.second, "CalibMuonsAux") );  
  ANA_CHECK( evtStore()->record(m_electrons, "CalibElectrons") );
  ANA_CHECK( evtStore()->record(electrons_shallowCopy.second, "CalibElectronsAux") );
 
  //retrieve the MET association map
  ANA_CHECK( evtStore()->retrieve(m_metMap, "METAssoc_AntiKt4EMPFlow") );
  m_metMap->resetObjSelectionFlags();
  
  // Create a MissingETContainer with its aux store for each systematic
  m_newMetContainer    = new xAOD::MissingETContainer();
  m_newMetAuxContainer = new xAOD::MissingETAuxContainer();
  m_newMetContainer->setStore(m_newMetAuxContainer);
  ANA_CHECK( evtStore()->record(m_newMetContainer, "newMETContainer") );
  ANA_CHECK( evtStore()->record(m_newMetAuxContainer, "newMETContainerAux") );  
  
  m_metMap->resetObjSelectionFlags();
  
  //Photons
  ConstDataVector<xAOD::PhotonContainer> metPhotons(SG::VIEW_ELEMENTS);
  for(const auto& pho : *m_photons) {
      if(pho->pt()>10e3 && pho->eta()<2.47 && Accessor_DFCommonPhotonsIsEMLoose( *pho ) ) metPhotons.push_back(pho);
  }
  ANA_CHECK( m_metutil->rebuildMET("RefGamma",             //name of metPhoton in metContainer
                          xAOD::Type::Photon,            //telling the rebuilder that this is electron met
                          m_newMetContainer,             //filling this met container
                          metPhotons.asDataVector(),   //using these metElectrons that accepted our cuts
                          m_metMap)                      //and this association map
                          );

  //Electrons
  ConstDataVector<xAOD::ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);
  for(const auto& el : *m_electrons) {
      if(el->pt()>10e3 && el->eta()<2.47 && Accessor_LHLoose( *el ) ) metElectrons.push_back(el);
  }
  ANA_CHECK( m_metutil->rebuildMET("RefEle",                   //name of metElectrons in metContainer
                          xAOD::Type::Electron,       //telling the rebuilder that this is electron met
                          m_newMetContainer,            //filling this met container
                          metElectrons.asDataVector(),//using these metElectrons that accepted our cuts
                          m_metMap)                     //and this association map
                          );
  
  //Muons
  ConstDataVector<xAOD::MuonContainer> metMuons(SG::VIEW_ELEMENTS);
  for(const auto& mu : *m_muons) {
      if(mu->pt()>10e3 && mu->eta()<2.7 && m_muonSelectionToolLoose->accept( *mu ) ) metMuons.push_back(mu);
  }
  
  ANA_CHECK( m_metutil->rebuildMET("RefMuon",
                                  xAOD::Type::Muon,
                                  m_newMetContainer,
                                  metMuons.asDataVector(),
                                  m_metMap)
                                  );
 
  //Now time to rebuild jetMet and get the soft term
  //This adds the necessary soft term for both CST and TST
  //these functions create an xAODMissingET object with the given names inside the container
  ANA_CHECK( m_metutil->rebuildJetMET("RefJet",        //name of jet met
                                 "SoftClus",      //name of soft cluster term met
                                 "PVSoftTrk",     //name of soft track term met
                                 m_newMetContainer, //adding to this new met container
                                 m_jets,          //using this jet collection to calculate jet met
                                 m_coreMet,         //core met container
                                 m_metMap,          //with this association map
                                 false            //don't apply jet jvt cut
                                 )
         );
  
  ANA_CHECK( m_metutil->buildMETSum("FinalClus", m_newMetContainer, MissingETBase::Source::LCTopo) );
  m_METmaker = ( *m_newMetContainer )["FinalClus"];
  
  
  // Get missing energy
  met_met = m_METmaker->met();
  met_phi = m_METmaker->phi();    
  
  // Do overlap removal
  ANA_CHECK ( m_orTool->removeOverlaps( m_electrons, m_muons, m_jets, nullptr, m_photons) ); 
  
  // Event specific variables
  if ( event_isMC ){
      event_mcChannelNumber = m_eventInfo->mcChannelNumber();
      event_MCWeight = m_eventInfo->mcEventWeight();
      event_crossSection = m_crossSections[event_mcChannelNumber] * m_kFactors[event_mcChannelNumber] * m_filterEffs[event_mcChannelNumber] ;
  }
  event_evtNumber = m_eventInfo->eventNumber();
  event_runNumber = m_eventInfo->runNumber();
  ANA_CHECK(m_PileupReweighting->apply(*m_eventInfo));
  event_pileupweight = m_eventInfo->auxdecor<float>("PileupWeight");
  
  event_correctedScaledAverageMu       = m_PileupReweighting->getCorrectedAverageInteractionsPerCrossing( *m_eventInfo, true );
  event_correctedScaledActualMu        = m_PileupReweighting->getCorrectedActualInteractionsPerCrossing( *m_eventInfo, true );
  
  NvtxReco = m_vertex->size();
  
  // Clear the electron, fwd electron, and muon vectors
  clearVectors();
  
  // Do preselection of electrons, fwd electrons, and muons
  std::vector < const xAOD::Photon* > selPhotons = selectPhotonCandidates();
  // std::vector < const xAOD::Electron* > selFwdElectrons = selectFwdElectronCandidates();
  
  nPhotons = selPhotons.size();
  // nFwdElectrons = selFwdElectrons.size();
  
  // if enough particles left, save information
  //if ( nPhotons + nFwdElectrons >= 2 ){ 
  if ( nPhotons >= 2){
          
      eventWeight = event_pileupweight;
      
      if ( event_isMC ){
          eventWeight = event_MCWeight * event_crossSection * event_pileupweight;
      }
      // fill histograms with number of particles
      hist("h_nPhotons")->Fill( nPhotons, eventWeight );      
      // hist("h_nFwdElectrons")->Fill( nFwdElectrons, eventWeight );                
      // Fill the electrons vectors
      for ( const xAOD::Photon* pho : selPhotons ){
          const TLorentzVector& pho_p4 = pho->p4();
          const xAOD::CaloCluster* cluster = pho->caloCluster();
          
          // ID information
          
          double pdf     = m_lgbm_pdfvars->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
          double pdfconv = m_lgbm_convvars->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
          double pdfbin  = m_lgbm_binvars->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
          double pdfnoconv  = m_lgbm_noconvvars->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
          double pdfnocell  = m_lgbm_nocellvars->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
          double pdfext  =  m_lgbm_extvars->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
          
          pdf_score.push_back( pdf );
          pdfconv_score.push_back( pdfconv );
          pdfbin_score.push_back( pdfbin );
          pdfnoconv_score.push_back( pdfnoconv );
          pdfnocell_score.push_back( pdfnocell );
          pdfext_score.push_back( pdfext );
          pdf_logitscore.push_back( -1. / 15. * log( 1. / pdf - 1. ) );
          pdfconv_logitscore.push_back( -1. / 15. * log( 1. / pdfconv - 1. ) );
          pdfbin_logitscore.push_back( -1. / 15. * log( 1. / pdfbin - 1. ) );
          pdfnoconv_logitscore.push_back( -1. / 15. * log( 1. / pdfnoconv - 1. ) );
          pdfnocell_logitscore.push_back( -1. / 15. * log( 1. / pdfnocell - 1. ) );
          pdfext_logitscore.push_back( -1. / 15. * log( 1. / pdfext - 1. ) );
          LHLoose.push_back( Accessor_DFCommonPhotonsIsEMLoose( *pho ) );
          LHTight.push_back( Accessor_DFCommonPhotonsIsEMTight( *pho ) );
        
          // Truth information
          if ( event_isMC ){
              const xAOD::TruthParticle* tp = xAOD::TruthHelpers::getTruthParticle( *pho );

            if ( tp == nullptr ){
                truthPdgId.push_back( 0 );
                truth_eta.push_back( -999 );
                truth_phi.push_back( -999 );
                truth_m.push_back( -999 );
                truth_px.push_back( -999 );
                truth_py.push_back( -999 );
                truth_pz.push_back( -999 );
                truth_E.push_back( 0 );                
            }
            else{
                truthPdgId.push_back( tp->pdgId() );
                truth_eta.push_back( tp->eta() );
                truth_phi.push_back( tp->phi() );
                truth_m.push_back( tp->m() / 1000. );
                truth_px.push_back( tp->px() / 1000. );
                truth_py.push_back( tp->py() / 1000. );
                truth_pz.push_back( tp->pz() / 1000. );
                truth_E.push_back( tp->e() / 1000. );
                
                
            }
            truthType.push_back( Accessor_truthType( *pho ) );
            truthOrigin.push_back( Accessor_truthOrigin( *pho ) );
            
            bool truthEgParticle = false;
            for ( size_t i = 0; i<TruthPointerIdx.size(); i++){
                if ( pho->index() == TruthPointerIdx[i] ) truthEgParticle = true;
            }
            truthParticle.push_back( truthEgParticle );
        }
        
        // kinematics
        e.push_back( pho->e() );
        eta.push_back( cluster->etaBE(2) );
        phi.push_back( pho_p4.Phi() );
        pt.push_back( pho_p4.Pt() / 1000. );
        et.push_back( pho_p4.Pt() / 1000. );
        
        // Create and same images
        m_images->create_images( *pho );
        em_calo.push_back( m_images->em_calo );
        h_calo.push_back( m_images->h_calo );
        
        em_calo0.push_back( em_calo.back()[0] );
        em_calo1.push_back( em_calo.back()[1] );
        em_calo2.push_back( em_calo.back()[2] );
        em_calo3.push_back( em_calo.back()[3] );
        em_calo4.push_back( em_calo.back()[4] );
        em_calo5.push_back( em_calo.back()[5] );
        em_calo6.push_back( em_calo.back()[6] );
        em_calo7.push_back( em_calo.back()[7] );
        
        h_calo0.push_back( h_calo.back()[0] );
        h_calo1.push_back( h_calo.back()[1] );
        h_calo2.push_back( h_calo.back()[2] );
        h_calo3.push_back( h_calo.back()[3] );
        h_calo4.push_back( h_calo.back()[4] );
        h_calo5.push_back( h_calo.back()[5] );
        h_calo6.push_back( h_calo.back()[6] );
        h_calo7.push_back( h_calo.back()[7] );
        
        // Scale factors
        double recoSf = 1;
        double IdSf = 1;
        double IsoSf = 1;
        if ( event_isMC ){
            if(!m_photonSFReco->getEfficiencyScaleFactor( *pho, recoSf)){
                ATH_MSG_WARNING( "Couldn't get photon scale factor!" );
            }
            if(!m_photonSFID->getEfficiencyScaleFactor( *pho, IdSf)){
                ATH_MSG_WARNING( "Couldn't get photon scale factor!" );
            }
            if(!m_photonSFIso->getEfficiencyScaleFactor( *pho, IsoSf)){
                ATH_MSG_WARNING( "Couldn't get photon scale factor!" );
            }
        }

        recoSF.push_back( recoSf );
        idSF.push_back( IdSf );
        isoSF.push_back( IsoSf );

        combSF.push_back( recoSf * IdSf * IsoSf );
        
        // Additional variables for ER with CNN
        
        eAccCluster.push_back( cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3) );
        double etaCalo;
        double phiCalo;
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::ETACALOFRAME, etaCalo ) )
            ANA_MSG_WARNING( "cannot find etaCalo" );
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::PHICALOFRAME, phiCalo ) )
            ANA_MSG_WARNING( "cannot find phiCalo" );        
              
        f0Cluster.push_back( (std::abs((cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3))) >= 0.0001) ? cluster->energyBE(0) / (cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3)) : -999.0f);
        R12.push_back( (std::abs(cluster->energyBE(2)) >= 0.0001) ? cluster->energyBE(1) / cluster->energyBE(2) : -999.0f ); // float
        fTG3.push_back( (std::abs(cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3)) >= 0.0001) ? (cluster->eSample(CaloSampling::TileGap3) / 1000.)/(cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3)) : -999.0f ); 
        cellIndexCluster.push_back( std::floor( std::abs( etaCalo/0.025 ) ) );
        etaModCalo.push_back( std::fmod(std::abs( etaCalo), 0.025 ) ); 
        phiModCalo.push_back( (std::abs(etaCalo) < 1.425) ? std::fmod(phiCalo, TMath::Pi()/512) : std::fmod(phiCalo, TMath::Pi()/384)); // float
        dPhiTH3.push_back( std::fmod(2.*TMath::Pi()+phiCalo,TMath::Pi()/32.)-TMath::Pi()/64.0 );
        poscs1.push_back( Accessor_poscs1( *pho ) );
        poscs2.push_back( Accessor_poscs2( *pho ) ); 
    
        // LH variables
        Rhad1.push_back( Accessor_Rhad( *pho ) );
        Rhad.push_back( Accessor_Rhad1( *pho ) );
        weta2.push_back( Accessor_weta2( *pho ) );
        Rphi.push_back( Accessor_Rphi( *pho ) );
        Reta.push_back( Accessor_Reta( *pho ) );
        Eratio.push_back( Accessor_Eratio( *pho ) );
        wtots1.push_back( Accessor_wtots1( *pho ) ); 
        DeltaE.push_back( Accessor_DeltaE( *pho ) );
        weta1.push_back( Accessor_weta1( *pho ) ); 
        fracs1.push_back( Accessor_fracs1( *pho ) );
        f1.push_back( Accessor_f1( *pho ) ); 
        
        photonConversionType.push_back( pho->conversionType() );
        photonConversionRadius.push_back( pho->conversionRadius() );    
        photonVertexConvPtRatio.push_back( compute_convPtRatio(pho) ); // float
        photonVertexConvEtOverPt.push_back( compute_convEtOverPt(pho) );
        
        // Photon Extra Variables
        maxEcell_time.push_back( Accessor_maxEcell_time( *pho ) );
        maxEcell_energy.push_back( Accessor_maxEcell_energy( *pho ) );
        core57cellsEnergyCorrection.push_back( Accessor_core57cellsEnergyCorrection( *pho ) );
        r33over37allcalo.push_back( Accessor_r33over37allcalo( *pho ) );
        
        // Isolation variables
        GradientIso.push_back( m_isolationSelectionToolLoose->accept( *pho ) );
        topoetcone20.push_back( Accessor_topoetcone20( *pho ) );
        topoetcone30.push_back( Accessor_topoetcone30( *pho ) );
        topoetcone40.push_back( Accessor_topoetcone40( *pho ) );
        ptvarcone20.push_back( Accessor_ptvarcone20( *pho ) );
        ptvarcone30.push_back( Accessor_ptvarcone30( *pho ) );
        ptvarcone40.push_back( Accessor_ptvarcone40( *pho ) );
        double iso = m_lgbm_iso->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
        LGBM_iso.push_back( iso );
        LGBM_logitiso.push_back(  -1. / 15. * log( 1. / iso - 1. ) );
        
        // Fill kinematics histograms
        if ( event_isMC ){
            hist( "h_eta" )->Fill( cluster->etaBE(2), eventWeight * combSF.back() );
            hist( "h_phi" )->Fill( pho->phi(), eventWeight * combSF.back() );
            hist( "h_et" )->Fill( pho_p4.Pt() / 1000., eventWeight * combSF.back() );
            if ( Accessor_DFCommonPhotonsIsEMLoose( *pho ) && m_isolationSelectionToolLoose->accept( *pho ) ){
                hist( "h_eta_LHLoose" )->Fill( cluster->etaBE(2), eventWeight * combSF.back() );
                hist( "h_phi_LHLoose" )->Fill( pho->phi(), eventWeight * combSF.back() );
                hist( "h_et_LHLoose" )->Fill( pho_p4.Pt() / 1000., eventWeight * combSF.back() );
            }
        }
        else{
            hist( "h_eta" )->Fill( cluster->etaBE(2) );
            hist( "h_phi" )->Fill( pho_p4.Phi() );
            hist( "h_et" )->Fill( pho_p4.Pt() / 1000. );
            if ( Accessor_DFCommonPhotonsIsEMLoose( *pho ) && m_isolationSelectionToolLoose->accept( *pho ) ){
                hist( "h_eta_LHLoose" )->Fill( cluster->etaBE(2) );
                hist( "h_phi_LHLoose" )->Fill( pho_p4.Phi() );
                hist( "h_et_LHLoose" )->Fill( pho_p4.Pt() / 1000. );
            }            
        }
    }
    
    // // Fill the fwd electrons vectors
    // for ( const xAOD::Electron* fel : selFwdElectrons ){
    //     const TLorentzVector& fel_p4 = fel->p4();
    // 
    //     // ID information
    //     fwd_LHLoose.push_back( Accessor_fwdLHLoose( *fel ) );
    //     fwd_LHMedium.push_back( Accessor_fwdLHMedium( *fel ) );
    //     fwd_LHTight.push_back( Accessor_fwdLHTight( *fel ) );
    //     fwd_LGBM_pid.push_back( m_lgbm_fwd_pid->predict( *fel ) );
    // 
    //     // kinematics
    //     fwd_eta.push_back( fel->eta() );
    //     fwd_phi.push_back( fel->phi() );
    //     fwd_et.push_back( fel_p4.Pt() / 1000. );
    //     fwd_LGBM_energy.push_back( m_lgbm_fwd_er->predict(*fel, NvtxReco, event_correctedScaledActualMu) );
    //     fwd_LGBM_et.push_back( fwd_LGBM_energy.back() / cosh(fwd_eta.back()) );
    // 
    //     // Truth information
    //     if ( event_isMC ){
    //         const xAOD::TruthParticle* tp = xAOD::TruthHelpers::getTruthParticle( *fel );
    // 
    //         fwd_truthType.push_back( Accessor_truthType( *fel ) );
    //         fwd_truthOrigin.push_back( Accessor_truthOrigin( *fel ) );
    // 
    //         if ( tp == nullptr ){
    //             fwd_truthPdgId.push_back( 0 );
    //             fwd_truth_eta.push_back( -999 );
    //             fwd_truth_phi.push_back( -999 );
    //             fwd_truth_E.push_back( 0 );                
    //         }
    //         else{            
    //             fwd_truthPdgId.push_back( tp->pdgId() );
    //             fwd_truth_eta.push_back( tp->eta() );
    //             fwd_truth_phi.push_back( tp->phi() );
    //             fwd_truth_E.push_back( tp->e() / 1000. );
    //         }
    // 
    //         bool fwd_truthEgParticle = false;
    //         for ( size_t i = 0; i<TruthPointerIdx_fwd.size(); i++){
    //             if ( fel->index() == TruthPointerIdx_fwd[i] ) fwd_truthEgParticle = true;
    //         }
    //         fwd_truthParticle.push_back( fwd_truthEgParticle );
    //     }
    // 
    //     // Scale factors
    //     double recoSf = 1;
    //     double IdSf = 1;
    //     if ( event_isMC ){
    //         if(!m_electronSFRecoFwd->getEfficiencyScaleFactor( *fel, recoSf)){
    //             ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
    //         }
    //         if(!m_electronSFIDFwd->getEfficiencyScaleFactor( *fel, IdSf)){
    //             ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
    //         }
    //     }
    // 
    //     fwd_recoSF.push_back( recoSf );
    //     fwd_idSF.push_back( IdSf );
    // 
    //     fwd_combSF.push_back( recoSf * IdSf );
    // 
    //     fwd_topoetcone20.push_back( Accessor_topoetcone20( *fel ) );
    //     fwd_topoetcone30.push_back( Accessor_topoetcone30( *fel ) );
    //     fwd_topoetcone40.push_back( Accessor_topoetcone40( *fel ) );
    //     fwd_topoetconecoreConeEnergyCorrection.push_back( Accessor_topoetconecoreConeEnergyCorrection( *fel ) );
    //     fwd_LGBM_iso.push_back( m_lgbm_fwd_iso->predict( *fel ) );
    // 
    // 
    //     // fill kinematic histograms
    //     if ( event_isMC ){
    //         hist( "h_fwd_eta" )->Fill( fel->eta(), eventWeight * combSF.back() );
    //         hist( "h_fwd_phi" )->Fill( fel->phi(), eventWeight * combSF.back() );
    //         hist( "h_fwd_et" )->Fill( fel_p4.Pt() / 1000., eventWeight * combSF.back() );
    //         if ( Accessor_fwdLHMedium( *fel ) ){
    //             hist( "h_fwd_eta_LHMedium" )->Fill( fel->eta(), eventWeight * combSF.back() );
    //             hist( "h_fwd_phi_LHMedium" )->Fill( fel->phi(), eventWeight * combSF.back() );
    //             hist( "h_fwd_et_LHMedium" )->Fill( fel_p4.Pt() / 1000., eventWeight * combSF.back() );
    //         }
    //     }
    //     else{
    //         hist( "h_fwd_eta" )->Fill( fel->eta() );
    //         hist( "h_fwd_phi" )->Fill( fel->phi() );
    //         hist( "h_fwd_et" )->Fill( fel_p4.Pt() / 1000. );
    //         if ( Accessor_fwdLHMedium( *fel ) ){
    //             hist( "h_fwd_eta_LHMedium" )->Fill( fel->eta() );
    //             hist( "h_fwd_phi_LHMedium" )->Fill( fel->phi() );
    //             hist( "h_fwd_et_LHMedium" )->Fill( fel_p4.Pt() / 1000. );
    //         }            
    //     }        
    //     
    // }
    
    // write everything into the tree
    tree ("analysis")->Fill();
  }
  

  
  return StatusCode::SUCCESS;
}

// Function to calculat delta R between two particles
float MyxAODAnalysis::deltaR( const float eta1, const float phi1, const float eta2, const float phi2 ) {
	const float deta = fabs(eta1 - eta2);

	float dphi;
	if ( fabs(phi1 - phi2 - 2.0*3.14159265359) < fabs(phi1 - phi2) ) {
		dphi = fabs(phi1 - phi2 - 2.0*3.14159265359) ;
	} else {
		dphi = fabs(phi1 - phi2);
	}

	return sqrt( deta*deta + dphi*dphi );
}

// Clear the vectors before filling the information of the new event
void MyxAODAnalysis::clearVectors(){
    LHLoose.clear();
    LHTight.clear();    
    pdf_score.clear();
    pdfconv_score.clear();
    pdfbin_score.clear();
    pdfnoconv_score.clear();
    pdfnocell_score.clear();
    pdfext_score.clear();
    pdf_logitscore.clear();
    pdfconv_logitscore.clear();
    pdfbin_logitscore.clear();
    pdfnoconv_logitscore.clear();
    pdfnocell_logitscore.clear();
    pdfext_logitscore.clear();
    truthPdgId.clear();
    truthType.clear();
    truthOrigin.clear();
    
    e.clear();
    m.clear();
    eta.clear();
    phi.clear();
    pt.clear();
    et.clear();
    
    truthParticle.clear();
    truth_m.clear();
    truth_px.clear();
    truth_py.clear();
    truth_pz.clear();
    truth_eta.clear();
    truth_phi.clear();
    truth_E.clear();
    
    recoSF.clear();
    idSF.clear();
    isoSF.clear();
    combSF.clear();
    
    TruthPointerIdx.clear();
    // TruthPointerIdx_fwd.clear();
    Rhad1.clear();
    Rhad.clear();
    weta2.clear();
    Rphi.clear();
    Reta.clear();
    Eratio.clear();
    f1.clear();
    wtots1.clear();
    DeltaE.clear();
    weta1.clear();
    fracs1.clear();
    
    nTrackPart.clear();
    photonConversionType.clear();
    photonConversionRadius.clear();
    photonVertexConvEtOverPt.clear();
    photonVertexConvPtRatio.clear();
    
    maxEcell_time.clear();
    maxEcell_energy.clear();
    core57cellsEnergyCorrection.clear();
    r33over37allcalo.clear();
    phiModCalo.clear();
    
    f0Cluster.clear();
    R12.clear();
    fTG3.clear();
    eAccCluster.clear();
    cellIndexCluster.clear();
    etaModCalo.clear();
    dPhiTH3.clear();
    poscs1.clear();
    poscs2.clear();
    
    GradientIso.clear();
    topoetcone20.clear();
    topoetcone30.clear();
    topoetcone40.clear();
    ptvarcone20.clear();
    ptvarcone30.clear();
    ptvarcone40.clear();
    LGBM_iso.clear();
    LGBM_logitiso.clear();
    
    em_calo.clear();
    h_calo.clear();

    em_calo0.clear();
    em_calo1.clear();
    em_calo2.clear();
    em_calo3.clear();
    em_calo4.clear();
    em_calo5.clear();
    em_calo6.clear();
    em_calo7.clear();
    
    h_calo0.clear();
    h_calo1.clear();
    h_calo2.clear();
    h_calo3.clear();
    h_calo4.clear();
    h_calo5.clear();
    h_calo6.clear();
    h_calo7.clear(); 
        
    // fwd_LHLoose.clear();
    // fwd_LHMedium.clear();
    // fwd_LHTight.clear();
    // fwd_truthPdgId.clear();
    // fwd_truthType.clear();
    // fwd_truthOrigin.clear();
    // fwd_truthParticle.clear();
    // fwd_eta.clear();
    // fwd_phi.clear();
    // fwd_pt.clear();
    // fwd_et.clear();
    // fwd_truth_eta.clear();
    // fwd_truth_phi.clear();
    // fwd_truth_E.clear();
    // fwd_recoSF.clear();
    // fwd_idSF.clear();
    // fwd_combSF.clear();
    // 
    // fwd_LGBM_energy.clear();
    // fwd_LGBM_et.clear();    
    // fwd_LGBM_pid.clear();
    // 
    // fwd_topoetcone20.clear();
    // fwd_topoetcone30.clear();
    // fwd_topoetcone40.clear();
    // fwd_topoetconecoreConeEnergyCorrection.clear();
    // fwd_LGBM_iso.clear();
}



void MyxAODAnalysis::resetCutFlow( std::map< TString, multiplicityCutFlow* > map ){
    for ( auto& cutFlow : map ){
        cutFlow.second->indiv = 0;
        cutFlow.second->acc = 0;
        cutFlow.second->truthIndiv = 0;
        cutFlow.second->truthAcc = 0;        
    }
}


StatusCode MyxAODAnalysis::createCutFlowHists( std::map<TString, multiplicityCutFlow* > map ){
    for ( const auto& string : map ){
        ANA_CHECK( book( TH1D ( "h_acc_" + string.first, "h_acc_" + string.first, 20, 0, 20) ) ) ;
        ANA_CHECK( book( TH1D ( "h_indiv_" + string.first, "h_indiv_" + string.first, 20, 0, 20) ) ) ;
        ANA_CHECK( book( TH1D ( "h_truthAcc_" + string.first, "h_truthAcc_" + string.first, 20, 0, 20) ) ) ;
        ANA_CHECK( book( TH1D ( "h_truthIndiv_" + string.first, "h_truthIndiv_" + string.first, 20, 0, 20) ) ) ;                        
    }
    
    
    
    return StatusCode::SUCCESS;
}

void MyxAODAnalysis::fillCutFlowHists( std::map<TString, multiplicityCutFlow* > map ){
    for ( const auto& string : map ){
        std::string type = string.first.Data();
        hist( "h_indiv_" + type )->Fill(string.second->indiv);
        hist( "h_acc_" + type )->Fill(string.second->acc);
        hist( "h_truthIndiv_" + type )->Fill(string.second->truthIndiv);
        hist( "h_truthAcc_" + type )->Fill(string.second->truthAcc);
    }    
}

void MyxAODAnalysis::increaseCutFlow( multiplicityCutFlow* cutFlow, bool pass, bool &passAll, bool truth_el, int idx, int electron){
    if ( electron == 0 ){
        if ( pass ) {
            cutFlow->indiv++;
            hist( "h_indivCutFlow" )->Fill(idx);
        }
        passAll = pass && passAll; 
        if ( passAll ) {
            cutFlow->acc++;
            hist( "h_cutFlow" )->Fill(idx);
            
        }
        if ( truth_el ){
            if ( pass ) {
                cutFlow->truthIndiv++;
                hist( "h_indivTruthCutFlow" )->Fill(idx);
            }
            if ( passAll ) {
                cutFlow->truthAcc++;        
                hist( "h_truthCutFlow" )->Fill(idx);
            }
        }
    }
    else if ( electron == 1){
        if ( pass ) {
            cutFlow->indiv++;
            hist( "h_muIndivCutFlow" )->Fill(idx);
        }
        passAll = pass && passAll; 
        if ( passAll ) {
            cutFlow->acc++;
            hist( "h_muCutFlow" )->Fill(idx);
            
        }
        if ( truth_el ){
            if ( pass ) {
                cutFlow->truthIndiv++;
                hist( "h_muIndivTruthCutFlow" )->Fill(idx);
            }
            if ( passAll ) {
                cutFlow->truthAcc++;        
                hist( "h_muTruthCutFlow" )->Fill(idx);
            }
        }        
    }
    
    // else if ( electron == 2){
    //     if ( pass ) {
    //         cutFlow->indiv++;
    //         hist( "h_FwdIndivCutFlow" )->Fill(idx);
    //     }
    //     passAll = pass && passAll; 
    //     if ( passAll ) {
    //         cutFlow->acc++;
    //         hist( "h_FwdCutFlow" )->Fill(idx);
    // 
    //     }
    //     if ( truth_el ){
    //         if ( pass ) {
    //             cutFlow->truthIndiv++;
    //             hist( "h_FwdIndivTruthCutFlow" )->Fill(idx);
    //         }
    //         if ( passAll ) {
    //             cutFlow->truthAcc++;        
    //             hist( "h_FwdTruthCutFlow" )->Fill(idx);
    //         }
    //     }        
    // }        
    
    
    return;
}

// std::vector < const xAOD::Electron* > MyxAODAnalysis::selectFwdElectronCandidates(){
//     std::vector < const xAOD::Electron* > electrons_out;
// 
//     resetCutFlow( m_FwdCutFlows );
// 
//     TruthPointerIdx_fwd.clear();
//     if ( event_isMC ){
//         for ( const xAOD::TruthParticle* tp : *m_egammaTruthContainer ){
//             if ( xAOD::EgammaHelpers::getRecoElectron( tp ) != nullptr){
//                 if ( xAOD::EgammaHelpers::getRecoElectron( tp )->author( xAOD::EgammaParameters::AuthorFwdElectron ) ) {
//                     TruthPointerIdx_fwd.push_back(xAOD::EgammaHelpers::getRecoElectron( tp )->index());
//                 }
//             }    
//         }
//     }
// 
//     for ( const auto& fel : *m_fwdElectrons ){
// 
//         bool truth_el = false;
//         if ( event_isMC ){
//             for ( size_t i = 0; i<TruthPointerIdx_fwd.size(); i++){
//                 if ( fel->index() == TruthPointerIdx_fwd[i] ) truth_el = true;
//             }
//         }
// 
//         bool passAll = true;
//         increaseCutFlow( m_FwdCutFlows["Fwd_Init"], true, passAll, truth_el, 0, 2);
// 
// 
//         // transverse momentum cut
//         increaseCutFlow( m_FwdCutFlows["Fwd_Pt"], fel->pt() > 20e3, passAll, truth_el, 1, 2);
// 
// 
//         // Eta cut
//         increaseCutFlow( m_FwdCutFlows["Fwd_Eta"], abs( fel->eta() ) < 4.9, passAll, truth_el, 2, 2);
// 
// 
//         // Object Quality
//         increaseCutFlow( m_FwdCutFlows["Fwd_OQ"], fel->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON), passAll, truth_el, 3, 2);
// 
//         if ( passAll ) electrons_out.push_back( fel );
// 
//         // Medium Likelihood 
//         increaseCutFlow( m_FwdCutFlows["Fwd_LHMedium"], bool(Accessor_fwdLHMedium( *fel )) , passAll, truth_el, 4, 2);
// 
// 
//     }
// 
//     fillCutFlowHists(m_FwdCutFlows);
// 
//     return electrons_out;
// }

std::vector < const xAOD::Photon* > MyxAODAnalysis::selectPhotonCandidates(){
    std::vector < const xAOD::Photon* > photons_out  ;
    
    resetCutFlow( m_CutFlows );
    
    TruthPointerIdx.clear();
    if ( event_isMC ){
        for ( const xAOD::TruthParticle* tp : *m_egammaTruthContainer ){
            if ( xAOD::EgammaHelpers::getRecoPhoton( tp ) != nullptr){
                if ( xAOD::EgammaHelpers::getRecoPhoton( tp )->author( xAOD::EgammaParameters::AuthorFwdElectron ) == false ){
                    TruthPointerIdx.push_back(xAOD::EgammaHelpers::getRecoPhoton( tp )->index());
                }
            }    
        }
    }
    
    for ( const xAOD::Photon* pho : *m_photons ){
    
        bool truth_pho = false;
        if ( event_isMC ){
            for ( size_t i = 0; i<TruthPointerIdx.size(); i++){
                if ( pho->index() == TruthPointerIdx[i] ) truth_pho = true;
            }
        }
        
        bool passAll = true;
        increaseCutFlow( m_CutFlows["Init"], true, passAll, truth_pho, 0, 0);
        
        // Overlap removal
        const ort::inputAccessor_t selectAcc("overlaps");
        increaseCutFlow( m_CutFlows["OR"], !selectAcc( *pho ), passAll, truth_pho, 1, 0);
        
        // transverse momentum cut
        increaseCutFlow( m_CutFlows["Pt"], pho->pt() > 4.5e3, passAll, truth_pho, 2, 0);
        
        if ( passAll ) photons_out.push_back( pho );
        
        // Isolation
        increaseCutFlow( m_CutFlows["Iso"], m_isolationSelectionToolLoose->accept( *pho ), passAll, truth_pho, 8, 0);
        
        // Loose Likelihood 
        increaseCutFlow( m_CutFlows["LHLoose"], bool(Accessor_DFCommonPhotonsIsEMLoose( *pho )), passAll, truth_pho, 10, 0);

    }
    
    fillCutFlowHists(m_CutFlows);
    
    return photons_out;
}

MyxAODAnalysis :: ~MyxAODAnalysis () {

}

StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

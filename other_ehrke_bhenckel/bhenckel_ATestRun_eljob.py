#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
parser.add_option( '-f', '--input-file', dest = 'input_file',
                   action = 'store', type = 'string', default = 'sample',
                   help = 'Input directory for EventLoop' )                   
parser.add_option( '-e', '--events', dest = 'events',
                   action = 'store', type = 'int', default = -1,
                   help = 'Number of events to run' ) 
parser.add_option( '-n', '--nFiles', dest = 'nFiles',
                   action = 'store', type = 'int', default = -1,
                   help = 'Number of files to run on (only interesting for the grid)' )                    
parser.add_option( '-g', '--grid', dest = 'grid',
                   action = 'store', type = 'int', default = 0,
                   help = 'Run on grid' ) 
parser.add_option( '-o', '--outputName', dest = 'outputName',
                   action = 'store', type = 'string', default = "user.lehrke.test",
                   help = 'Name of the outputFile' ) 

                   
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
if not options.grid:
    ROOT.SH.ScanDir().scan( sh, options.input_file ) 
else:
    ROOT.SH.scanRucio (sh, options.input_file )
if ( "mc16_13TeV" in options.input_file ):
    isData = False 
else:
    isData = True
sh.printContent()


# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, options.events )
job.outputAdd(ROOT.EL.OutputStream('ANALYSIS'))

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

alg.OutputLevel = 3

# ============================================================================
# Setting up tools
# ============================================================================


# ============================================================================
# Event
# ============================================================================
# GRL
addPrivateTool( alg, 'grlTool', 'GoodRunsListSelectionTool' )
GRLFilePath = "GoodRunsLists/data17_13TeV/20190708/data17_13TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
# GRLFilePath = "GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
# GRLFilePath = "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"

alg.grlTool.GoodRunsListVec = [ GRLFilePath ]
alg.grlTool.PassThrough = 0 # if true (default) will ignore result of GRL and will just pass all events

# PRW
addPrivateTool( alg, 'pileupReweighting', 'CP::PileupReweightingTool')
listOfLumicalcFiles = ["GoodRunsLists/data17_13TeV/20190708/ilumicalc_histograms_None_325713-340453_OflLumi-13TeV-010.root"]
# listOfLumicalcFiles = ["GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"]
# listOfLumicalcFiles = ["GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"]


PRWConfigFiles = [ 'MyAnalysis/NTUP_PILEUP.12973660._000001.pool.root.1',"GoodRunsLists/data17_13TeV/20190708/purw.actualMu.2017.root"]
# PRWConfigFiles = [ 'MyAnalysis/combined_prw_2017.root',"GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"]"GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"
alg.pileupReweighting.ConfigFiles = PRWConfigFiles
alg.pileupReweighting.LumiCalcFiles = listOfLumicalcFiles


# ============================================================================
# Electron
# ============================================================================

# electron/photon calibration and smearing
addPrivateTool( alg, 'egammaCalibrationAndSmearingTool', 'CP::EgammaCalibrationAndSmearingTool')
alg.egammaCalibrationAndSmearingTool.ESModel = "es2018_R21_v0"
alg.egammaCalibrationAndSmearingTool.randomRunNumber = 123456
alg.egammaCalibrationAndSmearingTool.doSmearing = 0
# electron/photon shifting 
addPrivateTool( alg, 'MCShifterTool', 'ElectronPhotonShowerShapeFudgeTool')
alg.MCShifterTool.Preselection = 22
alg.MCShifterTool.FFCalibFile = "ElectronPhotonShowerShapeFudgeTool/v2/PhotonFudgeFactors.root"

# Photon SF Reco
addPrivateTool( alg, 'photonSFReco', 'AsgPhotonEfficiencyCorrectionTool')
alg.photonSFReco.ForceDataType = 1

# Photon SF ID
addPrivateTool( alg, 'photonSFID', 'AsgPhotonEfficiencyCorrectionTool')
alg.photonSFID.ForceDataType = 1

# Photon SF Iso
addPrivateTool( alg, 'photonSFIso', 'AsgPhotonEfficiencyCorrectionTool')
alg.photonSFIso.IsoKey = "Loose"
alg.photonSFIso.ForceDataType = 1

# # Forward Electron SF Reco
# addPrivateTool( alg, 'electronSFRecoFwd', 'AsgElectronEfficiencyCorrectionTool')
# alg.electronSFRecoFwd.RecoKey = "Reconstruction"
# 
# # Forward Electron SF ID
# addPrivateTool( alg, 'electronSFIDFwd', 'AsgElectronEfficiencyCorrectionTool')
# alg.electronSFIDFwd.IdKey = "FwdMedium"

# ============================================================================
# Muon
# ============================================================================
# muon calibration and smearing
addPrivateTool( alg, 'muonCalibrationSmearingTool', 'CP::MuonCalibrationAndSmearingTool')
alg.muonCalibrationSmearingTool.Year = "Data16"

# muon selection
addPrivateTool( alg, 'muonSelectionToolLoose', 'CP::MuonSelectionTool')
alg.muonSelectionToolLoose.MaxEta = 2.7


addPrivateTool( alg, 'muonSF', 'CP::MuonEfficiencyScaleFactors')
alg.muonSF.WorkingPoint = 'Medium'

addPrivateTool( alg, 'muonSFIso', 'CP::MuonEfficiencyScaleFactors')
alg.muonSFIso.WorkingPoint = 'GradientIso'

# ============================================================================
# Isolation
# ============================================================================
# isolation electron/muon
addPrivateTool( alg, 'isolationSelectionToolLoose', 'CP::IsolationSelectionTool')
alg.isolationSelectionToolLoose.ElectronWP = "Gradient"
alg.isolationSelectionToolLoose.MuonWP = "Gradient"


# ============================================================================
# Jets
# ============================================================================
# jet calibration
addPrivateTool( alg, "jetCalibrationTool", "JetCalibrationTool")
alg.jetCalibrationTool.JetCollection = "AntiKt4EMPFlow"
alg.jetCalibrationTool.ConfigFile = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"
if ( isData ): 
	calibSequence = "JetArea_Residual_EtaJES_GSC_Insitu" 
else: 
	calibSequence = "JetArea_Residual_EtaJES_GSC_Smear"
alg.jetCalibrationTool.CalibSequence = calibSequence
alg.jetCalibrationTool.CalibArea = "00-04-82"
alg.jetCalibrationTool.IsData = isData


# ============================================================================
# MET
# ============================================================================
# met maker
addPrivateTool( alg, "metutil", "met::METMaker")
alg.metutil.DoMuonEloss = False
alg.metutil.DoRemoveMuonJets = True
alg.metutil.DoSetMuonJetEMScale = True


# ============================================================================
# LightGBM
# ============================================================================

addPrivateTool( alg, "lgbmPdfVar", "LightGBMPredictor")
alg.lgbmPdfVar.ConfigFile = "2019_08_15_11_47234812"

addPrivateTool( alg, "lgbmPdfConvVar", "LightGBMPredictor")
alg.lgbmPdfConvVar.ConfigFile = "2019_08_15_15_2437115810"

addPrivateTool( alg, "lgbmPdfBinVar", "LightGBMPredictor")
alg.lgbmPdfBinVar.ConfigFile = "2019_08_15_18_956659904"

addPrivateTool( alg, "lgbmPdfNoConvVar", "LightGBMPredictor")
alg.lgbmPdfNoConvVar.ConfigFile = "2019_08_21_19_3307376757"

addPrivateTool( alg, "lgbmPdfNoCellVar", "LightGBMPredictor")
alg.lgbmPdfNoCellVar.ConfigFile = "2019_23_08_20_4210130482"

addPrivateTool( alg, "lgbmPdfExtVar", "LightGBMPredictor")
alg.lgbmPdfExtVar.ConfigFile = "2019_08_15_22_896011677"

addPrivateTool( alg, "lgbmIso", "LightGBMPredictor")
alg.lgbmIso.ConfigFile = "2019_08_15_9_3898251691"

# addPrivateTool( alg, "lgbmFwdPid", "LightGBMPredictor")
# alg.lgbmFwdPid.ConfigFile = "2019_06_24_6_3697977657"
# 
# addPrivateTool( alg, "lgbmFwdEr", "LightGBMPredictor")
# alg.lgbmFwdEr.ConfigFile = "2019_06_24_9_1305298504" 
# 
# addPrivateTool( alg, "lgbmFwdIso", "LightGBMPredictor")
# alg.lgbmFwdIso.ConfigFile = "2019_06_24_4_2590916238" 

# ============================================================================
# Images
# ============================================================================
addPrivateTool( alg, "imageCreation", "ImageCreation")
alg.imageCreation.Time = True


# Add our algorithm to the job
job.algsAdd( alg )


# Run the job using the direct driver.
if ( not options.grid ):
    driver = ROOT.EL.DirectDriver()
    driver.submit( job, options.submission_dir )
else:
    driver = ROOT.EL.PrunDriver()
    driver.options().setString("nc_outputSampleName", options.outputName )
    driver.options().setDouble( ROOT.EL.Job.optGridNFiles, options.nFiles )
    driver.submitOnly( job, options.submission_dir)
    


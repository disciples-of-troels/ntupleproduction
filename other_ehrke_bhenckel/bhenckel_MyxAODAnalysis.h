#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <MyAnalysis/Helpers.h>

#include <AnaAlgorithm/AnaAlgorithm.h>

#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/EgammaTruthxAODHelpers.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

#include <TTree.h>
#include <TH1.h>
#include <vector>

#include <AsgTools/ToolHandle.h>

#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"

#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "PhotonEfficiencyCorrection/AsgPhotonEfficiencyCorrectionTool.h"

#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"

#include "IsolationSelection/IsolationSelectionTool.h"

#include "JetCalibTools/IJetCalibrationTool.h"

#include "xAODCaloEvent/CaloCluster.h"

#include "METUtilities/METMaker.h"

#include "TriggerMatchingTool/MatchingTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigGlobalEfficiencyCorrection/TrigGlobalEfficiencyCorrectionTool.h"

#include "LightGBMPredictor/LightGBMPredictor.h"

#include "AssociationUtils/ToolBox.h"
#include "AssociationUtils/OverlapRemovalInit.h"
#include "AssociationUtils/OverlapRemovalTool.h"
#include "AssociationUtils/EleJetOverlapTool.h"
#include "AssociationUtils/MuJetOverlapTool.h"
#include "ImageCreation/ImageCreation.h"


#pragma GCC diagnostic push
MORE_DIAGNOSTICS()

struct multiplicityCutFlow{
    int indiv = 0;
    int acc = 0;
    int truthIndiv = 0;
    int truthAcc = 0;
};


class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
    // this is a standard algorithm constructor
    MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
    ~MyxAODAnalysis () override;

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;
    virtual StatusCode fileExecute () override;


private:
    // Configuration, and any other types of variables go here.


    long int m_eventCounter; //!
    long int m_startTime; //!	
    int m_progressInterval = 1000; //!

    // Selection of electron, muon, fwd electron candidates
    std::vector < const xAOD::Photon* > selectPhotonCandidates();
    std::vector < const xAOD::Electron* > selectFwdElectronCandidates();

    void increaseCutFlow( multiplicityCutFlow* cutFlow, bool pass, bool &passAll, bool truth_el, int idx, int electron);
    void resetCutFlow( std::map< TString, multiplicityCutFlow* > map );
    StatusCode createCutFlowHists( std::map<TString, multiplicityCutFlow* > map );
    void fillCutFlowHists( std::map<TString, multiplicityCutFlow* > map );

    // multiplicity cutflow histograms
    std::map < TString, multiplicityCutFlow* > m_CutFlows; //!
    std::map < TString, multiplicityCutFlow* > m_FwdCutFlows; //!    
    std::map < TString, multiplicityCutFlow* > m_MuonCutFlows; //!


    //============================================================================
    // Container
    //============================================================================
    xAOD::PhotonContainer* m_photons = nullptr; //!
    xAOD::ElectronContainer* m_electrons = nullptr; //!
    xAOD::ElectronContainer* m_fwdElectrons = nullptr; //!
    xAOD::VertexContainer* m_vertex = nullptr; //!
    xAOD::MuonContainer* m_muons = nullptr; //!
    xAOD::JetContainer* m_jets = nullptr; //!
    xAOD::Vertex* m_primaryVertex = nullptr; //!
    std::unique_ptr< xAOD::TrackParticleContainer > m_indetTracks; //!
    std::unique_ptr< xAOD::ShallowAuxContainer > m_indetTracksAux; //!
    const xAOD::TruthParticleContainer* m_egammaTruthContainer = nullptr; //!
    const xAOD::TruthParticleContainer* m_truthParticles = nullptr; //!
 
    const xAOD::EventInfo *m_eventInfo = nullptr; //!
 
    const xAOD::MissingET* m_METmaker = nullptr;
    xAOD::MissingETContainer* m_newMetContainer = nullptr; //!
    xAOD::MissingETAuxContainer* m_newMetAuxContainer = nullptr; //!
    const xAOD::MissingETContainer* m_coreMet = nullptr; //!
    const xAOD::MissingETAssociationMap* m_metMap = nullptr; //!


    //============================================================================
    // Tools
    //============================================================================

    ToolHandle<IGoodRunsListSelectionTool> m_grl; //!
    ToolHandle<CP::IPileupReweightingTool> m_PileupReweighting; //!

    // photon
    ToolHandle<IElectronPhotonShowerShapeFudgeTool> m_MCShifterTool; //!
    ToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_egammaCalibrationSmearingTool; //!
    ToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_photonSFReco; //!
    ToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_photonSFID; //!
    ToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_photonSFIso; //!
    
    // // electron
    // ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_electronSFRecoFwd; //!    
    // ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_electronSFIDFwd; //!    
    
    // muon
    ToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationSmearingTool; //!
    ToolHandle<CP::IMuonSelectionTool> m_muonSelectionToolLoose; //!
    ToolHandle<CP::IMuonEfficiencyScaleFactors> m_muonSF; //!
    ToolHandle<CP::IMuonEfficiencyScaleFactors> m_muonSFIso; //!

    // isolation
    ToolHandle<CP::IIsolationSelectionTool> m_isolationSelectionToolLoose; //!
    
    // jet
    ToolHandle<IJetCalibrationTool> m_jetCalibrationTool; //!
    
    // met 
    ToolHandle<IMETMaker> m_metutil; //!
    
    // LGBM
    ToolHandle<ILightGBMPredictor> m_lgbm_pdfvars; //!
    ToolHandle<ILightGBMPredictor> m_lgbm_convvars; //!
    ToolHandle<ILightGBMPredictor> m_lgbm_binvars; //!
    ToolHandle<ILightGBMPredictor> m_lgbm_noconvvars; //!
    ToolHandle<ILightGBMPredictor> m_lgbm_nocellvars; //!
    ToolHandle<ILightGBMPredictor> m_lgbm_extvars; //!
    ToolHandle<ILightGBMPredictor> m_lgbm_iso; //!
    
    // ToolHandle<ILightGBMPredictor> m_lgbm_fwd_pid; //!
    // ToolHandle<ILightGBMPredictor> m_lgbm_fwd_er; //!
    // ToolHandle<ILightGBMPredictor> m_lgbm_fwd_iso; //!
    
    
    // define overlap removal tool
    asg::AnaToolHandle<ORUtils::IOverlapRemovalTool> m_orTool; //!
    ORUtils::ORFlags* m_orFlags = nullptr; //!
    ORUtils::ToolBox* m_orToolbox = nullptr; //!

    // Image Creation
    ToolHandle<IImageCreation> m_images; //!

    //============================================================================
    // Output tree branch variables
    //============================================================================
    
    bool event_isMC ; //!
    
    // Event Level variables
    uint32_t event_runNumber; //!
    int event_evtNumber; //!
    uint32_t event_mcChannelNumber; //!
    float event_correctedScaledAverageMu; //!
    float event_correctedScaledActualMu; //!
    float event_MCWeight; //!
    float event_pileupweight; //!
    float event_crossSection; //!
    float eventWeight; //!
    int NvtxReco; //!
    int nPhotons; //!
    // int nFwdElectrons; //!    
    int m_counter = 0; //!
    bool trig; //!

    // missing transverse energy
    double met_met; //!
    double met_phi; //!

    // process specific quantities
    std::map<uint32_t,float> m_crossSections; //!
    std::map<uint32_t,float> m_kFactors; //!
    std::map<uint32_t,float> m_filterEffs; //!

    // Photon ID scores
    std::vector< bool > LHLoose; //!
    std::vector< bool > LHTight; //!
    std::vector< double > pdf_score; //!
    std::vector< double > pdfconv_score; //!
    std::vector< double > pdfbin_score; //!
    std::vector< double > pdfnoconv_score; //!
    std::vector< double > pdfnocell_score; //!
    std::vector< double > pdfext_score; //!
    std::vector< double > pdf_logitscore; //!
    std::vector< double > pdfconv_logitscore; //!
    std::vector< double > pdfbin_logitscore; //!
    std::vector< double > pdfnoconv_logitscore; //!
    std::vector< double > pdfnocell_logitscore; //!
    std::vector< double > pdfext_logitscore; //!
    
    // Photon Truth information
    std::vector < int > truthPdgId; //!
    std::vector < int > truthType; //!
    std::vector < int > truthOrigin; //!
    std::vector < float > truth_eta; //!
    std::vector < float > truth_phi; //!
    std::vector < float > truth_m; //!
    std::vector < float > truth_px; //!
    std::vector < float > truth_py; //!
    std::vector < float > truth_pz; //!
    std::vector < float > truth_E; //!
    std::vector < bool > truthParticle; //!
    std::vector< size_t > TruthPointerIdx; //!
    // std::vector< size_t > TruthPointerIdx_fwd; //!


    // Photon kinematics
    std::vector < float > e; //!
    std::vector < float > pt; //!
    std::vector < float > eta; //!
    std::vector < float > phi; //!
    std::vector < float > et; //!
    std::vector < float > m; //!
    
    // Photon scale factors 
    std::vector < float > recoSF; //!
    std::vector < float > idSF; //!
    std::vector < float > isoSF; //!
    std::vector < float > combSF; //!
    

    // images
    std::vector < std::vector < std::vector < float > > > em_calo; //!
    std::vector < std::vector < std::vector < float > > > h_calo; //!

    std::vector < std::vector < float > > em_calo0; //!
    std::vector < std::vector < float > > em_calo1; //!
    std::vector < std::vector < float > > em_calo2; //!
    std::vector < std::vector < float > > em_calo3; //!
    std::vector < std::vector < float > > em_calo4; //!
    std::vector < std::vector < float > > em_calo5; //!
    std::vector < std::vector < float > > em_calo6; //!
    std::vector < std::vector < float > > em_calo7; //!

    std::vector < std::vector < float > > h_calo0; //!
    std::vector < std::vector < float > > h_calo1; //!
    std::vector < std::vector < float > > h_calo2; //!
    std::vector < std::vector < float > > h_calo3; //!
    std::vector < std::vector < float > > h_calo4; //!
    std::vector < std::vector < float > > h_calo5; //!
    std::vector < std::vector < float > > h_calo6; //!
    std::vector < std::vector < float > > h_calo7; //!

    // Photon Likelihood Variables 
    std::vector < float > Rhad1; //!
    std::vector < float > Rhad ; //!
    std::vector < float > weta2; //!
    std::vector < float > Rphi; //!
    std::vector < float > Reta; //!
    std::vector < float > Eratio; //!
    std::vector < float > f1; //!
    std::vector < float > wtots1; //!
    std::vector < float > DeltaE; //!
    std::vector < float > weta1; //!
    std::vector < float > fracs1; //!
    
    // Photon Conversion variables
    std::vector < int > nTrackPart; //!
    std::vector < int > photonConversionType; //!
    std::vector < float > photonConversionRadius; //!
    std::vector < float > photonVertexConvEtOverPt; //!
    std::vector < float > photonVertexConvPtRatio; //!
    
    // Photon Extra Variables
    std::vector < float > maxEcell_time; //!
    std::vector < float > maxEcell_energy; //!
    std::vector < float > core57cellsEnergyCorrection; //!
    std::vector < float > r33over37allcalo; //!
    std::vector < float > phiModCalo; //!
    
    // freddos scalars
    std::vector < float > f0Cluster; //!
    std::vector < float > R12; //!
    std::vector < float > fTG3; //!
    std::vector < float > eAccCluster; //!
    std::vector < float > cellIndexCluster; //!
    std::vector < float > etaModCalo; //!
    std::vector < float > dPhiTH3; //!
    std::vector < float > poscs1; //!
    std::vector < float > poscs2; //!
    
    // isolation variables
    std::vector < bool > GradientIso; //!
    std::vector < float > topoetcone20; //!
    std::vector < float > topoetcone30; //!
    std::vector < float > topoetcone40; //!
    std::vector < float > ptvarcone20; //!
    std::vector < float > ptvarcone30; //!
    std::vector < float > ptvarcone40; //!
    std::vector < float > LGBM_iso; //!
    std::vector < float > LGBM_logitiso; //!
    
    // // fwd electrons ID scores
    // std::vector < char > fwd_LHLoose; //!
    // std::vector < char > fwd_LHMedium; //!
    // std::vector < char > fwd_LHTight; //!
    // std::vector < float > fwd_LGBM_pid; //!
    // 
    // // fwd electrons truth information
    // std::vector < int > fwd_truthPdgId; //!
    // std::vector < int > fwd_truthType; //!
    // std::vector < int > fwd_truthOrigin; //!
    // std::vector < bool > fwd_truthParticle; //!
    // std::vector < float > fwd_truth_eta; //!
    // std::vector < float > fwd_truth_phi; //!
    // std::vector < float > fwd_truth_E; //!
    // 
    // // fwd electrons kinematics
    // std::vector < float > fwd_eta; //!
    // std::vector < float > fwd_phi; //!
    // std::vector < float > fwd_pt; //!
    // std::vector < float > fwd_et; //!
    // std::vector < float > fwd_LGBM_energy; //!
    // std::vector < float > fwd_LGBM_et; //!
    // 
    // // fwd electron isolation vars
    // std::vector < float > fwd_topoetcone20; //!
    // std::vector < float > fwd_topoetcone30; //!
    // std::vector < float > fwd_topoetcone40; //!
    // std::vector < float > fwd_topoetconecoreConeEnergyCorrection; //!
    // std::vector < float > fwd_LGBM_iso; //!
    // 
    // // fwd electrons scale factors
    // std::vector < float > fwd_recoSF; //!
    // std::vector < float > fwd_idSF; //!
    // std::vector < float > fwd_combSF; //!
    

    float deltaR( const float eta1, const float phi1, const float eta2, const float phi2 ) ; //!
    void clearVectors(); //!


    // all kinds of accessors 
    const SG::AuxElement::ConstAccessor< int > Accessor_truthType{ "truthType" } ; //!
    const SG::AuxElement::ConstAccessor< int > Accessor_truthOrigin{ "truthOrigin" } ; //!
        
    const SG::AuxElement::ConstAccessor< float > Accessor_Rhad{ "Rhad" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_Rhad1{ "Rhad1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_weta2{ "weta2" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_Rphi{ "Rphi" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_Reta{ "Reta" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_Eratio{ "Eratio" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_DeltaE{ "DeltaE" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_weta1{ "weta1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_fracs1{ "fracs1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_f1{ "f1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_wtots1{ "wtots1" } ; //!

    const SG::AuxElement::ConstAccessor< float > Accessor_poscs1{ "poscs1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_poscs2{ "poscs2" } ; //!

    // const SG::AuxElement::ConstAccessor< char > Accessor_fwdLHLoose{ "DFCommonForwardElectronsLHLoose" } ; //!
    // const SG::AuxElement::ConstAccessor< char > Accessor_fwdLHMedium{ "DFCommonForwardElectronsLHMedium" } ; //!
    // const SG::AuxElement::ConstAccessor< char > Accessor_fwdLHTight{ "DFCommonForwardElectronsLHTight" } ; //!

    const SG::AuxElement::ConstAccessor< char > Accessor_DFCommonPhotonsIsEMLoose{ "DFCommonPhotonsIsEMLoose" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_DFCommonPhotonsIsEMTight{ "DFCommonPhotonsIsEMTight" } ; //!

    const SG::AuxElement::ConstAccessor< char > Accessor_LHLoose{ "DFCommonElectronsLHLoose" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_LHMedium{ "DFCommonElectronsLHMedium" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_LHTight{ "DFCommonElectronsLHTight" } ; //!

    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone20{ "topoetcone20" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone30{ "topoetcone30" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone40{ "topoetcone40" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetconecoreConeEnergyCorrection{ "topoetconecoreConeEnergyCorrection" } ; //!
    
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone20{ "ptvarcone20" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone30{ "ptvarcone30" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone40{ "ptvarcone40" } ; //!
    
    const SG::AuxElement::ConstAccessor< float > Accessor_maxEcell_time{ "maxEcell_time" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_maxEcell_energy{ "maxEcell_energy" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_core57cellsEnergyCorrection{ "core57cellsEnergyCorrection" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_r33over37allcalo{ "r33over37allcalo" } ; //!

};

#pragma GCC diagnostic pop
#endif

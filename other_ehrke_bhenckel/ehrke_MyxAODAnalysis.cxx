// #include <AsgTools/MessageCheck.h>
// #include <MyAnalysis/MyxAODAnalysis.h>
// #include "xAODRootAccess/tools/TFileAccessTracer.h"
// #include <xAODEventInfo/EventInfo.h>
// #include <xAODCore/ShallowCopy.h>
// #include "xAODTruth/xAODTruthHelpers.h"
// #include "METUtilities/METHelpers.h"
// #include "xAODBase/IParticleHelpers.h"
// 
// #include "xAODMetaData/FileMetaData.h"
// 
// #include "xAODCutFlow/CutBookkeeper.h"
// #include "xAODCutFlow/CutBookkeeperContainer.h"
// 
// MORE_DIAGNOSTICS()
// 
// 
// MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
//                                   ISvcLocator *pSvcLocator)
//     : EL::AnaAlgorithm (name, pSvcLocator),
//     m_grl ("GoodRunsListSelectionTool/grl", this),
//     m_PileupReweighting ("CP::PileupReweightingTool/tool", this),
//     m_trigConfig ("TrigConf::xAODConfigTool/xAODConfigTool" ),
//     m_trigDecTool ("Trig::TrigDecisionTool/TrigDecisionTool" ),
//     m_triggerMatching ( "Trig::MatchingTool" ),
//     m_electronCalibrationSmearingTool ( "CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool", this),
//     m_electronSFReco ( "AsgElectronEfficiencyCorrection", this),
//     m_electronSFID ( "AsgElectronEfficiencyCorrection", this),
//     m_electronSFIDFwd ( "AsgElectronEfficiencyCorrection", this),    
//     m_electronSFIso ( "AsgElectronEfficiencyCorrection", this),
//     m_effTrig ( "AsgElectronEfficiencyCorrectionTool/effTool", this),    
//     m_sfTrig ( "AsgElectronEfficiencyCorrectionTool/sfTool", this),
//     m_electronSFECIDS ( "AsgElectronEfficiencyCorrection", this),
//     m_electronChargeIdSF ( "CP::ElectronChargeEfficiencyCorrectionTool", this),
//     m_MCShifterTool ( "ElectronPhotonShowerShapeFudgeTool", this),
//     m_muonCalibrationSmearingTool ( "CP::MuonCalibrationAndSmearingTool/MuonCalibrationAndSmearingTool", this),
//     m_muonSelectionToolLoose ( "CP::MuonSelectionTool", this),
//     m_muonSF ( "CP::MuonEfficiencyScaleFactors", this),
//     m_muonSFIso ( "CP::MuonEfficiencyScaleFactors", this),
//     m_isolationSelectionToolLoose ( "CP::IsolationSelectionTool", this),
//     m_jetCalibrationTool ( "JetCalibrationTool", this),
//     m_metutil ( "MetMaker", this),
//     m_lgbm_pdfvars ( "LightGBMpredictor", this),
//     m_lgbm_pdfcutvars ( "LightGBMpredictor", this),
//     m_lgbm_fwd_pid ( "LightGBMpredictor", this),
//     m_lgbm_fwd_er ( "LightGBMpredictor", this),
//     m_lgbm_fwd_iso ( "LightGBMpredictor", this),    
//     m_images ( "ImageCreation", this)
// {
//   // Here you put any code for the base initialization of variables,
//   // e.g. initialize all pointers to 0.  This is also where you
//   // declare all properties for your algorithm.  Note that things like
//   // resetting statistics variables or booking histograms should
//   // rather go into the initialize() function.
//   declareProperty ("grlTool", m_grl);
//   declareProperty ("pileupReweighting", m_PileupReweighting);
//   declareProperty ("egammaCalibrationAndSmearingTool", m_electronCalibrationSmearingTool);
//   declareProperty ("electronSFReco", m_electronSFReco);
//   declareProperty ("electronSFID", m_electronSFID);
//   declareProperty ("electronSFIDFwd", m_electronSFIDFwd);  
//   declareProperty ("electronSFIso", m_electronSFIso);
//   declareProperty ("electronSFECIDS", m_electronSFECIDS);
//   declareProperty ("electronChargeIdSF", m_electronChargeIdSF);
//   declareProperty ("MCShifterTool", m_MCShifterTool);
//   declareProperty ("muonCalibrationSmearingTool", m_muonCalibrationSmearingTool);
//   declareProperty ("muonSelectionToolLoose", m_muonSelectionToolLoose);
//   declareProperty ("muonSF", m_muonSF);
//   declareProperty ("muonSFIso", m_muonSFIso);
//   declareProperty ("isolationSelectionToolLoose", m_isolationSelectionToolLoose);
//   declareProperty ("jetCalibrationTool", m_jetCalibrationTool);
//   declareProperty ("metutil", m_metutil);
//   declareProperty ("lgbmPdfVar", m_lgbm_pdfvars);
//   declareProperty ("lgbmPdfCutVar", m_lgbm_pdfcutvars);
//   declareProperty ("lgbmFwdPid", m_lgbm_fwd_pid);
//   declareProperty ("lgbmFwdEr", m_lgbm_fwd_er);
//   declareProperty ("lgbmFwdIso", m_lgbm_fwd_iso);  
//   declareProperty ("imageCreation", m_images);
// 
// }
// 
// 
// StatusCode MyxAODAnalysis :: initialize ()
// {
//   // Here you do everything that needs to be done at the very
//   // beginning on each worker node, e.g. create histograms and output
//   // trees.  This method gets called before any input files are
//   // connected.
// 
// 
// 
//   ANA_MSG_DEBUG("::initialize: 4x::enableFailure");
//   CP::SystematicCode::enableFailure();
//   StatusCode::enableFailure();
//   xAOD::TReturnCode::enableFailure();
//   CP::CorrectionCode::enableFailure();
// 
//   ANA_CHECK( requestFileExecute() );
//   ANA_MSG_DEBUG("::initialize: xAOD::TFileAccessTracer::enableDataSubmission(false);");
//   xAOD::TFileAccessTracer::enableDataSubmission(false);
// 
//   // Setup electron multiplicity cutflow histograms
//   m_CutFlows["Init"] = new multiplicityCutFlow;
//   m_CutFlows["OR"] = new multiplicityCutFlow;
//   m_CutFlows["Pt"] = new multiplicityCutFlow;
//   m_CutFlows["Eta"] = new multiplicityCutFlow;
//   m_CutFlows["OQ"] = new multiplicityCutFlow;
//   m_CutFlows["Vtx"] = new multiplicityCutFlow;
//   m_CutFlows["z0"] = new multiplicityCutFlow;
//   m_CutFlows["d0"] = new multiplicityCutFlow;
//   m_CutFlows["Iso"] = new multiplicityCutFlow;
//   m_CutFlows["ECIDS"] = new multiplicityCutFlow;
//   m_CutFlows["LHMedium"] = new multiplicityCutFlow;
// 
//   // Setup fwd electron multiplicity cutflow histograms
//   m_FwdCutFlows["Fwd_Init"] = new multiplicityCutFlow;
//   m_FwdCutFlows["Fwd_Pt"] = new multiplicityCutFlow;
//   m_FwdCutFlows["Fwd_Eta"] = new multiplicityCutFlow;
//   m_FwdCutFlows["Fwd_OQ"] = new multiplicityCutFlow;
//   m_FwdCutFlows["Fwd_LHMedium"] = new multiplicityCutFlow;
// 
//   // Setup muon multiplicity cutflow histograms
//   m_MuonCutFlows["Mu_Init"] = new multiplicityCutFlow;
//   m_MuonCutFlows["Mu_OR"] = new multiplicityCutFlow;
//   m_MuonCutFlows["Mu_Pt"] = new multiplicityCutFlow;
//   m_MuonCutFlows["Mu_Eta"] = new multiplicityCutFlow;
//   m_MuonCutFlows["Mu_Vtx"] = new multiplicityCutFlow;
//   m_MuonCutFlows["Mu_z0"] = new multiplicityCutFlow;
//   m_MuonCutFlows["Mu_d0Sig"] = new multiplicityCutFlow;
//   m_MuonCutFlows["Mu_d0"] = new multiplicityCutFlow;  
//   m_MuonCutFlows["Mu_Combined"] = new multiplicityCutFlow;  
//   m_MuonCutFlows["Mu_Iso"] = new multiplicityCutFlow;
//   m_MuonCutFlows["Mu_Medium"] = new multiplicityCutFlow;
// 
// 
//   // Initialize cut flow histograms
//   ANA_CHECK( createCutFlowHists(m_CutFlows) );
//   ANA_CHECK( createCutFlowHists(m_FwdCutFlows) );  
//   ANA_CHECK( createCutFlowHists(m_MuonCutFlows) );
// 
//   // Initialize histograms for electron kinematics, with and without Iso/ECIDS/ID selection
//   ANA_CHECK( book( TH1F ( "h_eta", "h_eta", 50, -2.47, 2.47) ) );
//   ANA_CHECK( book( TH1F ( "h_phi", "h_phi", 50, -3.15, 3.15) ) );
//   ANA_CHECK( book( TH1F ( "h_et", "h_et", 50, 10, 200) ) );
//   ANA_CHECK( book( TH1F ( "h_eta_LHMedium", "h_eta_LHMedium", 50, -2.47, 2.47) ) );
//   ANA_CHECK( book( TH1F ( "h_phi_LHMedium", "h_phi_LHMedium", 50, -3.15, 3.15) ) );
//   ANA_CHECK( book( TH1F ( "h_et_LHMedium", "h_et_LHMedium", 50, 10, 200) ) ); 
// 
//   // Initialize histograms for fwd electron kinematics, with and without ID selection
//   ANA_CHECK( book( TH1F ( "h_fwd_eta", "h_fwd_eta", 50, -4.9, 4.9) ) );
//   ANA_CHECK( book( TH1F ( "h_fwd_phi", "h_fwd_phi", 50, -3.15, 3.15) ) );
//   ANA_CHECK( book( TH1F ( "h_fwd_et", "h_fwd_et", 50, 20, 200) ) );
//   ANA_CHECK( book( TH1F ( "h_fwd_eta_LHMedium", "h_fwd_eta_LHMedium", 50, -4.9, 4.9) ) );
//   ANA_CHECK( book( TH1F ( "h_fwd_phi_LHMedium", "h_fwd_phi_LHMedium", 50, -3.15, 3.15) ) );
//   ANA_CHECK( book( TH1F ( "h_fwd_et_LHMedium", "h_fwd_et_LHMedium", 50, 20, 200) ) ); 
// 
//   // Initialize histograms for muon kinematics
//   ANA_CHECK( book( TH1F ( "h_mu_eta", "h_mu_eta", 50, -2.7, 2.7) ) ); 
//   ANA_CHECK( book( TH1F ( "h_mu_phi", "h_mu_phi", 50, -3.15, 3.15) ) ); 
//   ANA_CHECK( book( TH1F ( "h_mu_pt", "h_mu_pt", 50, 10, 200) ) ); 
// 
//   // Initialize histograms for number of leptons saved and the sum of weights histogram
//   ANA_CHECK( book( TH1I ( "h_nElectrons", "h_nElectrons", 5, 0, 5) ) );
//   ANA_CHECK( book( TH1I ( "h_nFwdElectrons", "h_nFwdElectrons", 5, 0, 5) ) );  
//   ANA_CHECK( book( TH1I ( "h_nMuons", "h_nMuons", 5, 0, 5) ) );  
//   ANA_CHECK( book( TH1F ( "sumOfWeights", "sumOfWeights", 6, 0, 6) ) );
// 
//   // Cutflow histograms for electrons
//   ANA_CHECK( book( TH1I ( "h_cutFlow", "h_cutFlow", 11, 0, 11) ) );
//   ANA_CHECK( book( TH1I ( "h_indivCutFlow", "h_indivCutFlow", 11, 0, 11) ) );
//   ANA_CHECK( book( TH1I ( "h_truthCutFlow", "h_truthCutFlow", 11, 0, 11) ) );
//   ANA_CHECK( book( TH1I ( "h_indivTruthCutFlow", "h_indivTruthCutFlow", 11, 0, 11) ) ); 
// 
//   // Cutflow histograms for muons
//   ANA_CHECK( book( TH1I ( "h_muCutFlow", "h_muCutFlow", 11, 0, 11) ) );
//   ANA_CHECK( book( TH1I ( "h_muIndivCutFlow", "h_muIndivCutFlow", 11, 0, 11) ) );
//   ANA_CHECK( book( TH1I ( "h_muTruthCutFlow", "h_muTruthCutFlow", 11, 0, 11) ) );
//   ANA_CHECK( book( TH1I ( "h_muIndivTruthCutFlow", "h_muIndivTruthCutFlow", 11, 0, 11) ) ); 
// 
//   // Cutflow histograms for fwd electrons  
//   ANA_CHECK( book( TH1I ( "h_FwdCutFlow", "h_FwdCutFlow", 5, 0, 5) ) );
//   ANA_CHECK( book( TH1I ( "h_FwdIndivCutFlow", "h_FwdIndivCutFlow", 5, 0, 5) ) );
//   ANA_CHECK( book( TH1I ( "h_FwdTruthCutFlow", "h_FwdTruthCutFlow", 5, 0, 5) ) );
//   ANA_CHECK( book( TH1I ( "h_FwdIndivTruthCutFlow", "h_FwdIndivTruthCutFlow", 5, 0, 5) ) );   
// 
//   // Set the bin labels to the corresponding selection for electron cutflows
//   for ( const auto& hist : {hist( "h_cutFlow" ), hist( "h_indivCutFlow" ), hist( "h_truthCutFlow" ), hist( "h_indivTruthCutFlow" )}){
//       hist->GetXaxis()->SetBinLabel(1, "Init");
//       hist->GetXaxis()->SetBinLabel(2, "OR");
//       hist->GetXaxis()->SetBinLabel(3, "Pt");
//       hist->GetXaxis()->SetBinLabel(4, "Eta");
//       hist->GetXaxis()->SetBinLabel(5, "OQ");
//       hist->GetXaxis()->SetBinLabel(6, "Vtx");
//       hist->GetXaxis()->SetBinLabel(7, "z0");
//       hist->GetXaxis()->SetBinLabel(8, "d0Sig");
//       hist->GetXaxis()->SetBinLabel(9, "Iso");
//       hist->GetXaxis()->SetBinLabel(10, "ECIDS");
//       hist->GetXaxis()->SetBinLabel(11, "LHMedium");
//   }
// 
//   // Set the bin labels to the corresponding selection for fwd electron cutflows
//   for ( const auto& hist : {hist( "h_FwdCutFlow" ), hist( "h_FwdIndivCutFlow" ), hist( "h_FwdTruthCutFlow" ), hist( "h_FwdIndivTruthCutFlow" )}){
//       hist->GetXaxis()->SetBinLabel(1, "Init");
//       hist->GetXaxis()->SetBinLabel(2, "Pt");
//       hist->GetXaxis()->SetBinLabel(3, "Eta");
//       hist->GetXaxis()->SetBinLabel(4, "OQ");
//       hist->GetXaxis()->SetBinLabel(5, "LHMedium");
//   }
// 
//   // Set the bin labels to the corresponding selection for muon cutflows 
//   for ( const auto& hist : {hist( "h_muCutFlow" ), hist( "h_muIndivCutFlow" ), hist( "h_muTruthCutFlow" ), hist( "h_muIndivTruthCutFlow" )}){
//       hist->GetXaxis()->SetBinLabel(1, "Init");
//       hist->GetXaxis()->SetBinLabel(2, "OR");
//       hist->GetXaxis()->SetBinLabel(3, "Pt");
//       hist->GetXaxis()->SetBinLabel(4, "Eta");
//       hist->GetXaxis()->SetBinLabel(5, "Vtx");
//       hist->GetXaxis()->SetBinLabel(6, "z0");
//       hist->GetXaxis()->SetBinLabel(7, "d0Sig");
//       hist->GetXaxis()->SetBinLabel(8, "d0");
//       hist->GetXaxis()->SetBinLabel(9, "Combined");
//       hist->GetXaxis()->SetBinLabel(10, "Iso");
//       hist->GetXaxis()->SetBinLabel(11, "Medium");
//   }
// 
//   // Initialize output tree
//   ANA_CHECK( book( TTree ("analysis", "My analysis ntuple") ) );
//   TTree* m_myTree = tree ("analysis");
//   m_myTree->SetAutoFlush(500);
// 
//   // Event level variables
//   m_myTree->Branch( "runNumber", &event_runNumber );
//   m_myTree->Branch( "evtNumber", &event_evtNumber );
//   m_myTree->Branch( "MCEventWeight", &event_MCWeight ); 
//   m_myTree->Branch( "correctedScaledAverageMu", &event_correctedScaledAverageMu );
//   m_myTree->Branch( "correctedScaledActualMu", &event_correctedScaledActualMu );
//   m_myTree->Branch( "crossSectionFactor", &event_crossSection );
//   m_myTree->Branch( "NvtxReco", &NvtxReco );
//   m_myTree->Branch( "EventTrigger", &trig );
//   m_myTree->Branch( "eventWeight", &eventWeight );
//   m_myTree->Branch( "event_SFTrigger", &event_SFTrigger );
//   m_myTree->Branch( "mcChannelNumber", &event_mcChannelNumber );
//   m_myTree->Branch( "nElectrons", &nElectrons );
//   m_myTree->Branch( "met_met", &met_met );
//   m_myTree->Branch( "met_phi", &met_phi ); 
// 
//   // Electron ID outputs
//   m_myTree->Branch( "pdf_score", &pdf_score );
//   m_myTree->Branch( "pdfcut_score", &pdfcut_score ); 
//   m_myTree->Branch( "LHLoose", &LHLoose );
//   m_myTree->Branch( "LHMedium", &LHMedium );
//   m_myTree->Branch( "LHTight", &LHTight );
// 
//   // Electron truth information
//   m_myTree->Branch( "IFFTruth", &IFFTruth );
//   m_myTree->Branch( "truthPdgId", &truthPdgId );
//   m_myTree->Branch( "truthType", &truthType );
//   m_myTree->Branch( "truthOrigin", &truthOrigin );
//   m_myTree->Branch( "firstEgMotherTruthType", &firstEgMotherTruthType );
//   m_myTree->Branch( "firstEgMotherTruthOrigin", &firstEgMotherTruthOrigin );
//   m_myTree->Branch( "firstEgMotherPdgId", &firstEgMotherPdgId );
//   m_myTree->Branch( "truthParticle", &truthParticle );
//   m_myTree->Branch( "truth_eta", &truth_eta );
//   m_myTree->Branch( "truth_phi", &truth_phi );
//   m_myTree->Branch( "truth_E", &truth_E ); 
// 
//   // Electron kinematics
//   m_myTree->Branch( "eta", &eta );
//   m_myTree->Branch( "phi", &phi );
//   m_myTree->Branch( "pt", &pt );
//   m_myTree->Branch( "et", &et );
//   m_myTree->Branch( "charge", &charge );
// 
//   // Electron ECIDS
//   m_myTree->Branch( "ecids", &ecids );
//   m_myTree->Branch( "ecidsValue", &ecidsValue );
// 
//   // Electron scale factors
//   m_myTree->Branch( "recoSF", &recoSF );
//   m_myTree->Branch( "idSF", &idSF );
//   m_myTree->Branch( "isoSF", &isoSF );
//   m_myTree->Branch( "ecidsSF", &ecidsSF );
//   m_myTree->Branch( "chargeSF", &chargeSF );
//   m_myTree->Branch( "combSF", &combSF );
// 
//   // Electron LH variables
//   m_myTree->Branch( "Rhad1", &Rhad1 );
//   m_myTree->Branch( "Rhad", &Rhad );
//   m_myTree->Branch( "f3", &f3 );
//   m_myTree->Branch( "weta2", &weta2 );
//   m_myTree->Branch( "Rphi", &Rphi );
//   m_myTree->Branch( "Reta", &Reta );
//   m_myTree->Branch( "Eratio", &Eratio );
//   m_myTree->Branch( "f1", &f1 );
//   m_myTree->Branch( "numberOfInnermostPixelHits", &numberOfInnermostPixelHits );
//   m_myTree->Branch( "numberOfPixelHits", &numberOfPixelHits );
//   m_myTree->Branch( "numberOfSCTHits", &numberOfSCTHits );
//   m_myTree->Branch( "d0", &d0 );
//   m_myTree->Branch( "d0Sig", &d0Sig );
//   m_myTree->Branch( "dPOverP", &dPOverP );
//   m_myTree->Branch( "deltaEta1", &deltaEta1 );
//   m_myTree->Branch( "deltaPhiRescaled2", &deltaPhiRescaled2 );
//   m_myTree->Branch( "EptRatio", &EptRatio );
//   m_myTree->Branch( "TRTPID", &TRTPID );
//   m_myTree->Branch( "wtots1", &wtots1 );
// 
//   // Additional track variables
//   m_myTree->Branch( "vertexIndex", &vertexIndex );
//   m_myTree->Branch( "sigmad0", &sigmad0 );
//   m_myTree->Branch( "z0", &z0 );
// 
//   // Electron Isolation variables
//   m_myTree->Branch( "GradientIso", &GradientIso );
//   m_myTree->Branch( "topoetcone20", &topoetcone20 );
//   m_myTree->Branch( "topoetcone30", &topoetcone30 );
//   m_myTree->Branch( "topoetcone40", &topoetcone40 );
//   m_myTree->Branch( "ptvarcone20", &ptvarcone20 );
//   m_myTree->Branch( "ptvarcone30", &ptvarcone30 );
//   m_myTree->Branch( "ptvarcone40", &ptvarcone40 );
// 
//   // Trigger information
//   m_myTree->Branch( "trigger", &trigger );
// 
//   // Additional variables for ER with CNN
//   m_myTree->Branch( "eAccCluster", &eAccCluster );
//   m_myTree->Branch( "cellIndexCluster", &cellIndexCluster );
//   m_myTree->Branch( "etaModCalo", &etaModCalo );
//   m_myTree->Branch( "dPhiTH3", &dPhiTH3 );
//   m_myTree->Branch( "deltaEta2", &deltaEta2 );
//   m_myTree->Branch( "poscs1", &poscs1 );
//   m_myTree->Branch( "poscs2", &poscs2 );
// 
//   // Electron images 
//   // em calo energy
//   m_myTree->Branch( "em_calo0", &em_calo0 );
//   m_myTree->Branch( "em_calo1", &em_calo1 );
//   m_myTree->Branch( "em_calo2", &em_calo2 );
//   m_myTree->Branch( "em_calo3", &em_calo3 );
//   m_myTree->Branch( "em_calo4", &em_calo4 );
//   m_myTree->Branch( "em_calo5", &em_calo5 );
//   m_myTree->Branch( "em_calo6", &em_calo6 );
//   m_myTree->Branch( "em_calo7", &em_calo7 );
// 
//   // h calo energy
//   m_myTree->Branch( "h_calo0", &h_calo0 );
//   m_myTree->Branch( "h_calo1", &h_calo1 );
//   m_myTree->Branch( "h_calo2", &h_calo2 );
//   m_myTree->Branch( "h_calo3", &h_calo3 );
//   m_myTree->Branch( "h_calo4", &h_calo4 );
//   m_myTree->Branch( "h_calo5", &h_calo5 );
//   m_myTree->Branch( "h_calo6", &h_calo6 );
//   m_myTree->Branch( "h_calo7", &h_calo7 );
// 
//   // em calo time
//   m_myTree->Branch( "time_em0", &time_em0 );
//   m_myTree->Branch( "time_em1", &time_em1 );
//   m_myTree->Branch( "time_em2", &time_em2 );
//   m_myTree->Branch( "time_em3", &time_em3 );
//   m_myTree->Branch( "time_em4", &time_em4 );
//   m_myTree->Branch( "time_em5", &time_em5 );
//   m_myTree->Branch( "time_em6", &time_em6 );
//   m_myTree->Branch( "time_em7", &time_em7 );
// 
//   // h calo time 
//   m_myTree->Branch( "time_h0", &time_h0 );
//   m_myTree->Branch( "time_h1", &time_h1 );
//   m_myTree->Branch( "time_h2", &time_h2 );
//   m_myTree->Branch( "time_h3", &time_h3 );
//   m_myTree->Branch( "time_h4", &time_h4 );
//   m_myTree->Branch( "time_h5", &time_h5 );
//   m_myTree->Branch( "time_h6", &time_h6 );
//   m_myTree->Branch( "time_h7", &time_h7 );
// 
//   // Muon kinematics
//   m_myTree->Branch( "mu_eta", &mu_eta );
//   m_myTree->Branch( "mu_phi", &mu_phi );
//   m_myTree->Branch( "mu_et", &mu_et );
//   m_myTree->Branch( "mu_charge", &mu_charge );
// 
//   // Muon truth
//   m_myTree->Branch( "mu_iffTruth", &mu_iffTruth );
// 
//   // Muon scale factors
//   m_myTree->Branch( "mu_idSF", &mu_idSF );
//   m_myTree->Branch( "mu_isoSF", &mu_isoSF );
//   m_myTree->Branch( "mu_SF", &mu_SF );
// 
//   // fwd electrons ID outputs
//   m_myTree->Branch( "fwd_LHLoose", &fwd_LHLoose );
//   m_myTree->Branch( "fwd_LHMedium", &fwd_LHMedium );
//   m_myTree->Branch( "fwd_LHTight", &fwd_LHTight );
//   m_myTree->Branch( "fwd_LGBM_pid", &fwd_LGBM_pid );
// 
//   // fwd electrons truth information
//   m_myTree->Branch( "fwd_truthPdgId", &fwd_truthPdgId );
//   m_myTree->Branch( "fwd_truthType", &fwd_truthType );
//   m_myTree->Branch( "fwd_truthOrigin", &fwd_truthOrigin );
//   m_myTree->Branch( "fwd_truthParticle", &fwd_truthParticle );
//   m_myTree->Branch( "fwd_truth_eta", &fwd_truth_eta );
//   m_myTree->Branch( "fwd_truth_phi", &fwd_truth_phi );
//   m_myTree->Branch( "fwd_truth_E", &fwd_truth_E ); 
// 
//   // fwd electrons kinematics
//   m_myTree->Branch( "fwd_eta", &fwd_eta );
//   m_myTree->Branch( "fwd_phi", &fwd_phi );
//   m_myTree->Branch( "fwd_pt", &fwd_pt );
//   m_myTree->Branch( "fwd_et", &fwd_et );
//   m_myTree->Branch( "fwd_LGBM_energy", &fwd_LGBM_energy );
//   m_myTree->Branch( "fwd_LGBM_et", &fwd_LGBM_et );
// 
//   // fwd isolation vars
//   m_myTree->Branch( "fwd_topoetcone20", &fwd_topoetcone20 );
//   m_myTree->Branch( "fwd_topoetcone30", &fwd_topoetcone30 );
//   m_myTree->Branch( "fwd_topoetcone40", &fwd_topoetcone40 );
//   m_myTree->Branch( "fwd_topoetconecoreConeEnergyCorrection", &fwd_topoetconecoreConeEnergyCorrection );
//   m_myTree->Branch( "fwd_LGBM_iso", &fwd_LGBM_iso );
// 
//   // fwd electrons kinematics
//   m_myTree->Branch( "fwd_recoSF", &fwd_recoSF );
//   m_myTree->Branch( "fwd_idSF", &fwd_idSF );
//   m_myTree->Branch( "fwd_combSF", &fwd_combSF );
// 
// 
// 
//   //============================================================================
//   // Retrieving Tools
//   //============================================================================
//   ANA_MSG_DEBUG("::initialize: Retrieving Tools");
// 
//   // GRL tool
//   ANA_CHECK( m_grl.retrieve() );
// 
//   // PRW ( change appropiately to year )
//   ANA_CHECK( m_PileupReweighting.retrieve() );
// 
//   // Initialize and configure trigger tools
//   ANA_CHECK ( m_trigConfig.initialize() );
//   ANA_CHECK ( m_trigDecTool.setProperty( "ConfigTool", m_trigConfig.getHandle() ) ); // connect the TrigDecTool to the ConfigTool
//   ANA_CHECK ( m_trigDecTool.setProperty( "TrigDecisionKey", "xTrigDecision") );
//   ANA_CHECK ( m_trigDecTool.initialize() );
// 
//   // Trigger matching tool
//   ANA_CHECK ( m_triggerMatching.initialize() );
// 
//   // Loose Isolation selection tool
//   ANA_CHECK( m_isolationSelectionToolLoose.retrieve() );
// 
//    // Egamma shower shape fudge tool
//   ANA_CHECK( m_MCShifterTool.retrieve() );
// 
//   // Electron calibration and smearing tool
//   ANA_CHECK( m_electronCalibrationSmearingTool.retrieve() );
// 
//   // Electron reconstruction scale factor tool ( one is needed for all different tpyes(Reco,ID,...) )
//   ANA_CHECK( m_electronSFReco.retrieve() );
//   ANA_CHECK( m_electronSFID.retrieve() );
//   ANA_CHECK( m_electronSFIso.retrieve() );
//   ANA_CHECK( m_electronSFECIDS.retrieve() );
//   ANA_CHECK( m_electronChargeIdSF.retrieve() );
// 
//   std::vector < std::string > corrFileEff = {"ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiency.e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.2018.root"} ;
//   ANA_CHECK( m_effTrig.setProperty("CorrectionFileNameList", corrFileEff ) );
//   ANA_CHECK( m_effTrig.initialize() );
//   m_electronEffTrigger.push_back( m_effTrig.getHandle() );
// 
//   std::vector < std::string > corrFileSF = {"ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiencySF.e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.2018.root"} ; 
//   ANA_CHECK( m_sfTrig.setProperty("CorrectionFileNameList", corrFileSF ) );
//   ANA_CHECK( m_sfTrig.initialize() );
//   m_electronSFTrigger.push_back( m_sfTrig.getHandle() );
// 
//   // Muon calibration and smearing tool
//   ANA_CHECK( m_muonCalibrationSmearingTool.retrieve() );
// 
//   // Muon selection tool (loose)
//   ANA_CHECK( m_muonSelectionToolLoose.retrieve() );
// 
//   // Muon ID scale factor tool
//   ANA_CHECK( m_muonSF.retrieve() );
// 
//   // Muon Iso scale factor tool
//   ANA_CHECK( m_muonSFIso.retrieve() );    
// 
//   // Jet Calibration
//   ANA_CHECK( m_jetCalibrationTool.retrieve() );
// 
//   // met maker
//   ANA_CHECK( m_metutil.retrieve() );
// 
//   // Trigger ScaleFactors
//   m_trigScaleFactors = new TrigGlobalEfficiencyCorrectionTool("trigScaleFactors");
//   ANA_CHECK( m_trigScaleFactors->setProperty("ElectronEfficiencyTools", m_electronEffTrigger ) );
//   ANA_CHECK( m_trigScaleFactors->setProperty("ElectronScaleFactorTools", m_electronSFTrigger ) );
//   ANA_CHECK( m_trigScaleFactors->setProperty("TriggerCombination2018", "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0") );
//   ANA_CHECK( m_trigScaleFactors->initialize() );
// 
//   // LGBM
//   // ID for central electrons
//   ANA_CHECK( m_lgbm_pdfvars.retrieve() );
//   ANA_CHECK( m_lgbm_pdfcutvars.retrieve() );
// 
//   // ID/ER for fwd electrons
//   ANA_CHECK( m_lgbm_fwd_pid.retrieve() );
//   ANA_CHECK( m_lgbm_fwd_er.retrieve() );
//   ANA_CHECK( m_lgbm_fwd_iso.retrieve() );
// 
//   // Initialize OverlapRemovalTool
//   m_orFlags = new ORUtils::ORFlags("OverlapRemovalTool", "selected", "overlaps");
//   m_orFlags->doElectrons = true;
//   m_orFlags->doMuons = true;
//   m_orFlags->doJets = true;
//   m_orFlags->doTaus = false;
//   m_orFlags->doPhotons = false;
//   m_orFlags->doFatJets = false;
//   m_orFlags->doEleEleOR = true;
//   m_orToolbox = new ORUtils::ToolBox();
//   ANA_CHECK( ORUtils::recommendedTools(*m_orFlags, *m_orToolbox) );
//   ANA_CHECK( m_orToolbox->eleEleORT.setProperty("UseClusterMatch", true) );
//   ANA_CHECK( m_orToolbox->setGlobalProperty("OutputLevel", 3) );
//   ANA_CHECK( m_orToolbox->initialize() );
//   m_orTool = m_orToolbox->masterTool;
// 
//   // Images
//   ANA_CHECK( m_images.retrieve() );
// 
//   // IFFTruthClassifier
//   m_iff = new IFFTruthClassifier("IFFTruth");
//   ANA_CHECK( m_iff->initialize() );
// 
//   m_eventCounter = 0;
//   Info( "initialize()", "Initialization done" ); 
//   return StatusCode::SUCCESS;
// }
// 
// 
// StatusCode MyxAODAnalysis :: fileExecute ()
// {
//     TString dirname = gSystem->Getenv("WorkDir_DIR"); 
// 
// 
//     const xAOD::FileMetaData* fmld = new xAOD::FileMetaData();
//     float mcProcID = -999;
//     if( inputMetaStore()->retrieve(fmld, "FileMetaData").isSuccess()  ) {
//         fmld->value( xAOD::FileMetaData::mcProcID, mcProcID);   
//     }
// 
//     event_isMC = mcProcID > 1;
//     Info( "fileExecute()", "mcProcID: %f", mcProcID );
//     Info( "fileExecute()", "File is MC: %d", event_isMC);
// 
//     if ( event_isMC ){
//         //check if file is from a DxAOD
//         const std::string dummy = "StreamAOD";
//         bool m_isDerivation = !( inputMetaStore()->contains<std::string>( dummy ) );
// 
//         if(m_isDerivation ){
//             const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
//             if(!inputMetaStore()->retrieve(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
//                 Error("initializeEvent()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
//                 return StatusCode::FAILURE;
//             }
//             // if ( incompleteCBC->size() != 0 ) {
//             //   Error("initializeEvent()","Found incomplete Bookkeepers! Check file for corruption.");
//             //   return StatusCode::FAILURE;
//             // }
//             // Now, let's find the actual information
//             const xAOD::CutBookkeeperContainer* completeCBC = 0;
//             if(!inputMetaStore()->retrieve(completeCBC, "CutBookkeepers").isSuccess()){
//                 Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
//                 return StatusCode::FAILURE;
//             }
// 
//             // First, let's find the smallest cycle number,
//             // i.e., the original first processing step/cycle
//             int minCycle = 10000;
//             for ( auto cbk : *completeCBC ) {
//                 if ( ! cbk->name().empty()  && minCycle > cbk->cycle() ){ minCycle = cbk->cycle(); }
//             }
//             // Now, let's actually find the right one that contains all the needed info...
//             const xAOD::CutBookkeeper* allEventsCBK=0;
//             const xAOD::CutBookkeeper* DxAODEventsCBK=0;
//             std::string derivationName = "EGAM2Kernel"; //need to replace by appropriate name
//             int maxCycle = -1;
//             for (const auto& cbk: *completeCBC) {
//                 if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
//                     allEventsCBK = cbk;
//                     maxCycle = cbk->cycle();
//                 }
//                 if ( cbk->name() == derivationName){
//                     DxAODEventsCBK = cbk;
//                 }
//             }
//             uint64_t nEventsProcessed  = allEventsCBK->nAcceptedEvents() + hist("sumOfWeights")->GetBinContent(1);
//             double sumOfWeights        = allEventsCBK->sumOfEventWeights() + hist("sumOfWeights")->GetBinContent(2);
//             double sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared() + hist("sumOfWeights")->GetBinContent(3);
// 
//             uint64_t nEventsDxAOD           = DxAODEventsCBK->nAcceptedEvents() + hist("sumOfWeights")->GetBinContent(4);
//             double sumOfWeightsDxAOD        = DxAODEventsCBK->sumOfEventWeights() + hist("sumOfWeights")->GetBinContent(5);
//             double sumOfWeightsSquaredDxAOD = DxAODEventsCBK->sumOfEventWeightsSquared() + hist("sumOfWeights")->GetBinContent(6);
// 
//             hist("sumOfWeights")->SetBinContent(1,nEventsProcessed);
//             hist("sumOfWeights")->SetBinContent(2,sumOfWeights);
//             hist("sumOfWeights")->SetBinContent(3,sumOfWeightsSquared);
//             hist("sumOfWeights")->SetBinContent(4,nEventsDxAOD);
//             hist("sumOfWeights")->SetBinContent(5,sumOfWeightsDxAOD);
//             hist("sumOfWeights")->SetBinContent(6,sumOfWeightsSquaredDxAOD);
// 
//         }
// 
//         //Example of loading in the crossSections into a map
//         TTree cross_t; cross_t.ReadFile(dirname + "/data/MyAnalysis/my.metadata.txt"); 
//         uint32_t mcchannel=0; double crossSection=0; double kFactor = 0; double filterEff = 0;
//         cross_t.SetBranchAddress("dataset_number",&mcchannel);
//         cross_t.SetBranchAddress("crossSection",&crossSection);
//         cross_t.SetBranchAddress("kFactor",&kFactor);
//         cross_t.SetBranchAddress("genFiltEff",&filterEff);
//         for(int i=0;i<cross_t.GetEntries();i++) {
//             cross_t.GetEntry(i);
//             m_crossSections[mcchannel] = crossSection;
//             m_kFactors[mcchannel] = kFactor;
//             m_filterEffs[mcchannel] = filterEff;
//         }
//     }
// 
//     return StatusCode::SUCCESS;
// }
// 
// StatusCode MyxAODAnalysis :: execute ()
// {
//   // Here you do everything that needs to be done on every single
//   // events, e.g. read input variables, apply cuts, and fill
//   // histograms and trees.  This is where most of your actual analysis
//   // code will go.
// 
// 
//   // Print processing rate at set interval, so we know where we are
//   ++m_eventCounter;
//   if ( m_eventCounter == 1) m_startTime = time(0);
//   if ( m_eventCounter % m_progressInterval == 0 ) {
//   	Info("execute()","%li events processed so far  <<<==", m_eventCounter );
// 	Info("execute()","Processing rate = %f Hz", float(m_eventCounter)/float(time(0)-m_startTime));
//   }
//   ANA_MSG_DEBUG("::execute: Event " << m_eventCounter);
// 
//   // retrieve the eventInfo object from the event store
//   ANA_CHECK (evtStore()->retrieve (m_eventInfo, "EventInfo"));
// 
//   //============================================================================
//   // Event cleaning
//   //============================================================================
//   ANA_MSG_DEBUG("::execute: Event Cleaning");
// 
//   // detector errors
//   if (
//   ( m_eventInfo->errorState( xAOD::EventInfo::LAr )  == xAOD::EventInfo::Error )
//   || ( m_eventInfo->errorState( xAOD::EventInfo::Tile ) == xAOD::EventInfo::Error )
//   || ( m_eventInfo->errorState( xAOD::EventInfo::SCT )  == xAOD::EventInfo::Error )
//   || ( m_eventInfo->isEventFlagBitSet( xAOD::EventInfo::Core, 18 ) )
//   )
//   return StatusCode::SUCCESS;
// 
//   // if data check if event passes GRL
//   if (!event_isMC) { 
//       if (!m_grl->passRunLB(*m_eventInfo)) {
//           return StatusCode::SUCCESS; 
//       }
//   } 
// 
//   // Jet cleaning
//   if ( m_eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad") == 0 ){
//       return StatusCode::SUCCESS;
//   }
// 
// 
//   //============================================================================
//   // Retrieving containers
//   //============================================================================
//   ANA_MSG_DEBUG("::execute: Retrieving Containers");
//   // retrieve electron container 
//   ANA_MSG_DEBUG("::execute: Retrieving electrons...");
//   const xAOD::ElectronContainer* electrons_org = 0;
//   ANA_CHECK( evtStore()->retrieve( electrons_org, "Electrons" ) );
//   std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > electrons_shallowCopy = xAOD::shallowCopyContainer( *electrons_org );
//   m_electrons = electrons_shallowCopy.first;
// 
//   // retrieve fwd electron container 
//   ANA_MSG_DEBUG("::execute: Retrieving fwd electrons...");
//   const xAOD::ElectronContainer* fwdElectrons_org = 0;
//   ANA_CHECK( evtStore()->retrieve( fwdElectrons_org, "ForwardElectrons" ) );
//   std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > fwdElectrons_shallowCopy = xAOD::shallowCopyContainer( *fwdElectrons_org );
//   m_fwdElectrons = fwdElectrons_shallowCopy.first;  
//   std::unique_ptr< xAOD::ElectronContainer > fwdElectrons_p( fwdElectrons_shallowCopy.first );
//   std::unique_ptr< xAOD::ShallowAuxContainer > fwdElectrons_p2( fwdElectrons_shallowCopy.second );
// 
//   // retrieve vertices
//   ANA_MSG_DEBUG("::execute: Retrieving PrimaryVertices...");
//   const xAOD::VertexContainer* vertex_org = 0;
//   ANA_CHECK( evtStore()->retrieve( vertex_org, "PrimaryVertices") );
//   std::pair< xAOD::VertexContainer*, xAOD::ShallowAuxContainer* > vertex_shallowCopy = xAOD::shallowCopyContainer( *vertex_org );
//   m_vertex = vertex_shallowCopy.first;
//   std::unique_ptr< xAOD::VertexContainer > vertices_p( vertex_shallowCopy.first );
//   std::unique_ptr< xAOD::ShallowAuxContainer > vertices_p2( vertex_shallowCopy.second );
// 
//   // retrieve Tracks
//   ANA_MSG_DEBUG("::execute: Retrieving tracks...");
//   const xAOD::TrackParticleContainer* indet_tracks_org = 0;
//   ANA_CHECK( evtStore()->retrieve( indet_tracks_org, "InDetTrackParticles" ) );
//   std::pair< xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer* > indet_tracks_shallowCopy = xAOD::shallowCopyContainer( *indet_tracks_org );
//   m_indetTracks.reset( indet_tracks_shallowCopy.first );
//   m_indetTracksAux.reset( indet_tracks_shallowCopy.second );
// 
//   // retrieve muon container 
//   ANA_MSG_DEBUG("::execute: Retrieving muons...");
//   const xAOD::MuonContainer* muons_org = 0;
//   ANA_CHECK( evtStore()->retrieve( muons_org, "Muons" ) );
//   std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons_org );
//   m_muons = muons_shallowCopy.first;
// 
//   // retrieve jet container
//   ANA_MSG_DEBUG("::execute: Retrieving AntiKt4EMPFlowJets...");
//   const xAOD::JetContainer* jets_org = 0;
//   ANA_CHECK( evtStore()->retrieve( jets_org, "AntiKt4EMPFlowJets" ) );
//   std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets_org );
//   m_jets = jets_shallowCopy.first;
// 
//   if ( event_isMC ){
//       ANA_MSG_DEBUG("::execute: Retrieving egammaTruthParticles...");
//       ANA_CHECK( evtStore()->retrieve( m_egammaTruthContainer, "egammaTruthParticles" ) );
// 
//       ANA_MSG_DEBUG("::execute: Retrieving TruthParticles...");      
//       ANA_CHECK( evtStore()->retrieve( m_truthParticles, "TruthParticles" ) );
//   }
// 
//   // Missing et stuff
//   met::addGhostMuonsToJets(*muons_org, *m_jets);
// 
// 
//   for ( const auto& vtx : *m_vertex ){
//       if ( vtx->vertexType() == xAOD::VxType::PriVtx) {
//           m_primaryVertex = vtx;
//       }
//   }
//   if ( m_primaryVertex->vertexType() != xAOD::VxType::PriVtx) {
//       return StatusCode::SUCCESS;
//   }
// 
//   //============================================================================
//   // Calibrations and Corrections
//   //============================================================================
//   ANA_MSG_DEBUG("::execute: Calibrations and Corrections");
//   for ( const auto& el : *m_electrons ) {
//       if ( event_isMC ) {
//           // Apply fudge factors to electrons
//           if ( m_MCShifterTool->applyCorrection( *el ) != CP::CorrectionCode::Ok ) {
//               ANA_MSG_WARNING( "execute(): Electron correction failed on an object.");
//           }
//       }
//       // apply energy corrections to electrons
//       if( m_electronCalibrationSmearingTool->applyCorrection( *el ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Electron calibration failed" );
//   }
// 
//   for ( const auto& fel : *m_fwdElectrons ){
//       // apply energy corrections to fwd electrons      
//       if( m_electronCalibrationSmearingTool->applyCorrection( *fel ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Fwd Electron calibration failed" );
//   }
// 
//   for ( const auto& mu : *m_muons ) {
//       // apply energy corrections to muons
//       if( m_muonCalibrationSmearingTool->applyCorrection( *mu ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Muon calibration failed" );
//   }
// 
// 
//   for ( const auto& jet : *m_jets ) {
//       // apply energy corrections to jets
//       if( m_jetCalibrationTool->applyCorrection( *jet) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Jet calibration failed" );
//   }
// 
// 
//   ANA_MSG_DEBUG("::execute: Retrieving MET_Core_AntiKt4EMPFlow...");
//   ANA_CHECK( evtStore()->retrieve(m_coreMet, "MET_Core_AntiKt4EMPFlow") );
// 
//   // needed for MET_Maker
//   if(!xAOD::setOriginalObjectLink(*jets_org, *m_jets)){//tell calib container what old container it matches
//       std::cout << "Failed to set the original object links" << std::endl;
//       return 1;
//   }
//   if(!xAOD::setOriginalObjectLink(*electrons_org, *m_electrons)){//tell calib container what old container it matches
//       std::cout << "Failed to set the original object links" << std::endl;
//       return 1;
//   }
//   if(!xAOD::setOriginalObjectLink(*muons_org, *m_muons)){//tell calib container what old container it matches
//       std::cout << "Failed to set the original object links" << std::endl;
//       return 1;
//   }
// 
//   // Select objects which should be used in the overlap removal
//   const ort::inputDecorator_t selectDec("selected");
//   for ( auto el : *m_electrons ){
//       if ( el->pt() > 10000 && abs( el->caloCluster()->etaBE(2) ) < 2.47){
//           selectDec(*el) = true;
//       }
//       else{
//           selectDec(*el) = false;
//       }
//   }
// 
//   for ( auto mu : *m_muons ){
//       if ( mu->pt() > 10000 && abs( mu->eta() ) < 2.7 ){
//           selectDec(*mu) = true;
//       }
//       else{
//           selectDec(*mu) = false;
//       }
//   }  
// 
//   for ( auto jet : *m_jets ){
//       if ( jet->pt() > 20000 && abs( jet->eta() ) < 2.7 ){
//           selectDec(*jet) = true;
//       }
//       else{
//           selectDec(*jet) = false;
//       }
//   }  
// 
//   // record the calibrated objects and there shallow copies ( needed by MET_Maker )
//   ANA_CHECK( evtStore()->record(m_jets, "CalibJets") );
//   ANA_CHECK( evtStore()->record(jets_shallowCopy.second, "CalibJetsAux") );
//   ANA_CHECK( evtStore()->record(m_muons, "CalibMuons") );
//   ANA_CHECK( evtStore()->record(muons_shallowCopy.second, "CalibMuonsAux") );  
//   ANA_CHECK( evtStore()->record(m_electrons, "CalibElectrons") );
//   ANA_CHECK( evtStore()->record(electrons_shallowCopy.second, "CalibElectronsAux") );
// 
// 
// 
//   //retrieve the MET association map
//   ANA_CHECK( evtStore()->retrieve(m_metMap, "METAssoc_AntiKt4EMPFlow") );
//   m_metMap->resetObjSelectionFlags();
// 
// 
//   // Create a MissingETContainer with its aux store for each systematic
//   m_newMetContainer    = new xAOD::MissingETContainer();
//   m_newMetAuxContainer = new xAOD::MissingETAuxContainer();
//   m_newMetContainer->setStore(m_newMetAuxContainer);
//   ANA_CHECK( evtStore()->record(m_newMetContainer, "newMETContainer") );
//   ANA_CHECK( evtStore()->record(m_newMetAuxContainer, "newMETContainerAux") );  
// 
//   m_metMap->resetObjSelectionFlags();
// 
//   //Electrons
//   ConstDataVector<xAOD::ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);
//   for(const auto& el : *m_electrons) {
//       if(el->pt()>10e3 && el->eta()<2.47 && Accessor_LHLoose( *el ) ) metElectrons.push_back(el);
//   }
//   ANA_CHECK( m_metutil->rebuildMET("RefEle",                   //name of metElectrons in metContainer
//                           xAOD::Type::Electron,       //telling the rebuilder that this is electron met
//                           m_newMetContainer,            //filling this met container
//                           metElectrons.asDataVector(),//using these metElectrons that accepted our cuts
//                           m_metMap)                     //and this association map
//                           );
// 
//   //Muons
//   ConstDataVector<xAOD::MuonContainer> metMuons(SG::VIEW_ELEMENTS);
//   for(const auto& mu : *m_muons) {
//       if(mu->pt()>10e3 && mu->eta()<2.7 && m_muonSelectionToolLoose->accept( *mu ) ) metMuons.push_back(mu);
//   }
// 
//   ANA_CHECK( m_metutil->rebuildMET("RefMuon",
//                                   xAOD::Type::Muon,
//                                   m_newMetContainer,
//                                   metMuons.asDataVector(),
//                                   m_metMap)
//                                   );
// 
//   //Now time to rebuild jetMet and get the soft term
//   //This adds the necessary soft term for both CST and TST
//   //these functions create an xAODMissingET object with the given names inside the container
//   ANA_CHECK( m_metutil->rebuildJetMET("RefJet",        //name of jet met
//                                  "SoftClus",      //name of soft cluster term met
//                                  "PVSoftTrk",     //name of soft track term met
//                                  m_newMetContainer, //adding to this new met container
//                                  m_jets,          //using this jet collection to calculate jet met
//                                  m_coreMet,         //core met container
//                                  m_metMap,          //with this association map
//                                  false            //don't apply jet jvt cut
//                                  )
//          );
// 
//   ANA_CHECK( m_metutil->buildMETSum("FinalClus", m_newMetContainer, MissingETBase::Source::LCTopo) );
//   m_METmaker = ( *m_newMetContainer )["FinalClus"];
// 
// 
//   // Get missing energy
//   met_met = m_METmaker->met();
//   met_phi = m_METmaker->phi();    
// 
//   // Do overlap removal
//   ANA_CHECK ( m_orTool->removeOverlaps( m_electrons, m_muons, m_jets ) ); 
// 
//   // Event specific variables
//   if ( event_isMC ){
//       event_mcChannelNumber = m_eventInfo->mcChannelNumber();
//       event_MCWeight = m_eventInfo->mcEventWeight();
//       event_crossSection = m_crossSections[event_mcChannelNumber] * m_kFactors[event_mcChannelNumber] * m_filterEffs[event_mcChannelNumber] ;
//   }
//   event_evtNumber = m_eventInfo->eventNumber();
//   event_runNumber = m_eventInfo->runNumber();
//   ANA_CHECK(m_PileupReweighting->apply(*m_eventInfo));
//   event_pileupweight = m_eventInfo->auxdecor<float>("PileupWeight");
// 
//   event_correctedScaledAverageMu       = m_PileupReweighting->getCorrectedAverageInteractionsPerCrossing( *m_eventInfo, true );
//   event_correctedScaledActualMu        = m_PileupReweighting->getCorrectedActualInteractionsPerCrossing( *m_eventInfo, true );
// 
//   NvtxReco = m_vertex->size();
// 
//   // Clear the electron, fwd electron, and muon vectors
//   clearVectors();
// 
//   // if triggers are not passed, skip the event
//   trig = m_trigDecTool->isPassed("HLT_e26_lhtight_nod0_ivarloose") || m_trigDecTool->isPassed("HLT_e60_lhmedium_nod0") || m_trigDecTool->isPassed("HLT_e140_lhloose_nod0") ; 
//   if ( !trig ) return StatusCode::SUCCESS;
// 
//   // Do preselection of electrons, fwd electrons, and muons
//   std::vector < const xAOD::Electron* > selElectrons = selectElectronCandidates();
//   std::vector < const xAOD::Electron* > selFwdElectrons = selectFwdElectronCandidates();
//   std::vector < xAOD::Muon > selMuons = selectMuonCandidates();
// 
// 
//   nElectrons = selElectrons.size();
//   nFwdElectrons = selFwdElectrons.size();
//   nMuons = selMuons.size();
// 
//   // if enough particles left, save information
//   if ( ( nElectrons + nFwdElectrons + nMuons >= 2 )  && trig ){ 
//       // get trigger SF for event
//       event_SFTrigger = 1.;
//       std::vector<const xAOD::IParticle*> trigElectrons;
//       if ( event_isMC ){
//           for ( const xAOD::Electron* el : selElectrons ){
//               trigElectrons.push_back( el );
//           }       
//           if( !m_trigScaleFactors->getEfficiencyScaleFactor(trigElectrons, event_SFTrigger)){
//                                 ATH_MSG_WARNING( "Couldn't get trigger scale factor!" );
//                             } 
//       }
// 
//       eventWeight = event_pileupweight;
//       if ( event_isMC ){
//           eventWeight = event_MCWeight * event_crossSection * event_pileupweight * event_SFTrigger;
//       }
// 
//       // fill histograms with number of particles
//       hist("h_nElectrons")->Fill( nElectrons, eventWeight );      
//       hist("h_nFwdElectrons")->Fill( nFwdElectrons, eventWeight );                
//       hist("h_nMuons")->Fill( nMuons, eventWeight );      
// 
//     // Fill the electrons vectors
//     for ( const xAOD::Electron* el : selElectrons ){
//         const TLorentzVector& el_p4 = el->p4();
//         const xAOD::TrackParticle* tt = el->trackParticle();
//         const xAOD::CaloCluster* cluster = el->caloCluster();
// 
//         // ID information
//         pdf_score.push_back( m_lgbm_pdfvars->predict( *el ) );
//         pdfcut_score.push_back( m_lgbm_pdfcutvars->predict( *el ) );
//         LHLoose.push_back( Accessor_LHLoose( *el ) );
//         LHMedium.push_back( Accessor_LHMedium( *el ) );
//         LHTight.push_back( Accessor_LHTight( *el ) );
// 
//         // Truth information
//         if ( event_isMC ){
//             const xAOD::TruthParticle* tp = xAOD::TruthHelpers::getTruthParticle( *el );
// 
//             IFFTruth.push_back( static_cast<std::underlying_type_t<IFF::Type>>( m_iff->classify( *el ) ) );
//             if ( tp == nullptr ){
//                 truthPdgId.push_back( 0 );
//                 truth_eta.push_back( -999 );
//                 truth_phi.push_back( -999 );
//                 truth_E.push_back( 0 );                
//             }
//             else{
//                 truthPdgId.push_back( tp->pdgId() );
//                 truth_eta.push_back( tp->eta() );
//                 truth_phi.push_back( tp->phi() );
//                 truth_E.push_back( tp->e() / 1000. );
//             }
//             truthType.push_back( Accessor_truthType( *el ) );
//             truthOrigin.push_back( Accessor_truthOrigin( *el ) );
//             firstEgMotherTruthType.push_back( Accessor_firstEgMotherTruthType( *el ) );
//             firstEgMotherTruthOrigin.push_back( Accessor_firstEgMotherTruthOrigin( *el ) );
//             firstEgMotherPdgId.push_back( Accessor_firstEgMotherPdgId( *el ) );
// 
//             bool truthEgParticle = false;
//             for ( size_t i = 0; i<TruthPointerIdx.size(); i++){
//                 if ( el->index() == TruthPointerIdx[i] ) truthEgParticle = true;
//             }
//             truthParticle.push_back( truthEgParticle );
//         }
// 
//         // kinematics
//         eta.push_back( el->caloCluster()->etaBE(2) );
//         phi.push_back( el_p4.Phi() );
//         pt.push_back( tt->pt() / 1000. );
//         et.push_back( el_p4.Pt() / 1000. );
//         charge.push_back( el->charge() );
// 
//         // ecids
//         ecids.push_back( Accessor_ECIDS( *el ) );
//         ecidsValue.push_back( Accessor_ECIDSResult( *el ) );
// 
//         // Triger information
//         std::vector<const xAOD::IParticle*> myParticles;
//         myParticles.push_back( el );
//         trigger.push_back( m_triggerMatching->match( myParticles, "HLT_e26_lhtight_nod0_ivarloose", 0.07, false ) ||
//                             m_triggerMatching->match( myParticles, "HLT_e60_lhmedium_nod0", 0.07, false ) ||  // included for scale factors
//                             m_triggerMatching->match( myParticles, "HLT_e140_lhloose_nod0", 0.07, false ) );  // included for scale factors
// 
// 
//         // Create and same images
//         m_images->create_images( *el );
//         em_calo.push_back( m_images->em_calo );
//         h_calo.push_back( m_images->h_calo );
//         time_em.push_back( m_images->time_em );
//         time_h.push_back( m_images->time_h );
// 
//         em_calo0.push_back( em_calo.back()[0] );
//         em_calo1.push_back( em_calo.back()[1] );
//         em_calo2.push_back( em_calo.back()[2] );
//         em_calo3.push_back( em_calo.back()[3] );
//         em_calo4.push_back( em_calo.back()[4] );
//         em_calo5.push_back( em_calo.back()[5] );
//         em_calo6.push_back( em_calo.back()[6] );
//         em_calo7.push_back( em_calo.back()[7] );
// 
//         h_calo0.push_back( h_calo.back()[0] );
//         h_calo1.push_back( h_calo.back()[1] );
//         h_calo2.push_back( h_calo.back()[2] );
//         h_calo3.push_back( h_calo.back()[3] );
//         h_calo4.push_back( h_calo.back()[4] );
//         h_calo5.push_back( h_calo.back()[5] );
//         h_calo6.push_back( h_calo.back()[6] );
//         h_calo7.push_back( h_calo.back()[7] );
// 
//         time_em0.push_back( time_em.back()[0] );
//         time_em1.push_back( time_em.back()[1] );
//         time_em2.push_back( time_em.back()[2] );
//         time_em3.push_back( time_em.back()[3] );
//         time_em4.push_back( time_em.back()[4] );
//         time_em5.push_back( time_em.back()[5] );
//         time_em6.push_back( time_em.back()[6] );
//         time_em7.push_back( time_em.back()[7] );
// 
//         time_h0.push_back( time_h.back()[0] );
//         time_h1.push_back( time_h.back()[1] );
//         time_h2.push_back( time_h.back()[2] );
//         time_h3.push_back( time_h.back()[3] );
//         time_h4.push_back( time_h.back()[4] );
//         time_h5.push_back( time_h.back()[5] );
//         time_h6.push_back( time_h.back()[6] );
//         time_h7.push_back( time_h.back()[7] );
// 
//         // Scale factors
//         double recoSf = 1;
//         double IdSf = 1;
//         double IsoSf = 1;
//         double ECIDSSf = 1;
//         double charge_sf = 1;
//         if ( event_isMC ){
//             if(!m_electronSFReco->getEfficiencyScaleFactor( *el, recoSf)){
//                 ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
//             }
//             if(!m_electronSFID->getEfficiencyScaleFactor( *el, IdSf)){
//                 ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
//             }
//             if(!m_electronSFIso->getEfficiencyScaleFactor( *el, IsoSf)){
//                 ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
//             }
//             if(!m_electronSFECIDS->getEfficiencyScaleFactor( *el, ECIDSSf)){
//                 ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
//             }                                                 
//             if(!m_electronChargeIdSF->getEfficiencyScaleFactor( *el, charge_sf)){
//                 ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
//             }
//         }
// 
//         recoSF.push_back( recoSf );
//         idSF.push_back( IdSf );
//         isoSF.push_back( IsoSf );
//         ecidsSF.push_back( ECIDSSf );
//         chargeSF.push_back( charge_sf );
// 
//         combSF.push_back( recoSf * IdSf * IsoSf * ECIDSSf * charge_sf  );
// 
//         // Additional variables for ER with CNN
//         eAccCluster.push_back( cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3) );
//         double etaCalo;
//         if ( !cluster->retrieveMoment( xAOD::CaloCluster::ETACALOFRAME, etaCalo ) )
//             ANA_MSG_WARNING( "cannot find etaCalo" );
//         cellIndexCluster.push_back( std::floor( std::abs( etaCalo/0.025 ) ) );
//         etaModCalo.push_back( std::fmod(std::abs( etaCalo), 0.025 ) ); 
//         double phiCalo;
//         if ( !cluster->retrieveMoment( xAOD::CaloCluster::PHICALOFRAME, phiCalo ) )
//             ANA_MSG_WARNING( "cannot find phiCalo" );        
//         dPhiTH3.push_back( std::fmod(2.*TMath::Pi()+phiCalo,TMath::Pi()/32.)-TMath::Pi()/64.0 ); 
//         deltaEta2.push_back( Accessor_deltaEta2( *el ) );       
//         poscs1.push_back( Accessor_poscs1( *el ) );
//         poscs2.push_back( Accessor_poscs2( *el ) ); 
// 
//         // LH variables
//         Rhad1.push_back( Accessor_Rhad( *el ) );
//         Rhad.push_back( Accessor_Rhad1( *el ) );
//         f3.push_back( Accessor_f3( *el ) );
//         weta2.push_back( Accessor_weta2( *el ) );
//         Rphi.push_back( Accessor_Rphi( *el ) );
//         Reta.push_back( Accessor_Reta( *el ) );
//         Eratio.push_back( Accessor_Eratio( *el ) );
//         f1.push_back( Accessor_f1( *el ) );
//         deltaEta1.push_back( Accessor_deltaEta1( *el ) );
//         deltaPhiRescaled2.push_back( Accessor_deltaPhiRescaled2( *el ) );
//         wtots1.push_back( Accessor_wtots1( *el ) ); 
// 
//         numberOfInnermostPixelHits.push_back( Accessor_numberOfInnermostPixelLayerHits( *tt ) );
//         numberOfPixelHits.push_back( Accessor_numberOfPixelHits( *tt ) + Accessor_numberOfPixelDeadSensors( *tt ) );
//         numberOfSCTHits.push_back( Accessor_numberOfSCTHits( *tt ) + Accessor_numberOfSCTDeadSensors( *tt ) );
// 
//         d0.push_back( tt->d0() );
//         d0Sig.push_back( tt->d0() / sqrt( ( tt->definingParametersCovMatrix() )( 0, 0 ) ) );
//         EptRatio.push_back( el_p4.Pt() / tt->pt() );
//         vertexIndex.push_back( tt->vertex()->index() );
// 
//         float dPOverP_scalar;
//         unsigned int index;
//         if ( tt->indexOfParameterAtPosition( index, xAOD::LastMeasurement ) ) {
//             const double refittedTrack_LMqoverp = double( el->charge() ) / sqrt( std::pow( tt->parameterPX( index ), 2 ) +
//                     std::pow( tt->parameterPY( index ), 2 ) +
//                     std::pow( tt->parameterPZ( index ), 2 ));
//             dPOverP_scalar = 1.0 - tt->qOverP()/refittedTrack_LMqoverp;
//             if ( dPOverP_scalar < -4.0 ) dPOverP_scalar = -4.0;
//         } else {
//             dPOverP_scalar = -999.f;
//         }
//         dPOverP.push_back( dPOverP_scalar );
// 
//         float eProbHT;
//         constexpr double tau = 15.0;
//         constexpr double fEpsilon = 1.0e-30;  // to avoid zero division
//         //Transform the TRT PID output for use in the LH tool.
//         tt->summaryValue( eProbHT, ( xAOD::SummaryType )48 ); // type 48 = eProbabilityHT
//         double pid_tmp = eProbHT;
//         if ( pid_tmp >= 1.0 ) pid_tmp = 1.0 - 1.0e-15;  //this number comes from TMVA
//         else if ( pid_tmp <= fEpsilon ) pid_tmp = fEpsilon;
//         eProbHT = -log( 1.0/pid_tmp - 1.0 )*( 1./double( tau ));
//         TRTPID.push_back( eProbHT );
// 
//         // Additional variables
//         sigmad0.push_back( sqrt( ( tt->definingParametersCovMatrix() )( 0, 0 ) ) );
//         z0.push_back( ( tt->z0() + tt->vz() - tt->vertex()->z() ) );
// 
// 
//         // Isolation variables
//         GradientIso.push_back( m_isolationSelectionToolLoose->accept( *el ) );
// 
//         topoetcone20.push_back( Accessor_topoetcone20( *el ) );
//         topoetcone30.push_back( Accessor_topoetcone30( *el ) );
//         topoetcone40.push_back( Accessor_topoetcone40( *el ) );
//         ptvarcone20.push_back( Accessor_ptvarcone20( *el ) );
//         ptvarcone30.push_back( Accessor_ptvarcone30( *el ) );
//         ptvarcone40.push_back( Accessor_ptvarcone40( *el ) );
// 
// 
//         // Fill kinematics histograms
//         if ( event_isMC ){
//             hist( "h_eta" )->Fill( el->caloCluster()->etaBE(2), eventWeight * combSF.back() );
//             hist( "h_phi" )->Fill( el->phi(), eventWeight * combSF.back() );
//             hist( "h_et" )->Fill( el_p4.Pt() / 1000., eventWeight * combSF.back() );
//             if ( Accessor_LHMedium( *el ) && Accessor_ECIDS( *el ) && m_isolationSelectionToolLoose->accept( *el ) ){
//                 hist( "h_eta_LHMedium" )->Fill( el->caloCluster()->etaBE(2), eventWeight * combSF.back() );
//                 hist( "h_phi_LHMedium" )->Fill( el->phi(), eventWeight * combSF.back() );
//                 hist( "h_et_LHMedium" )->Fill( el_p4.Pt() / 1000., eventWeight * combSF.back() );
//             }
//         }
//         else{
//             hist( "h_eta" )->Fill( el->caloCluster()->etaBE(2) );
//             hist( "h_phi" )->Fill( el_p4.Phi() );
//             hist( "h_et" )->Fill( el_p4.Pt() / 1000. );
//             if ( Accessor_LHMedium( *el ) && Accessor_ECIDS( *el ) && m_isolationSelectionToolLoose->accept( *el ) ){
//                 hist( "h_eta_LHMedium" )->Fill( el->caloCluster()->etaBE(2) );
//                 hist( "h_phi_LHMedium" )->Fill( el_p4.Phi() );
//                 hist( "h_et_LHMedium" )->Fill( el_p4.Pt() / 1000. );
//             }            
//         }
//     }
// 
//     // Fill the muon vectors
//     for ( const auto& mu: selMuons){
//         const TLorentzVector& mu_p4 = mu.p4();
// 
//         // kinematics
//         mu_eta.push_back( mu_p4.Eta() );
//         mu_phi.push_back( mu_p4.Phi() );
//         mu_et.push_back( mu_p4.Pt() / 1000. );
//         mu_charge.push_back( mu.charge() );
// 
//         // truth information
//         if ( event_isMC ){
//             mu_iffTruth.push_back( static_cast<std::underlying_type_t<IFF::Type>>( m_iff->classify( mu ) ) );
//         }
// 
//         // scale factors
//         float id_sf, iso_sf;
//         if(!m_muonSF->getEfficiencyScaleFactor( mu , id_sf))  ATH_MSG_WARNING( "Couldn't get muon scale factor!" );
//         if(!m_muonSFIso->getEfficiencyScaleFactor( mu , iso_sf))  ATH_MSG_WARNING( "Couldn't get muon scale factor!" );
// 
//         mu_idSF.push_back( id_sf );
//         mu_isoSF.push_back( iso_sf );
// 
//         mu_SF.push_back( id_sf * iso_sf );
// 
//         // fill kinematics histograms
//         if ( event_isMC ){
//             hist( "h_mu_eta" )->Fill(mu_p4.Eta(), eventWeight * mu_SF.back() );
//             hist( "h_mu_phi" )->Fill(mu_p4.Phi(), eventWeight * mu_SF.back() );
//             hist( "h_mu_pt" )->Fill(mu_p4.Pt() / 1000. , eventWeight * mu_SF.back() );
//         }
//         else{
//             hist( "h_mu_eta" )->Fill(mu_p4.Eta(), eventWeight * mu_SF.back() );
//             hist( "h_mu_phi" )->Fill(mu_p4.Phi(), eventWeight * mu_SF.back() );
//             hist( "h_mu_pt" )->Fill(mu_p4.Pt() / 1000. , eventWeight * mu_SF.back() );            
//         }
//     }
// 
//     // Fill the fwd electrons vectors
//     for ( const xAOD::Electron* fel : selFwdElectrons ){
//         const TLorentzVector& fel_p4 = fel->p4();
// 
//         // ID information
//         fwd_LHLoose.push_back( Accessor_fwdLHLoose( *fel ) );
//         fwd_LHMedium.push_back( Accessor_fwdLHMedium( *fel ) );
//         fwd_LHTight.push_back( Accessor_fwdLHTight( *fel ) );
//         fwd_LGBM_pid.push_back( m_lgbm_fwd_pid->predict( *fel ) );
// 
//         // kinematics
//         fwd_eta.push_back( fel->eta() );
//         fwd_phi.push_back( fel->phi() );
//         fwd_et.push_back( fel_p4.Pt() / 1000. );
//         fwd_LGBM_energy.push_back( m_lgbm_fwd_er->predict(*fel, NvtxReco, event_correctedScaledActualMu) );
//         fwd_LGBM_et.push_back( fwd_LGBM_energy.back() / cosh(fwd_eta.back()) );
// 
//         // Truth information
//         if ( event_isMC ){
//             const xAOD::TruthParticle* tp = xAOD::TruthHelpers::getTruthParticle( *fel );
// 
//             fwd_truthType.push_back( Accessor_truthType( *fel ) );
//             fwd_truthOrigin.push_back( Accessor_truthOrigin( *fel ) );
// 
//             if ( tp == nullptr ){
//                 fwd_truthPdgId.push_back( 0 );
//                 fwd_truth_eta.push_back( -999 );
//                 fwd_truth_phi.push_back( -999 );
//                 fwd_truth_E.push_back( 0 );                
//             }
//             else{            
//                 fwd_truthPdgId.push_back( tp->pdgId() );
//                 fwd_truth_eta.push_back( tp->eta() );
//                 fwd_truth_phi.push_back( tp->phi() );
//                 fwd_truth_E.push_back( tp->e() / 1000. );
//             }
// 
//             bool fwd_truthEgParticle = false;
//             for ( size_t i = 0; i<TruthPointerIdx_fwd.size(); i++){
//                 if ( fel->index() == TruthPointerIdx_fwd[i] ) fwd_truthEgParticle = true;
//             }
//             fwd_truthParticle.push_back( fwd_truthEgParticle );
//         }
// 
//         // Scale factors
//         double recoSf = 1;
//         double IdSf = 1;
//         if ( event_isMC ){
//             if(!m_electronSFReco->getEfficiencyScaleFactor( *fel, recoSf)){
//                 ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
//             }
//             if(!m_electronSFIDFwd->getEfficiencyScaleFactor( *fel, IdSf)){
//                 ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
//             }
//         }
// 
//         fwd_recoSF.push_back( recoSf );
//         fwd_idSF.push_back( IdSf );
// 
//         fwd_combSF.push_back( recoSf * IdSf );
// 
//         fwd_topoetcone20.push_back( Accessor_topoetcone20( *fel ) );
//         fwd_topoetcone30.push_back( Accessor_topoetcone30( *fel ) );
//         fwd_topoetcone40.push_back( Accessor_topoetcone40( *fel ) );
//         fwd_topoetconecoreConeEnergyCorrection.push_back( Accessor_topoetconecoreConeEnergyCorrection( *fel ) );
//         fwd_LGBM_iso.push_back( m_lgbm_fwd_iso->predict( *fel ) );
// 
// 
//         // fill kinematic histograms
//         if ( event_isMC ){
//             hist( "h_fwd_eta" )->Fill( fel->eta(), eventWeight * combSF.back() );
//             hist( "h_fwd_phi" )->Fill( fel->phi(), eventWeight * combSF.back() );
//             hist( "h_fwd_et" )->Fill( fel_p4.Pt() / 1000., eventWeight * combSF.back() );
//             if ( Accessor_fwdLHMedium( *fel ) ){
//                 hist( "h_fwd_eta_LHMedium" )->Fill( fel->eta(), eventWeight * combSF.back() );
//                 hist( "h_fwd_phi_LHMedium" )->Fill( fel->phi(), eventWeight * combSF.back() );
//                 hist( "h_fwd_et_LHMedium" )->Fill( fel_p4.Pt() / 1000., eventWeight * combSF.back() );
//             }
//         }
//         else{
//             hist( "h_fwd_eta" )->Fill( fel->eta() );
//             hist( "h_fwd_phi" )->Fill( fel->phi() );
//             hist( "h_fwd_et" )->Fill( fel_p4.Pt() / 1000. );
//             if ( Accessor_fwdLHMedium( *fel ) ){
//                 hist( "h_fwd_eta_LHMedium" )->Fill( fel->eta() );
//                 hist( "h_fwd_phi_LHMedium" )->Fill( fel->phi() );
//                 hist( "h_fwd_et_LHMedium" )->Fill( fel_p4.Pt() / 1000. );
//             }            
//         }        
// 
//     }
// 
//     // write everything into the tree
//     tree ("analysis")->Fill();
//   }
// 
// 
// 
//   return StatusCode::SUCCESS;
// }
// 
// // Function to calculat delta R between two particles
// float MyxAODAnalysis::deltaR( const float eta1, const float phi1, const float eta2, const float phi2 ) {
// 	const float deta = fabs(eta1 - eta2);
// 
// 	float dphi;
// 	if ( fabs(phi1 - phi2 - 2.0*3.14159265359) < fabs(phi1 - phi2) ) {
// 		dphi = fabs(phi1 - phi2 - 2.0*3.14159265359) ;
// 	} else {
// 		dphi = fabs(phi1 - phi2);
// 	}
// 
// 	return sqrt( deta*deta + dphi*dphi );
// }
// 
// // Clear the vectors before filling the information of the new event
// void MyxAODAnalysis::clearVectors(){
//     LHLoose.clear();
//     LHMedium.clear();
//     LHTight.clear();    
//     pdf_score.clear();
//     pdfcut_score.clear();
//     IFFTruth.clear();
//     truthPdgId.clear();
//     truthType.clear();
//     truthOrigin.clear();
//     firstEgMotherTruthType.clear();
//     firstEgMotherTruthOrigin.clear();
//     firstEgMotherPdgId.clear();
//     eta.clear();
//     phi.clear();
//     pt.clear();
//     et.clear();
//     ecids.clear();
//     ecidsValue.clear();
// 
//     truthParticle.clear();
// 
//     truth_eta.clear();
//     truth_phi.clear();
//     truth_E.clear();
// 
//     recoSF.clear();
//     idSF.clear();
//     isoSF.clear();
//     ecidsSF.clear();
//     chargeSF.clear();
//     combSF.clear();
// 
//     TruthPointerIdx.clear();
//     TruthPointerIdx_fwd.clear();
//     Rhad1.clear();
//     Rhad.clear();
//     f3.clear();
//     weta2.clear();
//     Rphi.clear();
//     Reta.clear();
//     Eratio.clear();
//     f1.clear();
//     numberOfInnermostPixelHits.clear();
//     numberOfPixelHits.clear();
//     numberOfSCTHits.clear();
//     d0.clear();
//     d0Sig.clear();
//     dPOverP.clear();
//     deltaEta1.clear();
//     deltaPhiRescaled2.clear();
//     EptRatio.clear();
//     TRTPID.clear();
//     wtots1.clear();
//     charge.clear();
//     trigger.clear();
//     vertexIndex.clear();
// 
//     eAccCluster.clear();
//     cellIndexCluster.clear();
//     etaModCalo.clear();
//     dPhiTH3.clear();
//     deltaEta2.clear();
//     poscs1.clear();
//     poscs2.clear();
// 
//     sigmad0.clear();
//     z0.clear();
// 
//     GradientIso.clear();
//     topoetcone20.clear();
//     topoetcone30.clear();
//     topoetcone40.clear();
//     ptvarcone20.clear();
//     ptvarcone30.clear();
//     ptvarcone40.clear();
// 
// 
//     em_calo.clear();
//     h_calo.clear();
//     time_em.clear();
//     time_h.clear();
// 
//     em_calo0.clear();
//     em_calo1.clear();
//     em_calo2.clear();
//     em_calo3.clear();
//     em_calo4.clear();
//     em_calo5.clear();
//     em_calo6.clear();
//     em_calo7.clear();
// 
//     h_calo0.clear();
//     h_calo1.clear();
//     h_calo2.clear();
//     h_calo3.clear();
//     h_calo4.clear();
//     h_calo5.clear();
//     h_calo6.clear();
//     h_calo7.clear();
// 
//     time_em0.clear();
//     time_em1.clear();
//     time_em2.clear();
//     time_em3.clear();
//     time_em4.clear();
//     time_em5.clear();
//     time_em6.clear();
//     time_em7.clear();
// 
//     time_h0.clear();
//     time_h1.clear();
//     time_h2.clear();
//     time_h3.clear();
//     time_h4.clear();
//     time_h5.clear();
//     time_h6.clear();
//     time_h7.clear();    
// 
//     mu_eta.clear();
//     mu_phi.clear();
//     mu_et.clear();
//     mu_charge.clear();
//     mu_iffTruth.clear();
// 
//     mu_idSF.clear();
//     mu_isoSF.clear();
//     mu_SF.clear();
// 
//     fwd_LHLoose.clear();
//     fwd_LHMedium.clear();
//     fwd_LHTight.clear();
//     fwd_truthPdgId.clear();
//     fwd_truthType.clear();
//     fwd_truthOrigin.clear();
//     fwd_truthParticle.clear();
//     fwd_eta.clear();
//     fwd_phi.clear();
//     fwd_pt.clear();
//     fwd_et.clear();
//     fwd_truth_eta.clear();
//     fwd_truth_phi.clear();
//     fwd_truth_E.clear();
//     fwd_recoSF.clear();
//     fwd_idSF.clear();
//     fwd_combSF.clear();
// 
//     fwd_LGBM_energy.clear();
//     fwd_LGBM_et.clear();    
//     fwd_LGBM_pid.clear();
// 
//     fwd_topoetcone20.clear();
//     fwd_topoetcone30.clear();
//     fwd_topoetcone40.clear();
//     fwd_topoetconecoreConeEnergyCorrection.clear();
//     fwd_LGBM_iso.clear();
// }
// 
// 
// 
// void MyxAODAnalysis::resetCutFlow( std::map< TString, multiplicityCutFlow* > map ){
//     for ( auto& cutFlow : map ){
//         cutFlow.second->indiv = 0;
//         cutFlow.second->acc = 0;
//         cutFlow.second->truthIndiv = 0;
//         cutFlow.second->truthAcc = 0;        
//     }
// }
// 
// 
// StatusCode MyxAODAnalysis::createCutFlowHists( std::map<TString, multiplicityCutFlow* > map ){
//     for ( const auto& string : map ){
//         ANA_CHECK( book( TH1D ( "h_acc_" + string.first, "h_acc_" + string.first, 20, 0, 20) ) ) ;
//         ANA_CHECK( book( TH1D ( "h_indiv_" + string.first, "h_indiv_" + string.first, 20, 0, 20) ) ) ;
//         ANA_CHECK( book( TH1D ( "h_truthAcc_" + string.first, "h_truthAcc_" + string.first, 20, 0, 20) ) ) ;
//         ANA_CHECK( book( TH1D ( "h_truthIndiv_" + string.first, "h_truthIndiv_" + string.first, 20, 0, 20) ) ) ;                        
//     }
// 
// 
// 
//     return StatusCode::SUCCESS;
// }
// 
// void MyxAODAnalysis::fillCutFlowHists( std::map<TString, multiplicityCutFlow* > map ){
//     for ( const auto& string : map ){
//         std::string type = string.first.Data();
//         hist( "h_indiv_" + type )->Fill(string.second->indiv);
//         hist( "h_acc_" + type )->Fill(string.second->acc);
//         hist( "h_truthIndiv_" + type )->Fill(string.second->truthIndiv);
//         hist( "h_truthAcc_" + type )->Fill(string.second->truthAcc);
//     }    
// }
// 
// void MyxAODAnalysis::increaseCutFlow( multiplicityCutFlow* cutFlow, bool pass, bool &passAll, bool truth_el, int idx, int electron){
//     if ( electron == 0 ){
//         if ( pass ) {
//             cutFlow->indiv++;
//             hist( "h_indivCutFlow" )->Fill(idx);
//         }
//         passAll = pass && passAll; 
//         if ( passAll ) {
//             cutFlow->acc++;
//             hist( "h_cutFlow" )->Fill(idx);
// 
//         }
//         if ( truth_el ){
//             if ( pass ) {
//                 cutFlow->truthIndiv++;
//                 hist( "h_indivTruthCutFlow" )->Fill(idx);
//             }
//             if ( passAll ) {
//                 cutFlow->truthAcc++;        
//                 hist( "h_truthCutFlow" )->Fill(idx);
//             }
//         }
//     }
//     else if ( electron == 1){
//         if ( pass ) {
//             cutFlow->indiv++;
//             hist( "h_muIndivCutFlow" )->Fill(idx);
//         }
//         passAll = pass && passAll; 
//         if ( passAll ) {
//             cutFlow->acc++;
//             hist( "h_muCutFlow" )->Fill(idx);
// 
//         }
//         if ( truth_el ){
//             if ( pass ) {
//                 cutFlow->truthIndiv++;
//                 hist( "h_muIndivTruthCutFlow" )->Fill(idx);
//             }
//             if ( passAll ) {
//                 cutFlow->truthAcc++;        
//                 hist( "h_muTruthCutFlow" )->Fill(idx);
//             }
//         }        
//     }
// 
//     else if ( electron == 2){
//         if ( pass ) {
//             cutFlow->indiv++;
//             hist( "h_FwdIndivCutFlow" )->Fill(idx);
//         }
//         passAll = pass && passAll; 
//         if ( passAll ) {
//             cutFlow->acc++;
//             hist( "h_FwdCutFlow" )->Fill(idx);
// 
//         }
//         if ( truth_el ){
//             if ( pass ) {
//                 cutFlow->truthIndiv++;
//                 hist( "h_FwdIndivTruthCutFlow" )->Fill(idx);
//             }
//             if ( passAll ) {
//                 cutFlow->truthAcc++;        
//                 hist( "h_FwdTruthCutFlow" )->Fill(idx);
//             }
//         }        
//     }        
// 
// 
//     return;
// }
// 
// 
// std::vector < xAOD::Muon > MyxAODAnalysis::selectMuonCandidates(){
//     std::vector < xAOD::Muon > muons_out;
// 
//     resetCutFlow( m_MuonCutFlows );
// 
//     for ( const xAOD::Muon* mu : *m_muons ){
//         const xAOD::TrackParticle* tt = mu->primaryTrackParticle();
//         const xAOD::TrackParticle* indet = mu->inDetTrackParticleLink() != 0 ? *(mu->inDetTrackParticleLink()) : nullptr ; 
//         bool passAll = true;
//         bool truth_mu = false;
// 
//         increaseCutFlow( m_MuonCutFlows["Mu_Init"], true, passAll, truth_mu, 0, 1);
// 
//         // OverlapRemoval
//         const ort::inputAccessor_t selectAcc("overlaps");
//         increaseCutFlow( m_MuonCutFlows["Mu_OR"], !selectAcc( * mu ), passAll, truth_mu, 1, 1);        
// 
//         // transverse momentum cut
//         increaseCutFlow( m_MuonCutFlows["Mu_Pt"], mu->pt() > 10e3, passAll, truth_mu, 2, 1);        
// 
//         // eta cut
//         increaseCutFlow( m_MuonCutFlows["Mu_Eta"], abs(mu->eta()) < 2.7, passAll, truth_mu, 3, 1);        
// 
//         if ( indet != nullptr ){
//             // Vertex
//             if ( indet->vertex() == nullptr) {
//                 increaseCutFlow( m_MuonCutFlows["Mu_Vtx"], false, passAll, truth_mu, 4, 1);  
//                 increaseCutFlow( m_MuonCutFlows["Mu_z0"], false, passAll, truth_mu, 5, 1);         
//             }
//             else{
//                 // z0 theta
//                 increaseCutFlow( m_MuonCutFlows["Mu_Vtx"], true, passAll, truth_mu, 4, 1);        
//                 increaseCutFlow( m_MuonCutFlows["Mu_z0"], abs( ( tt->z0() + tt->vz() - indet->vertex()->z() ) * sin(tt->theta())) < 0.5, passAll, truth_mu, 5, 1);        
//             }
//         }
//         else{
//             increaseCutFlow( m_MuonCutFlows["Mu_Vtx"], false, passAll, truth_mu, 4, 1);  
//             increaseCutFlow( m_MuonCutFlows["Mu_z0"], false, passAll, truth_mu, 5, 1);                 
//         }
// 
//         if ( tt != nullptr ){
//             // d0Sig
//             increaseCutFlow( m_MuonCutFlows["Mu_d0Sig"], abs( tt->d0() / std::sqrt( tt->definingParametersCovMatrixVec()[0]) ) < 3, passAll, truth_mu, 6, 1);  
// 
//             // Cosmic Cut
//             increaseCutFlow( m_MuonCutFlows["Mu_d0"], abs( tt->d0() ) < 1, passAll, truth_mu, 7, 1);              
//         }
//         else{
//             increaseCutFlow( m_MuonCutFlows["Mu_d0Sig"], false, passAll, truth_mu, 6, 1);  
//             increaseCutFlow( m_MuonCutFlows["Mu_d0"], false, passAll, truth_mu, 7, 1);    
//         }
// 
//         // Combined muon
//         increaseCutFlow( m_MuonCutFlows["Mu_Combined"], mu->muonType() == xAOD::Muon::MuonType::Combined, passAll, truth_mu, 8, 1);          
// 
//         // Isolation
//         increaseCutFlow( m_MuonCutFlows["Mu_Iso"], m_isolationSelectionToolLoose->accept( *mu ), passAll, truth_mu, 9, 1);          
// 
//         // Medium
//         increaseCutFlow( m_MuonCutFlows["Mu_Medium"], m_muonSelectionToolLoose->accept( *mu ), passAll, truth_mu, 10, 1);                  
// 
// 
//         if ( passAll ){
//             muons_out.push_back( *mu );
//         }
// 
//     }
//     fillCutFlowHists(m_MuonCutFlows);
// 
//     return muons_out;
// 
// }
// 
// std::vector < const xAOD::Electron* > MyxAODAnalysis::selectFwdElectronCandidates(){
//     std::vector < const xAOD::Electron* > electrons_out;
// 
//     resetCutFlow( m_FwdCutFlows );
// 
//     TruthPointerIdx_fwd.clear();
//     if ( event_isMC ){
//         for ( const xAOD::TruthParticle* tp : *m_egammaTruthContainer ){
//             if ( xAOD::EgammaHelpers::getRecoElectron( tp ) != nullptr){
//                 if ( xAOD::EgammaHelpers::getRecoElectron( tp )->author( xAOD::EgammaParameters::AuthorFwdElectron ) ) {
//                     TruthPointerIdx_fwd.push_back(xAOD::EgammaHelpers::getRecoElectron( tp )->index());
//                 }
//             }    
//         }
//     }
// 
//     for ( const auto& fel : *m_fwdElectrons ){
// 
//         bool truth_el = false;
//         if ( event_isMC ){
//             for ( size_t i = 0; i<TruthPointerIdx_fwd.size(); i++){
//                 if ( fel->index() == TruthPointerIdx_fwd[i] ) truth_el = true;
//             }
//         }
// 
//         bool passAll = true;
//         increaseCutFlow( m_FwdCutFlows["Fwd_Init"], true, passAll, truth_el, 0, 2);
// 
// 
//         // transverse momentum cut
//         increaseCutFlow( m_FwdCutFlows["Fwd_Pt"], fel->pt() > 20e3, passAll, truth_el, 1, 2);
// 
// 
//         // Eta cut
//         increaseCutFlow( m_FwdCutFlows["Fwd_Eta"], abs( fel->eta() ) < 4.9, passAll, truth_el, 2, 2);
// 
// 
//         // Object Quality
//         increaseCutFlow( m_FwdCutFlows["Fwd_OQ"], fel->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON), passAll, truth_el, 3, 2);
// 
//         if ( passAll ) electrons_out.push_back( fel );
// 
//         // Medium Likelihood 
//         increaseCutFlow( m_FwdCutFlows["Fwd_LHMedium"], bool(Accessor_fwdLHMedium( *fel )) , passAll, truth_el, 4, 2);
// 
// 
//     }
// 
//     fillCutFlowHists(m_FwdCutFlows);
// 
//     return electrons_out;
// }
// 
// 
// 
// std::vector < const xAOD::Electron* > MyxAODAnalysis::selectElectronCandidates(){
//     std::vector < const xAOD::Electron* > electrons_out  ;
// 
//     resetCutFlow( m_CutFlows );
// 
//     TruthPointerIdx.clear();
//     if ( event_isMC ){
//         for ( const xAOD::TruthParticle* tp : *m_egammaTruthContainer ){
//             if ( xAOD::EgammaHelpers::getRecoElectron( tp ) != nullptr){
//                 if ( xAOD::EgammaHelpers::getRecoElectron( tp )->author( xAOD::EgammaParameters::AuthorFwdElectron ) == false ){
//                     TruthPointerIdx.push_back(xAOD::EgammaHelpers::getRecoElectron( tp )->index());
//                 }
//             }    
//         }
//     }
// 
//     for ( const xAOD::Electron* el : *m_electrons ){
// 
// 
//         bool truth_el = false;
//         if ( event_isMC ){
//             for ( size_t i = 0; i<TruthPointerIdx.size(); i++){
//                 if ( el->index() == TruthPointerIdx[i] ) truth_el = true;
//             }
//         }
// 
//         bool passAll = true;
//         increaseCutFlow( m_CutFlows["Init"], true, passAll, truth_el, 0, 0);
// 
// 
//         // Overlap removal
//         const ort::inputAccessor_t selectAcc("overlaps");
//         increaseCutFlow( m_CutFlows["OR"], !selectAcc( *el ), passAll, truth_el, 1, 0);
// 
// 
//         // transverse momentum cut
//         increaseCutFlow( m_CutFlows["Pt"], el->pt() > 10e3, passAll, truth_el, 2, 0);
// 
// 
//         // Eta cut
//         increaseCutFlow( m_CutFlows["Eta"], abs( el->caloCluster()->etaBE(2) ) < 2.47, passAll, truth_el, 3, 0);
// 
// 
//         // Object Quality
//         increaseCutFlow( m_CutFlows["OQ"], el->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON), passAll, truth_el, 4, 0);
// 
// 
//         // has vertex
//         const xAOD::TrackParticle* tt = el->trackParticle();
//         increaseCutFlow( m_CutFlows["Vtx"], tt->vertex() != nullptr, passAll, truth_el, 5, 0);
// 
// 
//         // z0 theta 
//         if ( tt->vertex() == nullptr ){
//             increaseCutFlow( m_CutFlows["z0"], false , passAll, truth_el, 6, 0);
//         }
//         else{
//             increaseCutFlow( m_CutFlows["z0"], abs( ( tt->z0() + tt->vz() - tt->vertex()->z() ) * sin(tt->theta())) < 0.5 , passAll, truth_el, 6, 0);
//         }
// 
// 
//         // d0 Sig
//         increaseCutFlow( m_CutFlows["d0"], abs( tt->d0() / std::sqrt( tt->definingParametersCovMatrixVec()[0]) ) < 5, passAll, truth_el, 7, 0);
// 
// 
// 
//         if ( passAll ) electrons_out.push_back( el );
// 
//         // Isolation
//         increaseCutFlow( m_CutFlows["Iso"], m_isolationSelectionToolLoose->accept( *el ), passAll, truth_el, 8, 0);
// 
//         // ECIDS 
//         increaseCutFlow( m_CutFlows["ECIDS"], Accessor_ECIDS( *el ), passAll, truth_el, 9, 0);
// 
//         // Medium Likelihood 
//         increaseCutFlow( m_CutFlows["LHMedium"], bool(Accessor_LHMedium( *el )), passAll, truth_el, 10, 0);
// 
// 
// 
// 
//     }
// 
//     fillCutFlowHists(m_CutFlows);
// 
// 
//     return electrons_out;
// }
// 
// 
// 
// MyxAODAnalysis :: ~MyxAODAnalysis () {
// 
// }
// 
// 
// StatusCode MyxAODAnalysis :: finalize ()
// {
//   // This method is the mirror image of initialize(), meaning it gets
//   // called after the last event has been processed on the worker node
//   // and allows you to finish up any objects you created in
//   // initialize() before they are written to disk.  This is actually
//   // fairly rare, since this happens separately for each worker node.
//   // Most of the time you want to do your post-processing on the
//   // submission node after all your histogram outputs have been
//   // merged.
//   return StatusCode::SUCCESS;
// }
// 



#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include "xAODRootAccess/tools/TFileAccessTracer.h"
#include <xAODEventInfo/EventInfo.h>
#include <xAODCore/ShallowCopy.h>
#include "xAODTruth/xAODTruthHelpers.h"
#include "METUtilities/METHelpers.h"
#include "xAODBase/IParticleHelpers.h"

#include "xAODMetaData/FileMetaData.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

MORE_DIAGNOSTICS()


MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
    m_grl ("GoodRunsListSelectionTool/grl", this),
    m_PileupReweighting ("CP::PileupReweightingTool/tool", this),
    m_trigConfig ("TrigConf::xAODConfigTool/xAODConfigTool" ),
    m_trigDecTool ("Trig::TrigDecisionTool/TrigDecisionTool" ),
    m_triggerMatching ( "Trig::MatchingTool" ),
    m_electronCalibrationSmearingTool ( "CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool", this),
    m_electronSFReco ( "AsgElectronEfficiencyCorrection", this),
    m_electronSFID ( "AsgElectronEfficiencyCorrection", this),
    m_electronSFIDFwd ( "AsgElectronEfficiencyCorrection", this),    
    m_electronSFIso ( "AsgElectronEfficiencyCorrection", this),
    m_effTrig ( "AsgElectronEfficiencyCorrectionTool/effTool", this),    
    m_sfTrig ( "AsgElectronEfficiencyCorrectionTool/sfTool", this),
    m_electronSFECIDS ( "AsgElectronEfficiencyCorrection", this),
    m_electronChargeIdSF ( "CP::ElectronChargeEfficiencyCorrectionTool", this),
    m_MCShifterTool ( "ElectronPhotonShowerShapeFudgeTool", this),
    m_muonCalibrationSmearingTool ( "CP::MuonCalibrationAndSmearingTool/MuonCalibrationAndSmearingTool", this),
    m_muonSelectionToolLoose ( "CP::MuonSelectionTool", this),
    m_muonSF ( "CP::MuonEfficiencyScaleFactors", this),
    m_muonSFIso ( "CP::MuonEfficiencyScaleFactors", this),
    m_isolationSelectionToolLoose ( "CP::IsolationSelectionTool", this),
    m_jetCalibrationTool ( "JetCalibrationTool", this),
    m_metutil ( "MetMaker", this),
    m_lgbm_pdfvars ( "LightGBMpredictor", this),
    m_lgbm_pdfcutvars ( "LightGBMpredictor", this),
    m_lgbm_fwd_pid ( "LightGBMpredictor", this),
    m_lgbm_fwd_er ( "LightGBMpredictor", this),
    m_lgbm_fwd_iso ( "LightGBMpredictor", this),    
    m_images ( "ImageCreation", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  declareProperty ("grlTool", m_grl);
  declareProperty ("pileupReweighting", m_PileupReweighting);
  declareProperty ("egammaCalibrationAndSmearingTool", m_electronCalibrationSmearingTool);
  declareProperty ("electronSFReco", m_electronSFReco);
  declareProperty ("electronSFID", m_electronSFID);
  declareProperty ("electronSFIDFwd", m_electronSFIDFwd);  
  declareProperty ("electronSFIso", m_electronSFIso);
  declareProperty ("electronSFECIDS", m_electronSFECIDS);
  declareProperty ("electronChargeIdSF", m_electronChargeIdSF);
  declareProperty ("MCShifterTool", m_MCShifterTool);
  declareProperty ("muonCalibrationSmearingTool", m_muonCalibrationSmearingTool);
  declareProperty ("muonSelectionToolLoose", m_muonSelectionToolLoose);
  declareProperty ("muonSF", m_muonSF);
  declareProperty ("muonSFIso", m_muonSFIso);
  declareProperty ("isolationSelectionToolLoose", m_isolationSelectionToolLoose);
  declareProperty ("jetCalibrationTool", m_jetCalibrationTool);
  declareProperty ("metutil", m_metutil);
  declareProperty ("lgbmPdfVar", m_lgbm_pdfvars);
  declareProperty ("lgbmPdfCutVar", m_lgbm_pdfcutvars);
  declareProperty ("lgbmFwdPid", m_lgbm_fwd_pid);
  declareProperty ("lgbmFwdEr", m_lgbm_fwd_er);
  declareProperty ("lgbmFwdIso", m_lgbm_fwd_iso);  
  declareProperty ("imageCreation", m_images);
  
}


StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.



  ANA_MSG_DEBUG("::initialize: 4x::enableFailure");
  CP::SystematicCode::enableFailure();
  StatusCode::enableFailure();
  xAOD::TReturnCode::enableFailure();
  CP::CorrectionCode::enableFailure();

  ANA_CHECK( requestFileExecute() );
  ANA_MSG_DEBUG("::initialize: xAOD::TFileAccessTracer::enableDataSubmission(false);");
  xAOD::TFileAccessTracer::enableDataSubmission(false);

  // Setup electron multiplicity cutflow histograms
  m_CutFlows["Init"] = new multiplicityCutFlow;
  m_CutFlows["OR"] = new multiplicityCutFlow;
  m_CutFlows["Pt"] = new multiplicityCutFlow;
  m_CutFlows["Eta"] = new multiplicityCutFlow;
  m_CutFlows["OQ"] = new multiplicityCutFlow;
  m_CutFlows["Vtx"] = new multiplicityCutFlow;
  m_CutFlows["z0"] = new multiplicityCutFlow;
  m_CutFlows["d0"] = new multiplicityCutFlow;
  m_CutFlows["Iso"] = new multiplicityCutFlow;
  m_CutFlows["ECIDS"] = new multiplicityCutFlow;
  m_CutFlows["LHMedium"] = new multiplicityCutFlow;
 

 
  // Initialize cut flow histograms
  ANA_CHECK( createCutFlowHists(m_CutFlows) );
  ANA_CHECK( createCutFlowHists(m_FwdCutFlows) );  
  ANA_CHECK( createCutFlowHists(m_MuonCutFlows) );

  // Initialize histograms for electron kinematics, with and without Iso/ECIDS/ID selection
  ANA_CHECK( book( TH1F ( "h_eta", "h_eta", 50, -2.47, 2.47) ) );
  ANA_CHECK( book( TH1F ( "h_phi", "h_phi", 50, -3.15, 3.15) ) );
  ANA_CHECK( book( TH1F ( "h_et", "h_et", 50, 10, 200) ) );
  ANA_CHECK( book( TH1F ( "h_eta_LHMedium", "h_eta_LHMedium", 50, -2.47, 2.47) ) );
  ANA_CHECK( book( TH1F ( "h_phi_LHMedium", "h_phi_LHMedium", 50, -3.15, 3.15) ) );
  ANA_CHECK( book( TH1F ( "h_et_LHMedium", "h_et_LHMedium", 50, 10, 200) ) ); 


 
  // Initialize histograms for number of leptons saved and the sum of weights histogram
  ANA_CHECK( book( TH1I ( "h_nElectrons", "h_nElectrons", 5, 0, 5) ) );

  ANA_CHECK( book( TH1F ( "sumOfWeights", "sumOfWeights", 6, 0, 6) ) );
  
  // Cutflow histograms for electrons
  ANA_CHECK( book( TH1I ( "h_cutFlow", "h_cutFlow", 11, 0, 11) ) );
  ANA_CHECK( book( TH1I ( "h_indivCutFlow", "h_indivCutFlow", 11, 0, 11) ) );
  ANA_CHECK( book( TH1I ( "h_truthCutFlow", "h_truthCutFlow", 11, 0, 11) ) );
  ANA_CHECK( book( TH1I ( "h_indivTruthCutFlow", "h_indivTruthCutFlow", 11, 0, 11) ) ); 


  // Set the bin labels to the corresponding selection for electron cutflows
  for ( const auto& hist : {hist( "h_cutFlow" ), hist( "h_indivCutFlow" ), hist( "h_truthCutFlow" ), hist( "h_indivTruthCutFlow" )}){
      hist->GetXaxis()->SetBinLabel(1, "Init");
      hist->GetXaxis()->SetBinLabel(2, "OR");
      hist->GetXaxis()->SetBinLabel(3, "Pt");
      hist->GetXaxis()->SetBinLabel(4, "Eta");
      hist->GetXaxis()->SetBinLabel(5, "OQ");
      hist->GetXaxis()->SetBinLabel(6, "Vtx");
      hist->GetXaxis()->SetBinLabel(7, "z0");
      hist->GetXaxis()->SetBinLabel(8, "d0Sig");
      hist->GetXaxis()->SetBinLabel(8, "Iso");
      hist->GetXaxis()->SetBinLabel(9, "ECIDS");
      hist->GetXaxis()->SetBinLabel(10, "LHMedium");
  }

  
  // Initialize output tree
  ANA_CHECK( book( TTree ("analysis", "My analysis ntuple") ) );
  TTree* m_myTree = tree ("analysis");
  m_myTree->SetAutoFlush(500);
  
  // Event level variables
  m_myTree->Branch( "runNumber", &event_runNumber );
  m_myTree->Branch( "evtNumber", &event_evtNumber );
  m_myTree->Branch( "MCEventWeight", &event_MCWeight ); 
  m_myTree->Branch( "correctedScaledAverageMu", &event_correctedScaledAverageMu );
  m_myTree->Branch( "correctedScaledActualMu", &event_correctedScaledActualMu );
  m_myTree->Branch( "crossSectionFactor", &event_crossSection );
  m_myTree->Branch( "NvtxReco", &NvtxReco );
  m_myTree->Branch( "EventTrigger", &trig );
  m_myTree->Branch( "eventWeight", &eventWeight );
  m_myTree->Branch( "event_SFTrigger", &event_SFTrigger );
  m_myTree->Branch( "mcChannelNumber", &event_mcChannelNumber );
  m_myTree->Branch( "nElectrons", &nElectrons );
  m_myTree->Branch( "met_met", &met_met );
  m_myTree->Branch( "met_phi", &met_phi ); 
  
  // Electron ID outputs
  m_myTree->Branch( "pdf_score", &pdf_score );
  m_myTree->Branch( "pdfcut_score", &pdfcut_score ); 
  m_myTree->Branch( "LHLoose", &LHLoose );
  m_myTree->Branch( "LHMedium", &LHMedium );
  m_myTree->Branch( "LHTight", &LHTight );
  
  // Electron truth information
  m_myTree->Branch( "IFFTruth", &IFFTruth );
  m_myTree->Branch( "truthPdgId", &truthPdgId );
  m_myTree->Branch( "truthType", &truthType );
  m_myTree->Branch( "truthOrigin", &truthOrigin );
  m_myTree->Branch( "firstEgMotherTruthType", &firstEgMotherTruthType );
  m_myTree->Branch( "firstEgMotherTruthOrigin", &firstEgMotherTruthOrigin );
  m_myTree->Branch( "firstEgMotherPdgId", &firstEgMotherPdgId );
  m_myTree->Branch( "truthParticle", &truthParticle );
  m_myTree->Branch( "truth_eta", &truth_eta );
  m_myTree->Branch( "truth_phi", &truth_phi );
  m_myTree->Branch( "truth_E", &truth_E ); 
  
  // Electron kinematics
  m_myTree->Branch( "eta", &eta );
  m_myTree->Branch( "phi", &phi );
  m_myTree->Branch( "pt", &pt );
  m_myTree->Branch( "et", &et );
  m_myTree->Branch( "charge", &charge );
  
  // Electron ECIDS
  m_myTree->Branch( "ecids", &ecids );
  m_myTree->Branch( "ecidsValue", &ecidsValue );

  // Electron scale factors
  m_myTree->Branch( "recoSF", &recoSF );
  m_myTree->Branch( "idSF", &idSF );
  m_myTree->Branch( "isoSF", &isoSF );
  m_myTree->Branch( "ecidsSF", &ecidsSF );
  m_myTree->Branch( "chargeSF", &chargeSF );
  m_myTree->Branch( "combSF", &combSF );

  // Trigger information
  m_myTree->Branch( "trigger", &trigger );
 
  
  //============================================================================
  // Retrieving Tools
  //============================================================================
  ANA_MSG_DEBUG("::initialize: Retrieving Tools");
  
  // GRL tool
  ANA_CHECK( m_grl.retrieve() );

  // PRW ( change appropiately to year )
  ANA_CHECK( m_PileupReweighting.retrieve() );

  // Initialize and configure trigger tools
  ANA_CHECK ( m_trigConfig.initialize() );
  ANA_CHECK ( m_trigDecTool.setProperty( "ConfigTool", m_trigConfig.getHandle() ) ); // connect the TrigDecTool to the ConfigTool
  ANA_CHECK ( m_trigDecTool.setProperty( "TrigDecisionKey", "xTrigDecision") );
  ANA_CHECK ( m_trigDecTool.initialize() );
  
  // Trigger matching tool
  ANA_CHECK ( m_triggerMatching.initialize() );

  // Loose Isolation selection tool
  ANA_CHECK( m_isolationSelectionToolLoose.retrieve() );

   // Egamma shower shape fudge tool
  ANA_CHECK( m_MCShifterTool.retrieve() );
  
  // Electron calibration and smearing tool
  ANA_CHECK( m_electronCalibrationSmearingTool.retrieve() );

  // Electron reconstruction scale factor tool ( one is needed for all different tpyes(Reco,ID,...) )
  ANA_CHECK( m_electronSFReco.retrieve() );
  ANA_CHECK( m_electronSFID.retrieve() );
  ANA_CHECK( m_electronSFIso.retrieve() );
  ANA_CHECK( m_electronSFECIDS.retrieve() );
  ANA_CHECK( m_electronChargeIdSF.retrieve() );
          
  std::vector < std::string > corrFileEff = {"ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiency.e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.2018.root"} ;
  ANA_CHECK( m_effTrig.setProperty("CorrectionFileNameList", corrFileEff ) );
  ANA_CHECK( m_effTrig.initialize() );
  m_electronEffTrigger.push_back( m_effTrig.getHandle() );
  
  std::vector < std::string > corrFileSF = {"ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/efficiencySF.e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolGradient.2018.root"} ; 
  ANA_CHECK( m_sfTrig.setProperty("CorrectionFileNameList", corrFileSF ) );
  ANA_CHECK( m_sfTrig.initialize() );
  m_electronSFTrigger.push_back( m_sfTrig.getHandle() );
  
  // Muon calibration and smearing tool
  ANA_CHECK( m_muonCalibrationSmearingTool.retrieve() );
  
  // Muon selection tool (loose)
  ANA_CHECK( m_muonSelectionToolLoose.retrieve() );
  
  // Muon ID scale factor tool
  ANA_CHECK( m_muonSF.retrieve() );
  
  // Muon Iso scale factor tool
  ANA_CHECK( m_muonSFIso.retrieve() );    

  // Jet Calibration
  ANA_CHECK( m_jetCalibrationTool.retrieve() );

  // met maker
  ANA_CHECK( m_metutil.retrieve() );

  // Trigger ScaleFactors
  m_trigScaleFactors = new TrigGlobalEfficiencyCorrectionTool("trigScaleFactors");
  ANA_CHECK( m_trigScaleFactors->setProperty("ElectronEfficiencyTools", m_electronEffTrigger ) );
  ANA_CHECK( m_trigScaleFactors->setProperty("ElectronScaleFactorTools", m_electronSFTrigger ) );
  ANA_CHECK( m_trigScaleFactors->setProperty("TriggerCombination2018", "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0") );
  ANA_CHECK( m_trigScaleFactors->initialize() );

  // LGBM
  // ID for central electrons
  ANA_CHECK( m_lgbm_pdfvars.retrieve() );
  ANA_CHECK( m_lgbm_pdfcutvars.retrieve() );
 
  // ID/ER for fwd electrons
  ANA_CHECK( m_lgbm_fwd_pid.retrieve() );
  ANA_CHECK( m_lgbm_fwd_er.retrieve() );
  ANA_CHECK( m_lgbm_fwd_iso.retrieve() );
    
  // Initialize OverlapRemovalTool
  m_orFlags = new ORUtils::ORFlags("OverlapRemovalTool", "selected", "overlaps");
  m_orFlags->doElectrons = true;
  m_orFlags->doMuons = true;
  m_orFlags->doJets = true;
  m_orFlags->doTaus = false;
  m_orFlags->doPhotons = false;
  m_orFlags->doFatJets = false;
  m_orFlags->doEleEleOR = true;
  m_orToolbox = new ORUtils::ToolBox();
  ANA_CHECK( ORUtils::recommendedTools(*m_orFlags, *m_orToolbox) );
  ANA_CHECK( m_orToolbox->eleEleORT.setProperty("UseClusterMatch", true) );
  ANA_CHECK( m_orToolbox->setGlobalProperty("OutputLevel", 3) );
  ANA_CHECK( m_orToolbox->initialize() );
  m_orTool = m_orToolbox->masterTool;

  // Images
  ANA_CHECK( m_images.retrieve() );

  // IFFTruthClassifier
  m_iff = new IFFTruthClassifier("IFFTruth");
  ANA_CHECK( m_iff->initialize() );

  m_eventCounter = 0;
  Info( "initialize()", "Initialization done" ); 
  return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis :: fileExecute ()
{
    TString dirname = gSystem->Getenv("WorkDir_DIR"); 

        
    const xAOD::FileMetaData* fmld = new xAOD::FileMetaData();
    float mcProcID = -999;
    if( inputMetaStore()->retrieve(fmld, "FileMetaData").isSuccess()  ) {
        fmld->value( xAOD::FileMetaData::mcProcID, mcProcID);   
    }

    event_isMC = mcProcID > 1;
    Info( "fileExecute()", "mcProcID: %f", mcProcID );
    Info( "fileExecute()", "File is MC: %d", event_isMC);

    if ( event_isMC ){
        //check if file is from a DxAOD
        const std::string dummy = "StreamAOD";
        bool m_isDerivation = !( inputMetaStore()->contains<std::string>( dummy ) );
    
        if(m_isDerivation ){
            const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
            if(!inputMetaStore()->retrieve(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
                Error("initializeEvent()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
                return StatusCode::FAILURE;
            }
            // if ( incompleteCBC->size() != 0 ) {
            //   Error("initializeEvent()","Found incomplete Bookkeepers! Check file for corruption.");
            //   return StatusCode::FAILURE;
            // }
            // Now, let's find the actual information
            const xAOD::CutBookkeeperContainer* completeCBC = 0;
            if(!inputMetaStore()->retrieve(completeCBC, "CutBookkeepers").isSuccess()){
                Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
                return StatusCode::FAILURE;
            }
    
            // First, let's find the smallest cycle number,
            // i.e., the original first processing step/cycle
            int minCycle = 10000;
            for ( auto cbk : *completeCBC ) {
                if ( ! cbk->name().empty()  && minCycle > cbk->cycle() ){ minCycle = cbk->cycle(); }
            }
            // Now, let's actually find the right one that contains all the needed info...
            const xAOD::CutBookkeeper* allEventsCBK=0;
            const xAOD::CutBookkeeper* DxAODEventsCBK=0;
            std::string derivationName = "STDM4Kernel"; //need to replace by appropriate name
            int maxCycle = -1;
            for (const auto& cbk: *completeCBC) {
                if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
                    allEventsCBK = cbk;
                    maxCycle = cbk->cycle();
                }
                if ( cbk->name() == derivationName){
                    DxAODEventsCBK = cbk;
                }
            }
            uint64_t nEventsProcessed  = allEventsCBK->nAcceptedEvents() + hist("sumOfWeights")->GetBinContent(1);
            double sumOfWeights        = allEventsCBK->sumOfEventWeights() + hist("sumOfWeights")->GetBinContent(2);
            double sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared() + hist("sumOfWeights")->GetBinContent(3);
    
            uint64_t nEventsDxAOD           = DxAODEventsCBK->nAcceptedEvents() + hist("sumOfWeights")->GetBinContent(4);
            double sumOfWeightsDxAOD        = DxAODEventsCBK->sumOfEventWeights() + hist("sumOfWeights")->GetBinContent(5);
            double sumOfWeightsSquaredDxAOD = DxAODEventsCBK->sumOfEventWeightsSquared() + hist("sumOfWeights")->GetBinContent(6);
    
            hist("sumOfWeights")->SetBinContent(1,nEventsProcessed);
            hist("sumOfWeights")->SetBinContent(2,sumOfWeights);
            hist("sumOfWeights")->SetBinContent(3,sumOfWeightsSquared);
            hist("sumOfWeights")->SetBinContent(4,nEventsDxAOD);
            hist("sumOfWeights")->SetBinContent(5,sumOfWeightsDxAOD);
            hist("sumOfWeights")->SetBinContent(6,sumOfWeightsSquaredDxAOD);
    
        }

        //Example of loading in the crossSections into a map
        TTree cross_t; cross_t.ReadFile(dirname + "/data/MyAnalysis/my.metadata.txt"); 
        uint32_t mcchannel=0; double crossSection=0; double kFactor = 0; double filterEff = 0;
        cross_t.SetBranchAddress("dataset_number",&mcchannel);
        cross_t.SetBranchAddress("crossSection",&crossSection);
        cross_t.SetBranchAddress("kFactor",&kFactor);
        cross_t.SetBranchAddress("genFiltEff",&filterEff);
        for(int i=0;i<cross_t.GetEntries();i++) {
            cross_t.GetEntry(i);
            m_crossSections[mcchannel] = crossSection;
            m_kFactors[mcchannel] = kFactor;
            m_filterEffs[mcchannel] = filterEff;
        }
    }

    return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.


  // Print processing rate at set interval, so we know where we are
  ++m_eventCounter;
  if ( m_eventCounter == 1) m_startTime = time(0);
  if ( m_eventCounter % m_progressInterval == 0 ) {
  	Info("execute()","%li events processed so far  <<<==", m_eventCounter );
	Info("execute()","Processing rate = %f Hz", float(m_eventCounter)/float(time(0)-m_startTime));
  }
  ANA_MSG_DEBUG("::execute: Event " << m_eventCounter);

  // retrieve the eventInfo object from the event store
  ANA_CHECK (evtStore()->retrieve (m_eventInfo, "EventInfo"));

  //============================================================================
  // Event cleaning
  //============================================================================
  ANA_MSG_DEBUG("::execute: Event Cleaning");

  // detector errors
  if (
  ( m_eventInfo->errorState( xAOD::EventInfo::LAr )  == xAOD::EventInfo::Error )
  || ( m_eventInfo->errorState( xAOD::EventInfo::Tile ) == xAOD::EventInfo::Error )
  || ( m_eventInfo->errorState( xAOD::EventInfo::SCT )  == xAOD::EventInfo::Error )
  || ( m_eventInfo->isEventFlagBitSet( xAOD::EventInfo::Core, 18 ) )
  )
  return StatusCode::SUCCESS;
  
  // if data check if event passes GRL
  if (!event_isMC) { 
      if (!m_grl->passRunLB(*m_eventInfo)) {
          return StatusCode::SUCCESS; 
      }
  } 

  // Jet cleaning
  if ( m_eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad") == 0 ){
      return StatusCode::SUCCESS;
  }


  //============================================================================
  // Retrieving containers
  //============================================================================
  ANA_MSG_DEBUG("::execute: Retrieving Containers");
  // retrieve electron container 
  ANA_MSG_DEBUG("::execute: Retrieving electrons...");
  const xAOD::ElectronContainer* electrons_org = 0;
  ANA_CHECK( evtStore()->retrieve( electrons_org, "Electrons" ) );
  std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > electrons_shallowCopy = xAOD::shallowCopyContainer( *electrons_org );
  m_electrons = electrons_shallowCopy.first;
  
  // retrieve vertices
  ANA_MSG_DEBUG("::execute: Retrieving PrimaryVertices...");
  const xAOD::VertexContainer* vertex_org = 0;
  ANA_CHECK( evtStore()->retrieve( vertex_org, "PrimaryVertices") );
  std::pair< xAOD::VertexContainer*, xAOD::ShallowAuxContainer* > vertex_shallowCopy = xAOD::shallowCopyContainer( *vertex_org );
  m_vertex = vertex_shallowCopy.first;
  std::unique_ptr< xAOD::VertexContainer > vertices_p( vertex_shallowCopy.first );
  std::unique_ptr< xAOD::ShallowAuxContainer > vertices_p2( vertex_shallowCopy.second );
  
  // retrieve Tracks
  ANA_MSG_DEBUG("::execute: Retrieving tracks...");
  const xAOD::TrackParticleContainer* indet_tracks_org = 0;
  ANA_CHECK( evtStore()->retrieve( indet_tracks_org, "InDetTrackParticles" ) );
  std::pair< xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer* > indet_tracks_shallowCopy = xAOD::shallowCopyContainer( *indet_tracks_org );
  m_indetTracks.reset( indet_tracks_shallowCopy.first );
  m_indetTracksAux.reset( indet_tracks_shallowCopy.second );
  
  // retrieve muon container 
  ANA_MSG_DEBUG("::execute: Retrieving muons...");
  const xAOD::MuonContainer* muons_org = 0;
  ANA_CHECK( evtStore()->retrieve( muons_org, "Muons" ) );
  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons_org );
  m_muons = muons_shallowCopy.first;
  
  // retrieve jet container
  ANA_MSG_DEBUG("::execute: Retrieving AntiKt4EMPFlowJets...");
  const xAOD::JetContainer* jets_org = 0;
  ANA_CHECK( evtStore()->retrieve( jets_org, "AntiKt4EMPFlowJets" ) );
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets_org );
  m_jets = jets_shallowCopy.first;
  
  if ( event_isMC ){
      // ANA_MSG_DEBUG("::execute: Retrieving egammaTruthParticles...");
      // ANA_CHECK( evtStore()->retrieve( m_egammaTruthContainer, "egammaTruthParticles" ) );
      
      ANA_MSG_DEBUG("::execute: Retrieving TruthParticles...");      
      ANA_CHECK( evtStore()->retrieve( m_truthParticles, "TruthParticles" ) );
  }
  
  // Missing et stuff
  met::addGhostMuonsToJets(*muons_org, *m_jets);
  
  m_primaryVertex = nullptr;
  for ( const auto& vtx : *m_vertex ){
      if ( vtx->vertexType() == xAOD::VxType::PriVtx) {
          m_primaryVertex = vtx;
      }
  }
  if ( m_primaryVertex == nullptr ) {
      return StatusCode::SUCCESS;
  }

  //============================================================================
  // Calibrations and Corrections
  //============================================================================
  ANA_MSG_DEBUG("::execute: Calibrations and Corrections");
  for ( const auto& el : *m_electrons ) {
      if ( event_isMC ) {
          // Apply fudge factors to electrons
          if ( m_MCShifterTool->applyCorrection( *el ) != CP::CorrectionCode::Ok ) {
              ANA_MSG_WARNING( "execute(): Electron correction failed on an object.");
          }
      }
      // apply energy corrections to electrons
      if( m_electronCalibrationSmearingTool->applyCorrection( *el ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Electron calibration failed" );
  }
 
  for ( const auto& mu : *m_muons ) {
      // apply energy corrections to muons
      if( m_muonCalibrationSmearingTool->applyCorrection( *mu ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Muon calibration failed" );
  }
  
  
  for ( const auto& jet : *m_jets ) {
      // apply energy corrections to jets
      if( m_jetCalibrationTool->applyCorrection( *jet) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Jet calibration failed" );
  }
  
  
  ANA_MSG_DEBUG("::execute: Retrieving MET_Core_AntiKt4EMPFlow...");
  ANA_CHECK( evtStore()->retrieve(m_coreMet, "MET_Core_AntiKt4EMPFlow") );
  
  // needed for MET_Maker
  if(!xAOD::setOriginalObjectLink(*jets_org, *m_jets)){//tell calib container what old container it matches
      std::cout << "Failed to set the original object links" << std::endl;
      return 1;
  }
  if(!xAOD::setOriginalObjectLink(*electrons_org, *m_electrons)){//tell calib container what old container it matches
      std::cout << "Failed to set the original object links" << std::endl;
      return 1;
  }
  if(!xAOD::setOriginalObjectLink(*muons_org, *m_muons)){//tell calib container what old container it matches
      std::cout << "Failed to set the original object links" << std::endl;
      return 1;
  }
  
  // Select objects which should be used in the overlap removal
  const ort::inputDecorator_t selectDec("selected");
  for ( auto el : *m_electrons ){
      if ( el->pt() > 10000 && abs( el->caloCluster()->etaBE(2) ) < 2.47){
          selectDec(*el) = true;
      }
      else{
          selectDec(*el) = false;
      }
  }
  
  for ( auto mu : *m_muons ){
      if ( mu->pt() > 10000 && abs( mu->eta() ) < 2.7 ){
          selectDec(*mu) = true;
      }
      else{
          selectDec(*mu) = false;
      }
  }  
  
  for ( auto jet : *m_jets ){
      if ( jet->pt() > 20000 && abs( jet->eta() ) < 2.7 ){
          selectDec(*jet) = true;
      }
      else{
          selectDec(*jet) = false;
      }
  }  
  
  // record the calibrated objects and there shallow copies ( needed by MET_Maker )
  ANA_CHECK( evtStore()->record(m_jets, "CalibJets") );
  ANA_CHECK( evtStore()->record(jets_shallowCopy.second, "CalibJetsAux") );
  ANA_CHECK( evtStore()->record(m_muons, "CalibMuons") );
  ANA_CHECK( evtStore()->record(muons_shallowCopy.second, "CalibMuonsAux") );  
  ANA_CHECK( evtStore()->record(m_electrons, "CalibElectrons") );
  ANA_CHECK( evtStore()->record(electrons_shallowCopy.second, "CalibElectronsAux") );
  
  
  //retrieve the MET association map
  ANA_CHECK( evtStore()->retrieve(m_metMap, "METAssoc_AntiKt4EMPFlow") );
  m_metMap->resetObjSelectionFlags();
  
  
  // Create a MissingETContainer with its aux store for each systematic
  m_newMetContainer    = new xAOD::MissingETContainer();
  m_newMetAuxContainer = new xAOD::MissingETAuxContainer();
  m_newMetContainer->setStore(m_newMetAuxContainer);
  ANA_CHECK( evtStore()->record(m_newMetContainer, "newMETContainer") );
  ANA_CHECK( evtStore()->record(m_newMetAuxContainer, "newMETContainerAux") );  
  
  m_metMap->resetObjSelectionFlags();
  
  //Electrons
  ConstDataVector<xAOD::ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);
  for(const auto& el : *m_electrons) {
      if(el->pt()>10e3 && el->eta()<2.47 && Accessor_LHLoose( *el ) ) metElectrons.push_back(el);
  }
  ANA_CHECK( m_metutil->rebuildMET("RefEle",                   //name of metElectrons in metContainer
                          xAOD::Type::Electron,       //telling the rebuilder that this is electron met
                          m_newMetContainer,            //filling this met container
                          metElectrons.asDataVector(),//using these metElectrons that accepted our cuts
                          m_metMap)                     //and this association map
                          );
  
  //Muons
  ConstDataVector<xAOD::MuonContainer> metMuons(SG::VIEW_ELEMENTS);
  for(const auto& mu : *m_muons) {
      if(mu->pt()>10e3 && mu->eta()<2.7 && m_muonSelectionToolLoose->accept( *mu ) ) metMuons.push_back(mu);
  }
  
  ANA_CHECK( m_metutil->rebuildMET("RefMuon",
                                  xAOD::Type::Muon,
                                  m_newMetContainer,
                                  metMuons.asDataVector(),
                                  m_metMap)
                                  );
 
  //Now time to rebuild jetMet and get the soft term
  //This adds the necessary soft term for both CST and TST
  //these functions create an xAODMissingET object with the given names inside the container
  ANA_CHECK( m_metutil->rebuildJetMET("RefJet",        //name of jet met
                                 "SoftClus",      //name of soft cluster term met
                                 "PVSoftTrk",     //name of soft track term met
                                 m_newMetContainer, //adding to this new met container
                                 m_jets,          //using this jet collection to calculate jet met
                                 m_coreMet,         //core met container
                                 m_metMap,          //with this association map
                                 false            //don't apply jet jvt cut
                                 )
         );
  
  ANA_CHECK( m_metutil->buildMETSum("FinalClus", m_newMetContainer, MissingETBase::Source::LCTopo) );
  m_METmaker = ( *m_newMetContainer )["FinalClus"];
  
  
  
  // Get missing energy
  met_met = m_METmaker->met();
  met_phi = m_METmaker->phi();    
  
  // Do overlap removal
  ANA_CHECK ( m_orTool->removeOverlaps( m_electrons, m_muons, m_jets ) ); 
  
  // Event specific variables
  if ( event_isMC ){
      event_mcChannelNumber = m_eventInfo->mcChannelNumber();
      event_MCWeight = m_eventInfo->mcEventWeight();
      event_crossSection = m_crossSections[event_mcChannelNumber] * m_kFactors[event_mcChannelNumber] * m_filterEffs[event_mcChannelNumber] ;
  }
  event_evtNumber = m_eventInfo->eventNumber();
  event_runNumber = m_eventInfo->runNumber();
  ANA_CHECK(m_PileupReweighting->apply(*m_eventInfo));
  event_pileupweight = m_eventInfo->auxdecor<float>("PileupWeight");
  
  event_correctedScaledAverageMu       = m_PileupReweighting->getCorrectedAverageInteractionsPerCrossing( *m_eventInfo, true );
  event_correctedScaledActualMu        = m_PileupReweighting->getCorrectedActualInteractionsPerCrossing( *m_eventInfo, true );
  
  NvtxReco = m_vertex->size();
  
  // Clear the electron, fwd electron, and muon vectors
  clearVectors();
  
  
  // Do preselection of electrons, fwd electrons, and muons
  std::vector < const xAOD::Electron* > selElectrons = selectElectronCandidates();
  
  
  nElectrons = selElectrons.size();
  
  // if enough particles left, save information
  if ( ( nElectrons >= 2 ) ){ 
      // get trigger SF for event
      event_SFTrigger = 1.;
      std::vector<const xAOD::IParticle*> trigElectrons;
      if ( event_isMC ){
          for ( const xAOD::Electron* el : selElectrons ){
              trigElectrons.push_back( el );
          }       
          if( !m_trigScaleFactors->getEfficiencyScaleFactor(trigElectrons, event_SFTrigger)){
                                ATH_MSG_WARNING( "Couldn't get trigger scale factor!" );
                            } 
      }

      eventWeight = event_pileupweight;
      if ( event_isMC ){
          eventWeight = event_MCWeight * event_crossSection * event_pileupweight * event_SFTrigger;
      }

      // fill histograms with number of particles
      hist("h_nElectrons")->Fill( nElectrons, eventWeight );      
    
      
    // Fill the electrons vectors
    for ( const xAOD::Electron* el : selElectrons ){
        const TLorentzVector& el_p4 = el->p4();
        const xAOD::TrackParticle* tt = el->trackParticle();
        // const xAOD::CaloCluster* cluster = el->caloCluster();
        
        // ID information
        LHLoose.push_back( Accessor_LHLoose( *el ) );
        LHMedium.push_back( Accessor_LHMedium( *el ) );
        LHTight.push_back( Accessor_LHTight( *el ) );
        
        // Truth information
        if ( event_isMC ){
            const xAOD::TruthParticle* tp = xAOD::TruthHelpers::getTruthParticle( *el );

            IFFTruth.push_back( static_cast<std::underlying_type_t<IFF::Type>>( m_iff->classify( *el ) ) );
            if ( tp == nullptr ){
                truthPdgId.push_back( 0 );
                truth_eta.push_back( -999 );
                truth_phi.push_back( -999 );
                truth_E.push_back( 0 );                
            }
            else{
                truthPdgId.push_back( tp->pdgId() );
                truth_eta.push_back( tp->eta() );
                truth_phi.push_back( tp->phi() );
                truth_E.push_back( tp->e() / 1000. );
            }
            truthType.push_back( Accessor_truthType( *el ) );
            truthOrigin.push_back( Accessor_truthOrigin( *el ) );
            firstEgMotherTruthType.push_back( Accessor_firstEgMotherTruthType( *el ) );
            firstEgMotherTruthOrigin.push_back( Accessor_firstEgMotherTruthOrigin( *el ) );
            firstEgMotherPdgId.push_back( Accessor_firstEgMotherPdgId( *el ) );
            
            bool truthEgParticle = false;
            for ( size_t i = 0; i<TruthPointerIdx.size(); i++){
                if ( el->index() == TruthPointerIdx[i] ) truthEgParticle = true;
            }
            truthParticle.push_back( truthEgParticle );
        }
        
        // kinematics
        eta.push_back( el->caloCluster()->etaBE(2) );
        phi.push_back( el_p4.Phi() );
        pt.push_back( tt->pt() / 1000. );
        et.push_back( el_p4.Pt() / 1000. );
        charge.push_back( el->charge() );
        
        // // ecids
        // ecids.push_back( Accessor_ECIDS( *el ) );
        // ecidsValue.push_back( Accessor_ECIDSResult( *el ) );
        
        // Triger information
        std::vector<const xAOD::IParticle*> myParticles;
        myParticles.push_back( el );
        trigger.push_back( m_triggerMatching->match( myParticles, "HLT_e26_lhtight_nod0_ivarloose", 0.07, false ) ||
                            m_triggerMatching->match( myParticles, "HLT_e60_lhmedium_nod0", 0.07, false ) ||  // included for scale factors
                            m_triggerMatching->match( myParticles, "HLT_e140_lhloose_nod0", 0.07, false ) );  // included for scale factors
        


        
        // Scale factors
        double recoSf = 1;
        double IdSf = 1;
        double IsoSf = 1;
        double ECIDSSf = 1;
        double charge_sf = 1;
        if ( event_isMC ){
            if(!m_electronSFReco->getEfficiencyScaleFactor( *el, recoSf)){
                ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
            }
            if(!m_electronSFID->getEfficiencyScaleFactor( *el, IdSf)){
                ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
            }
            if(!m_electronSFIso->getEfficiencyScaleFactor( *el, IsoSf)){
                ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
            }
            if(!m_electronSFECIDS->getEfficiencyScaleFactor( *el, ECIDSSf)){
                ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
            }                                                 
            if(!m_electronChargeIdSF->getEfficiencyScaleFactor( *el, charge_sf)){
                ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
            }
        }

        recoSF.push_back( recoSf );
        idSF.push_back( IdSf );
        isoSF.push_back( IsoSf );
        ecidsSF.push_back( ECIDSSf );
        chargeSF.push_back( charge_sf );

        combSF.push_back( recoSf * IdSf * IsoSf * ECIDSSf * charge_sf  );
    
        // Isolation variables
        GradientIso.push_back( m_isolationSelectionToolLoose->accept( *el ) );

        
        // Fill kinematics histograms
        if ( event_isMC ){
            hist( "h_eta" )->Fill( el->caloCluster()->etaBE(2), eventWeight * combSF.back() );
            hist( "h_phi" )->Fill( el->phi(), eventWeight * combSF.back() );
            hist( "h_et" )->Fill( el_p4.Pt() / 1000., eventWeight * combSF.back() );
        }
        else{
            hist( "h_eta" )->Fill( el->caloCluster()->etaBE(2) );
            hist( "h_phi" )->Fill( el_p4.Phi() );
            hist( "h_et" )->Fill( el_p4.Pt() / 1000. );    
        }
    }
    

    // write everything into the tree
    tree ("analysis")->Fill();
  }
  

  
  return StatusCode::SUCCESS;
}

// Function to calculat delta R between two particles
float MyxAODAnalysis::deltaR( const float eta1, const float phi1, const float eta2, const float phi2 ) {
	const float deta = fabs(eta1 - eta2);

	float dphi;
	if ( fabs(phi1 - phi2 - 2.0*3.14159265359) < fabs(phi1 - phi2) ) {
		dphi = fabs(phi1 - phi2 - 2.0*3.14159265359) ;
	} else {
		dphi = fabs(phi1 - phi2);
	}

	return sqrt( deta*deta + dphi*dphi );
}

// Clear the vectors before filling the information of the new event
void MyxAODAnalysis::clearVectors(){
    LHLoose.clear();
    LHMedium.clear();
    LHTight.clear();    
    IFFTruth.clear();
    truthPdgId.clear();
    truthType.clear();
    truthOrigin.clear();
    firstEgMotherTruthType.clear();
    firstEgMotherTruthOrigin.clear();
    firstEgMotherPdgId.clear();
    eta.clear();
    phi.clear();
    pt.clear();
    et.clear();
    ecids.clear();

    trigger.clear();
    charge.clear();
    
    truthParticle.clear();

    truth_eta.clear();
    truth_phi.clear();
    truth_E.clear();
    
    recoSF.clear();
    idSF.clear();
    isoSF.clear();
    ecidsSF.clear();
    chargeSF.clear();
    combSF.clear();
    
    TruthPointerIdx.clear();
    TruthPointerIdx_fwd.clear();

    
    GradientIso.clear();

    

}



void MyxAODAnalysis::resetCutFlow( std::map< TString, multiplicityCutFlow* > map ){
    for ( auto& cutFlow : map ){
        cutFlow.second->indiv = 0;
        cutFlow.second->acc = 0;
        cutFlow.second->truthIndiv = 0;
        cutFlow.second->truthAcc = 0;        
    }
}


StatusCode MyxAODAnalysis::createCutFlowHists( std::map<TString, multiplicityCutFlow* > map ){
    for ( const auto& string : map ){
        ANA_CHECK( book( TH1D ( "h_acc_" + string.first, "h_acc_" + string.first, 20, 0, 20) ) ) ;
        ANA_CHECK( book( TH1D ( "h_indiv_" + string.first, "h_indiv_" + string.first, 20, 0, 20) ) ) ;
        ANA_CHECK( book( TH1D ( "h_truthAcc_" + string.first, "h_truthAcc_" + string.first, 20, 0, 20) ) ) ;
        ANA_CHECK( book( TH1D ( "h_truthIndiv_" + string.first, "h_truthIndiv_" + string.first, 20, 0, 20) ) ) ;                        
    }
    
    
    
    return StatusCode::SUCCESS;
}

void MyxAODAnalysis::fillCutFlowHists( std::map<TString, multiplicityCutFlow* > map ){
    for ( const auto& string : map ){
        std::string type = string.first.Data();
        hist( "h_indiv_" + type )->Fill(string.second->indiv);
        hist( "h_acc_" + type )->Fill(string.second->acc);
        hist( "h_truthIndiv_" + type )->Fill(string.second->truthIndiv);
        hist( "h_truthAcc_" + type )->Fill(string.second->truthAcc);
    }    
}

void MyxAODAnalysis::increaseCutFlow( multiplicityCutFlow* cutFlow, bool pass, bool &passAll, bool truth_el, int idx, int electron){
    if ( electron == 0 ){
        if ( pass ) {
            cutFlow->indiv++;
            hist( "h_indivCutFlow" )->Fill(idx);
        }
        passAll = pass && passAll; 
        if ( passAll ) {
            cutFlow->acc++;
            hist( "h_cutFlow" )->Fill(idx);
            
        }
        if ( truth_el ){
            if ( pass ) {
                cutFlow->truthIndiv++;
                hist( "h_indivTruthCutFlow" )->Fill(idx);
            }
            if ( passAll ) {
                cutFlow->truthAcc++;        
                hist( "h_truthCutFlow" )->Fill(idx);
            }
        }
    }
    
    return;
}




std::vector < const xAOD::Electron* > MyxAODAnalysis::selectElectronCandidates(){
    std::vector < const xAOD::Electron* > electrons_out  ;
    
    resetCutFlow( m_CutFlows );
    
    // TruthPointerIdx.clear();
    // if ( event_isMC ){
    //     for ( const xAOD::TruthParticle* tp : *m_egammaTruthContainer ){
    //         if ( xAOD::EgammaHelpers::getRecoElectron( tp ) != nullptr){
    //             if ( xAOD::EgammaHelpers::getRecoElectron( tp )->author( xAOD::EgammaParameters::AuthorFwdElectron ) == false ){
    //                 TruthPointerIdx.push_back(xAOD::EgammaHelpers::getRecoElectron( tp )->index());
    //             }
    //         }    
    //     }
    // }
        
    for ( const xAOD::Electron* el : *m_electrons ){
    
        
        bool truth_el = false;
        // if ( event_isMC ){
        //     for ( size_t i = 0; i<TruthPointerIdx.size(); i++){
        //         if ( el->index() == TruthPointerIdx[i] ) truth_el = true;
        //     }
        // }

        bool passAll = true;
        increaseCutFlow( m_CutFlows["Init"], true, passAll, truth_el, 0, 0);


        // Overlap removal
        const ort::inputAccessor_t selectAcc("overlaps");
        increaseCutFlow( m_CutFlows["OR"], !selectAcc( *el ), passAll, truth_el, 1, 0);


        // transverse momentum cut
        increaseCutFlow( m_CutFlows["Pt"], el->pt() > 10e3, passAll, truth_el, 2, 0);


        // Eta cut
        increaseCutFlow( m_CutFlows["Eta"], abs( el->caloCluster()->etaBE(2) ) < 2.47, passAll, truth_el, 3, 0);


        // Object Quality
        increaseCutFlow( m_CutFlows["OQ"], el->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON), passAll, truth_el, 4, 0);


        // has vertex
        const xAOD::TrackParticle* tt = el->trackParticle();
        increaseCutFlow( m_CutFlows["Vtx"], tt->vertex() != nullptr, passAll, truth_el, 5, 0);


        // z0 theta 
        if ( tt->vertex() == nullptr ){
            increaseCutFlow( m_CutFlows["z0"], false , passAll, truth_el, 6, 0);
        }
        else{
            increaseCutFlow( m_CutFlows["z0"], abs( ( tt->z0() + tt->vz() - tt->vertex()->z() ) * sin(tt->theta())) < 0.5 , passAll, truth_el, 6, 0);
        }


        // d0 Sig
        increaseCutFlow( m_CutFlows["d0"], abs( tt->d0() / std::sqrt( tt->definingParametersCovMatrixVec()[0]) ) < 5, passAll, truth_el, 7, 0);


        // Medium Likelihood 
        increaseCutFlow( m_CutFlows["LHMedium"], bool(!Accessor_LHMedium( *el )), passAll, truth_el, 8, 0);

        if ( passAll ) electrons_out.push_back( el );

        // Isolation
        increaseCutFlow( m_CutFlows["Iso"], m_isolationSelectionToolLoose->accept( *el ), passAll, truth_el, 9, 0);
        
        // ECIDS 
        // increaseCutFlow( m_CutFlows["ECIDS"], Accessor_ECIDS( *el ), passAll, truth_el, 10, 0);


        
        

    }
    
    fillCutFlowHists(m_CutFlows);

    
    return electrons_out;
}



MyxAODAnalysis :: ~MyxAODAnalysis () {

}


StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}




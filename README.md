# NTUPLE PRODUCTION
How to setup and run on hep.


## 1) setupATLAS and setup rucio when you have user certificate

 
 - ssh to hep
 - Make a folder called .globus
 - Copy from own computer:  scp myCertificate.p12 xxx@hep03.hpc.ku.dk:~/.globus/
 - cd .globus and run:
    - openssl pkcs12 -nocerts -in myCertficate.p12 -out userkey.pem
    - openssl pkcs12 -nocerts -in myCertificate.p12 -out userkey.pem
    - openssl pkcs12 -clcerts -nokeys -in myCertificate.p12 -out usercert.pem
    - chmod g-r,o-r user*
- cd .. 
- cd .ssh: 
- ls -lh 
- if "id_rsa.pub" file does not exist:
    - ssh-keygen
    - cat id_rsa.pub 
	

Copy this to https://gitlab.cern.ch/profile/keys and make a new ssh-key here


- nano .bash_profile
    - Inset the following to the bottom of the file:
    - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    - alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
- Run the same two lines in the terminal, then ATLAS is both setup now, and when you start the terminal up again
- run setupATLAS
- run lsetup git
- run lsetup rucio
- run voms-proxy-init -voms atlas
    - If this returns an error: "...Cannot authenticate to account xxx with given credentials"
    - run export RUCIO_ACCOUNT=**** your cern username here ****
    - And try to re-run lsetup rucio + voms-proxy-init -voms atlas
 - run rucio whoami
    - If this works, you are good to go!




## 1) Setup

Inspired by: https://gitlab.cern.ch/EgammaMachineLearning/NtupleProd

Run the command:
  
-  setupATLAS && lsetup git
-  cd *The folder the folder should be in*
-  git clone ssh://git@gitlab.cern.ch:7999/disciples-of-troels/ntupleproduction.git
-  cd ntupleproduction
-  cd source
-  git clone ssh://git@gitlab.cern.ch:7999/ATLAS-IFF/IFFTruthClassifier.git
-  remove the build folder (rm -rf build)
-  create a new build folder (mkdir build)
-  cd ../build
-  acmSetup AnalysisBase,21.2.118 # Newest is fine, unless you need a specific version
-  acm compile 
-  acm compile # yes, run twice
-  cd ../run
-  mkdir outputDir outputDir_grid

The setup is now complete. If you get any error, please see the common errors in the bottom of this file.

## 2) Running Ntuple production

Create smaller root files (Ntuples) only containing the variables chosen.
The Ntuples are flattened so they can be converted to hdf5.
For every event electrons, photons, forward electron and muons are saved, depending on the defined filetype.

### Setup NtupleProd in a fresh hep terminal:
- setupATLAS: Setup Atlas
- lsetup rucio: Setup rucio
- cd work/ntupleproduction/build: Enter the build folder 
- acmSetup: Setup acm - On every login
- acm compile: Compile cxx file

If you change anything in the source folder, you re-compile by:
- acm compile: Compile cxx file

If you add any files to the source folder, you have to "find the files" and recompile using:
- acm find_packages: Find all files, and make sure they are updated.
- acm compile:	Compile cxx file


### Prepare production:
#### In folder run:
- dataset.txt  : The names of the containers (eg. AOD/EGAMx/MUON) containing the files to be converted to Ntuples. Each line that is not commented out are run. The number in front of the container name determines the maximal number of files to run. Put '-1' in front to run all files in container.
- runAnalysis.sh : search_dir must be the directory where the containers are located.

#### In folder source/MyAnalysis/Root:
- MyxAODAnalysis.cxx : The hardcoded string filetype = "filetype" (e.g. filetype = "EGAM1") must be set.

#### In folder source/MyAnalysis/share:
- ATestRun_eljob.py : Following files must fit your root file:
    -  "PRWConfigFiles": Placed in source/MyAnalysis/data. If new files are added you need to run "$ acm find_packages", since the program reads the files from bulid/x86_64-centos7-gcc8-opt/data/MyAnalysis/. Use rucio to find NTUP_PILEUP file with an AMI tag that matches your root files AMI tag.
    -  "listOfLumicalcFiles": You might need to change this file if the program crashes.
    -  "GRLFilePath": You might need to change this file if the program crashes.


### Run production (you have to be in the folder "run")
  
- run acm compile: compile the scripts
- ( run acm find_packages): if you have added new files
- rm -r outputDir/xxx*
- ./runAnalysis.sh : Add arguments if necessary. Eg. -e for number of events. 	(-e only works locally)
- less +F xxx.txt: see how far it is

#### Shortcut in production - testing setup
Add following line to bashrc (or your shortcut file). Then you are able to run "runAnalysis_test" to test your setup.
``` shell
alias runAnalysis_test='rm -r outputDir/*; ./runAnalysis.sh -e 100; sleep 2; less +F outputDir/*.txt'
```
```diff
--Running this shortcut you should not have any other files in outputDir--
```
### Output
Enter the folder: outputDir/
- "filename".txt  : Log file
- "filename"/data-ANALYSIS/"filename".root :Output Ntuple file



## Common Error in runAnalysis
###### Wrong Pillup file
If you get the error below probably a wrong Pillup file - Change PRWConfigFiles
```
CP::TPileupReweighting... WARNING Channel 366145 has no configuration for period 300000 -- 
this is symptomatic of you trying to reweight an MC campaign to data not intended for that campaign (e.g. MC16a with 2017 data)

```

###### LightGBM giving issues when setting up the ntuple production

We are not using LightGBM, so this can be removed. Do the following:
- Remove the folder "LightGBM" next to "MyAnalysis”
- Go to the CMakeLists.txt file in "MyAnalysis” and remove the three times "LightGBM..." is mentioned
- Go to the CMakeLists.txt file in the folder "source" and do the same
- Compile by "acm compile" in the build folder



#!/usr/bin/env bash



###########################################################################################
#   Variables modified by command line options
###########################################################################################

# number of events to run over locally (-1 = all)
events_max=-1

# text file containing list of datasets and analysis types
fileoverdatasets="dataset.txt"

# directory with datasets
# search_dir="/lustre/hpc/hep/mdv971/storage/dataHZgamma/MC_HZllgam"
# search_dir="/lustre/hpc/hep/mdv971/storage/dataHZgamma/MC_modelFebruary2020"
# search_dir="/lustre/hpc/hep/mdv971/storage/dataHZgamma/MC_modelFebruary2020_GridRunErrorTest"
# search_dir="/lustre/hpc/hep/malteal/storage/Master/raw_data/Zmumugam"
# search_dir="/lustre/hpc/hep/mdv971/storage/dataMuon"
search_dir="/lustre/hpc/hep/sda/storage/MUON1_files"

# output directory
output="outputDir"

output_grid="outputDir_grid"

# Ntuple version
output_version=0

# maximal number of processes to run at the same time
proc_max=5

# grid run
grid_run=false

# default text for help screen
run_grid_txt="default local"
if [ "$run_grid" = true ]; then run_grid_txt="this is the default behavior"; fi


# help screen
USAGE=$(cat <<-END
Usage: $(basename $0) [OPTION]...

$dry_run_txt0 the DAOD->ntuple EventLoop framework.${dry_run_txt1}
Fails if the output directory has any output from a dataset, unless -m/--missing-only is set.
Dataset names are re-used for the outgoing names with the _EXT0 ending changed to _NTUP#, where # is the analysisType #.

Mandatory arguments to long options are mandatory for short options too.
  -e, --max-events=N            if running locally, change the maximum events to N with no effect on
                                grid and -1 being all (default $events_max)
  -h, --help                    display this help and exit
  -l, --dataset-list=LIST       set dataset list to LIST, a text file containing a list of datasets and analyis type(s);
                                use the form "nfiles dsname 1 2" for dataset "dsname" running over analysis types
                                1 and 2 with nfiles number of files to be used when running on the grid;
                                comments only on start of line with '#' character
                                (default $fileoverdatasets)
  -s, --search-dir=DIR          set dataset directory to DIR; datasets must be directories containing .root
                                files and all directories must exist even if running on grid
                                (default $search_dir)
  -g, --grid-run                execute on grid instead of locally ($run_grid_txt)
  -v, --version=XX              specify ntuple version
  -p, --max-processes=N         change the number of concurrent executions to N (default $proc_max)

END
)

# parse options
OPTS=`getopt -o e:l:s:hgv:p: --long max-events:,dataset-list:,search-dir:,grid-run,help,version:,max-processes: -n "$(basename $0)" -- "$@"`
if [ $? != 0 ]; then echo "Try \`$(basename $0) --help' for more information."; exit 2; fi
# echo "$OPTS"
eval set -- "$OPTS"

while true; do
  case "$1" in
    -h | --help )          echo "$USAGE"; exit 0; shift ;;
    -e | --max-events )    events_max="$2"; shift 2 ;;
    -l | --dataset-list )  fileoverdatasets="$2"; shift 2 ;;
    -s | --search-dir )    search_dir="$2"; shift 2 ;;
    -g | --grid-run )      grid_run=true; shift ;;
    -v | --version )       output_version="$2"; shift 2 ;;
    -p | --max-processes ) proc_max="$2"; shift 2 ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

function prepTest {
	fail=false
	printf "[ \e[37m....\e[0m ] $1... "
}

# if needing several writes to the same line
# accepts formatted strings (second argument is the variable list)
function updateTest {
	printf "\r\033[2K[ \e[37m....\e[0m ] $1... " $2
}

function failTest {
	if [ $fail = false ]; then
		printf "$1"
		echo -e "\r[\e[91mFAILED\e[0m]"
	fi
	fail=true
}

function finishTest {
	if [ $fail = true ]; then
		echo -e "\nExiting."
		exit_good=true
		exit 1
	fi
	echo -e "\r[  \e[32mOK\e[0m  ]"
}

# just an info line
function printInfo {
	if [ $# != 1 ]; then
		echo "printInfo must only have ONE parameter (use quotation marks). Your first parameter was: $1"
		echo "Killing script."
		exit 1
	fi
	echo -e "\r[ \e[96mINFO\e[0m ] $1"
}

# if dry-running, echo the commands that would be executed
function runCMD {
    if [ $# != 1 ]; then
        echo "runCMD must only have ONE parameter (use parentheses). First parameter is: $1"
        echo "Killing script."
        exit 1
    fi
    eval $1
}

# even if not dry-running, echo the commands that execute
function runCMDVerbose {
    echo -e "\r[\e[93mVERBOS\e[0m] Executing: $1"
    runCMD "$@"
}

# removes unnecessary stuff from ds names
function trimFat {
    name=$1
    # double underscore names first
    name=${name/Pythia8B_A14_CTEQ6L1_/}
    name=${name/PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_/}
    name=${name/PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_/}
    name=${name/PowhegPythia8EvtGen_NNLOPS_nnlo_/}
    name=${name/Sherpa_221_NNPDF30NNLO/}
    name=${name/Sherpa_222_NNPDF30NNLO/}
    # then single underscore names
    name=${name/Sherpa_CT10_/}
    name=${name/PowhegPythia8EvtGen_AZNLOCTEQ6L1_/}
    name=${name/Pythia8EvtGen_A14NNPDF23LO_/}
    name=${name/PhPy8EG_A14_/}
    name=${name/PowhegPythia8EvtGen_A14_/}
    echo $name
}

# This makes the output name based on dataset and analysis type
function makeOutputDSName {
    if [ $# != 1 ]; then
        echo "makeOutputDSName must have EXACTLY TWO parameters (use parentheses). First parameter is: $1"
        echo "Killing script."
        exit 1
    fi
    datasettrimmed=$(trimFat $1)
    ds="${datasettrimmed/%_EXT0/_NTUP}_v${output_version}"
    # if input is made from a user, change user to this user
    ds=$(echo "$ds" | sed -E 's/^user.[^.]+\.//')
    ds="user.$RUCIO_ACCOUNT.$ds"
    echo $ds
}

###########################################################################################
#   Pre-run checks
###########################################################################################
prepTest "checking parameters"
if [ "$grid_run" = true ] && [ "$events_max" -gt -1 ]; then
	failTest "You cannot set number of events when running on grid"
fi
finishTest

# load datasets
declare -A datasets
declare -A datasetnfiles

while read -r line; do
    # skip lines that start with # (comments) and empty (at most only whitespace) lines
    if [ "${line::1}" = "#" ] || [[ "$line" =~ ^[[:space:]]*$ ]]; then
        continue
    fi
    # die if a comment characeter is found later
    if [[ "$line" =~ "#" ]]; then
        failTest "Bad line. Comments may only be at the start of a line:"
        echo "$line"
        break
    fi
    # must contain at least two spaces
    res="${line//[^ ]}"
    if [[ ${#res} -lt 1 ]]; then
        failTest "Bad line. Line must contain at least ONE spaces:"
        echo "$line"
        break
    fi

    nfiles="${line%% *}" # everything until first space
    ds="${line#* }" # everything after first space is the new line

    datasets["${ds}"]="${ds}"
    datasetnfiles["${ds}"]="${nfiles}"

done < $fileoverdatasets


pids=()
for dataset in "${!datasets[@]}"; do
    # wait if maximum number of pid has been reached
    while [[ ${#pids[@]} -ge $proc_max ]]; do
        # check all processes
        a_pid_finished=false
        for p in "${pids[@]}"; do
            # if this processes has ended, remove it from the list
            if ! kill -0 $p &> /dev/null; then
                # pids=(12 1 11 2 3)
                # add spaces on either side since we match the element WITH spaces to ensure that we don't match parts of a pid
                pids=" ${pids[@]} "
                pids="${pids/ $p / }"
                pids=($pids)
                # echo ${pids[@]}
                a_pid_finished=true
                break
            fi
        done
        if $a_pid_finished; then break; fi
        sleep 2
    done

    # specific job settings
    ds=$(makeOutputDSName $dataset)

    if [ "$grid_run" = false ]; then
        runCMDVerbose "nohup ATestRun_eljob.py -e $events_max -f $search_dir/$dataset/ -s $output/$ds/ -g 0 -o $ds &> $output/$ds.txt & disown"
        pid=$!
    else
        n_files=${datasetnfiles[$dataset]}
        runCMDVerbose "nohup ATestRun_eljob.py -f $dataset/ -s $output_grid/$ds/ -g 1 -n $n_files -o $ds &> $output_grid/$ds.txt & disown"
        pid=$!
    fi

    pids+=($pid)
done

# Install script for directory: /lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/InstallArea/x86_64-centos7-gcc8-opt")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/src/MyAnalysis" TYPE DIRECTORY FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/" USE_SOURCE_PERMISSIONS REGEX "/\\.svn$" EXCLUDE REGEX "/\\.git$" EXCLUDE REGEX "/[^/]*\\~$" EXCLUDE)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process( COMMAND ${CMAKE_COMMAND}
      -E make_directory
      $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/include )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process( COMMAND ${CMAKE_COMMAND}
         -E create_symlink ../src/MyAnalysis/MyAnalysis
         $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/include/MyAnalysis )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/lustre/hpc/hep/sda/work/ntupleproduction/build/x86_64-centos7-gcc8-opt/lib/libMyAnalysisLib.so.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY OPTIONAL FILES "/lustre/hpc/hep/sda/work/ntupleproduction/build/x86_64-centos7-gcc8-opt/lib/libMyAnalysisLib.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libMyAnalysisLib.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libMyAnalysisLib.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/sw/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libMyAnalysisLib.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/lustre/hpc/hep/sda/work/ntupleproduction/build/x86_64-centos7-gcc8-opt/lib/libMyAnalysisDict_rdict.pcm")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDebugx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/lustre/hpc/hep/sda/work/ntupleproduction/build/x86_64-centos7-gcc8-opt/lib/libMyAnalysisDict.so.dbg")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY OPTIONAL FILES "/lustre/hpc/hep/sda/work/ntupleproduction/build/x86_64-centos7-gcc8-opt/lib/libMyAnalysisDict.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libMyAnalysisDict.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libMyAnalysisDict.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/cvmfs/atlas.cern.ch/repo/sw/software/21.2/sw/lcg/releases/binutils/2.30-e5b21/x86_64-centos7/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libMyAnalysisDict.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "NTUP_PILEUP.12911415._000001.pool.root.1" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/NTUP_PILEUP.12911415._000001.pool.root.1")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "NTUP_PILEUP.12911423._000001.pool.root.1" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/NTUP_PILEUP.12911423._000001.pool.root.1")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "NTUP_PILEUP.12973660._000001.pool.root.1" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/NTUP_PILEUP.12973660._000001.pool.root.1")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "NTUP_PILEUP.12974783._000001.pool.root.1" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/NTUP_PILEUP.12974783._000001.pool.root.1")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "NTUP_PILEUP.13182843._000001.pool.root.1" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/NTUP_PILEUP.13182843._000001.pool.root.1")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "NTUP_PILEUP.13182880._000001.pool.root.1" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/NTUP_PILEUP.13182880._000001.pool.root.1")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "NTUP_PILEUP.13182909._000001.pool.root.1" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/NTUP_PILEUP.13182909._000001.pool.root.1")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "NTUP_PILEUP.14204493._000004.pool.root.1" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/NTUP_PILEUP.14204493._000004.pool.root.1")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "NTUP_PILEUP.17252366._000001.pool.root.1" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/NTUP_PILEUP.17252366._000001.pool.root.1")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "NTUP_PILEUP.18373650._000003.pool.root.1" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/NTUP_PILEUP.18373650._000003.pool.root.1")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "Zee_pileup_merge.root" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/Zee_pileup_merge.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "Zmumu_pileup_merge.root" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/Zmumu_pileup_merge.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "combined_prw.root" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/combined_prw.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "combined_prw_2017.root" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/combined_prw_2017.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "my.metadata.txt" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/my.metadata.txt")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/data/MyAnalysis" TYPE FILE RENAME "ttbar_pileup_merge.root" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/data/ttbar_pileup_merge.root")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM RENAME "._ATestRun_eljob.py" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/share/._ATestRun_eljob.py")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xMainx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE PROGRAM RENAME "ATestRun_eljob.py" FILES "/lustre/hpc/hep/sda/work/ntupleproduction/source/MyAnalysis/share/ATestRun_eljob.py")
endif()


// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME IFFTruthClassifierReflexDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/lustre/hpc/hep/sda/work/ntupleproduction/source/IFFTruthClassifier/IFFTruthClassifier/IFFTruthClassifierDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *IFFTruthClassifier_Dictionary();
   static void IFFTruthClassifier_TClassManip(TClass*);
   static void delete_IFFTruthClassifier(void *p);
   static void deleteArray_IFFTruthClassifier(void *p);
   static void destruct_IFFTruthClassifier(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::IFFTruthClassifier*)
   {
      ::IFFTruthClassifier *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::IFFTruthClassifier));
      static ::ROOT::TGenericClassInfo 
         instance("IFFTruthClassifier", "IFFTruthClassifier.h", 18,
                  typeid(::IFFTruthClassifier), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &IFFTruthClassifier_Dictionary, isa_proxy, 4,
                  sizeof(::IFFTruthClassifier) );
      instance.SetDelete(&delete_IFFTruthClassifier);
      instance.SetDeleteArray(&deleteArray_IFFTruthClassifier);
      instance.SetDestructor(&destruct_IFFTruthClassifier);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::IFFTruthClassifier*)
   {
      return GenerateInitInstanceLocal((::IFFTruthClassifier*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::IFFTruthClassifier*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *IFFTruthClassifier_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::IFFTruthClassifier*)0x0)->GetClass();
      IFFTruthClassifier_TClassManip(theClass);
   return theClass;
   }

   static void IFFTruthClassifier_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrapper around operator delete
   static void delete_IFFTruthClassifier(void *p) {
      delete ((::IFFTruthClassifier*)p);
   }
   static void deleteArray_IFFTruthClassifier(void *p) {
      delete [] ((::IFFTruthClassifier*)p);
   }
   static void destruct_IFFTruthClassifier(void *p) {
      typedef ::IFFTruthClassifier current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::IFFTruthClassifier

namespace {
  void TriggerDictionaryInitialization_libIFFTruthClassifier_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libIFFTruthClassifier dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$IFFTruthClassifier/IFFTruthClassifier.h")))  IFFTruthClassifier;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libIFFTruthClassifier dictionary payload"

#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "IFFTruthClassifier-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ IFFTruthClassifier-00-00-00
#endif
#ifndef EIGEN_DONT_VECTORIZE
  #define EIGEN_DONT_VECTORIZE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#ifndef IFFTRUTHCLASSIFIER_IFFTRUTHCLASSIFIERDICT_H_
#define IFFTRUTHCLASSIFIER_IFFTRUTHCLASSIFIERDICT_H_

#include <IFFTruthClassifier/IFFTruthClassifier.h>

#endif  // IFFTRUTHCLASSIFIER_IFFTRUTHCLASSIFIERDICT_H_

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"IFFTruthClassifier", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libIFFTruthClassifier",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libIFFTruthClassifier_Impl, {{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1},{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1}}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libIFFTruthClassifier_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libIFFTruthClassifier() {
  TriggerDictionaryInitialization_libIFFTruthClassifier_Impl();
}

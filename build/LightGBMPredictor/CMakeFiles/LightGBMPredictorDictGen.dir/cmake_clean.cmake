file(REMOVE_RECURSE
  "../x86_64-centos7-gcc8-opt/include/LightGBMPredictor"
  "../x86_64-centos7-gcc8-opt/data/LightGBMPredictor/.gitignore"
  "CMakeFiles/LightGBMPredictorDictGen"
  "CMakeFiles/LightGBMPredictor.dsomap"
  "CMakeFiles/LightGBMPredictorReflexDict.cxx"
  "../x86_64-centos7-gcc8-opt/lib/libLightGBMPredictor_rdict.pcm"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/LightGBMPredictorDictGen.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()

#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "WorkDir::IFFTruthClassifierLib" for configuration "RelWithDebInfo"
set_property(TARGET WorkDir::IFFTruthClassifierLib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(WorkDir::IFFTruthClassifierLib PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libIFFTruthClassifierLib.so"
  IMPORTED_SONAME_RELWITHDEBINFO "libIFFTruthClassifierLib.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS WorkDir::IFFTruthClassifierLib )
list(APPEND _IMPORT_CHECK_FILES_FOR_WorkDir::IFFTruthClassifierLib "${_IMPORT_PREFIX}/lib/libIFFTruthClassifierLib.so" )

# Import target "WorkDir::testIFFClassifier" for configuration "RelWithDebInfo"
set_property(TARGET WorkDir::testIFFClassifier APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(WorkDir::testIFFClassifier PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/testIFFClassifier"
  )

list(APPEND _IMPORT_CHECK_TARGETS WorkDir::testIFFClassifier )
list(APPEND _IMPORT_CHECK_FILES_FOR_WorkDir::testIFFClassifier "${_IMPORT_PREFIX}/bin/testIFFClassifier" )

# Import target "WorkDir::testRecoTruth" for configuration "RelWithDebInfo"
set_property(TARGET WorkDir::testRecoTruth APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(WorkDir::testRecoTruth PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/testRecoTruth"
  )

list(APPEND _IMPORT_CHECK_TARGETS WorkDir::testRecoTruth )
list(APPEND _IMPORT_CHECK_FILES_FOR_WorkDir::testRecoTruth "${_IMPORT_PREFIX}/bin/testRecoTruth" )

# Import target "WorkDir::ImageCreationLib" for configuration "RelWithDebInfo"
set_property(TARGET WorkDir::ImageCreationLib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(WorkDir::ImageCreationLib PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libImageCreationLib.so"
  IMPORTED_SONAME_RELWITHDEBINFO "libImageCreationLib.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS WorkDir::ImageCreationLib )
list(APPEND _IMPORT_CHECK_FILES_FOR_WorkDir::ImageCreationLib "${_IMPORT_PREFIX}/lib/libImageCreationLib.so" )

# Import target "WorkDir::LightGBMPredictorLib" for configuration "RelWithDebInfo"
set_property(TARGET WorkDir::LightGBMPredictorLib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(WorkDir::LightGBMPredictorLib PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libLightGBMPredictorLib.so"
  IMPORTED_SONAME_RELWITHDEBINFO "libLightGBMPredictorLib.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS WorkDir::LightGBMPredictorLib )
list(APPEND _IMPORT_CHECK_FILES_FOR_WorkDir::LightGBMPredictorLib "${_IMPORT_PREFIX}/lib/libLightGBMPredictorLib.so" )

# Import target "WorkDir::MyAnalysisLib" for configuration "RelWithDebInfo"
set_property(TARGET WorkDir::MyAnalysisLib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(WorkDir::MyAnalysisLib PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELWITHDEBINFO "WorkDir::LightGBMPredictorLib;WorkDir::ImageCreationLib;WorkDir::IFFTruthClassifierLib"
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libMyAnalysisLib.so"
  IMPORTED_SONAME_RELWITHDEBINFO "libMyAnalysisLib.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS WorkDir::MyAnalysisLib )
list(APPEND _IMPORT_CHECK_FILES_FOR_WorkDir::MyAnalysisLib "${_IMPORT_PREFIX}/lib/libMyAnalysisLib.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)

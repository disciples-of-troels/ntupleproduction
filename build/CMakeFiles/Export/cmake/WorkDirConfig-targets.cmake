# Generated by CMake

if("${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}" LESS 2.5)
   message(FATAL_ERROR "CMake >= 2.6.0 required")
endif()
cmake_policy(PUSH)
cmake_policy(VERSION 2.6)
#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Protect against multiple inclusion, which would fail when already imported targets are added once more.
set(_targetsDefined)
set(_targetsNotDefined)
set(_expectedTargets)
foreach(_expectedTarget WorkDir::IFFTruthClassifierPkg WorkDir::IFFTruthClassifierPkgPrivate WorkDir::IFFTruthClassifierLib WorkDir::testIFFClassifier WorkDir::testRecoTruth WorkDir::ImageCreationPkg WorkDir::ImageCreationPkgPrivate WorkDir::ImageCreationLib WorkDir::LightGBMPredictorPkg WorkDir::LightGBMPredictorPkgPrivate WorkDir::LightGBMPredictorLib WorkDir::MyAnalysisPkg WorkDir::MyAnalysisPkgPrivate WorkDir::MyAnalysisLib)
  list(APPEND _expectedTargets ${_expectedTarget})
  if(NOT TARGET ${_expectedTarget})
    list(APPEND _targetsNotDefined ${_expectedTarget})
  endif()
  if(TARGET ${_expectedTarget})
    list(APPEND _targetsDefined ${_expectedTarget})
  endif()
endforeach()
if("${_targetsDefined}" STREQUAL "${_expectedTargets}")
  unset(_targetsDefined)
  unset(_targetsNotDefined)
  unset(_expectedTargets)
  set(CMAKE_IMPORT_FILE_VERSION)
  cmake_policy(POP)
  return()
endif()
if(NOT "${_targetsDefined}" STREQUAL "")
  message(FATAL_ERROR "Some (but not all) targets in this export set were already defined.\nTargets Defined: ${_targetsDefined}\nTargets not yet defined: ${_targetsNotDefined}\n")
endif()
unset(_targetsDefined)
unset(_targetsNotDefined)
unset(_expectedTargets)


# Compute the installation prefix relative to this file.
get_filename_component(_IMPORT_PREFIX "${CMAKE_CURRENT_LIST_FILE}" PATH)
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH)
if(_IMPORT_PREFIX STREQUAL "/")
  set(_IMPORT_PREFIX "")
endif()

# Create imported target WorkDir::IFFTruthClassifierPkg
add_library(WorkDir::IFFTruthClassifierPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::IFFTruthClassifierPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/IFFTruthClassifier"
  INTERFACE_LINK_LIBRARIES "MCTruthClassifierPkg;MuonAnalysisInterfacesPkg;xAODMuonPkg;xAODEgammaPkg;xAODJetPkg;xAODTruthPkg;xAODEventInfoPkg;xAODTrackingPkg"
)

# Create imported target WorkDir::IFFTruthClassifierPkgPrivate
add_library(WorkDir::IFFTruthClassifierPkgPrivate INTERFACE IMPORTED)

# Create imported target WorkDir::IFFTruthClassifierLib
add_library(WorkDir::IFFTruthClassifierLib SHARED IMPORTED)

set_target_properties(WorkDir::IFFTruthClassifierLib PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::IFFTruthClassifierPkg,INTERFACE_INCLUDE_DIRECTORIES>"
  INTERFACE_LINK_LIBRARIES "AsgTools;xAODEgamma;xAODMuon;xAODJet;xAODEventInfo"
)

# Create imported target WorkDir::testIFFClassifier
add_executable(WorkDir::testIFFClassifier IMPORTED)

# Create imported target WorkDir::testRecoTruth
add_executable(WorkDir::testRecoTruth IMPORTED)

# Create imported target WorkDir::ImageCreationPkg
add_library(WorkDir::ImageCreationPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::ImageCreationPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/ImageCreation"
)

# Create imported target WorkDir::ImageCreationPkgPrivate
add_library(WorkDir::ImageCreationPkgPrivate INTERFACE IMPORTED)

# Create imported target WorkDir::ImageCreationLib
add_library(WorkDir::ImageCreationLib SHARED IMPORTED)

set_target_properties(WorkDir::ImageCreationLib PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::ImageCreationPkg,INTERFACE_INCLUDE_DIRECTORIES>"
  INTERFACE_LINK_LIBRARIES "AsgTools;xAODEgamma"
)

# Create imported target WorkDir::LightGBMPredictorPkg
add_library(WorkDir::LightGBMPredictorPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::LightGBMPredictorPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/LightGBMPredictor"
)

# Create imported target WorkDir::LightGBMPredictorPkgPrivate
add_library(WorkDir::LightGBMPredictorPkgPrivate INTERFACE IMPORTED)

# Create imported target WorkDir::LightGBMPredictorLib
add_library(WorkDir::LightGBMPredictorLib SHARED IMPORTED)

set_target_properties(WorkDir::LightGBMPredictorLib PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::LightGBMPredictorPkg,INTERFACE_INCLUDE_DIRECTORIES>;${_IMPORT_PREFIX}/include"
  INTERFACE_LINK_LIBRARIES "AsgTools;xAODEgamma;xAODTracking;/lustre/hpc/hep/sda/work/ntupleproduction/build/CMakeFiles/_lightgbmBuild/lib/lib_lightgbm.so"
  INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "include"
)

# Create imported target WorkDir::MyAnalysisPkg
add_library(WorkDir::MyAnalysisPkg INTERFACE IMPORTED)

set_target_properties(WorkDir::MyAnalysisPkg PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/src/MyAnalysis"
)

# Create imported target WorkDir::MyAnalysisPkgPrivate
add_library(WorkDir::MyAnalysisPkgPrivate INTERFACE IMPORTED)

# Create imported target WorkDir::MyAnalysisLib
add_library(WorkDir::MyAnalysisLib SHARED IMPORTED)

set_target_properties(WorkDir::MyAnalysisLib PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "\$<TARGET_PROPERTY:WorkDir::MyAnalysisPkg,INTERFACE_INCLUDE_DIRECTORIES>"
  INTERFACE_LINK_LIBRARIES "AnaAlgorithmLib;xAODEventInfo;xAODEgamma;xAODCore;xAODTracking;xAODMuon;xAODJet;xAODMissingET;xAODTruth;xAODBase;xAODCaloEvent;xAODMetaData;xAODCutFlow;EgammaAnalysisInterfacesLib;ElectronPhotonSelectorToolsLib;ElectronPhotonFourMomentumCorrectionLib;ElectronPhotonShowerShapeFudgeToolLib;ElectronEfficiencyCorrectionLib;PhotonEfficiencyCorrectionLib;IsolationSelectionLib;MuonEfficiencyCorrectionsLib;MuonMomentumCorrectionsLib;MuonSelectorToolsLib;JetCalibToolsLib;JetSelectorToolsLib;METInterface;METUtilitiesLib;TrigConfxAODLib;TrigDecisionToolLib;TriggerMatchingToolLib;TrigGlobalEfficiencyCorrectionLib;AssociationUtilsLib"
)

if(CMAKE_VERSION VERSION_LESS 3.0.0)
  message(FATAL_ERROR "This file relies on consumers using CMake 3.0.0 or greater.")
endif()

# Load information for each installed configuration.
get_filename_component(_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
file(GLOB CONFIG_FILES "${_DIR}/WorkDirConfig-targets-*.cmake")
foreach(f ${CONFIG_FILES})
  include(${f})
endforeach()

# Cleanup temporary variables.
set(_IMPORT_PREFIX)

# Loop over all imported files and verify that they actually exist
foreach(target ${_IMPORT_CHECK_TARGETS} )
  foreach(file ${_IMPORT_CHECK_FILES_FOR_${target}} )
    if(NOT EXISTS "${file}" )
      message(FATAL_ERROR "The imported target \"${target}\" references the file
   \"${file}\"
but this file does not exist.  Possible reasons include:
* The file was deleted, renamed, or moved to another location.
* An install or uninstall procedure did not complete successfully.
* The installation package was faulty and contained
   \"${CMAKE_CURRENT_LIST_FILE}\"
but not all the files it references.
")
    endif()
  endforeach()
  unset(_IMPORT_CHECK_FILES_FOR_${target})
endforeach()
unset(_IMPORT_CHECK_TARGETS)

# This file does not depend on other imported targets which have
# been exported from the same project but in a separate export set.

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
cmake_policy(POP)

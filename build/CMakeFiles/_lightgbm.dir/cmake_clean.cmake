file(REMOVE_RECURSE
  "x86_64-centos7-gcc8-opt/setup.sh"
  "x86_64-centos7-gcc8-opt/lib/WorkDir.rootmap"
  "CMakeFiles/_lightgbm"
  "CMakeFiles/_lightgbm-complete"
  "src/_lightgbm-stamp/_lightgbm-install"
  "src/_lightgbm-stamp/_lightgbm-mkdir"
  "src/_lightgbm-stamp/_lightgbm-download"
  "src/_lightgbm-stamp/_lightgbm-update"
  "src/_lightgbm-stamp/_lightgbm-patch"
  "src/_lightgbm-stamp/_lightgbm-configure"
  "src/_lightgbm-stamp/_lightgbm-build"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/_lightgbm.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()

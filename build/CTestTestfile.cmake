# CMake generated Testfile for 
# Source directory: /lustre/hpc/hep/sda/work/ntupleproduction/source
# Build directory: /lustre/hpc/hep/sda/work/ntupleproduction/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("IFFTruthClassifier")
subdirs("ImageCreation")
subdirs("LightGBMPredictor")
subdirs("MyAnalysis")

#include <set>
#include <string>

#include "ImageCreation/ImageCreation.h"


ImageCreation::ImageCreation(const std::string& type) :
    asg::AsgTool(type),
    cell_energy("cell_energy"),
    cell_eta("cell_eta"),
    cell_deta("cell_deta"),
    cell_phi("cell_phi"),
    cell_dphi("cell_dphi"),
    cell_sampling("cell_sampling"),
    cell_time("cell_time"),
    cell_gain("cell_gain")

    {
        declareProperty("FineEta", m_fine_eta = true, "Resolution of em Images in eta 56 (true) or 7 (false)");
        declareProperty("FinePhi", m_fine_phi = false, "Resolution of em Images in phi 11 (true) or 55 (false)");
        declareProperty("Time", m_time = false, "Create time images or not");
        declareProperty("Gain", m_gain = false, "Create gain images or not");
    }

StatusCode ImageCreation::initialize(){
    ATH_MSG_INFO("Initialize ImageCreation");

    em_calo.resize( 8 );
    h_calo.resize( 8 );
    if ( m_time ) {
        time_em.resize( 8 );
        time_h.resize( 8 );
    }

    resize_images();
    set_pointers();
    sampling_layer[0] = 0; //!
    sampling_layer[1] = 1; //!
    sampling_layer[2] = 2; //!
    sampling_layer[3] = 3; //!
    sampling_layer[4] = 4; //!
    sampling_layer[5] = 5; //!
    sampling_layer[6] = 6; //!
    sampling_layer[7] = 7; //!
    sampling_layer[8] = 0; //!
    sampling_layer[9] = 1; //!
    sampling_layer[10] = 2; //!
    sampling_layer[11] = 3; //!
    sampling_layer[12] = 4; //!
    sampling_layer[13] = 5; //!
    sampling_layer[14] = 6; //!
    sampling_layer[15] = 7; //!
    sampling_layer[16] = 7; //!
    sampling_layer[17] = 7; //!
    sampling_layer[18] = 4; //!
    sampling_layer[19] = 5; //!
    sampling_layer[20] = 6; //!

    return StatusCode::SUCCESS;
}

StatusCode ImageCreation::finalize() {
  ATH_MSG_INFO("Finalize ImageCreation");
  return StatusCode::SUCCESS;
}

void ImageCreation::set_pointers(){
    em_barrel_Lr0 = &em_calo[0];
    em_barrel_Lr1 = &em_calo[1];
    em_barrel_Lr2 = &em_calo[2];
    em_barrel_Lr3 = &em_calo[3];
    em_endcap_Lr0 = &em_calo[4];
    em_endcap_Lr1 = &em_calo[5];
    em_endcap_Lr2 = &em_calo[6];
    em_endcap_Lr3 = &em_calo[7];
    lar_endcap_Lr0 = &h_calo[0];
    lar_endcap_Lr1 = &h_calo[1];
    lar_endcap_Lr2 = &h_calo[2];
    lar_endcap_Lr3 = &h_calo[3];
    tile_barrel_Lr1 = &h_calo[4];
    tile_barrel_Lr2 = &h_calo[5];
    tile_barrel_Lr3 = &h_calo[6];
    tile_gap_Lr1 = &h_calo[7];

    time_em_barrel_Lr0 = &time_em[0];
    time_em_barrel_Lr1 = &time_em[1];
    time_em_barrel_Lr2 = &time_em[2];
    time_em_barrel_Lr3 = &time_em[3];
    time_em_endcap_Lr0 = &time_em[4];
    time_em_endcap_Lr1 = &time_em[5];
    time_em_endcap_Lr2 = &time_em[6];
    time_em_endcap_Lr3 = &time_em[7];
    time_lar_endcap_Lr0 = &time_h[0];
    time_lar_endcap_Lr1 = &time_h[1];
    time_lar_endcap_Lr2 = &time_h[2];
    time_lar_endcap_Lr3 = &time_h[3];
    time_tile_barrel_Lr1 = &time_h[4];
    time_tile_barrel_Lr2 = &time_h[5];
    time_tile_barrel_Lr3 = &time_h[6];
    time_tile_gap_Lr1 = &time_h[7];

}


void ImageCreation::resize_images(){
    if ( m_fine_eta ){
        for ( auto& img : em_calo ) {
            img.resize(56 * 11);
        }

        if ( m_time ){
            for ( auto& img : time_em ) {
                img.resize(56 * 11);
            }
        }

    }
    else{
        for ( auto& img : em_calo ){
            img.resize(7 * 11);
        }

        if ( m_time ){
            for ( auto& img : time_em ) {
                img.resize(7 * 11);
            }

        }
    }
    // for ( auto& img : em_calo ){
    //     for ( size_t i = 0; i < img.size(); i++){
    //         if ( m_fine_phi ){
    //             img[i].resize(55);
    //         }
    //         else{
    //             img[i].resize(11);
    //         }
    //     }
    // }
    // if ( m_time ){
    //     for ( auto& img : time_em ){
    //         for ( size_t i = 0; i < img.size(); i++){
    //             if ( m_fine_phi ){
    //                 img[i].resize(55);
    //             }
    //             else{
    //                 img[i].resize(11);
    //             }
    //         }
    //     }
    // }


    for ( auto& img : h_calo ) {
        img.resize(7 * 11);
    }
    // for ( auto& img : h_calo ){
    //     for ( size_t i = 0; i < img.size(); i++){
    //         img[i].resize(11);
    //     }
    // }

    if ( m_time ){
        for ( auto& img : time_h ) {
            img.resize(7 * 11);
        }
        // for ( auto& img : time_h ){
        //     for ( size_t i = 0; i < img.size(); i++){
        //         img[i].resize(11);
        //     }
        // }
    }



    return;
}

void ImageCreation::phi_upscale(){
    // define the upscaled vectors
    std::vector<float> c_energy_up, c_eta_up, c_deta_up, c_phi_up, c_time_up;
    std::vector<int> c_gain_up, c_sampling_up;

    // loop over all the cells that are given
    for( size_t i = 0; i < cell_dphi_v.size(); i++ ){
        size_t size_cell;
        // for all ecalo layers consider upscaling
        if ( cell_sampling_v[i] < 8 ){
            // get the size of the cell based on the binning you want ( how often does the fine binned cell fit in the original cell ) and round it to integers
            if ( m_fine_phi ){
                size_cell = std::round(cell_dphi_v[i] / m_fine_phi_len);
            }
            else{
                size_cell = std::round(cell_dphi_v[i] / m_phi_len);
            }
            // loop over the number of new cells
            for (size_t j = 0; j < size_cell; j++ ){
                // the new cells should all have the same eta, deta, sampling and time value. the energy is divided by the number of cells it is split into
                c_eta_up.push_back(cell_eta_v[i]);
                c_deta_up.push_back(cell_deta_v[i]);
                c_energy_up.push_back(cell_energy_v[i] / size_cell);
                // put the phi coordinates evenly spaces around the original coordinate
                if ( m_fine_phi ){
                    c_phi_up.push_back( cell_phi_v[i] + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_fine_phi_len ) );
                }
                else{
                    c_phi_up.push_back( cell_phi_v[i] + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_phi_len ) );
                }
                c_sampling_up.push_back( cell_sampling_v[i] );

                if ( m_time ){
                    c_time_up.push_back( cell_time_v[i] );
                }
                if ( m_gain ){
                    c_gain_up.push_back( cell_gain_v[i] );
                }
            }
        }
        else{
            // loop over the number of new cells
            size_cell = std::round(cell_dphi_v[i] / m_phi_len);
            for (size_t j = 0; j < size_cell; j++ ){
                // the new cells should all have the same eta, deta, sampling and time value. the energy is divided by the number of cells it is split into
                c_eta_up.push_back(cell_eta_v[i]);
                c_deta_up.push_back(cell_deta_v[i]);
                c_energy_up.push_back(cell_energy_v[i] / size_cell);
                // put the phi coordinates evenly spaces around the original coordinate
                c_phi_up.push_back( cell_phi_v[i] + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_phi_len ) );
                c_sampling_up.push_back( cell_sampling_v[i]);

                if ( m_time ){
                    c_time_up.push_back( cell_time_v[i] );
                }
                if ( m_gain ){
                    c_gain_up.push_back( cell_gain_v[i] );
                }
            }
        }
    }

    // write upscaled vectors in the member variables
    cell_energy_v.clear();
    cell_energy_v = c_energy_up;

    cell_eta_v.clear();
    cell_eta_v = c_eta_up;

    cell_deta_v.clear();
    cell_deta_v = c_deta_up;

    cell_phi_v.clear();
    cell_phi_v = c_phi_up;

    cell_sampling_v.clear();
    cell_sampling_v = c_sampling_up;

    if ( m_time ){
        cell_time_v.clear();
        cell_time_v = c_time_up;
    }
    if ( m_gain ){
        cell_gain_v.clear();
        cell_gain_v = c_gain_up;
    }

    return ;
}


void ImageCreation::eta_upscale( double cluster_eta ){
    // define upscaled vectors
    std::vector<float> c_energy_up, c_eta_up, c_phi_up, c_time_up;
    std::vector<int> c_gain_up, c_sampling_up;

    // loop over all cells
    for( size_t i = 0; i < cell_eta_v.size(); i++ ){
        // ecalo cells are considered for the fine binning
        if( cell_sampling_v[i] < 8 ){
            // for layer 1 in the em endcap there is a region which has a resolution of 0.025/6 which is not integer divisible (is that a word??) by the fine resolution 0.025/8 therefore this has to be considered seperately
            if( cell_sampling_v[i] == 5 ){
                // do not round the cell size immediately to integers to catch the mentioned region
                double size_cell_d;
                if ( m_fine_eta ){
                    size_cell_d = cell_deta_v[i] / m_fine_eta_len;
                }
                else{
                    size_cell_d = cell_deta_v[i] / m_eta_len;
                }
                // in the mentioned region the cell size should be 4/3
                if( std::abs(size_cell_d - 1.33333) < 0.1 ){
                    // get the number of cells that are in the region in front of the considered cell (the region starts at 1.8)
                    int rounded_value = std::round( (std::abs(cell_eta_v[i] + cluster_eta) - 1.8) / m_fine_eta_len * 4. / 3. );
                    // depending on which cell was hit the energy split has to be different ( each subcell gets the energy based on the area in the eta phi space )
                    int position = rounded_value % 3;
                    if( position == 0 ){
                        // if the position is zero 3/4 of the cell belongs to the first upscaled cell and 1/4 to the second one
                        // std::signbit gives 0 for positive values and 1 for negative values
                        // the first cell is put 5/6 (1/6) to the left ( if eta runs from left to right ) of the eta coordinate for positive (negative) eta
                        c_eta_up.push_back( cell_eta_v[i] - ( 1. + 4. * std::signbit(cluster_eta) ) * m_fine_eta_len / 6. );
                        c_eta_up.push_back( cell_eta_v[i] + ( 5. - 4. * std::signbit(cluster_eta) ) * m_fine_eta_len / 6. );
                        c_energy_up.push_back( cell_energy_v[i] * ( 3. - 2. * std::signbit(cluster_eta) ) / 4.);
                        c_energy_up.push_back( cell_energy_v[i] * ( 1. + 2. * std::signbit(cluster_eta) ) / 4.);

                        if ( m_time ){
                            c_time_up.push_back( cell_time_v[i] ); // attempt to give the subcell only the time of the cell with the largest area covered
                            c_time_up.push_back( 0. );
                        }
                        if ( m_gain ){
                            c_gain_up.push_back( cell_gain_v[i] ); // attempt to give the subcell only the gain of the cell with the largest area covered
                            c_gain_up.push_back( 0. );
                        }
                    }
                    else if( position == 1 ){
                        // if the position is one both cells have 1/2
                        // one cell is 4/8 to the left and one 3/6 to the right of the original eta coordinate
                        c_eta_up.push_back( cell_eta_v[i] - 3. * m_fine_eta_len / 6. );
                        c_eta_up.push_back( cell_eta_v[i] + 3 * m_fine_eta_len / 6. );
                        // both get half of the energy
                        c_energy_up.push_back( cell_energy_v[i] * 2. / 4.);
                        c_energy_up.push_back( cell_energy_v[i] * 2. / 4.);

                        if ( m_time ){
                            c_time_up.push_back( cell_time_v[i] ); // attempt to give the subcell only the time of the cell with the largest area covered
                            c_time_up.push_back( cell_time_v[i] );
                        }
                        if ( m_gain ){
                            c_gain_up.push_back( cell_gain_v[i] ); // attempt to give the subcell only the gain of the cell with the largest area covered
                            c_gain_up.push_back( cell_gain_v[i] );
                        }

                    }
                    else{
                        // if the position is two 3/4 of the cell belongs to the second upscaled cell and 1/4 to the first one
                        // the first cell is put 1/6 (5/6) to the left ( if eta runs from left to right ) of the eta coordinate for positive (negative) eta
                        c_eta_up.push_back( cell_eta_v[i] - ( 5. + 4. * std::signbit(cluster_eta) ) * m_fine_eta_len / 6. );
                        c_eta_up.push_back( cell_eta_v[i] + ( 1. - 4. * std::signbit(cluster_eta) ) * m_fine_eta_len / 6. );
                        c_energy_up.push_back( cell_energy_v[i] * ( 1. - 2. * std::signbit(cluster_eta) ) / 4.);
                        c_energy_up.push_back( cell_energy_v[i] * ( 3. + 2. * std::signbit(cluster_eta) ) / 4.);

                        if ( m_time ){
                            c_time_up.push_back( 0. ); // attempt to give the subcell only the time of the cell with the largest area covered
                            c_time_up.push_back( cell_time_v[i] );
                        }
                        if ( m_gain ){
                            c_gain_up.push_back( 0. ); // attempt to give the subcell only the gain of the cell with the largest area covered
                            c_gain_up.push_back( cell_gain_v[i] );
                        }
                    }
                    // push back all other values twice for the two cells
                    c_phi_up.push_back( cell_phi_v[i] );
                    c_phi_up.push_back( cell_phi_v[i] );
                    c_sampling_up.push_back( cell_sampling_v[i] );
                    c_sampling_up.push_back( cell_sampling_v[i] );
                }
                else{
                    // if the cell is outside of the bad region the "standard" upscaling can be used
                    size_t size_cell = std::round(size_cell_d);
                    // if the cells are smaller than the desired binning (only if 7 is asked) just keep the cell by setting size_cell to 1
                    if (size_cell == 0) size_cell = 1;
                    for (size_t j = 0; j < size_cell; j++ ){
                        // the new cells should all have the same phi, sampling and time value. the energy is divided by the number of cells it is split into
                        // put the eta coordinates evenly spaces around the original coordinate
                        if ( m_fine_eta ){
                            c_eta_up.push_back(cell_eta_v[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_fine_eta_len ));
                        }
                        else{
                            c_eta_up.push_back(cell_eta_v[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_eta_len ));
                        }
                        c_energy_up.push_back(cell_energy_v[i] / size_cell);
                        c_phi_up.push_back( cell_phi_v[i] );
                        c_sampling_up.push_back( cell_sampling_v[i]);
                        if ( m_time ){
                            c_time_up.push_back( cell_time_v[i]);
                        }
                        if ( m_gain ){
                            c_gain_up.push_back( cell_gain_v[i]);
                        }
                    }
                }
            }
            else{
                // if the cell is not in the em encap layer 1 the "standard" upscaling can be used
                size_t size_cell;
                if ( m_fine_eta ){
                     size_cell = std::round(cell_deta_v[i] / m_fine_eta_len);
                 }
                 else{
                     // if the cells are smaller than the desired binning (only if 7 is asked) just keep the cell by setting size_cell to 1
                     size_cell = std::round(cell_deta_v[i] / m_eta_len);
                     if (size_cell == 0) size_cell = 1;
                 }
                for (size_t j = 0; j < size_cell; j++ ){
                    // the new cells should all have the same phi, sampling and time value. the energy is divided by the number of cells it is split into
                    // put the eta coordinates evenly spaces around the original coordinate
                    if ( m_fine_eta ){
                        c_eta_up.push_back(cell_eta_v[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_fine_eta_len ));
                    }
                    else{
                        c_eta_up.push_back(cell_eta_v[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_eta_len ));
                    }
                    c_energy_up.push_back(cell_energy_v[i] / size_cell);
                    c_phi_up.push_back( cell_phi_v[i] );
                    c_sampling_up.push_back( cell_sampling_v[i]);

                    if ( m_time ){
                        c_time_up.push_back( cell_time_v[i]);
                    }
                    if ( m_gain ){
                        c_gain_up.push_back( cell_gain_v[i]);
                    }
                }
            }
        }
        // had calo always not fine binning
        else{
            size_t size_cell = std::round(cell_deta_v[i] / m_eta_len);
            // loop over the number of new cells
            for (size_t j = 0; j < size_cell; j++ ){
                // the new cells should all have the same phi, sampling and time value. the energy is divided by the number of cells it is split into
                // put the eta coordinates evenly spaces around the original coordinate
                c_eta_up.push_back(cell_eta_v[i]  + ( ( ( - (size_cell / 2.) + 0.5 ) + j ) * m_eta_len ));
                c_energy_up.push_back(cell_energy_v[i] / size_cell);
                c_phi_up.push_back( cell_phi_v[i] );
                c_sampling_up.push_back( cell_sampling_v[i]);
                if ( m_time ){
                    c_time_up.push_back( cell_time_v[i]);
                }
                if ( m_gain ){
                    c_gain_up.push_back( cell_gain_v[i]);
                }
            }
        }
    }

    // write upscaled vectors in the member variables
    cell_energy_v.clear();
    cell_energy_v = c_energy_up;

    cell_eta_v.clear();
    cell_eta_v = c_eta_up;

    cell_phi_v.clear();
    cell_phi_v = c_phi_up;

    cell_sampling_v.clear();
    cell_sampling_v = c_sampling_up;

    if ( m_time ){
        cell_time_v.clear();
        cell_time_v = c_time_up;
    }
    if ( m_gain ){
        cell_gain_v.clear();
        cell_gain_v = c_gain_up;
    }
    return ;
}


void ImageCreation::set_images_zero(){
    for ( auto& img : em_calo ){
        for ( size_t i = 0; i < img.size(); i++ ){
            img[i] = 0.0;
        }
    }

    for ( auto& img : h_calo ){
        for ( size_t i = 0; i < img.size(); i++ ){
            img[i] = 0.0;
        }
    }

    if ( m_time ){
        for ( auto& img : time_em ){
            for ( size_t i = 0; i < img.size(); i++ ){
                img[i] = 0.0;
            }
        }

        for ( auto& img : time_h ){
            for ( size_t i = 0; i < img.size(); i++ ){
                img[i] = 0.0;
            }
        }
    }

    return;
}


// void ImageCreation::create_images(const xAOD::Photon& photon ) {
//
//     // Get all the necessary cell values
//     cell_energy_v                               = cell_energy( photon );
//     cell_eta_v                                  = cell_eta( photon );
//     cell_deta_v                                 = cell_deta( photon );
//     cell_phi_v                                  = cell_phi( photon );
//     cell_dphi_v                                 = cell_dphi( photon );
//     cell_sampling_v                             = cell_sampling( photon );
//
//     if ( m_time ){
//         cell_time_v = cell_time( photon );
//     }
//     if ( m_gain ){
//         cell_gain_v = cell_gain( photon );
//     }
//
//
//     // get the cluster
//     const xAOD::CaloCluster cluster = *(photon.caloCluster());
//
//     // get the eta/phi coordinate of the cluster
//     double cl_eta = cluster.eta();
//     double cl_phi = cluster.phi();
//
//     // if the phi coordinate is larger than 2 or smaller than -2, the cluster could go over pi/-pi and the cell coordinates will not be continuuos, but will be broken
//     // therefore all phi cell coordinates which are on the "wrong" side are put to the other side
//     if ( cl_phi > 2 ){
//         for( size_t i = 0; i < cell_phi_v.size(); i++ ){
//             if (cell_phi_v[i] < 0){
//                 cell_phi_v[i] += 2 * TMath::Pi();
//             }
//         }
//     }
//     else if ( cl_phi < -2 ){
//         for( size_t i = 0; i < cell_phi_v.size(); i++ ){
//             if (cell_phi_v[i] > 0){
//                 cell_phi_v[i] += -2 * TMath::Pi();
//             }
//         }
//     }
//
//     // center the cell coordinates around the cluster coordinates
//     std::transform(cell_eta_v.begin(), cell_eta_v.end(), cell_eta_v.begin(),
//           bind2nd(std::plus<double>(), -cl_eta));
//     std::transform(cell_phi_v.begin(), cell_phi_v.end(), cell_phi_v.begin(),
//           bind2nd(std::plus<double>(), -cl_phi));
//
//
//     // scale up in eta and phi
//     phi_upscale();
//     eta_upscale( cl_eta );
//
//     set_images_zero();
//
//     fill_images();
//
//     return;
// }

size_t ImageCreation::get_index(const std::vector < float > bins, const float value ){
    size_t index = 60;
    for( size_t k = 0; k < bins.size() - 1 ; k++ ){
        if ( bins[k] <= value && bins[k+1] > value ){
            index = k;
        }
    }

    return index;
}


void ImageCreation::fill_images(){
    for( size_t index = 0; index < cell_sampling_v.size(); index++){

        // Fill the image if the cell is in the right layer for the image
        // set a initial value which is larger than the highest index
        size_t eta_index = 60;
        // if fine biining in eta is wanted
        if( m_fine_eta && cell_sampling_v[index] < 8 ){
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            eta_index = get_index( m_fine_eta_bins, cell_eta_v[index]);
        }
        // if no fine binning in eta is wanted, had calo images will always use the not fine binning
        else{
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            eta_index = get_index( m_eta_bins, cell_eta_v[index]);
        }

        // set a initial value which is larger than the highest index
        size_t phi_index = 60;
        // if fine biining in phi is wanted
        if( m_fine_phi && cell_sampling_v[index] < 8  ){
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            phi_index = get_index( m_fine_phi_bins, cell_phi_v[index]);
        }
        // if no fine binning in phi is wanted, had calo images will always use the not fine binning
        else{
            // Loop over the bin edges and check if the cell eta value is in the bin and save the bin
            phi_index = get_index( m_phi_bins, cell_phi_v[index]);
        }

        // ANA_MSG_INFO("Reached filling");
        // if the initial eta or phi index is not updated something went wrong
        if( eta_index == 60 ) continue;
        if( phi_index == 60 ) continue;

        // Fill all the images. Add the value to the according bin. NOTE if there are more than one cell that contriutes to one bin, the energy image is fine, the others probably not
        fill_vectors( index, eta_index, phi_index );

    }
    return;

}


void ImageCreation::fill_vectors(size_t index, size_t eta_index, size_t phi_index ){
    if ( cell_sampling_v[index] < 8 ){
        em_calo[sampling_layer[cell_sampling_v[index]]][eta_index * 11 + phi_index] = cell_energy_v[index] / 1000.; // scaled to GeV

        if ( m_time ){
            time_em[sampling_layer[cell_sampling_v[index]]][eta_index * 11 + phi_index] = cell_time_v[index] ; // scaled to GeV
        }
    }
    else{
        h_calo[sampling_layer[cell_sampling_v[index]]][eta_index * 11 + phi_index] = cell_energy_v[index] / 1000.; // scaled to GeV
        if ( m_time ){
            time_h[sampling_layer[cell_sampling_v[index]]][eta_index * 11 + phi_index] = cell_time_v[index] ; // scaled to GeV
        }
    }
}

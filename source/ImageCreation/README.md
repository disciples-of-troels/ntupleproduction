# About 
This tool can be used to create images for electrons ( photons maybe in the future ) if the electrons were decorated with vectors of the cell values, (eta, phi, energy, ...).

# Description of creation
To come 

# Implementation
To use this tool
1. Clone this repository into your source folder. 

In your CMakeLists.txt 
~~~shell
atlas_add_library ([...]
    PRIVATE_LINK_LIBRARIES [...] ImageCreationLib )


atlas_add_dictionary ([...]
    LINK_LIBRARIES [...] ImageCreationLib)


atlas_add_component ([...]
    LINK_LIBRARIES [...] ImageCreationLib)
~~~

# Usage in EventLoop:
Note: You can also use a ToolHandle or an AnaToolHandle  
In your header file
~~~c++
#include "ImageCreation/ImageCreation.h"

~

ImageCreation* m_image = nullptr;
~~~

In your source file
~~~c++
m_image = new ImageCreation("image");
ANA_CHECK( m_image->initialize() )

~

m_image->create_images( el );
std::vector < std::vector < float > > em_calo[8];
for ( size_t i = 0; i < 8; i++ ){
    em_calo[i] = m_image->em_calo[i]
}
~~~

This will create images for the electron 'el', and save the electromagnetic calorimeter images in the array em_calo. To retrieve the hadronic images use h_calo instead of em_calo. One can also access pointers to single layers, by getting a member variable

| Object in the array | Layer in the Calorimeter | Pointer to image |
|---|---|---|
| em_calo 0  | ECAL Presampler in the barrel | em_barrel_Lr0 |
| em_calo 1  | ECAL Layer 1 in the barrel | em_barrel_Lr1 |
| em_calo 2  | ECAL Layer 2 in the barrel | em_barrel_Lr2 |
| em_calo 3  | ECAL Layer 3 in the barrel | em_barrel_Lr3 |
| em_calo 4  | ECAL Presampler in the endcap | em_endcap_Lr0 |
| em_calo 5  | ECAL Layer 1 in the endcap | em_endcap_Lr1 |
| em_calo 6  | ECAL Layer 2 in the endcap | em_endcap_Lr2 |
| em_calo 7  | ECAL Layer 3 in the endcap | em_endcap_Lr3 |
|---|---|---|
| h_calo 0 | HCAL LAr endcap Layer 0 | lar_endcap_Lr0 |
| h_calo 1 | HCAL LAr endcap Layer 1 | lar_endcap_Lr1 |
| h_calo 2 | HCAL LAr endcap Layer 2 | lar_endcap_Lr2 |
| h_calo 3 | HCAL LAr endcap Layer 3 | lar_endcap_Lr3 |
| h_calo 4 | HCAL Tile barrel + Tile ext barrel Layer 1 (there is no overlap between these two, therefore they are added to save space) | tile_barrel_Lr1 |
| h_calo 5 | HCAL Tile barrel + Tile ext barrel Layer 2 (there is no overlap between these two, therefore they are added to save space)  | tile_barrel_Lr2 |
| h_calo 6 | HCAL Tile barrel + Tile ext barrel Layer 3 (there is no overlap between these two, therefore they are added to save space)  | tile_barrel_Lr3 |
| h_calo 7 | HCAL Gap scintillators | tile_gap_Lr1 |

# Options
- FineEta: (default true) When this is true the ECAL images will be produced with 56 pixels with Delta eta = 0.025/8 in the eta direction. If it is false the images will be produced with 7 pixels with with Delta eta = 0.025. The HCAL images are always saved with the low resolution 
- FinePhi: (default false) When this is true the ECAL images will be produced with 55 pixels with Delta phi = 0.025/5 in the phi direction. If it is false the images will be produced with 11 pixel with Delta phi = 0.025. The HCAL images are always saved with the low resolution
- Time : (default false) Will create images where the time is saved additionally to the em images.
The time images can be accessed with 'time_em' for the ECAL images and 'time_h' for the HCAL images.




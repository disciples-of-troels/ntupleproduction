#ifndef IMAGECREATION_IMAGECREATIONDICT_H_
#define IMAGECREATION_IMAGECREATIONDICT_H_

#include <string>
#include <set>

#include "AsgTools/AsgTool.h"
#include "xAODEgamma/Photon.h"
#include "ImageCreation/IImageCreation.h"


#ifndef XAOD_ANALYSIS
#include "GaudiKernel/ToolHandle.h"
#endif



class ImageCreation :
virtual public IImageCreation, public asg::AsgTool {
  ASG_TOOL_CLASS(ImageCreation, IImageCreation)

 public:
  explicit ImageCreation(const std::string& type);
  virtual ~ImageCreation() {}

  StatusCode initialize() override;
  StatusCode finalize();

  // virtual void create_images(const xAOD::Photon& photon) override;


#ifndef XAOD_ANALYSIS
    override
#endif




 private:

    void set_pointers();
    void phi_upscale();
    void eta_upscale( double cluster_eta );

    void resize_images();
    // void set_zero( std::vector < std::vector < float > > images[8] );
    void set_images_zero();
    void fill_images();
    void fill_vectors(size_t index, size_t eta_index, size_t phi_index );
    size_t get_index(const std::vector < float > bins, const float value );



    const SG::AuxElement::ConstAccessor< std::vector < float > > cell_energy; //!
    const SG::AuxElement::ConstAccessor< std::vector < float > > cell_eta; //!
    const SG::AuxElement::ConstAccessor< std::vector < float > > cell_deta; //!
    const SG::AuxElement::ConstAccessor< std::vector < float > > cell_phi; //!
    const SG::AuxElement::ConstAccessor< std::vector < float > > cell_dphi; //!
    const SG::AuxElement::ConstAccessor< std::vector < int > >   cell_sampling; //!
    const SG::AuxElement::ConstAccessor< std::vector < float > > cell_time; //!
    const SG::AuxElement::ConstAccessor< std::vector < int > >   cell_gain; //!


    std::vector < float > cell_eta_v; //!
    std::vector < float > cell_energy_v; //!
    std::vector < float > cell_deta_v; //!
    std::vector < float > cell_phi_v; //!
    std::vector < float > cell_dphi_v; //!
    std::vector < int >   cell_sampling_v; //!
    std::vector < float > cell_time_v; //!
    std::vector < int >   cell_gain_v; //!

    std::map < int, int > sampling_layer; //!



    const std::vector < float > m_eta_bins = {-0.0875, -0.0625, -0.0375, -0.0125,  0.0125,  0.0375,  0.0625,
             0.0875 };

    const std::vector < float > m_fine_eta_bins = { -0.0875  , -0.084375, -0.08125 , -0.078125, -0.075   , -0.071875,
            -0.06875 , -0.065625, -0.0625  , -0.059375, -0.05625 , -0.053125,
            -0.05    , -0.046875, -0.04375 , -0.040625, -0.0375  , -0.034375,
            -0.03125 , -0.028125, -0.025   , -0.021875, -0.01875 , -0.015625,
            -0.0125  , -0.009375, -0.00625 , -0.003125,  0.      ,  0.003125,
             0.00625 ,  0.009375,  0.0125  ,  0.015625,  0.01875 ,  0.021875,
             0.025   ,  0.028125,  0.03125 ,  0.034375,  0.0375  ,  0.040625,
             0.04375 ,  0.046875,  0.05    ,  0.053125,  0.05625 ,  0.059375,
             0.0625  ,  0.065625,  0.06875 ,  0.071875,  0.075   ,  0.078125,
             0.08125 ,  0.084375,  0.0875  };

    const std::vector < float > m_phi_bins = {-0.13499031, -0.11044662, -0.08590292, -0.06135923, -0.03681554,
            -0.01227185,  0.01227185,  0.03681554,  0.06135923,  0.08590292,
             0.11044662,  0.13499031};

    const std::vector < float > m_fine_phi_bins = {-0.13499031, -0.13008157, -0.12517283, -0.12026409, -0.11535536,
            -0.11044662, -0.10553788, -0.10062914, -0.0957204 , -0.09081166,
            -0.08590292, -0.08099419, -0.07608545, -0.07117671, -0.06626797,
            -0.06135923, -0.05645049, -0.05154175, -0.04663302, -0.04172428,
            -0.03681554, -0.0319068 , -0.02699806, -0.02208932, -0.01718058,
            -0.01227185, -0.00736311, -0.00245437,  0.00245437,  0.00736311,
             0.01227185,  0.01718058,  0.02208932,  0.02699806,  0.0319068 ,
             0.03681554,  0.04172428,  0.04663302,  0.05154175,  0.05645049,
             0.06135923,  0.06626797,  0.07117671,  0.07608545,  0.08099419,
             0.08590292,  0.09081166,  0.0957204 ,  0.10062914,  0.10553788,
             0.11044662,  0.11535536,  0.12026409,  0.12517283,  0.13008157,
             0.13499031};

     bool m_fine_eta = true; //!
     bool m_fine_phi = false; //!
     bool m_time = false;//!
     bool m_gain = false;//!

     const double m_phi_len = 2. * TMath::Pi() / 256.; //!
     const double m_fine_phi_len = 2. * TMath::Pi() / ( 256. * 5. ); //!
     const double m_fine_eta_len = 0.025 / 8.; //!
     const double m_eta_len = 0.025; //!

};

#endif  // IMAGECREATION_IIMAGECREATIONDICT_H_

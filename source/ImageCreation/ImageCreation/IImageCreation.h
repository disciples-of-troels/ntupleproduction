#ifndef IMAGECREATION_IIMAGECREATIONDICT_H_
#define IMAGECREATION_IIMAGECREATIONDICT_H_

#include "AsgTools/AsgTool.h"


class IImageCreation : virtual public asg::IAsgTool {
  ASG_TOOL_INTERFACE(IImageCreation)
 public:
  virtual ~IImageCreation() {}

  // virtual void create_images(const xAOD::Photon& photon) {};

  std::vector < std::vector < float > > em_calo;
  std::vector < std::vector < float > > h_calo;

  std::vector < std::vector < float > > time_em;
  std::vector < std::vector < float > > time_h;

  std::vector < float >* em_barrel_Lr0 = nullptr;
  std::vector < float >* em_barrel_Lr1 = nullptr;
  std::vector < float >* em_barrel_Lr2 = nullptr;
  std::vector < float >* em_barrel_Lr3 = nullptr;
  std::vector < float >* em_endcap_Lr0 = nullptr;
  std::vector < float >* em_endcap_Lr1 = nullptr;
  std::vector < float >* em_endcap_Lr2 = nullptr;
  std::vector < float >* em_endcap_Lr3 = nullptr;

  std::vector < float >* lar_endcap_Lr0 = nullptr;
  std::vector < float >* lar_endcap_Lr1 = nullptr;
  std::vector < float >* lar_endcap_Lr2 = nullptr;
  std::vector < float >* lar_endcap_Lr3 = nullptr;

  std::vector < float >* tile_barrel_Lr1 = nullptr;
  std::vector < float >* tile_barrel_Lr2 = nullptr;
  std::vector < float >* tile_barrel_Lr3 = nullptr;

  std::vector < float >* tile_gap_Lr1 = nullptr;


  std::vector < float >* time_em_barrel_Lr0 = nullptr;
  std::vector < float >* time_em_barrel_Lr1 = nullptr;
  std::vector < float >* time_em_barrel_Lr2 = nullptr;
  std::vector < float >* time_em_barrel_Lr3 = nullptr;
  std::vector < float >* time_em_endcap_Lr0 = nullptr;
  std::vector < float >* time_em_endcap_Lr1 = nullptr;
  std::vector < float >* time_em_endcap_Lr2 = nullptr;
  std::vector < float >* time_em_endcap_Lr3 = nullptr;

  std::vector < float >* time_lar_endcap_Lr0 = nullptr;
  std::vector < float >* time_lar_endcap_Lr1 = nullptr;
  std::vector < float >* time_lar_endcap_Lr2 = nullptr;
  std::vector < float >* time_lar_endcap_Lr3 = nullptr;

  std::vector < float >* time_tile_barrel_Lr1 = nullptr;
  std::vector < float >* time_tile_barrel_Lr2 = nullptr;
  std::vector < float >* time_tile_barrel_Lr3 = nullptr;

  std::vector < float >* time_tile_gap_Lr1 = nullptr;

};

#endif  // IMAGECREATION_IIMAGECREATIONDICT_H_

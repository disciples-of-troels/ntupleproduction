#ifndef MyAnalysis_Helpers_H
#define MyAnalysis_Helpers_H

#if !defined(__clang__)
#define MORE_DIAGNOSTICS() \
	_Pragma("GCC diagnostic error \"-Wall\"") \
	_Pragma("GCC diagnostic error \"-Wextra\"") \
	_Pragma("GCC diagnostic error \"-Wshadow\"") \
	_Pragma("GCC diagnostic error \"-Wcast-align\"") \
	_Pragma("GCC diagnostic error \"-Wctor-dtor-privacy\"") \
	_Pragma("GCC diagnostic error \"-Wdisabled-optimization\"") \
	_Pragma("GCC diagnostic error \"-Wformat\"=2") \
	_Pragma("GCC diagnostic error \"-Winit-self\"") \
	_Pragma("GCC diagnostic error \"-Wlogical-op\"") \
	_Pragma("GCC diagnostic error \"-Wmissing-include-dirs\"") \
	_Pragma("GCC diagnostic error \"-Wnoexcept\"") \
	_Pragma("GCC diagnostic error \"-Woverloaded-virtual\"") \
	_Pragma("GCC diagnostic error \"-Wredundant-decls\"") \
	_Pragma("GCC diagnostic error \"-Wsign-conversion\"") \
	_Pragma("GCC diagnostic error \"-Wsign-promo\"") \
	_Pragma("GCC diagnostic error \"-Wstrict-null-sentinel\"") \
	_Pragma("GCC diagnostic error \"-Wstrict-overflow\"=5") \
	_Pragma("GCC diagnostic error \"-Wswitch-default\"") \
	_Pragma("GCC diagnostic error \"-Wundef\"") \
	_Pragma("GCC diagnostic error \"-Wfloat-equal\"") \
	_Pragma("GCC diagnostic error \"-Wpointer-arith\"") \
	_Pragma("GCC diagnostic error \"-Wwrite-strings\"") \
	_Pragma("GCC diagnostic error \"-Wswitch-enum\"") \
	_Pragma("GCC diagnostic error \"-Wunreachable-code\"") \
	_Pragma("GCC diagnostic error \"-Wdeprecated\"") \
	_Pragma("GCC diagnostic error \"-Wmisleading-indentation\"") \
	_Pragma("GCC diagnostic error \"-Wuninitialized\"")
#else
#define MORE_DIAGNOSTICS() \
	_Pragma("clang diagnostic error \"-Wall\"") \
	_Pragma("clang diagnostic error \"-Wextra\"") \
	_Pragma("clang diagnostic error \"-Wshadow\"") \
	_Pragma("clang diagnostic error \"-Wcast-align\"") \
	_Pragma("clang diagnostic error \"-Wctor-dtor-privacy\"") \
	_Pragma("clang diagnostic error \"-Wdisabled-optimization\"") \
	_Pragma("clang diagnostic error \"-Winit-self\"") \
	_Pragma("clang diagnostic error \"-Wmissing-include-dirs\"") \
	_Pragma("clang diagnostic error \"-Woverloaded-virtual\"") \
	_Pragma("clang diagnostic error \"-Wredundant-decls\"") \
	_Pragma("clang diagnostic error \"-Wsign-conversion\"") \
	_Pragma("clang diagnostic error \"-Wsign-promo\"") \
	_Pragma("clang diagnostic error \"-Wswitch-default\"") \
	_Pragma("clang diagnostic error \"-Wundef\"") \
	_Pragma("clang diagnostic error \"-Wfloat-equal\"") \
	_Pragma("clang diagnostic error \"-Wpointer-arith\"") \
	_Pragma("clang diagnostic error \"-Wwrite-strings\"") \
	_Pragma("clang diagnostic error \"-Wswitch-enum\"") \
	_Pragma("clang diagnostic error \"-Wunreachable-code\"") \
	_Pragma("clang diagnostic error \"-Wdeprecated\"") \
	\
	_Pragma("clang diagnostic error \"-Wformat\"") \
	_Pragma("clang diagnostic error \"-Wunused-private-field\"") \
	_Pragma("clang diagnostic error \"-Weverything\"") \
	_Pragma("clang diagnostic ignored \"-Wc++98-compat\"") \
	_Pragma("clang diagnostic ignored \"-Wc++98-compat-pedantic\"") \
	_Pragma("clang diagnostic ignored \"-Wpadded\"") \
	_Pragma("clang diagnostic ignored \"-Wgnu-zero-variadic-macro-arguments\"")
	//_Pragma("GCC diagnostic error \"-Wlogical-op\"")
	//_Pragma("GCC diagnostic error \"-Wnoexcept\"")
	//_Pragma("GCC diagnostic error \"-Wstrict-null-sentinel\"")
	//_Pragma("GCC diagnostic error \"-Wstrict-overflow\"=5")
#endif

#define LESS_DIAGNOSTICS() \
	_Pragma("GCC diagnostic ignored \"-Wall\"") \
	_Pragma("GCC diagnostic ignored \"-Wextra\"") \
	_Pragma("GCC diagnostic ignored \"-Wshadow\"") \
	_Pragma("GCC diagnostic ignored \"-Wcast-align\"") \
	_Pragma("GCC diagnostic ignored \"-Wctor-dtor-privacy\"") \
	_Pragma("GCC diagnostic ignored \"-Wdisabled-optimization\"") \
	_Pragma("GCC diagnostic ignored \"-Wformat\"=2") \
	_Pragma("GCC diagnostic ignored \"-Winit-self\"") \
	_Pragma("GCC diagnostic ignored \"-Wlogical-op\"") \
	_Pragma("GCC diagnostic ignored \"-Wmissing-include-dirs\"") \
	_Pragma("GCC diagnostic ignored \"-Wnoexcept\"") \
	_Pragma("GCC diagnostic ignored \"-Woverloaded-virtual\"") \
	_Pragma("GCC diagnostic ignored \"-Wredundant-decls\"") \
	_Pragma("GCC diagnostic ignored \"-Wsign-conversion\"") \
	_Pragma("GCC diagnostic ignored \"-Wsign-promo\"") \
	_Pragma("GCC diagnostic ignored \"-Wstrict-null-sentinel\"") \
	_Pragma("GCC diagnostic ignored \"-Wstrict-overflow\"=5") \
	_Pragma("GCC diagnostic ignored \"-Wswitch-default\"") \
	_Pragma("GCC diagnostic ignored \"-Wundef\"") \
	_Pragma("GCC diagnostic ignored \"-Wfloat-equal\"") \
	_Pragma("GCC diagnostic ignored \"-Wpointer-arith\"") \
	_Pragma("GCC diagnostic ignored \"-Wwrite-strings\"") \
	_Pragma("GCC diagnostic ignored \"-Wswitch-enum\"") \
	_Pragma("GCC diagnostic ignored \"-Wunreachable-code\"") \
	_Pragma("GCC diagnostic ignored \"-Wdeprecated\"") \
	_Pragma("GCC diagnostic ignored \"-Wmisleading-indentation\"")


#endif

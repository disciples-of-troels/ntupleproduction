#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <MyAnalysis/Helpers.h>

#include <AnaAlgorithm/AnaAlgorithm.h>

#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/EgammaTruthxAODHelpers.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

#include <TTree.h>
#include <TH1.h>
#include <vector>

#include <AsgTools/ToolHandle.h>

#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"

#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgForwardElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronChargeIDSelectorTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "PhotonEfficiencyCorrection/AsgPhotonEfficiencyCorrectionTool.h"

#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"

#include "IsolationSelection/IsolationSelectionTool.h"

#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"

#include "xAODCaloEvent/CaloCluster.h"

#include "METUtilities/METMaker.h"

#include "TriggerMatchingTool/MatchingTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TrigGlobalEfficiencyCorrection/TrigGlobalEfficiencyCorrectionTool.h"

#include "LightGBMPredictor/LightGBMPredictor.h"

#include "AssociationUtils/ToolBox.h"
#include "AssociationUtils/OverlapRemovalInit.h"
#include "AssociationUtils/OverlapRemovalTool.h"
#include "AssociationUtils/EleJetOverlapTool.h"
#include "AssociationUtils/MuJetOverlapTool.h"
#include "ImageCreation/ImageCreation.h"

// #include "shared.h" //Different version of "MORE_DIAGNOSTICS()"

#pragma GCC diagnostic push
MORE_DIAGNOSTICS()

struct multiplicityCutFlow{
    int indiv = 0;
    int acc = 0;
    int truthIndiv = 0;
    int truthAcc = 0;
};


class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
    // this is a standard algorithm constructor
    MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
    ~MyxAODAnalysis () override;

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;
    virtual StatusCode fileExecute () override;


private:
    // Configuration, and any other types of variables go here.


    long int m_eventCounter; //!
    long int m_startTime; //!
    int m_progressInterval = 1000; //!

    // Selection of electron, muon, fwd electron candidates
    std::vector < const xAOD::Electron* > selectElectronCandidates();
    std::vector < const xAOD::Muon* > selectMuonCandidates();
    std::vector < const xAOD::Photon* > selectPhotonCandidates();
    std::vector < const xAOD::Electron* > selectFwdElectronCandidates();

    void increaseCutFlow( multiplicityCutFlow* cutFlow, bool pass, bool &passAll, bool truth_el, int idx, int electron);
    void resetCutFlow( std::map< TString, multiplicityCutFlow* > map );
    StatusCode createCutFlowHists( std::map<TString, multiplicityCutFlow* > map );
    void fillCutFlowHists( std::map<TString, multiplicityCutFlow* > map );

    // multiplicity cutflow histograms
    std::map < TString, multiplicityCutFlow* > m_PhotonCutFlows; //!
    std::map < TString, multiplicityCutFlow* > m_ElectronCutFlows; //!
    std::map < TString, multiplicityCutFlow* > m_FwdCutFlows; //!
    std::map < TString, multiplicityCutFlow* > m_MuonCutFlows; //!


    //============================================================================
    // Container
    //============================================================================
    xAOD::PhotonContainer* m_photons = nullptr; //!
    xAOD::ElectronContainer* m_electrons = nullptr; //!
    xAOD::ElectronContainer* m_fwdElectrons = nullptr; //!
    xAOD::VertexContainer* m_vertex = nullptr; //!
    xAOD::MuonContainer* m_muons = nullptr; //!
    xAOD::JetContainer* m_jets = nullptr; //!
    xAOD::Vertex* m_primaryVertex = nullptr; //!
    std::unique_ptr< xAOD::TrackParticleContainer > m_indetTracks; //!
    std::unique_ptr< xAOD::ShallowAuxContainer > m_indetTracksAux; //!
    const xAOD::TruthParticleContainer* m_egammaTruthContainer = nullptr; //!
    const xAOD::TruthParticleContainer* m_truthParticles = nullptr; //!

    const xAOD::EventInfo *m_eventInfo = nullptr; //!

    const xAOD::MissingET* m_METmaker = nullptr;
    xAOD::MissingETContainer* m_newMetContainer = nullptr; //!
    xAOD::MissingETAuxContainer* m_newMetAuxContainer = nullptr; //!
    const xAOD::MissingETContainer* m_coreMet = nullptr; //!
    const xAOD::MissingETAssociationMap* m_metMap = nullptr; //!




    //============================================================================
    // Tools
    //============================================================================

    ToolHandle<IGoodRunsListSelectionTool> m_grl; //!
    ToolHandle<CP::IPileupReweightingTool> m_PileupReweighting; //!

    // Trigger
    asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfig; //!
    asg::AnaToolHandle<Trig::ITrigDecisionTool> m_trigDecTool; //!
    asg::AnaToolHandle<Trig::IMatchingTool> m_triggerMatching; //!

    // electron, photon
    ToolHandle<IElectronPhotonShowerShapeFudgeTool> m_MCShifterTool; //!
    ToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_egammaCalibrationSmearingTool; //!

    // electron
    ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_electronSFReco; //!
    ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_electronSFID; //!
    ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_electronSFIso; //!
    ToolHandle<IAsgElectronLikelihoodTool> m_electronLHLoose; //!
    ToolHandle<IAsgElectronLikelihoodTool> m_electronLHMedium; //!
    ToolHandle<IAsgElectronLikelihoodTool> m_electronLHTight; //!
    ToolHandle<AsgElectronChargeIDSelectorTool> m_ECIDSToolLoose; //!

    // photon
    ToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_photonSFReco; //!
    ToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_photonSFID; //!
    ToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_photonSFIso; //!
    ToolHandle<IAsgPhotonIsEMSelector> m_photonIsEMLoose; //!
    ToolHandle<IAsgPhotonIsEMSelector> m_photonIsEMTight; //!

    // electron - forward
    ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_electronSFRecoFwd; //!
    ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_electronSFIDFwd; //!
    ToolHandle<AsgForwardElectronLikelihoodTool> m_electronLHLooseFwd; //!
    ToolHandle<AsgForwardElectronLikelihoodTool> m_electronLHMediumFwd; //!
    ToolHandle<AsgForwardElectronLikelihoodTool> m_electronLHTightFwd; //!

    // muon
    ToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibrationSmearingTool; //!
    ToolHandle<CP::IMuonSelectionTool> m_muonSelectionToolLoose; //!
    ToolHandle<CP::IMuonSelectionTool> m_muonSelectionToolMedium; //!
    ToolHandle<CP::IMuonSelectionTool> m_muonSelectionToolTight; //!
    ToolHandle<CP::IMuonEfficiencyScaleFactors> m_muonSF; //!
    ToolHandle<CP::IMuonEfficiencyScaleFactors> m_muonSFIso; //!

    // isolation
    ToolHandle<CP::IIsolationSelectionTool> m_isolationSelectionToolLoose; //!

    // jet
    ToolHandle<IJetCalibrationTool> m_jetCalibrationTool; //!
    ToolHandle<JetCleaningTool> m_jetCleaningTool; //!

    // met
    ToolHandle<IMETMaker> m_metutil; //!

    // LGBM ############## Behøver måske ikke at vær med ##############
    // ToolHandle<ILightGBMPredictor> m_ele_lgbm_pdfvars; //!
    // ToolHandle<ILightGBMPredictor> m_ele_lgbm_pdfcutvars; //!
    //
    // ToolHandle<ILightGBMPredictor> m_pho_lgbm_pdfvars_data; //!
    // ToolHandle<ILightGBMPredictor> m_pho_lgbm_convvars_data; //!
    // ToolHandle<ILightGBMPredictor> m_pho_lgbm_binvars_data; //!
    // ToolHandle<ILightGBMPredictor> m_pho_lgbm_pdfvars_mc; //!
    // ToolHandle<ILightGBMPredictor> m_pho_lgbm_convvars_mc; //!
    // ToolHandle<ILightGBMPredictor> m_pho_lgbm_binvars_mc; //!
    // ToolHandle<ILightGBMPredictor> m_pho_lgbm_iso; //!
    // // ToolHandle<ILightGBMPredictor> m_pho_lgbm_noconvvars; //! // Uses maxEcell_energy that is not available for AODs
    // // ToolHandle<ILightGBMPredictor> m_pho_lgbm_nocellvars; //!
    // // ToolHandle<ILightGBMPredictor> m_pho_lgbm_extvars; //! // Uses maxEcell_energy that is not available for AODs
    //
    // ToolHandle<ILightGBMPredictor> m_fwd_lgbm_pid; //!
    // ToolHandle<ILightGBMPredictor> m_fwd_lgbm_er; //!
    // ToolHandle<ILightGBMPredictor> m_fwd_lgbm_iso; //!


    // define overlap removal tool
    asg::AnaToolHandle<ORUtils::IOverlapRemovalTool> m_orTool; //!
    ORUtils::ORFlags* m_orFlags = nullptr; //!
    ORUtils::ToolBox* m_orToolbox = nullptr; //!

    // Image Creation
    // ToolHandle<IImageCreation> m_images; //!

    //============================================================================
    // Output tree branch variables
    //============================================================================
    // bool event_isMC = true; //! // Used for debugging
    bool event_isMC; //!
    float event_pileupweight; //!
    int m_counter = 0; //!

    // process specific quantities
    std::map<uint32_t,float> m_crossSections; //!
    std::map<uint32_t,float> m_kFactors; //!
    std::map<uint32_t,float> m_filterEffs; //!

    // Event Level variables
    int event_evtNumber; //!
    uint32_t event_mcChannelNumber; //!
    uint32_t event_runNumber; //!
    float event_averageInteractionsPerCrossing; //!
    float event_actualInteractionsPerCrossing; //!
    float event_correctedScaledAverageMu; //!
    float event_correctedScaledActualMu; //!
    float event_MCWeight; //!
    float event_crossSection; //!
    float eventWeight; //!
    float event_lumiBlock; //!
    float event_bcid; //!
    // bool trig; //!
    int NvtxReco; //!
    int nPhotons; //!
    int nElectrons; //!
    int nFwdElectrons; //!
    int nMuons; //!

    // missing transverse energy
    double met_met; //!
    double met_phi; //!

    // Electron variables:
    // Electron - Truth information
    std::vector < int > ele_truthPdgId_egam; //!
    std::vector < float > ele_truth_eta_egam; //!
    std::vector < float > ele_truth_phi_egam; //!
    std::vector < float > ele_truth_m_egam; //!
    std::vector < float > ele_truth_px_egam; //!
    std::vector < float > ele_truth_py_egam; //!
    std::vector < float > ele_truth_pz_egam; //!
    std::vector < float > ele_truth_E_egam; //!
    std::vector < int > ele_truthPdgId_atlas; //!
    std::vector < float > ele_truth_eta_atlas; //!
    std::vector < float > ele_truth_phi_atlas; //!
    std::vector < float > ele_truth_m_atlas; //!
    std::vector < float > ele_truth_px_atlas; //!
    std::vector < float > ele_truth_py_atlas; //!
    std::vector < float > ele_truth_pz_atlas; //!
    std::vector < float > ele_truth_E_atlas; //!
    std::vector < bool > ele_egamTruthParticle; //!
    std::vector < int > ele_truthType; //!
    std::vector < int > ele_truthOrigin; //!
    std::vector< size_t > ele_TruthPointerIdx; //!
    std::vector< const xAOD::TruthParticle* > ele_TruthPointer; //!

    // Electron - ID Scores
    std::vector< bool > ele_trigger; //!
    // std::vector< bool > ele_LHValue; //!
    std::vector< bool > ele_LHLoose; //!
    std::vector< bool > ele_LHMedium; //!
    std::vector< bool > ele_LHTight; //!
    // std::vector< unsigned int > ele_LHLooseIsEMValue; //!
    // std::vector< unsigned int > ele_LHMediumIsEMValue; //!
    // std::vector< unsigned int > ele_LHTightIsEMValue; //!

    // Electron - ID variables information "LH variables"
    std::vector < float > ele_Rhad; //!
    std::vector < float > ele_Reta; //!
    std::vector < float > ele_Rhad1; //!
    std::vector < float > ele_deltaEta1; //!
    std::vector < float > ele_Eratio; //!
    std::vector < float > ele_Rphi; //!
    std::vector < float > ele_eProbHT; //! "TRTPID"
    std::vector < float > ele_weta2; //!
    std::vector < float > ele_dPOverP; //!
    std::vector < float > ele_f3; //!
    std::vector < float > ele_d0; //!
    std::vector < float > ele_z0; //!
    std::vector < float > ele_deltaPhiRescaled2; //!
    std::vector < float > ele_f1; //!

    // Electron - Binning vars - Kinematic information
    std::vector < float > ele_eta; //!

    // Electron - Selection vars
    std::vector < float > ele_wtots1; //!
    std::vector < float > ele_EptRatio; //!
    std::vector < unsigned char > ele_numberOfPixelHits; //!
    std::vector < unsigned char > ele_numberOfSCTHits; //!
    std::vector < int > ele_numberOfInnermostPixelHits; //!

    // Electron - Extra Variables
    std::vector < float > ele_E7x11_Lr3; //!
    std::vector < float > ele_nTracks; //!
    std::vector < float > ele_core57cellsEnergyCorrection; //!

    // Electron - other variables
    std::vector < float > ele_d0Sig; //!
    std::vector < float > ele_z0Sig; //!
    std::vector < double > ele_ECIDSResult; //!
    std::vector < unsigned short > ele_author; //!
    std::vector < int > ele_expectInnermostPixelLayerHit; //!
    std::vector < int > ele_expectNextToInnermostPixelLayerHit; //!
    std::vector < float > ele_chiSquared; //!
    std::vector < float > ele_radiusOfFirstHit; //!
    std::vector < int > ele_numberOfTRTHits; //!
    std::vector < float > ele_pixeldEdx; //!

    // Electron - other Kinematic information
    std::vector < float > ele_e; //!
    std::vector < float > ele_et; //!
    std::vector < float > ele_pt_track; //!
    std::vector < float > ele_phi; //!
    std::vector < float > ele_m; //!
    std::vector < float > ele_charge; //!


    // Electron - isolation variables
    std::vector < bool > ele_GradientIso; //!
    std::vector < float > ele_topoetcone20; //!
    std::vector < float > ele_topoetcone20ptCorrection; //!
    std::vector < float > ele_topoetcone40; //!
    std::vector < float > ele_ptvarcone20; //!

    std::vector < float > ele_ptvarcone30_TightTTVA_pt500; //!
    std::vector < float > ele_ptvarcone20_TightTTVA_pt1000; //!
    std::vector < float > ele_ptvarcone30_TightTTVA_pt1000; //!
    std::vector < float > ele_ptvarcone30_TightTTVALooseCone_pt500; //!
    std::vector < float > ele_ptvarcone20_TightTTVALooseCone_pt1000; //!
    std::vector < float > ele_ptvarcone30_TightTTVALooseCone_pt1000; //!
    std::vector < float > ele_ptvarcone40_TightTTVALooseCone_pt1000; //!
    std::vector < float > ele_ptcone20_TightTTVALooseCone_pt500; //!
    std::vector < float > ele_ptcone20_TightTTVALooseCone_pt1000; //!

    // Electron - scale factors
    std::vector < float > ele_recoSF; //!
    std::vector < float > ele_idSF; //!
    std::vector < float > ele_isoSF; //!
    std::vector < float > ele_combSF; //!

    // Electron - LGBM
    // std::vector< double > ele_pdf_score; //!
    // std::vector< double > ele_pdfconv_score; //!


    // fwd Electron variables:
    // fwd Electron - Truth information
    std::vector < int > fwd_truthPdgId; //!
    std::vector < int > fwd_truthType; //!
    std::vector < int > fwd_truthOrigin; //!
    std::vector < bool > fwd_truthParticle; //!
    std::vector < float > fwd_truth_eta; //!
    std::vector < float > fwd_truth_phi; //!
    std::vector < float > fwd_truth_E; //!
    std::vector< size_t > fwd_TruthPointerIdx; //!

    // fwd electron - ID scores
    std::vector < char > fwd_LHLoose; //!
    std::vector < char > fwd_LHMedium; //!
    std::vector < char > fwd_LHTight; //!

    // fwd Electron - LGBM
    // std::vector < float > fwd_LGBM_pid; //!
    // std::vector < float > fwd_LGBM_energy; //!
    // std::vector < float > fwd_LGBM_et; //!
    // std::vector < float > fwd_LGBM_iso; //!

    // fwd Electron - kinematics
    std::vector < float > fwd_eta; //!
    std::vector < float > fwd_phi; //!
    std::vector < float > fwd_pt; //!
    std::vector < float > fwd_et; //!

    // fwd electron isolation vars
    std::vector < float > fwd_topoetcone20; //!
    std::vector < float > fwd_topoetcone30; //!
    std::vector < float > fwd_topoetcone40; //!
    std::vector < float > fwd_topoetconecoreConeEnergyCorrection; //!

    // fwd electrons scale factors
    std::vector < float > fwd_recoSF; //!
    std::vector < float > fwd_idSF; //!
    std::vector < float > fwd_combSF; //!

    // fwd electron - other variables
    std::vector < float > fwd_secondLambdaCluster; //!
    std::vector < float > fwd_lateralCluster; //!
    std::vector < float > fwd_longitudinalCluster; //!
    std::vector < float > fwd_fracMaxCluster; //!
    std::vector < float > fwd_secondRCluster; //!
    std::vector < float > fwd_centerLambdaCluster; //!


    // Photon variables:
    // Photon - Truth information
    std::vector < int > pho_truthPdgId_egam; //!
    std::vector < float > pho_truth_eta_egam; //!
    std::vector < float > pho_truth_phi_egam; //!
    std::vector < float > pho_truth_m_egam; //!
    std::vector < float > pho_truth_px_egam; //!
    std::vector < float > pho_truth_py_egam; //!
    std::vector < float > pho_truth_pz_egam; //!
    std::vector < float > pho_truth_E_egam; //!
    std::vector < int > pho_truthPdgId_atlas; //!
    std::vector < float > pho_truth_eta_atlas; //!
    std::vector < float > pho_truth_phi_atlas; //!
    std::vector < float > pho_truth_m_atlas; //!
    std::vector < float > pho_truth_px_atlas; //!
    std::vector < float > pho_truth_py_atlas; //!
    std::vector < float > pho_truth_pz_atlas; //!
    std::vector < float > pho_truth_E_atlas; //!
    std::vector < bool > pho_egamTruthParticle; //!
    std::vector < int > pho_truthType; //!
    std::vector < int > pho_truthOrigin; //!
    std::vector< size_t > pho_TruthPointerIdx; //!
    std::vector< const xAOD::TruthParticle* > pho_TruthPointer; //!

    // Photon - ID scores
    std::vector< bool > pho_isPhotonEMLoose; //!
    std::vector< bool > pho_isPhotonEMTight; //!

    // Photon - LGBM scores
    // std::vector< double > pho_pdf_score_data; //!
    // std::vector< double > pho_pdfconv_score_data; //!
    // std::vector< double > pho_pdfbin_score_data; //!
    // std::vector< double > pho_pdf_logitscore_data; //!
    // std::vector< double > pho_pdfconv_logitscore_data; //!
    // std::vector< double > pho_pdfbin_logitscore_data; //!
    // std::vector< double > pho_pdf_score_mc; //!
    // std::vector< double > pho_pdfconv_score_mc; //!
    // std::vector< double > pho_pdfbin_score_mc; //!
    // std::vector< double > pho_pdf_logitscore_mc; //!
    // std::vector< double > pho_pdfconv_logitscore_mc; //!
    // std::vector< double > pho_pdfbin_logitscore_mc; //!
    // std::vector< double > pho_pdfnoconv_score; //!
    // std::vector< double > pho_pdfnocell_score; //!
    // std::vector< double > pho_pdfext_score; //!
    // std::vector< double > pho_pdfnoconv_logitscore; //!
    // std::vector< double > pho_pdfnocell_logitscore; //!
    // std::vector< double > pho_pdfext_logitscore; //!

    // Photon - Kinematics
    std::vector < float > pho_e; //!
    std::vector < float > pho_eta; //!
    std::vector < float > pho_phi; //!
    std::vector < float > pho_et; //!
    std::vector < float > pho_m; //!

    // Photon - LH Variables
    std::vector < float > pho_Rhad1; //!
    std::vector < float > pho_Rhad ; //!
    std::vector < float > pho_weta2; //!
    std::vector < float > pho_Rphi; //!
    std::vector < float > pho_Reta; //!
    std::vector < float > pho_Eratio; //!
    std::vector < float > pho_f1; //!
    std::vector < float > pho_wtots1; //!
    std::vector < float > pho_DeltaE; //!
    std::vector < float > pho_weta1; //!
    std::vector < float > pho_fracs1; //!

    // Photon - Conversion variables
    std::vector < int > pho_nTrackPart; //!
    std::vector < int > pho_ConversionType; //!
    std::vector < float > pho_ConversionRadius; //!
    std::vector < float > pho_VertexConvEtOverPt; //!
    std::vector < float > pho_VertexConvPtRatio; //!

    // Photon - Extra Variables
    std::vector < float > pho_maxEcell_time; //!
    std::vector < float > pho_maxEcell_energy; //!
    std::vector < float > pho_core57cellsEnergyCorrection; //!
    std::vector < float > pho_r33over37allcalo; //!

    // Photon - Isolation variables
    std::vector < bool > pho_GradientIso; //!
    std::vector < float > pho_topoetcone20; //!
    std::vector < float > pho_topoetcone40; //!
    std::vector < float > pho_ptvarcone20; //!
    std::vector < float > pho_LGBM_iso; //!
    std::vector < float > pho_LGBM_logitiso; //!

    // Photon - Scale factors
    std::vector < float > pho_recoSF; //!
    std::vector < float > pho_idSF; //!
    std::vector < float > pho_isoSF; //!
    std::vector < float > pho_combSF; //!

    // // Photons - Additional variables for ER with CNN
    // std::vector < float > f0Cluster; //!
    // std::vector < float > R12; //!
    // std::vector < float > fTG3; //!
    // std::vector < float > eAccCluster; //!
    // std::vector < float > cellIndexCluster; //!
    // std::vector < float > etaModCalo; //!
    // std::vector < float > pho_phiModCalo; //!
    // std::vector < float > dPhiTH3; //!
    // std::vector < float > poscs1; //!
    // std::vector < float > poscs2; //!

    // // Photon - Images
    // std::vector < std::vector < std::vector < float > > > em_calo; //!
    // std::vector < std::vector < std::vector < float > > > h_calo; //!
    // // Photon - Images - em calo energy
    // std::vector < std::vector < float > > em_calo0; //!
    // std::vector < std::vector < float > > em_calo1; //!
    // std::vector < std::vector < float > > em_calo2; //!
    // std::vector < std::vector < float > > em_calo3; //!
    // std::vector < std::vector < float > > em_calo4; //!
    // std::vector < std::vector < float > > em_calo5; //!
    // std::vector < std::vector < float > > em_calo6; //!
    // std::vector < std::vector < float > > em_calo7; //!
    // // Photon - Images - h calo energy
    // std::vector < std::vector < float > > h_calo0; //!
    // std::vector < std::vector < float > > h_calo1; //!
    // std::vector < std::vector < float > > h_calo2; //!
    // std::vector < std::vector < float > > h_calo3; //!
    // std::vector < std::vector < float > > h_calo4; //!
    // std::vector < std::vector < float > > h_calo5; //!
    // std::vector < std::vector < float > > h_calo6; //!
    // std::vector < std::vector < float > > h_calo7; //!


    // Muon variables:

    // Electron - ID Scores
    std::vector< bool > muo_trigger; //!
    // std::vector< bool > muo_LHValue; //!
    std::vector< bool > muo_LHLoose; //!
    std::vector< bool > muo_LHMedium; //!
    std::vector< bool > muo_LHTight; //!
    // std::vector< unsigned int > muo_LHLooseIsEMValue; //!
    // std::vector< unsigned int > muo_LHMediumIsEMValue; //!
    // std::vector< unsigned int > muo_LHTightIsEMValue; //!

    // Muon - Truth information
    std::vector < int > muo_truthPdgId; //!
    std::vector < int > muo_truthType; //!
    std::vector < int > muo_truthOrigin; //!
    std::vector < float > muo_truth_eta; //!
    std::vector < float > muo_truth_phi; //!
    std::vector < float > muo_truth_m; //!
    std::vector < float > muo_truth_px; //!
    std::vector < float > muo_truth_py; //!
    std::vector < float > muo_truth_pz; //!
    std::vector < float > muo_truth_E; //!

    // Muon - Isolation
    std::vector < float > muo_etcone20; //!
    std::vector < float > muo_etcone30; //!
    std::vector < float > muo_etcone40; //!
    std::vector < float > muo_ptcone20; //!
    std::vector < float > muo_ptcone30; //!
    std::vector < float > muo_ptcone40; //!
    std::vector < float > muo_ptvarcone20; //!
    std::vector < float > muo_ptvarcone30; //!
    std::vector < float > muo_ptvarcone40; //!

    // Muon - Kinematics
    std::vector < float > muo_pt; //!
    std::vector < float > muo_eta; //!
    std::vector < float > muo_phi; //!
    std::vector < float > muo_charge; //!

    // Muon - Hits
    std::vector < unsigned char > muo_innerSmallHits; //!
    std::vector < unsigned char > muo_innerLargeHits; //!
    std::vector < unsigned char > muo_middleSmallHits; //!
    std::vector < unsigned char > muo_middleLargeHits; //!
    std::vector < unsigned char > muo_outerSmallHits; //!
    std::vector < unsigned char > muo_outerLargeHits; //!
    std::vector < unsigned char > muo_extendedSmallHits; //!
    std::vector < unsigned char > muo_extendedLargeHits; //!
    std::vector < unsigned char > muo_cscEtaHits; //!
    std::vector < unsigned char > muo_cscUnspoiledEtaHits; //!

    // Muon - Holes
    std::vector < unsigned char > muo_innerSmallHoles; //!
    std::vector < unsigned char > muo_innerLargeHoles; //!
    std::vector < unsigned char > muo_middleSmallHoles; //!
    std::vector < unsigned char > muo_middleLargeHoles; //!
    std::vector < unsigned char > muo_outerSmallHoles; //!
    std::vector < unsigned char > muo_outerLargeHoles; //!
    std::vector < unsigned char > muo_extendedSmallHoles; //!
    std::vector < unsigned char > muo_extendedLargeHoles; //!

    // Muon - Other variables
    std::vector < unsigned short > muo_author; //!
    std::vector < unsigned short > muo_allAuthors; //!
    std::vector < unsigned short > muo_muonType; //!
    std::vector < unsigned char > muo_numberOfPrecisionLayers; //!
    std::vector < unsigned char > muo_numberOfPrecisionHoleLayers; //!
    std::vector < unsigned char > muo_quality; //!
    std::vector < unsigned char > muo_energyLossType; //!
    std::vector < float > muo_spectrometerFieldIntegral; //!
    std::vector < float > muo_scatteringCurvatureSignificance; //!
    std::vector < float > muo_scatteringNeighbourSignificance; //!
    std::vector < float > muo_momentumBalanceSignificance; //!
    std::vector < float > muo_segmentDeltaEta; //!
    std::vector < float > muo_CaloLRLikelihood; //!
    std::vector < float > muo_EnergyLoss; //!
    std::vector < int > muo_CaloMuonIDTag; //!
    std::vector < char > muo_DFCommonGoodMuon; //!
    std::vector < char > muo_DFCommonMuonsPreselection; //!

    // Muon - Track particle
    std::vector < float > muo_priTrack_d0; //!
    std::vector < float > muo_priTrack_z0; //!
    std::vector < float > muo_priTrack_d0Sig; //!
    std::vector < float > muo_priTrack_z0Sig; //!
    std::vector < float > muo_priTrack_theta; //!
    std::vector < float > muo_priTrack_qOverP; //!
    std::vector < float > muo_priTrack_vx; //!
    std::vector < float > muo_priTrack_vy; //!
    std::vector < float > muo_priTrack_vz; //!
    std::vector < float > muo_priTrack_phi; //!
    std::vector < float > muo_priTrack_chiSquared; //!
    std::vector < float > muo_priTrack_numberDoF; //!
    std::vector < float > muo_priTrack_radiusOfFirstHit; //!
    std::vector < float > muo_priTrack_trackFitter; //!
    std::vector < float > muo_priTrack_particleHypothesis; //!
    std::vector < int > muo_priTrack_numberOfUsedHitsdEdx; //!
    std::vector < unsigned char > muo_priTrack_numberOfContribPixelLayers; //!
    std::vector < unsigned char > muo_priTrack_numberOfInnermostPixelLayerHits; //!
    std::vector < unsigned char > muo_priTrack_expectInnermostPixelLayerHit; //!
    std::vector < unsigned char > muo_priTrack_numberOfNextToInnermostPixelLayerHits; //!
    std::vector < unsigned char > muo_priTrack_expectNextToInnermostPixelLayerHit; //!
    std::vector < unsigned char > muo_priTrack_numberOfPixelHits; //!
    std::vector < unsigned char > muo_priTrack_numberOfGangedPixels; //!
    std::vector < unsigned char > muo_priTrack_numberOfGangedFlaggedFakes; //!
    std::vector < unsigned char > muo_priTrack_numberOfPixelSpoiltHits; //!
    std::vector < unsigned char > muo_priTrack_numberOfDBMHits; //!
    std::vector < unsigned char > muo_priTrack_numberOfSCTHits; //!
    std::vector < unsigned char > muo_priTrack_numberOfTRTHits; //!
    std::vector < unsigned char > muo_priTrack_numberOfOutliersOnTrack; //!
    std::vector < unsigned char > muo_priTrack_standardDeviationOfChi2OS; //!
    std::vector < float > muo_priTrack_pixeldEdx; //!

    std::vector < float > muo_IDTrack_d0; //!
    std::vector < float > muo_IDTrack_z0; //!
    std::vector < float > muo_IDTrack_d0Sig; //!
    std::vector < float > muo_IDTrack_z0Sig; //!
    std::vector < float > muo_IDTrack_theta; //!
    std::vector < float > muo_IDTrack_qOverP; //!
    std::vector < float > muo_IDTrack_vx; //!
    std::vector < float > muo_IDTrack_vy; //!
    std::vector < float > muo_IDTrack_vz; //!
    std::vector < float > muo_IDTrack_phi; //!
    std::vector < float > muo_IDTrack_chiSquared; //!
    std::vector < float > muo_IDTrack_numberDoF; //!
    std::vector < float > muo_IDTrack_radiusOfFirstHit; //!
    std::vector < float > muo_IDTrack_trackFitter; //!
    std::vector < float > muo_IDTrack_particleHypothesis; //!
    std::vector < int > muo_IDTrack_numberOfUsedHitsdEdx; //!


    // Muon - Equal Values
    std::vector < float > muo_ET_Core; //!
    std::vector < float > muo_ET_EMCore; //!
    std::vector < float > muo_ET_HECCore; //!
    std::vector < float > muo_ET_TileCore; //!
    std::vector < float > muo_FSR_CandidateEnergy; //!
    std::vector < float > muo_InnerDetectorPt; //!
    std::vector < float > muo_MuonSpectrometerPt; //!
    std::vector < unsigned char > muo_combinedTrackOutBoundsPrecisionHits; //!
    std::vector < float > muo_coreMuonEnergyCorrection; //!
    std::vector < float > muo_deltaphi_0; //!
    std::vector < float > muo_deltaphi_1; //!
    std::vector < float > muo_deltatheta_0; //!
    std::vector < float > muo_deltatheta_1; //!
    std::vector < float > muo_etconecoreConeEnergyCorrection; //!
    std::vector < unsigned char > muo_extendedClosePrecisionHits; //!
    std::vector < unsigned char > muo_extendedOutBoundsPrecisionHits; //!
    std::vector < unsigned char > muo_innerClosePrecisionHits; //!
    std::vector < unsigned char > muo_innerOutBoundsPrecisionHits; //!
    std::vector < unsigned char > muo_isEndcapGoodLayers; //!
    std::vector < unsigned char > muo_isSmallGoodSectors; //!
    std::vector < unsigned char > muo_middleClosePrecisionHits; //!
    std::vector < unsigned char > muo_middleOutBoundsPrecisionHits; //!
    std::vector < unsigned int > muo_numEnergyLossPerTrack; //!
    std::vector < unsigned char > muo_numberOfGoodPrecisionLayers; //!
    std::vector < unsigned char > muo_outerClosePrecisionHits; //!
    std::vector < unsigned char > muo_outerOutBoundsPrecisionHits; //!
    std::vector < float > muo_sigmadeltaphi_0; //!
    std::vector < float > muo_sigmadeltaphi_1; //!
    std::vector < float > muo_sigmadeltatheta_0; //!
    std::vector < float > muo_sigmadeltatheta_1; //!
    std::vector < unsigned int > muo_etconeCorrBitset; //!
    std::vector < float > muo_neflowisol20; //!
    std::vector < float > muo_neflowisol30; //!
    std::vector < float > muo_neflowisol40; //!
    std::vector < unsigned int > muo_neflowisolCorrBitset; //!
    std::vector < float > muo_neflowisolcoreConeEnergyCorrection; //!
    std::vector < unsigned int > muo_ptconeCorrBitset; //!
    std::vector < float > muo_ptconecoreTrackPtrCorrection; //!
    std::vector < unsigned int > muo_topoetconeCorrBitset; //!
    std::vector < float > muo_topoetconecoreConeEnergyCorrection; //!

    std::vector < float > muo_CT_EL_Type; //!
    std::vector < float > muo_CT_ET_Core; //!
    std::vector < float > muo_CT_ET_FSRCandidateEnergy; //!
    std::vector < float > muo_CT_ET_LRLikelihood; //!
    std::vector < float > muo_d0_staco; //!
    std::vector < float > muo_phi0_staco; //!
    std::vector < float > muo_qOverPErr_staco; //!
    std::vector < float > muo_qOverP_staco; //!
    std::vector < float > muo_theta_staco; //!
    std::vector < float > muo_z0_staco; //!

    // std::vector < unsigned char > muo_nphiMatchedHitsPerChamberLayer; //!
    // std::vector < unsigned char > muo_nprecMatchedHitsPerChamberLayer; //!
    // std::vector < unsigned char > muo_ntrigEtaMatchedHitsPerChamberLayer; //!
    // std::vector < float > muo_segmentsOnTrack; //!

    float deltaR( const float eta1, const float phi1, const float eta2, const float phi2 ) ; //!
    void clearVectors(); //!

    // All kinds of accessors

    // Truth variables
    const SG::AuxElement::ConstAccessor< int > Accessor_truthType{ "truthType" } ; //!
    const SG::AuxElement::ConstAccessor< int > Accessor_truthOrigin{ "truthOrigin" } ; //!

    // ID information.
    // const SG::AuxElement::ConstAccessor< char > Accessor_LHValue{ "DFCommonElectronsLHValue" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_LHLoose{ "DFCommonElectronsLHLoose" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_LHMedium{ "DFCommonElectronsLHMedium" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_LHTight{ "DFCommonElectronsLHTight" } ; //!
    // const SG::AuxElement::ConstAccessor< unsigned int > Accessor_LHLooseIsEMValue{ "DFCommonElectronsLHLooseIsEMValue" } ; //!
    // const SG::AuxElement::ConstAccessor< unsigned int > Accessor_LHMediumIsEMValue{ "DFCommonElectronsLHMediumIsEMValue" } ; //!
    // const SG::AuxElement::ConstAccessor< unsigned int > Accessor_LHTightIsEMValue{ "DFCommonElectronsLHTightIsEMValue" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_fwdLHLoose{ "DFCommonForwardElectronsLHLoose" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_fwdLHMedium{ "DFCommonForwardElectronsLHMedium" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_fwdLHTight{ "DFCommonForwardElectronsLHTight" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_DFCommonPhotonsIsEMLoose{ "DFCommonPhotonsIsEMLoose" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_DFCommonPhotonsIsEMTight{ "DFCommonPhotonsIsEMTight" } ; //!

    // ID variables
    const SG::AuxElement::ConstAccessor< float > Accessor_Rhad{ "Rhad" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_Reta{ "Reta" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_Rhad1{ "Rhad1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaEta1{ "deltaEta1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_Eratio{ "Eratio" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_Rphi{ "Rphi" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_weta2{ "weta2" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_f3{ "f3" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaPhiRescaled2{ "deltaPhiRescaled2" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_f1{ "f1" } ; //!
    // DAOD
    const SG::AuxElement::ConstAccessor< float > Accessor_d0{ "d0" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_eProbHT{ "eProbHT" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_dPOverP{ "dPOverP" } ; //!

    // Selection variables
    const SG::AuxElement::ConstAccessor< float > Accessor_wtots1{ "wtots1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_EptRatio{ "EptRatio" } ; //! // For DAODs
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfPixelHits{ "numberOfPixelHits" } ; //! //  For DAODs
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfSCTHits{ "numberOfSCTHits" } ; //! //  For DAODs
    const SG::AuxElement::ConstAccessor< int > Accessor_numberOfInnermostPixelHits{ "numberOfInnermostPixelHits" } ; //! //  For DAODs

    // Extra variables
    const SG::AuxElement::ConstAccessor< float > Accessor_nTracks{ "nTracks" } ; //! // For DAODs and EGAM
    const SG::AuxElement::ConstAccessor< float > Accessor_E7x11_Lr3{ "E7x11_Lr3" } ; //! //  For DAODs and EGAM
    const SG::AuxElement::ConstAccessor< float > Accessor_core57cellsEnergyCorrection{ "core57cellsEnergyCorrection" } ; //! //  For DAODs and EGAM

    // Other variables
    const SG::AuxElement::ConstAccessor< float > Accessor_d0Sig{ "d0Sig" } ; //! // For DAODs
    const SG::AuxElement::ConstAccessor< double > Accessor_ECIDSResult{ "DFCommonElectronsECIDSResult" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned short > Accessor_author{ "author" } ; //! // For EGAM

    // Isolation variables
    const SG::AuxElement::ConstAccessor< float > Accessor_etcone20{ "etcone20" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_etcone30{ "etcone30" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_etcone40{ "etcone40" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone20{ "ptcone20" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone30{ "ptcone30" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone40{ "ptcone40" } ; //!

    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone20{ "topoetcone20" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone20ptCorrection{ "topoetcone20ptCorrection" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone30{ "topoetcone20" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetcone40{ "topoetcone40" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_topoetconecoreConeEnergyCorrection{ "topoetconecoreConeEnergyCorrection" } ; //!

    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone20{ "ptvarcone20" } ; //!

    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone30_TightTTVA_pt500{ "ptvarcone30_TightTTVA_pt500" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone20_TightTTVA_pt1000{ "ptvarcone20_TightTTVA_pt1000" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone30_TightTTVA_pt1000{ "ptvarcone30_TightTTVA_pt1000" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone30_TightTTVALooseCone_pt500{ "ptvarcone30_TightTTVALooseCone_pt500" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone20_TightTTVALooseCone_pt1000{ "ptvarcone20_TightTTVALooseCone_pt1000" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone30_TightTTVALooseCone_pt1000{ "ptvarcone30_TightTTVALooseCone_pt1000" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone40_TightTTVALooseCone_pt1000{ "ptvarcone40_TightTTVALooseCone_pt1000" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone20_TightTTVALooseCone_pt500{ "ptcone20_TightTTVALooseCone_pt500" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptcone20_TightTTVALooseCone_pt1000{ "ptcone20_TightTTVALooseCone_pt1000" } ; //!


    // Photon variables
    const SG::AuxElement::ConstAccessor< float > Accessor_DeltaE{ "DeltaE" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_weta1{ "weta1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_fracs1{ "fracs1" } ; //!

    const SG::AuxElement::ConstAccessor< float > Accessor_poscs1{ "poscs1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_poscs2{ "poscs2" } ; //!

    const SG::AuxElement::ConstAccessor< float > Accessor_maxEcell_time{ "maxEcell_time" } ; //! // For DAODs
    const SG::AuxElement::ConstAccessor< float > Accessor_maxEcell_energy{ "maxEcell_energy" } ; //! // For DAODs
    const SG::AuxElement::ConstAccessor< float > Accessor_r33over37allcalo{ "r33over37allcalo" } ; //!


    // Muon variables
    // const SG::AuxElement::ConstAccessor< float > Accessor_DeltaE{ "DeltaE" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_pt{ "pt" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_eta{ "eta" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_phi{ "phi" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_charge{ "charge" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfPrecisionLayers{ "numberOfPrecisionLayers" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfPrecisionHoleLayers{ "numberOfPrecisionHoleLayers" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone30{ "ptvarcone30" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptvarcone40{ "ptvarcone40" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_innerSmallHits{ "innerSmallHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_innerLargeHits{ "innerLargeHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_middleSmallHits{ "middleSmallHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_middleLargeHits{ "middleLargeHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_outerSmallHits{ "outerSmallHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_outerLargeHits{ "outerLargeHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_extendedSmallHits{ "extendedSmallHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_extendedLargeHits{ "extendedLargeHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned short > Accessor_allAuthors{ "allAuthors" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned short > Accessor_muonType{ "muonType" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_quality{ "quality" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_energyLossType{ "energyLossType" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_innerSmallHoles{ "innerSmallHoles" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_innerLargeHoles{ "innerLargeHoles" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_middleSmallHoles{ "middleSmallHoles" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_middleLargeHoles{ "middleLargeHoles" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_outerSmallHoles{ "outerSmallHoles" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_outerLargeHoles{ "outerLargeHoles" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_extendedSmallHoles{ "extendedSmallHoles" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_extendedLargeHoles{ "extendedLargeHoles" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_cscEtaHits{ "cscEtaHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_cscUnspoiledEtaHits{ "cscUnspoiledEtaHits" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_spectrometerFieldIntegral{ "spectrometerFieldIntegral" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_scatteringCurvatureSignificance{ "scatteringCurvatureSignificance" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_scatteringNeighbourSignificance{ "scatteringNeighbourSignificance" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_momentumBalanceSignificance{ "momentumBalanceSignificance" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_segmentDeltaEta{ "segmentDeltaEta" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_CaloLRLikelihood{ "CaloLRLikelihood" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_EnergyLoss{ "EnergyLoss" } ; //!
    const SG::AuxElement::ConstAccessor< int > Accessor_CaloMuonIDTag{ "CaloMuonIDTag" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_DFCommonGoodMuon{ "DFCommonGoodMuon" } ; //!
    const SG::AuxElement::ConstAccessor< char > Accessor_DFCommonMuonsPreselection{ "DFCommonMuonsPreselection" } ; //!

    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfContribPixelLayers{ "numberOfContribPixelLayers" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfInnermostPixelLayerHits{ "numberOfInnermostPixelLayerHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_expectInnermostPixelLayerHit{ "expectInnermostPixelLayerHit" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfNextToInnermostPixelLayerHits{ "numberOfNextToInnermostPixelLayerHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_expectNextToInnermostPixelLayerHit{ "expectNextToInnermostPixelLayerHit" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfGangedPixels{ "numberOfGangedPixels" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfGangedFlaggedFakes{ "numberOfGangedFlaggedFakes" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfPixelSpoiltHits{ "numberOfPixelSpoiltHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfDBMHits{ "numberOfDBMHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfTRTHits{ "numberOfTRTHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfOutliersOnTrack{ "numberOfOutliersOnTrack" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_standardDeviationOfChi2OS{ "standardDeviationOfChi2OS" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_pixeldEdx{ "pixeldEdx" } ; //!

    const SG::AuxElement::ConstAccessor< float > Accessor_ET_Core{ "ET_Core" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ET_EMCore{ "ET_EMCore" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ET_HECCore{ "ET_HECCore" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ET_TileCore{ "ET_TileCore" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_FSR_CandidateEnergy{ "FSR_CandidateEnergy" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_InnerDetectorPt{ "InnerDetectorPt" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_MuonSpectrometerPt{ "MuonSpectrometerPt" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_combinedTrackOutBoundsPrecisionHits{ "combinedTrackOutBoundsPrecisionHits" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_coreMuonEnergyCorrection{ "coreMuonEnergyCorrection" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaphi_0{ "deltaphi_0" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltaphi_1{ "deltaphi_1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltatheta_0{ "deltatheta_0" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_deltatheta_1{ "deltatheta_1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_etconecoreConeEnergyCorrection{ "etconecoreConeEnergyCorrection" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_extendedClosePrecisionHits{ "extendedClosePrecisionHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_extendedOutBoundsPrecisionHits{ "extendedOutBoundsPrecisionHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_innerClosePrecisionHits{ "innerClosePrecisionHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_innerOutBoundsPrecisionHits{ "innerOutBoundsPrecisionHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_isEndcapGoodLayers{ "isEndcapGoodLayers" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_isSmallGoodSectors{ "isSmallGoodSectors" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_middleClosePrecisionHits{ "middleClosePrecisionHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_middleOutBoundsPrecisionHits{ "middleOutBoundsPrecisionHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned int > Accessor_numEnergyLossPerTrack{ "numEnergyLossPerTrack" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_numberOfGoodPrecisionLayers{ "numberOfGoodPrecisionLayers" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_outerClosePrecisionHits{ "outerClosePrecisionHits" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned char > Accessor_outerOutBoundsPrecisionHits{ "outerOutBoundsPrecisionHits" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_sigmadeltaphi_0{ "sigmadeltaphi_0" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_sigmadeltaphi_1{ "sigmadeltaphi_1" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_sigmadeltatheta_0{ "sigmadeltatheta_0" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_sigmadeltatheta_1{ "sigmadeltatheta_1" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned int > Accessor_etconeCorrBitset{ "etconeCorrBitset" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_neflowisol20{ "neflowisol20" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_neflowisol30{ "neflowisol30" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_neflowisol40{ "neflowisol40" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned int > Accessor_neflowisolCorrBitset{ "neflowisolCorrBitset" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_neflowisolcoreConeEnergyCorrection{ "neflowisolcoreConeEnergyCorrection" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned int > Accessor_ptconeCorrBitset{ "ptconeCorrBitset" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_ptconecoreTrackPtrCorrection{ "ptconecoreTrackPtrCorrection" } ; //!
    const SG::AuxElement::ConstAccessor< unsigned int > Accessor_topoetconeCorrBitset{ "topoetconeCorrBitset" } ; //!

    const SG::AuxElement::ConstAccessor< float > Accessor_CT_EL_Type{ "CT_EL_Type" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_CT_ET_Core{ "CT_ET_Core" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_CT_ET_FSRCandidateEnergy{ "CT_ET_FSRCandidateEnergy" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_CT_ET_LRLikelihood{ "CT_ET_LRLikelihood" } ; //!

    const SG::AuxElement::ConstAccessor< float > Accessor_d0_staco{ "d0_staco" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_phi0_staco{ "phi0_staco" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_qOverPErr_staco{ "qOverPErr_staco" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_qOverP_staco{ "qOverP_staco" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_theta_staco{ "theta_staco" } ; //!
    const SG::AuxElement::ConstAccessor< float > Accessor_z0_staco{ "z0_staco" } ; //!

    // const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nphiMatchedHitsPerChamberLayer{ "nphiMatchedHitsPerChamberLayer" } ; //!
    // const SG::AuxElement::ConstAccessor< unsigned char > Accessor_nprecMatchedHitsPerChamberLayer{ "nprecMatchedHitsPerChamberLayer" } ; //!
    // const SG::AuxElement::ConstAccessor< unsigned char > Accessor_ntrigEtaMatchedHitsPerChamberLayer{ "ntrigEtaMatchedHitsPerChamberLayer" } ; //!
    // const SG::AuxElement::ConstAccessor< float > Accessor_segmentsOnTrack{ "segmentsOnTrack" } ; //!
};

#pragma GCC diagnostic pop
#endif

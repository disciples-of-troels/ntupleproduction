#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include "xAODRootAccess/tools/TFileAccessTracer.h"
#include <xAODEventInfo/EventInfo.h>
#include <xAODCore/ShallowCopy.h>
#include "xAODTruth/xAODTruthHelpers.h"
#include "METUtilities/METHelpers.h"
#include "xAODBase/IParticleHelpers.h"

#include "xAODMetaData/FileMetaData.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

MORE_DIAGNOSTICS()

// ----------------------------------------------------------------------------
// Hardcode values for different file outcomes
// ----------------------------------------------------------------------------
// // Hardcoded filetype: EGAM, EGAM1, DAOD, AOD
// const std::string filetype = "AOD";
// const std::string filetype = "EGAM1";
// const std::string filetype = "EGAM3";
// const std::string filetype = "EGAM4";
// const std::string filetype = "EGAM8";
const std::string filetype = "MUON";
// const std::string filetype = "MUONaod";

// const std::string filetype = "DAOD";


// What containers are included - set under initialize, depending on filetype
bool includesEle = true;
bool includesPho = true;
bool includesFwd = true;
bool includesMuo = true;

// ----------------------------------------------------------------------------
// Functions
// ----------------------------------------------------------------------------
// Used to check filetypes
bool startsWith(std::string mainStr, std::string toMatch)
{
	// std::string::find returns 0 if toMatch is found at starting
	if(mainStr.find(toMatch) == 0)
		return true;
	else
		return false;
}

// Used for photon LH Variabes
float getPtAtFirstMeasurement( const xAOD::TrackParticle* tp ) {
    if ( tp == nullptr ) return 0.0f;
    unsigned int index;
    if ( tp->indexOfParameterAtPosition( index, xAOD::FirstMeasurement ) )
        return std::hypot(tp->parameterPX(index), tp->parameterPY(index)); // hypotenuse with overflow safety
    return tp->pt();
}

/// This ptconv is the old one used by MVACalib
float compute_ptconv(const xAOD::Photon* ph) {
    auto vx = ph->vertex();
    if (!vx) return 0.0;

    TLorentzVector sum;
    if (vx->trackParticle(0)) sum += vx->trackParticle(0)->p4();
    if (vx->trackParticle(1)) sum += vx->trackParticle(1)->p4();
    return sum.Perp();
}

float compute_pt1conv(const xAOD::Photon* ph) {
    static const SG::AuxElement::Accessor<float> accPt1("pt1");
    const xAOD::Vertex* vx = ph->vertex();

    if (!vx) return 0.0;
    if (accPt1.isAvailable(*vx)) {
        return accPt1(*vx);
    } else {
        return getPtAtFirstMeasurement(vx->trackParticle(0));
    }
}

float compute_pt2conv(const xAOD::Photon* ph) {
    static const SG::AuxElement::Accessor<float> accPt2("pt2");
    const xAOD::Vertex* vx = ph->vertex();

    if (!vx) return 0.0;
    if (accPt2.isAvailable(*vx)) {
    return accPt2(*vx);
    } else {
    return getPtAtFirstMeasurement(vx->trackParticle(1));
    }
}

float compute_convEtOverPt(const xAOD::Photon* ph) {
    auto cl = ph->caloCluster();

    float Eacc = cl->energyBE(1) + cl->energyBE(2) + cl->energyBE(3);
    float cl_eta = cl->eta();

    float rv = 0.0;
    if (xAOD::EgammaHelpers::numberOfSiTracks(ph) == 2) {
        rv = std::max(0.0f, Eacc/(std::cosh(cl_eta)*compute_ptconv(ph)));
    }
    return std::min(rv, 2.0f);
}

float compute_convPtRatio(const xAOD::Photon* ph) {
    if (xAOD::EgammaHelpers::numberOfSiTracks(ph) == 2) {
        auto pt1 = compute_pt1conv(ph);
        auto pt2 = compute_pt2conv(ph);
        return std::max(pt1, pt2)/(pt1+pt2);
    } else {
        return 1.0f;
    }
}

// // Potential way to get maxEcell - not tested
// float compute_maxEcell_energy(const xAOD::Egamma* egamma) {
//     float emax = -9999.;
//     const xAOD::CaloCluster *cluster = egamma->caloCluster();
//
//     // enten denne, men energy_max giver fejl
//     // error: invalid conversion from 'int' to 'xAOD::CaloCluster_v1::CaloSample' {aka 'CaloSampling::CaloSample'} [-fpermissive]
//     // emax = std::max({cluster->energy_max(2),cluster->energy_max(6)});
//
//     // ellers denne, men vi can ikke access'e CaloCell
//     // if( cluster ){
//     //     // if( !cluster->getCellLinks() ){
//     //     //     ATH_MSG_WARNING("CellLinks not found");
//     //     // }
//     //
//     //     for( const CaloCell* cell : *cluster ){
//     //         int sampling = cell->caloDDE()->getSampling();
//     //         // if( sampling== CaloCell_ID::EMB2 || sampling== CaloCell_ID::EME2 ){
//     //         if( sampling== 2 || sampling== 6 ){
//     //             if( (cell->provenance() & 0x2000) ){
//     //                 if( cell->energy() > emax ) {
//     //                     emax = cell->energy();
//     //                 }
//     //             }
//     //         }
//     //     }
//     // }
//     return emax;
// }


// ----------------------------------------------------------------------------
// Start analysis
// ----------------------------------------------------------------------------

MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
    // Tools
    m_grl ("GoodRunsListSelectionTool/grl", this),
    m_PileupReweighting ("CP::PileupReweightingTool/tool", this),
    m_trigConfig ("TrigConf::xAODConfigTool/xAODConfigTool" ),
    m_trigDecTool ("Trig::TrigDecisionTool/TrigDecisionTool" ),
    m_triggerMatching ( "Trig::MatchingTool" ),
    m_MCShifterTool ( "ElectronPhotonShowerShapeFudgeTool", this),
    m_egammaCalibrationSmearingTool ( "CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool", this),
    // Electron
    m_electronSFReco ( "AsgElectronEfficiencyCorrection", this),
    m_electronSFID ( "AsgElectronEfficiencyCorrection", this),
    m_electronSFIso ( "AsgElectronEfficiencyCorrection", this),
    m_electronLHLoose ( "AsgElectronLikelihoodTool", this),
    m_electronLHMedium ( "AsgElectronLikelihoodTool", this),
    m_electronLHTight ( "AsgElectronLikelihoodTool", this),
    m_ECIDSToolLoose ( "AsgElectronChargeIDSelectorTool", this),
    // Photon
    m_photonSFReco ( "AsgPhotonEfficiencyCorrection", this),
    m_photonSFID ( "AsgPhotonEfficiencyCorrection", this),
    m_photonSFIso ( "AsgPhotonEfficiencyCorrection", this),
    m_photonIsEMLoose ( "AsgPhotonIsEMSelector", this),
    m_photonIsEMTight ( "AsgPhotonIsEMSelector", this),
    // Forward electron
    m_electronSFRecoFwd ( "AsgElectronEfficiencyCorrection", this),
    m_electronSFIDFwd ( "AsgElectronEfficiencyCorrection", this),
    m_electronLHLooseFwd ( "AsgForwardElectronLikelihoodTool", this),
    m_electronLHMediumFwd ( "AsgForwardElectronLikelihoodTool", this),
    m_electronLHTightFwd ( "AsgForwardElectronLikelihoodTool", this),
    // Muon
    m_muonCalibrationSmearingTool ( "CP::MuonCalibrationAndSmearingTool/MuonCalibrationAndSmearingTool", this),
    m_muonSelectionToolLoose ( "CP::MuonSelectionTool", this),

    m_muonSelectionToolMedium ( "CP::MuonSelectionTool", this),
    m_muonSelectionToolTight ( "CP::MuonSelectionTool", this),

    m_muonSF ( "CP::MuonEfficiencyScaleFactors", this),
    // m_muonSFIso ( "CP::MuonEfficiencyScaleFactors", this),
    // Jet og Met
    m_isolationSelectionToolLoose ( "CP::IsolationSelectionTool", this),
    m_jetCalibrationTool ( "JetCalibrationTool", this),
    m_jetCleaningTool ( "JetCleaningTool", this),
    m_metutil ( "MetMaker", this)
    // LGBM
    // m_ele_lgbm_pdfvars ( "LightGBMpredictor", this),
    // m_ele_lgbm_pdfcutvars ( "LightGBMpredictor", this),
    // m_pho_lgbm_pdfvars_data ( "LightGBMpredictor", this),
    // m_pho_lgbm_convvars_data ( "LightGBMpredictor", this),
    // m_pho_lgbm_binvars_data ( "LightGBMpredictor", this),
    // m_pho_lgbm_pdfvars_mc ( "LightGBMpredictor", this),
    // m_pho_lgbm_convvars_mc ( "LightGBMpredictor", this),
    // m_pho_lgbm_binvars_mc ( "LightGBMpredictor", this),
    // m_pho_lgbm_iso ( "LightGBMpredictor", this),
    // // m_pho_lgbm_noconvvars ( "LightGBMpredictor", this) // Uses maxEcell_energy that is not available for AODs
    // // m_pho_lgbm_nocellvars ( "LightGBMpredictor", this), // Uses maxEcell_energy that is not available for AODs
    // // m_pho_lgbm_extvars ( "LightGBMpredictor", this) // Uses maxEcell_energy that is not available for AODs
    // m_fwd_lgbm_pid ( "LightGBMpredictor", this),
    // m_fwd_lgbm_er ( "LightGBMpredictor", this),
    // m_fwd_lgbm_iso ( "LightGBMpredictor", this)
    // m_images ( "ImageCreation", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  // Tools
  declareProperty ("grlTool", m_grl);
  declareProperty ("pileupReweighting", m_PileupReweighting);
  declareProperty ("MCShifterTool", m_MCShifterTool);
  declareProperty ("egammaCalibrationAndSmearingTool", m_egammaCalibrationSmearingTool);
  // Electron
  declareProperty ("electronSFReco", m_electronSFReco);
  declareProperty ("electronSFID", m_electronSFID);
  declareProperty ("electronSFIso", m_electronSFIso);
  declareProperty ("electronLHLoose", m_electronLHLoose);
  declareProperty ("electronLHMedium", m_electronLHMedium);
  declareProperty ("electronLHTight", m_electronLHTight);
  declareProperty ("ECIDSToolLoose", m_ECIDSToolLoose);
  // Photon
  declareProperty ("photonSFReco", m_photonSFReco);
  declareProperty ("photonSFID", m_photonSFID);
  declareProperty ("photonSFIso", m_photonSFIso);
  declareProperty ("photonIsEMLoose", m_photonIsEMLoose);
  declareProperty ("photonIsEMTight", m_photonIsEMTight);
  // Forward electron
  declareProperty ("electronSFRecoFwd", m_electronSFRecoFwd);
  declareProperty ("electronSFIDFwd", m_electronSFIDFwd);
  declareProperty ("electronLHLooseFwd", m_electronLHLooseFwd);
  declareProperty ("electronLHMediumFwd", m_electronLHMediumFwd);
  declareProperty ("electronLHTightFwd", m_electronLHTightFwd);
  // Muon
  declareProperty ("muonCalibrationSmearingTool", m_muonCalibrationSmearingTool);
  declareProperty ("muonSelectionToolLoose", m_muonSelectionToolLoose);
  declareProperty ("muonSelectionToolMedium", m_muonSelectionToolMedium);
  declareProperty ("muonSelectionToolTight", m_muonSelectionToolTight);

  declareProperty ("muonSF", m_muonSF);
  // declareProperty ("muonSFIso", m_muonSFIso);
  // Jet og Met
  declareProperty ("isolationSelectionToolLoose", m_isolationSelectionToolLoose);
  declareProperty ("jetCalibrationTool", m_jetCalibrationTool);
  declareProperty ("jetCleaningTool", m_jetCleaningTool);
  declareProperty ("metutil", m_metutil);
  // LGBM
  // declareProperty ("ele_lgbmPdfVar", m_ele_lgbm_pdfvars);
  // declareProperty ("ele_lgbmPdfCutVar", m_ele_lgbm_pdfcutvars);
  // declareProperty ("pho_lgbmPdfVar_data", m_pho_lgbm_pdfvars_data);
  // declareProperty ("pho_lgbmPdfConvVar_data", m_pho_lgbm_convvars_data);
  // declareProperty ("pho_lgbmPdfBinVar_data", m_pho_lgbm_binvars_data);
  // declareProperty ("pho_lgbmPdfVar_mc", m_pho_lgbm_pdfvars_mc);
  // declareProperty ("pho_lgbmPdfConvVar_mc", m_pho_lgbm_convvars_mc);
  // declareProperty ("pho_lgbmPdfBinVar_mc", m_pho_lgbm_binvars_mc);
  // declareProperty ("pho_lgbmIso", m_pho_lgbm_iso);
  // // declareProperty ("pho_lgbmPdfNoConvVar", m_pho_lgbm_noconvvars); // Uses maxEcell_energy that is not available for AODs
  // // declareProperty ("pho_lgbmPdfNoCellVar", m_pho_lgbm_nocellvars);
  // // declareProperty ("pho_lgbmPdfExtVar", m_pho_lgbm_extvars); // Uses maxEcell_energy that is not available for AODs
  // declareProperty ("fwd_lgbmPid", m_fwd_lgbm_pid);
  // declareProperty ("fwd_lgbmEr", m_fwd_lgbm_er);
  // declareProperty ("fwd_lgbmIso", m_fwd_lgbm_iso);
  // // declareProperty ("imageCreation", m_images);
}


StatusCode MyxAODAnalysis :: initialize ()
{
    if (filetype == "EGAM1"){
		includesEle =  true; includesPho =  true; includesFwd = false; includesMuo = false;
    }else if (filetype == "EGAM3"){
		includesEle =  true; includesPho =  true; includesFwd =  true; includesMuo = false;
	}else if (filetype == "EGAM4"){
		includesEle = false; includesPho =  true; includesFwd = false; includesMuo =  true;
    }else if (filetype == "EGAM8"){
		includesEle =  true; includesPho = false; includesFwd =  true; includesMuo = false;
    }else if (filetype == "MUON"){
		includesEle = false; includesPho = false; includesFwd = false; includesMuo =  true;
    }else if (filetype == "MUONaod"){
		includesEle = false; includesPho = false; includesFwd = false; includesMuo =  true;
    }else if (filetype == "DAOD"){
		includesEle =  true; includesPho =  true; includesFwd =  true; includesMuo = false;
    }else if (filetype == "AOD"){
		includesEle =  true; includesPho =  true; includesFwd =  true; includesMuo = false;
    }

    Info("initialize()","Filetype set to: %s",filetype.c_str());
    Info("initialize()","Data includes electrons: %d",includesEle);
    Info("initialize()","Data includes photons:   %d",includesPho);
    Info("initialize()","Data includes forward:   %d",includesFwd);
    Info("initialize()","Data includes muons:     %d",includesMuo);

    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    ANA_MSG_DEBUG("::initialize: 4x::enableFailure");
    CP::SystematicCode::enableFailure();
    StatusCode::enableFailure();
    xAOD::TReturnCode::enableFailure();
    CP::CorrectionCode::enableFailure();

    ANA_CHECK( requestFileExecute() );
    ANA_MSG_DEBUG("::initialize: xAOD::TFileAccessTracer::enableDataSubmission(false);");
    xAOD::TFileAccessTracer::enableDataSubmission(false);

    // Setup electron multiplicity cutflow histograms
    m_ElectronCutFlows["Init"] = new multiplicityCutFlow;
    m_ElectronCutFlows["OR"] = new multiplicityCutFlow;
    // m_ElectronCutFlows["Pt"] = new multiplicityCutFlow;
    // m_ElectronCutFlows["Eta"] = new multiplicityCutFlow;
    // m_ElectronCutFlows["Iso"] = new multiplicityCutFlow;
    // m_ElectronCutFlows["LHLoose"] = new multiplicityCutFlow;

    // Setup photon multiplicity cutflow histograms
    m_PhotonCutFlows["Init"] = new multiplicityCutFlow;
    m_PhotonCutFlows["OR"] = new multiplicityCutFlow;
    // m_PhotonCutFlows["Pt"] = new multiplicityCutFlow;
    // m_PhotonCutFlows["Eta"] = new multiplicityCutFlow;
    // m_PhotonCutFlows["Iso"] = new multiplicityCutFlow;
    // m_PhotonCutFlows["isPhotonEMLoose"] = new multiplicityCutFlow;

    // Setup fwd electron multiplicity cutflow histograms
    m_FwdCutFlows["Init"] = new multiplicityCutFlow;
    // m_FwdCutFlows["Pt"] = new multiplicityCutFlow;
    // m_FwdCutFlows["Eta"] = new multiplicityCutFlow;
    // m_FwdCutFlows["OQ"] = new multiplicityCutFlow;
    // m_FwdCutFlows["LHMedium"] = new multiplicityCutFlow;

    // Setup muon multiplicity cutflow histograms
    m_MuonCutFlows["Init"] = new multiplicityCutFlow;
    m_MuonCutFlows["OR"] = new multiplicityCutFlow;

    // Initialize cut flow histograms
    ANA_CHECK( createCutFlowHists(m_ElectronCutFlows) );
    ANA_CHECK( createCutFlowHists(m_PhotonCutFlows) );
    ANA_CHECK( createCutFlowHists(m_FwdCutFlows) );
    ANA_CHECK( createCutFlowHists(m_MuonCutFlows) );

    // Initialize histograms for electron kinematics, with and without Iso/ID selection
    ANA_CHECK( book( TH1F ( "h_eta", "h_eta", 50, -2.47, 2.47) ) );
    ANA_CHECK( book( TH1F ( "h_phi", "h_phi", 50, -3.15, 3.15) ) );
    ANA_CHECK( book( TH1F ( "h_et", "h_et", 50, 10, 200) ) );
    ANA_CHECK( book( TH1F ( "h_eta_LHLoose", "h_eta_LHLoose", 50, -2.47, 2.47) ) );
    ANA_CHECK( book( TH1F ( "h_phi_LHLoose", "h_phi_LHLoose", 50, -3.15, 3.15) ) );
    ANA_CHECK( book( TH1F ( "h_et_LHLoose", "h_et_LHLoose", 50, 10, 200) ) );

    // Initialize histograms for fwd electron kinematics, with and without ID selection
    ANA_CHECK( book( TH1F ( "h_fwd_eta", "h_fwd_eta", 50, -4.9, 4.9) ) );
    ANA_CHECK( book( TH1F ( "h_fwd_phi", "h_fwd_phi", 50, -3.15, 3.15) ) );
    ANA_CHECK( book( TH1F ( "h_fwd_et", "h_fwd_et", 50, 20, 200) ) );
    ANA_CHECK( book( TH1F ( "h_fwd_eta_LHMedium", "h_fwd_eta_LHMedium", 50, -4.9, 4.9) ) );
    ANA_CHECK( book( TH1F ( "h_fwd_phi_LHMedium", "h_fwd_phi_LHMedium", 50, -3.15, 3.15) ) );
    ANA_CHECK( book( TH1F ( "h_fwd_et_LHMedium", "h_fwd_et_LHMedium", 50, 20, 200) ) );

    // Initialize histograms for number of leptons saved and the sum of weights histogram
    ANA_CHECK( book( TH1I ( "h_nPhotons", "h_nPhotons", 5, 0, 5) ) );
    // ANA_CHECK( book( TH1I ( "h_nFwdElectrons", "h_nFwdElectrons", 5, 0, 5) ) );
    ANA_CHECK( book( TH1F ( "sumOfWeights", "sumOfWeights", 6, 0, 6) ) );

    // Cutflow histograms for electrons
    ANA_CHECK( book( TH1I ( "h_ElectronCutFlow", "h_ElectronCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_ElectronIndivCutFlow", "h_ElectronIndivCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_ElectronTruthCutFlow", "h_ElectronTruthCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_ElectronIndivTruthCutFlow", "h_ElectronIndivTruthCutFlow", 8, 0, 8) ) );

    // Cutflow histograms for photons
    ANA_CHECK( book( TH1I ( "h_PhotonCutFlow", "h_PhotonCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_PhotonIndivCutFlow", "h_PhotonIndivCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_PhotonTruthCutFlow", "h_PhotonTruthCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_PhotonIndivTruthCutFlow", "h_PhotonIndivTruthCutFlow", 8, 0, 8) ) );

    // Cutflow histograms for fwd electrons
    ANA_CHECK( book( TH1I ( "h_FwdCutFlow", "h_FwdCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_FwdIndivCutFlow", "h_FwdIndivCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_FwdTruthCutFlow", "h_FwdTruthCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_FwdIndivTruthCutFlow", "h_FwdIndivTruthCutFlow", 8, 0, 8) ) );

    // Cutflow histograms for muons
    ANA_CHECK( book( TH1I ( "h_MuonCutFlow", "h_MuonCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_MuonIndivCutFlow", "h_MuonIndivCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_MuonTruthCutFlow", "h_MuonTruthCutFlow", 8, 0, 8) ) );
    ANA_CHECK( book( TH1I ( "h_MuonIndivTruthCutFlow", "h_MuonIndivTruthCutFlow", 8, 0, 8) ) );


    // Set the bin labels to the corresponding selection for photon cutflows
    for ( const auto& hist : {hist( "h_ElectronCutFlow" ), hist( "h_ElectronIndivCutFlow" ), hist( "h_ElectronTruthCutFlow" ), hist( "h_ElectronIndivTruthCutFlow" )}){
    hist->GetXaxis()->SetBinLabel(1, "Init");
    hist->GetXaxis()->SetBinLabel(2, "OR");
    // hist->GetXaxis()->SetBinLabel(3, "Pt");
    // hist->GetXaxis()->SetBinLabel(4, "Eta");
    // hist->GetXaxis()->SetBinLabel(5, "Iso");
    // hist->GetXaxis()->SetBinLabel(6, "LHLoose");
    }

    // Set the bin labels to the corresponding selection for photon cutflows
    for ( const auto& hist : {hist( "h_PhotonCutFlow" ), hist( "h_PhotonIndivCutFlow" ), hist( "h_PhotonTruthCutFlow" ), hist( "h_PhotonIndivTruthCutFlow" )}){
    hist->GetXaxis()->SetBinLabel(1, "Init");
    hist->GetXaxis()->SetBinLabel(2, "OR");
    // hist->GetXaxis()->SetBinLabel(3, "Pt");
    // hist->GetXaxis()->SetBinLabel(4, "Eta");
    // hist->GetXaxis()->SetBinLabel(5, "Iso");
    // hist->GetXaxis()->SetBinLabel(6, "isPhotonEMLoose");
    }

    // Set the bin labels to the corresponding selection for fwd electron cutflows
    for ( const auto& hist : {hist( "h_FwdCutFlow" ), hist( "h_FwdIndivCutFlow" ), hist( "h_FwdTruthCutFlow" ), hist( "h_FwdIndivTruthCutFlow" )}){
    hist->GetXaxis()->SetBinLabel(1, "Init");
    // hist->GetXaxis()->SetBinLabel(2, "Pt");
    // hist->GetXaxis()->SetBinLabel(3, "Eta");
    // hist->GetXaxis()->SetBinLabel(4, "OQ");
    // hist->GetXaxis()->SetBinLabel(5, "LHMedium");
    }

    // Set the bin labels to the corresponding selection for photon cutflows
    for ( const auto& hist : {hist( "h_MuonCutFlow" ), hist( "h_MuonIndivCutFlow" ), hist( "h_MuonTruthCutFlow" ), hist( "h_MuonIndivTruthCutFlow" )}){
    hist->GetXaxis()->SetBinLabel(1, "Init");
    // hist->GetXaxis()->SetBinLabel(2, "OR");
    // hist->GetXaxis()->SetBinLabel(3, "Pt");
    // hist->GetXaxis()->SetBinLabel(4, "Eta");
    }

    // Initialize output tree
    ANA_CHECK( book( TTree ("analysis", "My analysis ntuple") ) );
    TTree* m_myTree = tree ("analysis");
    m_myTree->SetAutoFlush(500);

    // Event level variables
    m_myTree->Branch( "evtNumber", &event_evtNumber );
    m_myTree->Branch( "mcChannelNumber", &event_mcChannelNumber );
    m_myTree->Branch( "runNumber", &event_runNumber );
    m_myTree->Branch( "averageInteractionsPerCrossing", &event_averageInteractionsPerCrossing );
    m_myTree->Branch( "actualInteractionsPerCrossing", &event_actualInteractionsPerCrossing );
    m_myTree->Branch( "correctedScaledAverageMu", &event_correctedScaledAverageMu );
    m_myTree->Branch( "correctedScaledActualMu", &event_correctedScaledActualMu );
    m_myTree->Branch( "MCEventWeight", &event_MCWeight );
    m_myTree->Branch( "crossSectionFactor", &event_crossSection );
    m_myTree->Branch( "eventWeight", &eventWeight );
    m_myTree->Branch( "event_lumiBlock", &event_lumiBlock );
    m_myTree->Branch( "event_bcid", &event_bcid );
    // m_myTree->Branch( "EventTrigger", &trig );
    m_myTree->Branch( "NvtxReco", &NvtxReco );
    m_myTree->Branch( "nPhotons", &nPhotons );
    m_myTree->Branch( "nElectrons", &nElectrons );
    m_myTree->Branch( "nFwdElectrons", &nFwdElectrons );
    m_myTree->Branch( "nMuons", &nMuons );
    m_myTree->Branch( "met_met", &met_met );
    m_myTree->Branch( "met_phi", &met_phi );

	if (includesEle){
	    // Electron variables:
	    // Electron - Truth information
	    m_myTree->Branch( "ele_truthPdgId_egam", &ele_truthPdgId_egam );
	    m_myTree->Branch( "ele_truth_eta_egam", &ele_truth_eta_egam );
	    m_myTree->Branch( "ele_truth_phi_egam", &ele_truth_phi_egam );
	    m_myTree->Branch( "ele_truth_m_egam", &ele_truth_m_egam );
	    m_myTree->Branch( "ele_truth_px_egam", &ele_truth_px_egam );
	    m_myTree->Branch( "ele_truth_py_egam", &ele_truth_py_egam );
	    m_myTree->Branch( "ele_truth_pz_egam", &ele_truth_pz_egam );
	    m_myTree->Branch( "ele_truth_E_egam", &ele_truth_E_egam );
	    m_myTree->Branch( "ele_truthPdgId_atlas", &ele_truthPdgId_atlas );
	    m_myTree->Branch( "ele_truth_eta_atlas", &ele_truth_eta_atlas );
	    m_myTree->Branch( "ele_truth_phi_atlas", &ele_truth_phi_atlas );
	    m_myTree->Branch( "ele_truth_m_atlas", &ele_truth_m_atlas );
	    m_myTree->Branch( "ele_truth_px_atlas", &ele_truth_px_atlas );
	    m_myTree->Branch( "ele_truth_py_atlas", &ele_truth_py_atlas );
	    m_myTree->Branch( "ele_truth_pz_atlas", &ele_truth_pz_atlas );
	    m_myTree->Branch( "ele_truth_E_atlas", &ele_truth_E_atlas );
	    m_myTree->Branch( "ele_egamTruthParticle", &ele_egamTruthParticle );
	    m_myTree->Branch( "ele_truthType", &ele_truthType );
	    m_myTree->Branch( "ele_truthOrigin", &ele_truthOrigin );

	    // Electron - ID Scores
	    m_myTree->Branch( "ele_trigger", &ele_trigger );
	    // m_myTree->Branch( "ele_LHValue", &ele_LHValue );
	    m_myTree->Branch( "ele_LHLoose", &ele_LHLoose );
	    m_myTree->Branch( "ele_LHMedium", &ele_LHMedium );
	    m_myTree->Branch( "ele_LHTight", &ele_LHTight );
	    // m_myTree->Branch( "ele_LHLooseIsEMValue", &ele_LHLooseIsEMValue );
	    // m_myTree->Branch( "ele_LHMediumIsEMValue", &ele_LHMediumIsEMValue );
	    // m_myTree->Branch( "ele_LHTightIsEMValue", &ele_LHTightIsEMValue );

	    // Electron - ID variables information
	    m_myTree->Branch( "ele_Rhad", &ele_Rhad ); // X
	    m_myTree->Branch( "ele_Reta", &ele_Reta ); // X
	    m_myTree->Branch( "ele_Rhad1", &ele_Rhad1 ); // X
	    m_myTree->Branch( "ele_deltaEta1", &ele_deltaEta1 );
	    m_myTree->Branch( "ele_Eratio", &ele_Eratio ); // X
	    m_myTree->Branch( "ele_Rphi", &ele_Rphi ); // X
	    m_myTree->Branch( "ele_TRTPID", &ele_eProbHT );
	    m_myTree->Branch( "ele_weta2", &ele_weta2 ); // X
	    m_myTree->Branch( "ele_dPOverP", &ele_dPOverP );
	    m_myTree->Branch( "ele_f3", &ele_f3 );
	    m_myTree->Branch( "ele_d0", &ele_d0 );
	    m_myTree->Branch( "ele_z0", &ele_z0 );
	    m_myTree->Branch( "ele_deltaPhiRescaled2", &ele_deltaPhiRescaled2 );
	    m_myTree->Branch( "ele_f1", &ele_f1 );

	    // Electron - Binning vars - Kinematic information
	    m_myTree->Branch( "ele_eta", &ele_eta );

	    // Electron - Selection vars
	    m_myTree->Branch( "ele_wtots1", &ele_wtots1 ); // X
	    m_myTree->Branch( "ele_EptRatio", &ele_EptRatio );
	    m_myTree->Branch( "ele_numberOfPixelHits", &ele_numberOfPixelHits );
	    m_myTree->Branch( "ele_numberOfSCTHits", &ele_numberOfSCTHits );
	    m_myTree->Branch( "ele_numberOfInnermostPixelHits", &ele_numberOfInnermostPixelHits );

	    // Electron - Extra Variables
	    m_myTree->Branch( "ele_E7x11_Lr3", &ele_E7x11_Lr3 );
	    m_myTree->Branch( "ele_nTracks", &ele_nTracks );
	    m_myTree->Branch( "ele_core57cellsEnergyCorrection", &ele_core57cellsEnergyCorrection );

	    // Electron - other variables
	    m_myTree->Branch( "ele_d0Sig", &ele_d0Sig );
	    m_myTree->Branch( "ele_z0Sig", &ele_z0Sig );
	    m_myTree->Branch( "ele_ECIDSResult", &ele_ECIDSResult );
	    m_myTree->Branch( "ele_author", &ele_author );
	    m_myTree->Branch( "ele_expectInnermostPixelLayerHit", &ele_expectInnermostPixelLayerHit );
	    m_myTree->Branch( "ele_expectNextToInnermostPixelLayerHit", &ele_expectNextToInnermostPixelLayerHit );
	    m_myTree->Branch( "ele_chiSquared", &ele_chiSquared );
		m_myTree->Branch( "ele_radiusOfFirstHit", &ele_radiusOfFirstHit );
		m_myTree->Branch( "ele_numberOfTRTHits", &ele_numberOfTRTHits );
		m_myTree->Branch( "ele_pixeldEdx", &ele_pixeldEdx );

	    // Electron - other Kinematic information
	    m_myTree->Branch( "ele_e", &ele_e );
	    m_myTree->Branch( "ele_et", &ele_et );
	    m_myTree->Branch( "ele_pt_track", &ele_pt_track );
	    m_myTree->Branch( "ele_phi", &ele_phi );
	    m_myTree->Branch( "ele_m", &ele_m );
	    m_myTree->Branch( "ele_charge", &ele_charge );

	    // Electron - isolation variables
	    m_myTree->Branch( "ele_GradientIso", &ele_GradientIso );
	    m_myTree->Branch( "ele_topoetcone20", &ele_topoetcone20 );
	    m_myTree->Branch( "ele_topoetcone20ptCorrection", &ele_topoetcone20ptCorrection );
	    m_myTree->Branch( "ele_topoetcone40", &ele_topoetcone40 );
	    m_myTree->Branch( "ele_ptvarcone20", &ele_ptvarcone20 );

	    m_myTree->Branch( "ele_ptvarcone30_TightTTVA_pt500", &ele_ptvarcone30_TightTTVA_pt500 );
	    m_myTree->Branch( "ele_ptvarcone20_TightTTVA_pt1000", &ele_ptvarcone20_TightTTVA_pt1000 );
	    m_myTree->Branch( "ele_ptvarcone30_TightTTVA_pt1000", &ele_ptvarcone30_TightTTVA_pt1000 );
	    m_myTree->Branch( "ele_ptvarcone30_TightTTVALooseCone_pt500", &ele_ptvarcone30_TightTTVALooseCone_pt500 );
	    m_myTree->Branch( "ele_ptvarcone20_TightTTVALooseCone_pt1000", &ele_ptvarcone20_TightTTVALooseCone_pt1000 );
	    m_myTree->Branch( "ele_ptvarcone30_TightTTVALooseCone_pt1000", &ele_ptvarcone30_TightTTVALooseCone_pt1000 );
	    m_myTree->Branch( "ele_ptvarcone40_TightTTVALooseCone_pt1000", &ele_ptvarcone40_TightTTVALooseCone_pt1000 );
	    m_myTree->Branch( "ele_ptcone20_TightTTVALooseCone_pt500", &ele_ptcone20_TightTTVALooseCone_pt500 );
	    m_myTree->Branch( "ele_ptcone20_TightTTVALooseCone_pt1000", &ele_ptcone20_TightTTVALooseCone_pt1000 );

	    // Electron - scale factors
	    m_myTree->Branch( "ele_recoSF", &ele_recoSF );
	    m_myTree->Branch( "ele_idSF", &ele_idSF );
	    m_myTree->Branch( "ele_isoSF", &ele_isoSF );
	    m_myTree->Branch( "ele_combSF", &ele_combSF );

	    // Electron - LGBM
	    // m_myTree->Branch( "ele_pdf_score", &ele_pdf_score );
	    // m_myTree->Branch( "ele_pdfconv_score", &ele_pdfconv_score );
	}

	if (includesFwd){
	    // Forward electron variables:
		// fwd electrons truth information
		m_myTree->Branch( "fwd_truthPdgId", &fwd_truthPdgId );
		m_myTree->Branch( "fwd_truthType", &fwd_truthType );
		m_myTree->Branch( "fwd_truthOrigin", &fwd_truthOrigin );
		m_myTree->Branch( "fwd_truthParticle", &fwd_truthParticle );
		m_myTree->Branch( "fwd_truth_eta", &fwd_truth_eta );
		m_myTree->Branch( "fwd_truth_phi", &fwd_truth_phi );
		m_myTree->Branch( "fwd_truth_E", &fwd_truth_E );

	    // fwd electrons ID outputs
	    m_myTree->Branch( "fwd_LHLoose", &fwd_LHLoose );
	    m_myTree->Branch( "fwd_LHMedium", &fwd_LHMedium );
	    m_myTree->Branch( "fwd_LHTight", &fwd_LHTight );

		// fwd Electron LGBM
	  //   m_myTree->Branch( "fwd_LGBM_pid", &fwd_LGBM_pid );
		// m_myTree->Branch( "fwd_LGBM_energy", &fwd_LGBM_energy );
		// m_myTree->Branch( "fwd_LGBM_et", &fwd_LGBM_et );
		// m_myTree->Branch( "fwd_LGBM_iso", &fwd_LGBM_iso );

	    // fwd electrons kinematics
	    m_myTree->Branch( "fwd_eta", &fwd_eta );
	    m_myTree->Branch( "fwd_phi", &fwd_phi );
	    m_myTree->Branch( "fwd_pt", &fwd_pt );
	    m_myTree->Branch( "fwd_et", &fwd_et );

	    // fwd isolation vars
	    m_myTree->Branch( "fwd_topoetcone20", &fwd_topoetcone20 );
	    m_myTree->Branch( "fwd_topoetcone30", &fwd_topoetcone30 );
	    m_myTree->Branch( "fwd_topoetcone40", &fwd_topoetcone40 );
	    m_myTree->Branch( "fwd_topoetconecoreConeEnergyCorrection", &fwd_topoetconecoreConeEnergyCorrection );

	    // fwd electrons kinematics
	    m_myTree->Branch( "fwd_recoSF", &fwd_recoSF );
	    m_myTree->Branch( "fwd_idSF", &fwd_idSF );
	    m_myTree->Branch( "fwd_combSF", &fwd_combSF );

		// fwd electron other variables
		m_myTree->Branch( "fwd_secondLambdaCluster", &fwd_secondLambdaCluster );
		m_myTree->Branch( "fwd_lateralCluster", &fwd_lateralCluster );
		m_myTree->Branch( "fwd_longitudinalCluster", &fwd_longitudinalCluster );
		m_myTree->Branch( "fwd_fracMaxCluster", &fwd_fracMaxCluster );
		m_myTree->Branch( "fwd_secondRCluster", &fwd_secondRCluster );
		m_myTree->Branch( "fwd_centerLambdaCluster", &fwd_centerLambdaCluster );

	}

	if (includesPho){
	    // Photon variables:
	    // Photon ID outputs
	    m_myTree->Branch( "pho_isPhotonEMLoose", &pho_isPhotonEMLoose );
	    m_myTree->Branch( "pho_isPhotonEMTight", &pho_isPhotonEMTight );
	  //   m_myTree->Branch( "pho_pdf_score_data", &pho_pdf_score_data );
	  //   m_myTree->Branch( "pho_pdfconv_score_data", &pho_pdfconv_score_data );
	  //   m_myTree->Branch( "pho_pdfbin_score_data", &pho_pdfbin_score_data );
	  //   m_myTree->Branch( "pho_pdf_logitscore_data", &pho_pdf_logitscore_data );
	  //   m_myTree->Branch( "pho_pdfconv_logitscore_data", &pho_pdfconv_logitscore_data );
	  //   m_myTree->Branch( "pho_pdfbin_logitscore_data", &pho_pdfbin_logitscore_data );
		// m_myTree->Branch( "pho_pdf_score_mc", &pho_pdf_score_mc );
		// m_myTree->Branch( "pho_pdfconv_score_mc", &pho_pdfconv_score_mc );
		// m_myTree->Branch( "pho_pdfbin_score_mc", &pho_pdfbin_score_mc );
		// m_myTree->Branch( "pho_pdf_logitscore_mc", &pho_pdf_logitscore_mc );
		// m_myTree->Branch( "pho_pdfconv_logitscore_mc", &pho_pdfconv_logitscore_mc );
	  //   m_myTree->Branch( "pho_pdfbin_logitscore_mc", &pho_pdfbin_logitscore_mc );
	    // m_myTree->Branch( "pho_pdfnoconv_score", &pho_pdfnoconv_score ); // Uses maxEcell_energy that is not available for AODs
	    // m_myTree->Branch( "pho_pdfnocell_score", &pho_pdfnocell_score );
	    // m_myTree->Branch( "pho_pdfext_score", &pho_pdfext_score ); // Uses maxEcell_energy that is not available for AODs
	    // m_myTree->Branch( "pho_pdfnoconv_logitscore", &pho_pdfnoconv_logitscore ); // Uses maxEcell_energy that is not available for AODs
	    // m_myTree->Branch( "pho_pdfnocell_logitscore", &pho_pdfnocell_logitscore );
	    // m_myTree->Branch( "pho_pdfext_logitscore", &pho_pdfext_logitscore ); // Uses maxEcell_energy that is not available for AODs

	    // Photon truth information
	    m_myTree->Branch( "pho_truthPdgId_egam", &pho_truthPdgId_egam );
	    m_myTree->Branch( "pho_truth_eta_egam", &pho_truth_eta_egam );
	    m_myTree->Branch( "pho_truth_phi_egam", &pho_truth_phi_egam );
	    m_myTree->Branch( "pho_truth_m_egam", &pho_truth_m_egam );
	    m_myTree->Branch( "pho_truth_px_egam", &pho_truth_px_egam );
	    m_myTree->Branch( "pho_truth_py_egam", &pho_truth_py_egam );
	    m_myTree->Branch( "pho_truth_pz_egam", &pho_truth_pz_egam );
	    m_myTree->Branch( "pho_truth_E_egam", &pho_truth_E_egam );
	    m_myTree->Branch( "pho_truthPdgId_atlas", &pho_truthPdgId_atlas );
	    m_myTree->Branch( "pho_truth_eta_atlas", &pho_truth_eta_atlas );
	    m_myTree->Branch( "pho_truth_phi_atlas", &pho_truth_phi_atlas );
	    m_myTree->Branch( "pho_truth_m_atlas", &pho_truth_m_atlas );
	    m_myTree->Branch( "pho_truth_px_atlas", &pho_truth_px_atlas );
	    m_myTree->Branch( "pho_truth_py_atlas", &pho_truth_py_atlas );
	    m_myTree->Branch( "pho_truth_pz_atlas", &pho_truth_pz_atlas );
	    m_myTree->Branch( "pho_truth_E_atlas", &pho_truth_E_atlas );
		m_myTree->Branch( "pho_egamTruthParticle", &pho_egamTruthParticle );
		m_myTree->Branch( "pho_truthType", &pho_truthType );
		m_myTree->Branch( "pho_truthOrigin", &pho_truthOrigin );

	    // Photon kinematics
	    m_myTree->Branch( "pho_e", &pho_e );
	    m_myTree->Branch( "pho_eta", &pho_eta );
	    m_myTree->Branch( "pho_phi", &pho_phi );
	    m_myTree->Branch( "pho_et", &pho_et );

	    // Photon scale factors
	    m_myTree->Branch( "pho_recoSF", &pho_recoSF );
	    m_myTree->Branch( "pho_idSF", &pho_idSF );
	    m_myTree->Branch( "pho_isoSF", &pho_isoSF );
	    m_myTree->Branch( "pho_combSF", &pho_combSF );

	    // Photon LH variables
	    m_myTree->Branch( "pho_Rhad1", &pho_Rhad1 ); // X
	    m_myTree->Branch( "pho_Rhad", &pho_Rhad ); // X
	    m_myTree->Branch( "pho_weta2", &pho_weta2 ); // X
	    m_myTree->Branch( "pho_Rphi", &pho_Rphi ); // X
	    m_myTree->Branch( "pho_Reta", &pho_Reta ); // X
	    m_myTree->Branch( "pho_Eratio", &pho_Eratio ); // X
	    m_myTree->Branch( "pho_wtots1", &pho_wtots1 ); // X
	    m_myTree->Branch( "pho_DeltaE", &pho_DeltaE ); // X
	    m_myTree->Branch( "pho_weta1", &pho_weta1 ); // X ws3
	    m_myTree->Branch( "pho_fracs1", &pho_fracs1 ); // X fside
	    m_myTree->Branch( "pho_f1", &pho_f1 );

	    // Photon Conversion variable
	    m_myTree->Branch( "pho_nTrackPart", &pho_nTrackPart );
	    m_myTree->Branch( "pho_ConversionType", &pho_ConversionType );
	    m_myTree->Branch( "pho_ConversionRadius", &pho_ConversionRadius );
	    m_myTree->Branch( "pho_VertexConvEtOverPt", &pho_VertexConvEtOverPt );
	    m_myTree->Branch( "pho_VertexConvPtRatio", &pho_VertexConvPtRatio );

	    // Photon Extra Variables
	    m_myTree->Branch( "pho_maxEcell_time", &pho_maxEcell_time ); // Only in DAODs!
	    m_myTree->Branch( "pho_maxEcell_energy", &pho_maxEcell_energy ); // Only in DAOD's!
	    m_myTree->Branch( "pho_core57cellsEnergyCorrection", &pho_core57cellsEnergyCorrection );
	    m_myTree->Branch( "pho_r33over37allcalo", &pho_r33over37allcalo );

	    // Photon Isolation variables
	    m_myTree->Branch( "pho_GradientIso", &pho_GradientIso );
	    m_myTree->Branch( "pho_topoetcone20", &pho_topoetcone20 );
	    // m_myTree->Branch( "pho_topoetcone30", &pho_topoetcone30 ); // Not in EGAM
	    m_myTree->Branch( "pho_topoetcone40", &pho_topoetcone40 );
	    m_myTree->Branch( "pho_ptvarcone20", &pho_ptvarcone20 );
	    // m_myTree->Branch( "pho_ptvarcone30", &pho_ptvarcone30 ); // Not in EGAM
	    // m_myTree->Branch( "pho_ptvarcone40", &pho_ptvarcone40 );// Not in EGAM
	    // m_myTree->Branch( "pho_LGBM_iso", &pho_LGBM_iso );
	    // m_myTree->Branch( "pho_LGBM_logitiso", &pho_LGBM_logitiso );

	    // // Additional variables for ER with CNN
	    // m_myTree->Branch( "f0Cluster", &f0Cluster );
	    // m_myTree->Branch( "R12", &R12 );
	    // m_myTree->Branch( "fTG3", &fTG3 );
	    // m_myTree->Branch( "eAccCluster", &eAccCluster );
	    // m_myTree->Branch( "cellIndexCluster", &cellIndexCluster );
	    // m_myTree->Branch( "etaModCalo", &etaModCalo );
	    // m_myTree->Branch( "pho_phiModCalo", &pho_phiModCalo );
	    // m_myTree->Branch( "dPhiTH3", &dPhiTH3 );
	    // m_myTree->Branch( "poscs1", &poscs1 );
	    // m_myTree->Branch( "poscs2", &poscs2 );

	    // // Photon images
	    // // em calo energy
	    // m_myTree->Branch( "em_calo0", &em_calo0 );
	    // m_myTree->Branch( "em_calo1", &em_calo1 );
	    // m_myTree->Branch( "em_calo2", &em_calo2 );
	    // m_myTree->Branch( "em_calo3", &em_calo3 );
	    // m_myTree->Branch( "em_calo4", &em_calo4 );
	    // m_myTree->Branch( "em_calo5", &em_calo5 );
	    // m_myTree->Branch( "em_calo6", &em_calo6 );
	    // m_myTree->Branch( "em_calo7", &em_calo7 );
	    //
	    // // h calo energy
	    // m_myTree->Branch( "h_calo0", &h_calo0 );
	    // m_myTree->Branch( "h_calo1", &h_calo1 );
	    // m_myTree->Branch( "h_calo2", &h_calo2 );
	    // m_myTree->Branch( "h_calo3", &h_calo3 );
	    // m_myTree->Branch( "h_calo4", &h_calo4 );
	    // m_myTree->Branch( "h_calo5", &h_calo5 );
	    // m_myTree->Branch( "h_calo6", &h_calo6 );
	    // m_myTree->Branch( "h_calo7", &h_calo7 );
	}

	if (includesMuo){
		// Muon variables:
		// Muon truth information
		m_myTree->Branch( "muo_truthPdgId", &muo_truthPdgId );
		m_myTree->Branch( "muo_truthType", &muo_truthType );
		m_myTree->Branch( "muo_truthOrigin", &muo_truthOrigin );
		m_myTree->Branch( "muo_truth_eta", &muo_truth_eta );
		m_myTree->Branch( "muo_truth_phi", &muo_truth_phi );
		m_myTree->Branch( "muo_truth_m", &muo_truth_m );
		m_myTree->Branch( "muo_truth_px", &muo_truth_px );
		m_myTree->Branch( "muo_truth_py", &muo_truth_py );
		m_myTree->Branch( "muo_truth_pz", &muo_truth_pz );
		m_myTree->Branch( "muo_truth_E", &muo_truth_E );

		// Muon isolation
		m_myTree->Branch( "muo_etcone20", &muo_etcone20 );
		m_myTree->Branch( "muo_etcone30", &muo_etcone30 );
		m_myTree->Branch( "muo_etcone40", &muo_etcone40 );
		m_myTree->Branch( "muo_ptcone20", &muo_ptcone20 );
		m_myTree->Branch( "muo_ptcone30", &muo_ptcone30 );
		m_myTree->Branch( "muo_ptcone40", &muo_ptcone40 );
		m_myTree->Branch( "muo_ptvarcone20", &muo_ptvarcone20 );
		m_myTree->Branch( "muo_ptvarcone30", &muo_ptvarcone30 );
		m_myTree->Branch( "muo_ptvarcone40", &muo_ptvarcone40 );

		// Muon kinematics
		m_myTree->Branch( "muo_pt", &muo_pt );
		m_myTree->Branch( "muo_eta", &muo_eta );
		m_myTree->Branch( "muo_phi", &muo_phi );
		m_myTree->Branch( "muo_charge", &muo_charge );

		// Muon hits
		m_myTree->Branch( "muo_innerSmallHits", &muo_innerSmallHits );
		m_myTree->Branch( "muo_innerLargeHits", &muo_innerLargeHits );
		m_myTree->Branch( "muo_middleSmallHits", &muo_middleSmallHits );
		m_myTree->Branch( "muo_middleLargeHits", &muo_middleLargeHits );
		m_myTree->Branch( "muo_outerSmallHits", &muo_outerSmallHits );
		m_myTree->Branch( "muo_outerLargeHits", &muo_outerLargeHits );
		m_myTree->Branch( "muo_extendedSmallHits", &muo_extendedSmallHits );
		m_myTree->Branch( "muo_extendedLargeHits", &muo_extendedLargeHits );
		m_myTree->Branch( "muo_cscEtaHits", &muo_cscEtaHits );
		m_myTree->Branch( "muo_cscUnspoiledEtaHits", &muo_cscUnspoiledEtaHits );

		// Muon holes
		m_myTree->Branch( "muo_innerSmallHoles", &muo_innerSmallHoles );
		m_myTree->Branch( "muo_innerLargeHoles", &muo_innerLargeHoles );
		m_myTree->Branch( "muo_middleSmallHoles", &muo_middleSmallHoles );
		m_myTree->Branch( "muo_middleLargeHoles", &muo_middleLargeHoles );
		m_myTree->Branch( "muo_outerSmallHoles", &muo_outerSmallHoles );
		m_myTree->Branch( "muo_outerLargeHoles", &muo_outerLargeHoles );
		m_myTree->Branch( "muo_extendedSmallHoles", &muo_extendedSmallHoles );
		m_myTree->Branch( "muo_extendedLargeHoles", &muo_extendedLargeHoles );

		// Muon other variables
		m_myTree->Branch( "muo_author", &muo_author );
		m_myTree->Branch( "muo_allAuthors", &muo_allAuthors );
		m_myTree->Branch( "muo_muonType", &muo_muonType );
		m_myTree->Branch( "muo_numberOfPrecisionLayers", &muo_numberOfPrecisionLayers );
		m_myTree->Branch( "muo_numberOfPrecisionHoleLayers", &muo_numberOfPrecisionHoleLayers );
		m_myTree->Branch( "muo_quality", &muo_quality );
		m_myTree->Branch( "muo_energyLossType", &muo_energyLossType );
		m_myTree->Branch( "muo_spectrometerFieldIntegral", &muo_spectrometerFieldIntegral );
		m_myTree->Branch( "muo_scatteringCurvatureSignificance", &muo_scatteringCurvatureSignificance );
		m_myTree->Branch( "muo_scatteringNeighbourSignificance", &muo_scatteringNeighbourSignificance );
		m_myTree->Branch( "muo_momentumBalanceSignificance", &muo_momentumBalanceSignificance );
		m_myTree->Branch( "muo_segmentDeltaEta", &muo_segmentDeltaEta );
		m_myTree->Branch( "muo_CaloLRLikelihood", &muo_CaloLRLikelihood );
		m_myTree->Branch( "muo_EnergyLoss", &muo_EnergyLoss );
		m_myTree->Branch( "muo_CaloMuonIDTag", &muo_CaloMuonIDTag );
		m_myTree->Branch( "muo_DFCommonGoodMuon", &muo_DFCommonGoodMuon );
		m_myTree->Branch( "muo_DFCommonMuonsPreselection", &muo_DFCommonMuonsPreselection );
    m_myTree->Branch( "muo_LHLoose", &muo_LHLoose );
    m_myTree->Branch( "muo_LHMedium", &muo_LHMedium );
    m_myTree->Branch( "muo_LHTight", &muo_LHTight );

		// Trigger information
		m_myTree->Branch( "muo_trigger", &muo_trigger );

		// Muon primary track particle
		m_myTree->Branch( "muo_priTrack_d0", &muo_priTrack_d0 );
		m_myTree->Branch( "muo_priTrack_z0", &muo_priTrack_z0 );
		m_myTree->Branch( "muo_priTrack_d0Sig", &muo_priTrack_d0Sig );
		m_myTree->Branch( "muo_priTrack_z0Sig", &muo_priTrack_z0Sig );
		m_myTree->Branch( "muo_priTrack_theta", &muo_priTrack_theta );
		m_myTree->Branch( "muo_priTrack_qOverP", &muo_priTrack_qOverP );
		m_myTree->Branch( "muo_priTrack_vx", &muo_priTrack_vx );
		m_myTree->Branch( "muo_priTrack_vy", &muo_priTrack_vy );
		m_myTree->Branch( "muo_priTrack_vz", &muo_priTrack_vz );
		m_myTree->Branch( "muo_priTrack_phi", &muo_priTrack_phi );
		m_myTree->Branch( "muo_priTrack_chiSquared", &muo_priTrack_chiSquared );
		m_myTree->Branch( "muo_priTrack_numberDoF", &muo_priTrack_numberDoF );
		m_myTree->Branch( "muo_priTrack_radiusOfFirstHit", &muo_priTrack_radiusOfFirstHit );
		m_myTree->Branch( "muo_priTrack_trackFitter", &muo_priTrack_trackFitter );
		m_myTree->Branch( "muo_priTrack_particleHypothesis", &muo_priTrack_particleHypothesis );
		m_myTree->Branch( "muo_priTrack_numberOfUsedHitsdEdx", &muo_priTrack_numberOfUsedHitsdEdx );
		m_myTree->Branch( "muo_priTrack_numberOfContribPixelLayers", &muo_priTrack_numberOfContribPixelLayers );
		m_myTree->Branch( "muo_priTrack_numberOfInnermostPixelLayerHits", &muo_priTrack_numberOfInnermostPixelLayerHits );
		m_myTree->Branch( "muo_priTrack_expectInnermostPixelLayerHit", &muo_priTrack_expectInnermostPixelLayerHit );
		m_myTree->Branch( "muo_priTrack_numberOfNextToInnermostPixelLayerHits", &muo_priTrack_numberOfNextToInnermostPixelLayerHits );
		m_myTree->Branch( "muo_priTrack_expectNextToInnermostPixelLayerHit", &muo_priTrack_expectNextToInnermostPixelLayerHit );
		m_myTree->Branch( "muo_priTrack_numberOfPixelHits", &muo_priTrack_numberOfPixelHits );
		m_myTree->Branch( "muo_priTrack_numberOfGangedPixels", &muo_priTrack_numberOfGangedPixels );
		m_myTree->Branch( "muo_priTrack_numberOfGangedFlaggedFakes", &muo_priTrack_numberOfGangedFlaggedFakes );
		m_myTree->Branch( "muo_priTrack_numberOfPixelSpoiltHits", &muo_priTrack_numberOfPixelSpoiltHits );
		m_myTree->Branch( "muo_priTrack_numberOfDBMHits", &muo_priTrack_numberOfDBMHits );
		m_myTree->Branch( "muo_priTrack_numberOfSCTHits", &muo_priTrack_numberOfSCTHits );
		m_myTree->Branch( "muo_priTrack_numberOfTRTHits", &muo_priTrack_numberOfTRTHits );
		m_myTree->Branch( "muo_priTrack_numberOfOutliersOnTrack", &muo_priTrack_numberOfOutliersOnTrack );
		m_myTree->Branch( "muo_priTrack_standardDeviationOfChi2OS", &muo_priTrack_standardDeviationOfChi2OS );
		m_myTree->Branch( "muo_priTrack_pixeldEdx", &muo_priTrack_pixeldEdx );

		// Muon inner detector track particle
		m_myTree->Branch( "muo_IDTrack_d0", &muo_IDTrack_d0 );
		m_myTree->Branch( "muo_IDTrack_z0", &muo_IDTrack_z0 );
		m_myTree->Branch( "muo_IDTrack_d0Sig", &muo_IDTrack_d0Sig );
		m_myTree->Branch( "muo_IDTrack_z0Sig", &muo_IDTrack_z0Sig );
		m_myTree->Branch( "muo_IDTrack_theta", &muo_IDTrack_theta );
		m_myTree->Branch( "muo_IDTrack_qOverP", &muo_IDTrack_qOverP );
		m_myTree->Branch( "muo_IDTrack_vx", &muo_IDTrack_vx );
		m_myTree->Branch( "muo_IDTrack_vy", &muo_IDTrack_vy );
		m_myTree->Branch( "muo_IDTrack_vz", &muo_IDTrack_vz );
		m_myTree->Branch( "muo_IDTrack_phi", &muo_IDTrack_phi );
		m_myTree->Branch( "muo_IDTrack_chiSquared", &muo_IDTrack_chiSquared );
		m_myTree->Branch( "muo_IDTrack_numberDoF", &muo_IDTrack_numberDoF );
		m_myTree->Branch( "muo_IDTrack_radiusOfFirstHit", &muo_IDTrack_radiusOfFirstHit );
		m_myTree->Branch( "muo_IDTrack_trackFitter", &muo_IDTrack_trackFitter );
		m_myTree->Branch( "muo_IDTrack_particleHypothesis", &muo_IDTrack_particleHypothesis );
		m_myTree->Branch( "muo_IDTrack_numberOfUsedHitsdEdx", &muo_IDTrack_numberOfUsedHitsdEdx );


		m_myTree->Branch( "muo_ET_Core", &muo_ET_Core );
		m_myTree->Branch( "muo_ET_EMCore", &muo_ET_EMCore );
		m_myTree->Branch( "muo_ET_HECCore", &muo_ET_HECCore );
		m_myTree->Branch( "muo_ET_TileCore", &muo_ET_TileCore );
		m_myTree->Branch( "muo_FSR_CandidateEnergy", &muo_FSR_CandidateEnergy );
		m_myTree->Branch( "muo_InnerDetectorPt", &muo_InnerDetectorPt );
		m_myTree->Branch( "muo_MuonSpectrometerPt", &muo_MuonSpectrometerPt );
		m_myTree->Branch( "muo_combinedTrackOutBoundsPrecisionHits", &muo_combinedTrackOutBoundsPrecisionHits );
		m_myTree->Branch( "muo_coreMuonEnergyCorrection", &muo_coreMuonEnergyCorrection );
		m_myTree->Branch( "muo_deltaphi_0", &muo_deltaphi_0 );
		m_myTree->Branch( "muo_deltaphi_1", &muo_deltaphi_1 );
		m_myTree->Branch( "muo_deltatheta_0", &muo_deltatheta_0 );
		m_myTree->Branch( "muo_deltatheta_1", &muo_deltatheta_1 );
		m_myTree->Branch( "muo_etconecoreConeEnergyCorrection", &muo_etconecoreConeEnergyCorrection );
		m_myTree->Branch( "muo_extendedClosePrecisionHits", &muo_extendedClosePrecisionHits );
		m_myTree->Branch( "muo_extendedOutBoundsPrecisionHits", &muo_extendedOutBoundsPrecisionHits );
		m_myTree->Branch( "muo_innerClosePrecisionHits", &muo_innerClosePrecisionHits );
		m_myTree->Branch( "muo_innerOutBoundsPrecisionHits", &muo_innerOutBoundsPrecisionHits );
		m_myTree->Branch( "muo_isEndcapGoodLayers", &muo_isEndcapGoodLayers );
		m_myTree->Branch( "muo_isSmallGoodSectors", &muo_isSmallGoodSectors );
		m_myTree->Branch( "muo_middleClosePrecisionHits", &muo_middleClosePrecisionHits );
		m_myTree->Branch( "muo_middleOutBoundsPrecisionHits", &muo_middleOutBoundsPrecisionHits );
		m_myTree->Branch( "muo_numEnergyLossPerTrack", &muo_numEnergyLossPerTrack );
		m_myTree->Branch( "muo_numberOfGoodPrecisionLayers", &muo_numberOfGoodPrecisionLayers );
		m_myTree->Branch( "muo_outerClosePrecisionHits", &muo_outerClosePrecisionHits );
		m_myTree->Branch( "muo_outerOutBoundsPrecisionHits", &muo_outerOutBoundsPrecisionHits );
		m_myTree->Branch( "muo_sigmadeltaphi_0", &muo_sigmadeltaphi_0 );
		m_myTree->Branch( "muo_sigmadeltaphi_1", &muo_sigmadeltaphi_1 );
		m_myTree->Branch( "muo_sigmadeltatheta_0", &muo_sigmadeltatheta_0 );
		m_myTree->Branch( "muo_sigmadeltatheta_1", &muo_sigmadeltatheta_1 );
		m_myTree->Branch( "muo_etconeCorrBitset", &muo_etconeCorrBitset );
		m_myTree->Branch( "muo_neflowisol20", &muo_neflowisol20 );
		m_myTree->Branch( "muo_neflowisol30", &muo_neflowisol30 );
		m_myTree->Branch( "muo_neflowisol40", &muo_neflowisol40 );
		m_myTree->Branch( "muo_neflowisolCorrBitset", &muo_neflowisolCorrBitset );
		m_myTree->Branch( "muo_neflowisolcoreConeEnergyCorrection", &muo_neflowisolcoreConeEnergyCorrection );
		m_myTree->Branch( "muo_ptconeCorrBitset", &muo_ptconeCorrBitset );
		m_myTree->Branch( "muo_ptconecoreTrackPtrCorrection", &muo_ptconecoreTrackPtrCorrection );
		m_myTree->Branch( "muo_topoetconeCorrBitset", &muo_topoetconeCorrBitset );
		m_myTree->Branch( "muo_topoetconecoreConeEnergyCorrection", &muo_topoetconecoreConeEnergyCorrection );

		m_myTree->Branch( "muo_CT_EL_Type", &muo_CT_EL_Type );
		m_myTree->Branch( "muo_CT_ET_Core", &muo_CT_ET_Core );
		m_myTree->Branch( "muo_CT_ET_FSRCandidateEnergy", &muo_CT_ET_FSRCandidateEnergy );
		m_myTree->Branch( "muo_CT_ET_LRLikelihood", &muo_CT_ET_LRLikelihood );

		m_myTree->Branch( "muo_d0_staco", &muo_d0_staco );
		m_myTree->Branch( "muo_phi0_staco", &muo_phi0_staco );
		m_myTree->Branch( "muo_qOverPErr_staco", &muo_qOverPErr_staco );
		m_myTree->Branch( "muo_qOverP_staco", &muo_qOverP_staco );
		m_myTree->Branch( "muo_theta_staco", &muo_theta_staco );
		m_myTree->Branch( "muo_z0_staco", &muo_z0_staco );

		// m_myTree->Branch( "muo_nphiMatchedHitsPerChamberLayer", &muo_nphiMatchedHitsPerChamberLayer );
		// m_myTree->Branch( "muo_nprecMatchedHitsPerChamberLayer", &muo_nprecMatchedHitsPerChamberLayer );
		// m_myTree->Branch( "muo_ntrigEtaMatchedHitsPerChamberLayer", &muo_ntrigEtaMatchedHitsPerChamberLayer );
		// m_myTree->Branch( "muo_segmentsOnTrack", &muo_segmentsOnTrack );
	}



    //============================================================================
    // Retrieving Tools
    //============================================================================
    ANA_MSG_DEBUG("::initialize: Retrieving Tools");

    // GRL tool
    ANA_CHECK( m_grl.retrieve() );

    // PRW ( change appropiately to year )
    ANA_CHECK( m_PileupReweighting.retrieve() );

    // Initialize and configure trigger tools
    ANA_CHECK ( m_trigConfig.initialize() );
    ANA_CHECK ( m_trigDecTool.setProperty( "ConfigTool", m_trigConfig.getHandle() ) ); // connect the TrigDecTool to the ConfigTool
    ANA_CHECK ( m_trigDecTool.setProperty( "TrigDecisionKey", "xTrigDecision") );
    ANA_CHECK ( m_trigDecTool.initialize() );

    // Trigger matching tool
    ANA_CHECK ( m_triggerMatching.initialize() );

    // Loose Isolation selection tool
    ANA_CHECK( m_isolationSelectionToolLoose.retrieve() );

    // Egamma shower shape fudge tool
    ANA_CHECK( m_MCShifterTool.retrieve() );

    // Photon calibration and smearing tool
    ANA_CHECK( m_egammaCalibrationSmearingTool.retrieve() )

    // Electron reconstruction scale factor tool ( one is needed for all different tpyes(Reco,ID,...) )
    ANA_CHECK( m_electronSFReco.retrieve() );
    ANA_CHECK( m_electronSFID.retrieve() );
    ANA_CHECK( m_electronSFIso.retrieve() );;
    ANA_CHECK( m_electronLHLoose.retrieve() );
    ANA_CHECK( m_electronLHMedium.retrieve() );
    ANA_CHECK( m_electronLHTight.retrieve() );
    ANA_CHECK( m_ECIDSToolLoose.retrieve() );

    // Photon reconstruction scale factor tool ( one is needed for all different tpyes(Reco,ID,...) )
    ANA_CHECK( m_photonSFReco.retrieve() );
    ANA_CHECK( m_photonSFID.retrieve() );
    ANA_CHECK( m_photonSFIso.retrieve() );
    ANA_CHECK( m_photonIsEMLoose.retrieve() );
    ANA_CHECK( m_photonIsEMTight.retrieve() );

    // Forward electron reconstruction scale factor tool ( one is needed for all different tpyes(Reco,ID,...) )
    ANA_CHECK( m_electronSFRecoFwd.retrieve() );
    ANA_CHECK( m_electronSFIDFwd.retrieve() );
    ANA_CHECK( m_electronLHLooseFwd.retrieve() );
    ANA_CHECK( m_electronLHMediumFwd.retrieve() );
    ANA_CHECK( m_electronLHTightFwd.retrieve() );

    // Muon Tools
    ANA_CHECK( m_muonCalibrationSmearingTool.retrieve() );
    ANA_CHECK( m_muonSelectionToolLoose.retrieve() );

    ANA_CHECK( m_muonSelectionToolMedium.retrieve() );
    ANA_CHECK( m_muonSelectionToolTight.retrieve() );
    ANA_CHECK( m_muonSF.retrieve() );
    // ANA_CHECK( m_muonSFIso.retrieve() );

    // Jet Calibration
    ANA_CHECK( m_jetCalibrationTool.retrieve() );
    ANA_CHECK( m_jetCleaningTool.retrieve() );

    // met maker
    ANA_CHECK( m_metutil.retrieve() );

    // LGBM
    // ID for electrons
    // ANA_CHECK( m_ele_lgbm_pdfvars.retrieve() );
    // ANA_CHECK( m_ele_lgbm_pdfcutvars.retrieve() );
    // // ID/ISO for photons
    // ANA_CHECK( m_pho_lgbm_pdfvars_data.retrieve() );
    // ANA_CHECK( m_pho_lgbm_convvars_data.retrieve() );
    // ANA_CHECK( m_pho_lgbm_binvars_data.retrieve() );
    // ANA_CHECK( m_pho_lgbm_pdfvars_mc.retrieve() );
    // ANA_CHECK( m_pho_lgbm_convvars_mc.retrieve() );
    // ANA_CHECK( m_pho_lgbm_binvars_mc.retrieve() );
    // ANA_CHECK( m_pho_lgbm_iso.retrieve() );
    // // ANA_CHECK( m_pho_lgbm_noconvvars.retrieve() );
    // // ANA_CHECK( m_pho_lgbm_nocellvars.retrieve() );
    // // ANA_CHECK( m_pho_lgbm_extvars.retrieve() );
    // // ID/ER for fwd electrons
    // ANA_CHECK( m_fwd_lgbm_pid.retrieve() );
    // ANA_CHECK( m_fwd_lgbm_er.retrieve() );
    // ANA_CHECK( m_fwd_lgbm_iso.retrieve() );

    // Initialize OverlapRemovalTool
    m_orFlags = new ORUtils::ORFlags("OverlapRemovalTool", "selected", "overlaps");
    m_orFlags->doMuons = true;
    m_orFlags->doJets = true;
    m_orFlags->doTaus = false;
    m_orFlags->doPhotons = true;
    m_orFlags->doFatJets = false;
    m_orFlags->doEleEleOR = true;
    m_orToolbox = new ORUtils::ToolBox();
    ANA_CHECK( ORUtils::recommendedTools(*m_orFlags, *m_orToolbox) );
    ANA_CHECK( m_orToolbox->eleEleORT.setProperty("UseClusterMatch", true) );
    ANA_CHECK( m_orToolbox->setGlobalProperty("OutputLevel", 3) );
    ANA_CHECK( m_orToolbox->initialize() );
    m_orTool = m_orToolbox->masterTool;

    // Images - for photon images
    // ANA_CHECK( m_images.retrieve() );

    m_eventCounter = 0;
    Info( "initialize()", "Initialization done" );
    return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis :: fileExecute ()
{
    ANA_MSG_DEBUG("::fileExecute: Running fileExecute...");

    // Get event_isMC for AODs, DAODs and EGAM
    const xAOD::EventInfo* eventInfo = 0;
    ANA_CHECK(evtStore()->retrieve( eventInfo, "EventInfo"));
    event_isMC = eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION);
    Info( "fileExecute()", "File is MC: %d", event_isMC);

    TString dirname = gSystem->Getenv("WorkDir_DIR");

    // // Get event_isMC for DAODs and EGAM
    // const xAOD::FileMetaData* fmld = new xAOD::FileMetaData();
    // float mcProcID = -999;
    // if( inputMetaStore()->retrieve(fmld, "FileMetaData").isSuccess()  ) {
    //     fmld->value( xAOD::FileMetaData::mcProcID, mcProcID);
    // }
    // event_isMC = mcProcID > 1;
    // Info( "fileExecute()", "mcProcID: %f", mcProcID );
    // Info( "fileExecute()", "File is MC: %d", event_isMC);

    if ( event_isMC ){
        // Check if file contains "StreamAOD"
        const std::string dummy = "StreamAOD";
        bool containsDummy = !( inputMetaStore()->contains<std::string>( dummy ) );

        if(containsDummy ){
            const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
            if(!inputMetaStore()->retrieve(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
                Error("initializeEvent()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
                return StatusCode::FAILURE;
            }
            // if ( incompleteCBC->size() != 0 ) {      // Also commented for bhenckel and ehrke
            //   Error("initializeEvent()","Found incomplete Bookkeepers! Check file for corruption.");
            //   return StatusCode::FAILURE;
            // }
            // Now, let's find the actual information
            const xAOD::CutBookkeeperContainer* completeCBC = 0;
            if(!inputMetaStore()->retrieve(completeCBC, "CutBookkeepers").isSuccess()){
                Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
                return StatusCode::FAILURE;
            }

            // First, let's find the smallest cycle number,
            // i.e., the original first processing step/cycle
            int minCycle = 10000;
            for ( auto cbk : *completeCBC ) {
                if ( ! cbk->name().empty()  && minCycle > cbk->cycle() ){ minCycle = cbk->cycle(); }
            }
            // Now, let's actually find the right one that contains all the needed info...
            const xAOD::CutBookkeeper* allEventsCBK=0;
            const xAOD::CutBookkeeper* DxAODEventsCBK=0;
            std::string derivationName = "HIGG1D1Kernel"; // For DAODs - need to replace by appropriate name
            int maxCycle = -1;
            for (const auto& cbk: *completeCBC) {
                if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
                    allEventsCBK = cbk;
                    maxCycle = cbk->cycle();
                }
                if ( cbk->name() == derivationName){
                    DxAODEventsCBK = cbk;
                }
            }

            uint64_t nEventsProcessed  = allEventsCBK->nAcceptedEvents() + hist("sumOfWeights")->GetBinContent(1);
            double sumOfWeights        = allEventsCBK->sumOfEventWeights() + hist("sumOfWeights")->GetBinContent(2);
            double sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared() + hist("sumOfWeights")->GetBinContent(3);
            hist("sumOfWeights")->SetBinContent(1,nEventsProcessed);
            hist("sumOfWeights")->SetBinContent(2,sumOfWeights);
            hist("sumOfWeights")->SetBinContent(3,sumOfWeightsSquared);

            uint64_t nEventsDxAOD;
            double sumOfWeightsDxAOD;
            double sumOfWeightsSquaredDxAOD;
            if (DxAODEventsCBK){
                nEventsDxAOD             = DxAODEventsCBK->nAcceptedEvents() + hist("sumOfWeights")->GetBinContent(4);
                sumOfWeightsDxAOD        = DxAODEventsCBK->sumOfEventWeights() + hist("sumOfWeights")->GetBinContent(5);
                sumOfWeightsSquaredDxAOD = DxAODEventsCBK->sumOfEventWeightsSquared() + hist("sumOfWeights")->GetBinContent(6);
                hist("sumOfWeights")->SetBinContent(4,nEventsDxAOD);
                hist("sumOfWeights")->SetBinContent(5,sumOfWeightsDxAOD);
                hist("sumOfWeights")->SetBinContent(6,sumOfWeightsSquaredDxAOD);
            }
        }

        //Example of loading in the crossSections into a map
        TTree cross_t; cross_t.ReadFile(dirname + "/data/MyAnalysis/my.metadata.txt");
        uint32_t mcchannel=0; double crossSection=0; double kFactor = 0; double filterEff = 0;
        cross_t.SetBranchAddress("dataset_number",&mcchannel);
        cross_t.SetBranchAddress("crossSection",&crossSection);
        cross_t.SetBranchAddress("kFactor",&kFactor);
        cross_t.SetBranchAddress("genFiltEff",&filterEff);
        for(int i=0;i<cross_t.GetEntries();i++) {
            cross_t.GetEntry(i);
            m_crossSections[mcchannel] = crossSection;
            m_kFactors[mcchannel] = kFactor;
            m_filterEffs[mcchannel] = filterEff;
        }
    }

    return StatusCode::SUCCESS;
}

StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.


  // Print processing rate at set interval, so we know where we are
  ++m_eventCounter;
  if ( m_eventCounter == 1) m_startTime = time(0);
  if ( m_eventCounter % m_progressInterval == 0 ) {
  	 Info("execute()","%li events processed so far  <<<==", m_eventCounter );
	 Info("execute()","Processing rate = %f Hz", float(m_eventCounter)/float(time(0)-m_startTime));
  }
  ANA_MSG_DEBUG("::execute: Event " << m_eventCounter);

  // retrieve the eventInfo object from the event store
  ANA_CHECK (evtStore()->retrieve (m_eventInfo, "EventInfo"));

  //============================================================================
  // Event cleaning
  //============================================================================
  ANA_MSG_DEBUG("::execute: Event Cleaning");

  ANA_MSG_DEBUG("::execute: Detector errors?...");
  // detector errors
  if (
  ( m_eventInfo->errorState( xAOD::EventInfo::LAr )  == xAOD::EventInfo::Error )
  || ( m_eventInfo->errorState( xAOD::EventInfo::Tile ) == xAOD::EventInfo::Error )
  || ( m_eventInfo->errorState( xAOD::EventInfo::SCT )  == xAOD::EventInfo::Error )
  || ( m_eventInfo->isEventFlagBitSet( xAOD::EventInfo::Core, 18 ) )
  )
  return StatusCode::SUCCESS;

  ANA_MSG_DEBUG("::execute: if data check if event passes GRL...");
  // if data check if event passes GRL
  if (!event_isMC) {
      if (!m_grl->passRunLB(*m_eventInfo)) {
          return StatusCode::SUCCESS;
      }
  }


  //============================================================================
  // Jet cleaning and Retrieving container
  //============================================================================
  ANA_MSG_DEBUG("::execute: if filetype!=AOD check DFCommonJets_eventClean_LooseBad");
  if ( !((filetype == "AOD") or (filetype == "MUONaod")) ){
      if ( m_eventInfo->auxdata<char>("DFCommonJets_eventClean_LooseBad") == 0 ){
          return StatusCode::SUCCESS;
      }
  }

  // retrieve jet container
  const xAOD::JetContainer* jets_org = 0;
  if (startsWith(filetype, "EGAM")){
      ANA_MSG_DEBUG("::execute: Retrieving AntiKt4EMTopoJets...");
      ANA_CHECK( evtStore()->retrieve( jets_org, "AntiKt4EMTopoJets" ) );
  } else if (filetype == "MUON"){
      ANA_MSG_DEBUG("::execute: Retrieving AntiKt4EMTopoJets...");
      ANA_CHECK( evtStore()->retrieve( jets_org, "AntiKt4EMTopoJets" ) );
  } else if (!startsWith(filetype, "EGAM")){
      ANA_MSG_DEBUG("::execute: Retrieving AntiKt4EMPFlowJets...");
      ANA_CHECK( evtStore()->retrieve( jets_org, "AntiKt4EMPFlowJets" ) );
  }

  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets_org );
  m_jets = jets_shallowCopy.first;
  // std::unique_ptr< xAOD::JetContainer > jets_p( jets_shallowCopy.first );
  // std::unique_ptr< xAOD::ShallowAuxContainer > jets_p2( jets_shallowCopy.second );

  if ((filetype == "AOD") or (filetype == "MUONaod")){
      for (const auto& jet : *m_jets) {
        if (!m_jetCleaningTool->keep(*jet) ) {
          return StatusCode::SUCCESS;
        }
      }
  }

  //============================================================================
  // Retrieving containers
  //============================================================================
  ANA_MSG_DEBUG("::execute: Retrieving Containers");
  // retrieve photon container
  ANA_MSG_DEBUG("::execute: Retrieving photons...");
  const xAOD::PhotonContainer* photons_org = 0;
  ANA_CHECK( evtStore()->retrieve( photons_org, "Photons" ) );
  std::pair< xAOD::PhotonContainer*, xAOD::ShallowAuxContainer* > photons_shallowCopy = xAOD::shallowCopyContainer( *photons_org );
  m_photons = photons_shallowCopy.first;
  // std::unique_ptr< xAOD::PhotonContainer > photons_p( photons_shallowCopy.first );
  // std::unique_ptr< xAOD::ShallowAuxContainer > photons_p2( photons_shallowCopy.second );

  // retrieve electron container
  ANA_MSG_DEBUG("::execute: Retrieving electrons...");
  const xAOD::ElectronContainer* electrons_org = 0;
  ANA_CHECK( evtStore()->retrieve( electrons_org, "Electrons" ) );
  std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > electrons_shallowCopy = xAOD::shallowCopyContainer( *electrons_org );
  m_electrons = electrons_shallowCopy.first;
  // std::unique_ptr< xAOD::ElectronContainer > electrons_p( electrons_shallowCopy.first );
  // std::unique_ptr< xAOD::ShallowAuxContainer > electrons_p2( electrons_shallowCopy.second );

  // retrieve fwd electron container
  if (includesFwd){
      ANA_MSG_DEBUG("::execute: Retrieving fwd electrons...");
      const xAOD::ElectronContainer* fwdElectrons_org = 0;
      ANA_CHECK( evtStore()->retrieve( fwdElectrons_org, "ForwardElectrons" ) );
      std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > fwdElectrons_shallowCopy = xAOD::shallowCopyContainer( *fwdElectrons_org );
      m_fwdElectrons = fwdElectrons_shallowCopy.first;
      // std::unique_ptr< xAOD::ElectronContainer > fwdElectrons_p( fwdElectrons_shallowCopy.first );
      // std::unique_ptr< xAOD::ShallowAuxContainer > fwdElectrons_p2( fwdElectrons_shallowCopy.second );
  }

  // retrieve vertices
  ANA_MSG_DEBUG("::execute: Retrieving PrimaryVertices...");
  const xAOD::VertexContainer* vertex_org = 0;
  ANA_CHECK( evtStore()->retrieve( vertex_org, "PrimaryVertices") );
  std::pair< xAOD::VertexContainer*, xAOD::ShallowAuxContainer* > vertex_shallowCopy = xAOD::shallowCopyContainer( *vertex_org );
  m_vertex = vertex_shallowCopy.first;
  std::unique_ptr< xAOD::VertexContainer > vertices_p( vertex_shallowCopy.first );
  std::unique_ptr< xAOD::ShallowAuxContainer > vertices_p2( vertex_shallowCopy.second );

  // retrieve Tracks
  ANA_MSG_DEBUG("::execute: Retrieving tracks...");
  const xAOD::TrackParticleContainer* indet_tracks_org = 0;
  ANA_CHECK( evtStore()->retrieve( indet_tracks_org, "InDetTrackParticles" ) );
  std::pair< xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer* > indet_tracks_shallowCopy = xAOD::shallowCopyContainer( *indet_tracks_org );
  m_indetTracks.reset( indet_tracks_shallowCopy.first );
  m_indetTracksAux.reset( indet_tracks_shallowCopy.second );

  // retrieve muon container
  ANA_MSG_DEBUG("::execute: Retrieving muons...");
  const xAOD::MuonContainer* muons_org = 0;
  ANA_CHECK( evtStore()->retrieve( muons_org, "Muons" ) );
  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons_org );
  m_muons = muons_shallowCopy.first;
  // std::unique_ptr< xAOD::MuonContainer > muons_p( muons_shallowCopy.first );
  // std::unique_ptr< xAOD::ShallowAuxContainer > muons_p2( muons_shallowCopy.second );

  if ( event_isMC ){
	  if ( !startsWith(filetype, "MUON") ){
	      ANA_MSG_DEBUG("::execute: Retrieving egammaTruthParticles...");
	      ANA_CHECK( evtStore()->retrieve( m_egammaTruthContainer, "egammaTruthParticles" ) );

		  ANA_MSG_DEBUG("::execute: Retrieving TruthParticles...");
		  ANA_CHECK( evtStore()->retrieve( m_truthParticles, "TruthParticles" ) );
	  }
	  else if ( startsWith(filetype, "MUON") ){
		  ANA_MSG_DEBUG("::execute: Retrieving MuonTruthParticles...");
		  ANA_CHECK( evtStore()->retrieve( m_truthParticles, "MuonTruthParticles" ) );
	  }
  }

  // Missing et stuff
  met::addGhostMuonsToJets(*muons_org, *m_jets);
  m_primaryVertex = nullptr;
  for ( const auto& vtx : *m_vertex ){
      if ( vtx->vertexType() == xAOD::VxType::PriVtx) {
          m_primaryVertex = vtx;
      }
  }
  // Basically the same as the above.
  // if ( m_primaryVertex->vertexType() != xAOD::VxType::PriVtx) {
  //     return StatusCode::SUCCESS;
  // }

  //============================================================================
  // Calibrations and Corrections
  //============================================================================
  ANA_MSG_DEBUG("::execute: Calibrations and Corrections");
  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: CalibrationSmearing Photons");
  if ( !startsWith(filetype, "MUON") ){
	  for ( const auto& pho : *m_photons ) {
	      if ( event_isMC ) {
	          // Apply fudge factors to photons
	          if ( m_MCShifterTool->applyCorrection( *pho ) != CP::CorrectionCode::Ok ) {
	              ANA_MSG_WARNING( "execute(): Photon correction failed on an object.");
	          }
	      }
	      // apply energy corrections to photons
	      if( m_egammaCalibrationSmearingTool->applyCorrection( *pho ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Photon calibration failed" );
	  }

	  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: CalibrationSmearing Electrons");
	  for ( const auto& el : *m_electrons ) {
	      if ( event_isMC) {
	          // Apply fudge factors to electrons
	          if ( m_MCShifterTool->applyCorrection( *el ) != CP::CorrectionCode::Ok ) {
	              ANA_MSG_WARNING( "execute(): Electron correction failed on an object.");
	          }
	      }
	      // apply energy corrections to electrons
	      if( m_egammaCalibrationSmearingTool->applyCorrection( *el ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Electron calibration failed" );
	  }
  }

  if (includesFwd){
  	  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: CalibrationSmearing Forward Electrons");
      for ( const auto& fel : *m_fwdElectrons ){
          // apply energy corrections to fwd electrons
          if( m_egammaCalibrationSmearingTool->applyCorrection( *fel ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Fwd Electron calibration failed" );
      }
  }

  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: CalibrationSmearing Muons");
  for ( const auto& mu : *m_muons ) {
      // apply energy corrections to muons
      if( m_muonCalibrationSmearingTool->applyCorrection( *mu ) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Muon calibration failed" );
  }

  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: CalibrationSmearing Jets");
  for ( const auto& jet : *m_jets ) {
      // apply energy corrections to jets
      if( m_jetCalibrationTool->applyCorrection( *jet) != CP::CorrectionCode::Ok ) ANA_MSG_WARNING( "execute(): Jet calibration failed" );
  }

  if ( !startsWith(filetype, "MUON") ){
	  if (filetype == "AOD"){
	      ANA_MSG_DEBUG("::execute: Retrieving MET_Core_AntiKt4EMPFlow...");
	      ANA_CHECK( evtStore()->retrieve(m_coreMet, "MET_Core_AntiKt4EMPFlow") );
	  } else if (filetype != "AOD"){
	      ANA_MSG_DEBUG("::execute: Retrieving MET_Core_AntiKt4EMTopo...");
	      ANA_CHECK( evtStore()->retrieve(m_coreMet, "MET_Core_AntiKt4EMTopo") );
	  }
  }

  // needed for MET_Maker
  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: Object links");
  if(!xAOD::setOriginalObjectLink(*photons_org, *m_photons)){//tell calib container what old container it matches
      std::cout << "Failed to set the original object links" << std::endl;
      return 1;
  }
  if(!xAOD::setOriginalObjectLink(*jets_org, *m_jets)){//tell calib container what old container it matches
      std::cout << "Failed to set the original object links" << std::endl;
      return 1;
  }
  if(!xAOD::setOriginalObjectLink(*electrons_org, *m_electrons)){//tell calib container what old container it matches
      std::cout << "Failed to set the original object links" << std::endl;
      return 1;
  }
  if(!xAOD::setOriginalObjectLink(*muons_org, *m_muons)){//tell calib container what old container it matches
      std::cout << "Failed to set the original object links" << std::endl;
      return 1;
  }

  // Select objects which should be used in the overlap removal
  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: Objects for overlap removal");
  const ort::inputDecorator_t selectDec("selected");
  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: Objects for overlap removal: photons");
  for ( auto pho : *m_photons ){
	  if ( !startsWith(filetype, "MUON") ){
	      if ( pho->pt() > 10000 && abs( pho->caloCluster()->etaBE(2) ) < 2.47){
	          selectDec(*pho) = true;
	      }
	      else{
	          selectDec(*pho) = false;
	      }
	  } else if ( startsWith(filetype, "MUON") ){
		  selectDec(*pho) = false;
	  }
  }

  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: Objects for overlap removal: electrons");
  for ( auto el : *m_electrons ){
	  if ( !startsWith(filetype, "MUON") ){
	      if ( el->pt() > 10000 && abs( el->caloCluster()->etaBE(2) ) < 2.47){
	          selectDec(*el) = true;
	      }
	      else{
	          selectDec(*el) = false;
	      }
	  } else if ( startsWith(filetype, "MUON") ){
		  selectDec(*el) = false;
	  }
  }

  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: Objects for overlap removal: muons");
  for ( auto mu : *m_muons ){
      if ( mu->pt() > 10000 && abs( mu->eta() ) < 2.7 ){
          selectDec(*mu) = true;
      }
      else{
          selectDec(*mu) = false;
      }
  }

  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: Objects for overlap removal: jets");
  for ( auto jet : *m_jets ){
      if ( jet->pt() > 20000 && abs( jet->eta() ) < 2.7 ){
          selectDec(*jet) = true;
      }
      else{
          selectDec(*jet) = false;
      }
  }

  // record the calibrated objects and their shallow copies ( needed by MET_Maker )
  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: Calibration objects");
  ANA_CHECK( evtStore()->record(m_photons, "CalibPhotons") );
  ANA_CHECK( evtStore()->record(photons_shallowCopy.second, "CalibPhotonsAux") );
  ANA_CHECK( evtStore()->record(m_jets, "CalibJets") );
  ANA_CHECK( evtStore()->record(jets_shallowCopy.second, "CalibJetsAux") );
  ANA_CHECK( evtStore()->record(m_muons, "CalibMuons") );
  ANA_CHECK( evtStore()->record(muons_shallowCopy.second, "CalibMuonsAux") );
  ANA_CHECK( evtStore()->record(m_electrons, "CalibElectrons") );
  ANA_CHECK( evtStore()->record(electrons_shallowCopy.second, "CalibElectronsAux") );

  if ( !startsWith(filetype, "MUON") ){

	  //retrieve the MET association map
	  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: MET assicoation map");
	  if (!startsWith(filetype, "EGAM")){
	      ANA_CHECK( evtStore()->retrieve(m_metMap, "METAssoc_AntiKt4EMPFlow") )
	  } else if (startsWith(filetype, "EGAM")){
	      ANA_CHECK( evtStore()->retrieve(m_metMap, "METAssoc_AntiKt4EMTopo") );
	  }
	  m_metMap->resetObjSelectionFlags();

	  // Create a MissingETContainer with its aux store for each systematic
	  m_newMetContainer    = new xAOD::MissingETContainer();
	  m_newMetAuxContainer = new xAOD::MissingETAuxContainer();
	  m_newMetContainer->setStore(m_newMetAuxContainer);
	  ANA_CHECK( evtStore()->record(m_newMetContainer, "newMETContainer") );
	  ANA_CHECK( evtStore()->record(m_newMetAuxContainer, "newMETContainerAux") );

	  m_metMap->resetObjSelectionFlags();

	  //Photons
	  ConstDataVector<xAOD::PhotonContainer> metPhotons(SG::VIEW_ELEMENTS);
	  for(const auto& pho : *m_photons) {
	      if (filetype != "AOD"){
	          if(pho->pt()>10e3 && pho->eta()<2.47 && Accessor_DFCommonPhotonsIsEMLoose( *pho ) ) metPhotons.push_back(pho);
	      } else if (filetype == "AOD"){
	          if(pho->pt()>10e3 && pho->eta()<2.47 && m_photonIsEMLoose->accept( pho ) ) metPhotons.push_back(pho);
	      }
	  }
	  ANA_CHECK( m_metutil->rebuildMET("RefGamma",             //name of metPhoton in metContainer
	                          xAOD::Type::Photon,            //telling the rebuilder that this is electron met
	                          m_newMetContainer,             //filling this met container
	                          metPhotons.asDataVector(),   //using these metElectrons that accepted our cuts
	                          m_metMap)                      //and this association map
	                          );

	  //Electrons
	  ConstDataVector<xAOD::ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);
	  for(const auto& el : *m_electrons) {
	      if (filetype != "AOD"){
	          if(el->pt()>10e3 && el->eta()<2.47 && Accessor_LHLoose( *el ) ) metElectrons.push_back(el);
	      } else if (filetype == "AOD"){
	          if(el->pt()>10e3 && el->eta()<2.47 && m_electronLHLoose->accept( el ) ) metElectrons.push_back(el);
	      }
	  }
	  ANA_CHECK( m_metutil->rebuildMET("RefEle",                   //name of metElectrons in metContainer
	                          xAOD::Type::Electron,       //telling the rebuilder that this is electron met
	                          m_newMetContainer,            //filling this met container
	                          metElectrons.asDataVector(),//using these metElectrons that accepted our cuts
	                          m_metMap)                     //and this association map
	                          );

	  //Muons
	  ConstDataVector<xAOD::MuonContainer> metMuons(SG::VIEW_ELEMENTS);
	  for(const auto& mu : *m_muons) {
	      if(mu->pt()>10e3 && mu->eta()<2.7 && m_muonSelectionToolLoose->accept( *mu ) ) metMuons.push_back(mu);
	  }

	  ANA_CHECK( m_metutil->rebuildMET("RefMuon",
	                                  xAOD::Type::Muon,
	                                  m_newMetContainer,
	                                  metMuons.asDataVector(),
	                                  m_metMap)
	                                  );

	  ANA_MSG_DEBUG("::execute: Rebuild jetMET...");
	  //Now time to rebuild jetMet and get the soft term
	  //This adds the necessary soft term for both CST and TST
	  //these functions create an xAODMissingET object with the given names inside the container
	  ANA_CHECK( m_metutil->rebuildJetMET("RefJet",        //name of jet met
	                                 "SoftClus",      //name of soft cluster term met
	                                 "PVSoftTrk",     //name of soft track term met
	                                 m_newMetContainer, //adding to this new met container
	                                 m_jets,          //using this jet collection to calculate jet met
	                                 m_coreMet,         //core met container
	                                 m_metMap,          //with this association map
	                                 false            //don't apply jet jvt cut
	                                 )
				);

	  ANA_CHECK( m_metutil->buildMETSum("FinalClus", m_newMetContainer, MissingETBase::Source::LCTopo) );
	  m_METmaker = ( *m_newMetContainer )["FinalClus"];


	  // Get missing energy
	  met_met = m_METmaker->met();
	  met_phi = m_METmaker->phi();

  }

  // Do overlap removal
  ANA_MSG_DEBUG("::execute: Calibrations and Corrections: Overlap removal");
  ANA_CHECK ( m_orTool->removeOverlaps( m_electrons, m_muons, m_jets, nullptr, m_photons) );

  // Event specific variables
  ANA_MSG_DEBUG("::execute: Event specific variables");
  if ( event_isMC ){
      event_mcChannelNumber = m_eventInfo->mcChannelNumber();
      event_MCWeight = m_eventInfo->mcEventWeight();
      event_crossSection = m_crossSections[event_mcChannelNumber] * m_kFactors[event_mcChannelNumber] * m_filterEffs[event_mcChannelNumber] ;
  }
  event_evtNumber = m_eventInfo->eventNumber();
  event_runNumber = m_eventInfo->runNumber();
  event_averageInteractionsPerCrossing = m_eventInfo->averageInteractionsPerCrossing();
  event_actualInteractionsPerCrossing = m_eventInfo->actualInteractionsPerCrossing();
  event_lumiBlock = m_eventInfo->lumiBlock();
  event_bcid = m_eventInfo->bcid();

  // ######## Get correct pileup file ########
  ANA_CHECK(m_PileupReweighting->apply(*m_eventInfo));
  event_pileupweight = m_eventInfo->auxdecor<float>("PileupWeight");

  event_correctedScaledAverageMu       = m_PileupReweighting->getCorrectedAverageInteractionsPerCrossing( *m_eventInfo, true );
  event_correctedScaledActualMu        = m_PileupReweighting->getCorrectedActualInteractionsPerCrossing( *m_eventInfo, true );

  NvtxReco = m_vertex->size();

  // Clear the electron, fwd electron, and muon vectors
  clearVectors();

  ANA_MSG_DEBUG("::execute: Do preselection...");
  // Do preselection of electrons, fwd electrons, and muons
  std::vector < const xAOD::Photon* > selPhotons;
  nPhotons = -999;
  if (includesPho){
      selPhotons = selectPhotonCandidates();
      nPhotons = selPhotons.size();
  }

  std::vector < const xAOD::Electron* > selElectrons;
  nElectrons = -999;
  if (includesEle){
      selElectrons = selectElectronCandidates();
      nElectrons = selElectrons.size();
  }

  std::vector < const xAOD::Muon* > selMuons;
  nMuons = -999;
  if (includesMuo){
      selMuons = selectMuonCandidates();
      nMuons = selMuons.size();
  }

  std::vector < const xAOD::Electron* > selFwdElectrons;
  nFwdElectrons = -999;
  if (includesFwd){
      selFwdElectrons = selectFwdElectronCandidates();
      nFwdElectrons = selFwdElectrons.size();
  }

  // if enough particles left, save information
  //if ( nPhotons + nFwdElectrons >= 2 ){
  // if ( nPhotons >= 2){

  eventWeight = event_pileupweight;

  if ( event_isMC ){
      eventWeight = event_MCWeight * event_crossSection * event_pileupweight;
  }

  // HISTOGRAMS
  // fill histograms with number of particles
  hist("h_nPhotons")->Fill( nPhotons, eventWeight );
  // hist("h_nFwdElectrons")->Fill( nFwdElectrons, eventWeight );

  // Fill the electron vectors
  if (includesEle){
	  ANA_MSG_DEBUG("::execute: Filling electron vectors...");
	  bool debug_FillingElectronVectors = false;
	  for ( const xAOD::Electron* ele : selElectrons ){
	      const TLorentzVector& ele_p4 = ele->p4();
	      const xAOD::CaloCluster* ele_cluster = ele->caloCluster();
	      const xAOD::TrackParticle* ele_trackP = ele->trackParticle();

		  if (ele_trackP == nullptr){
			  Warning("execute()","Electron in event %li has trackParticle()=0. Continue to next electron.", m_eventCounter);
			  continue;
		  }

	      // Electron - Truth information
	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: Truth information push back...");}
	      if ( event_isMC ){
	          const xAOD::TruthParticle* tp_egam = nullptr;
			  bool truthEgParticle = false;
			  for ( size_t i = 0; i<ele_TruthPointerIdx.size(); i++){
				  if ( ele->index() == ele_TruthPointerIdx[i] ){
					  truthEgParticle = true;
					  tp_egam = ele_TruthPointer[i];
					  break;
				  }
			  }
			  ele_egamTruthParticle.push_back( truthEgParticle );

	          if ( tp_egam == nullptr ){
	              ele_truthPdgId_egam.push_back( 0 );
	              ele_truth_eta_egam.push_back( -999.0 );
	              ele_truth_phi_egam.push_back( -999.0 );
	              ele_truth_m_egam.push_back( -999.0 );
	              ele_truth_px_egam.push_back( -999.0 );
	              ele_truth_py_egam.push_back( -999.0 );
	              ele_truth_pz_egam.push_back( -999.0 );
	              ele_truth_E_egam.push_back( 0 );
	          }
	          else{
	              ele_truthPdgId_egam.push_back( tp_egam->pdgId() );
	              ele_truth_eta_egam.push_back( tp_egam->eta() );
	              ele_truth_phi_egam.push_back( tp_egam->phi() );
	              ele_truth_m_egam.push_back( tp_egam->m() / 1000. );
	              ele_truth_px_egam.push_back( tp_egam->px() / 1000. );
	              ele_truth_py_egam.push_back( tp_egam->py() / 1000. );
	              ele_truth_pz_egam.push_back( tp_egam->pz() / 1000. );
	              ele_truth_E_egam.push_back( tp_egam->e() / 1000. );
	          }

	          const xAOD::TruthParticle* tp_atlas = xAOD::TruthHelpers::getTruthParticle( *ele );
			  if ( tp_atlas == nullptr ){
	              ele_truthPdgId_atlas.push_back( 0 );
	              ele_truth_eta_atlas.push_back( -999.0 );
	              ele_truth_phi_atlas.push_back( -999.0 );
	              ele_truth_m_atlas.push_back( -999.0 );
	              ele_truth_px_atlas.push_back( -999.0 );
	              ele_truth_py_atlas.push_back( -999.0 );
	              ele_truth_pz_atlas.push_back( -999.0 );
	              ele_truth_E_atlas.push_back( 0 );
	          }
	          else{
	              ele_truthPdgId_atlas.push_back( tp_atlas->pdgId() );
	              ele_truth_eta_atlas.push_back( tp_atlas->eta() );
	              ele_truth_phi_atlas.push_back( tp_atlas->phi() );
	              ele_truth_m_atlas.push_back( tp_atlas->m() / 1000. );
	              ele_truth_px_atlas.push_back( tp_atlas->px() / 1000. );
	              ele_truth_py_atlas.push_back( tp_atlas->py() / 1000. );
	              ele_truth_pz_atlas.push_back( tp_atlas->pz() / 1000. );
	              ele_truth_E_atlas.push_back( tp_atlas->e() / 1000. );
	          }

	          ele_truthType.push_back( Accessor_truthType( *ele ) );
	          ele_truthOrigin.push_back( Accessor_truthOrigin( *ele ) );

	      }

	      // Electron - ID information
	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: ID information push back...");}
	      // Triger information
	      std::vector<const xAOD::IParticle*> myParticles;
	      myParticles.push_back( ele );
	      ele_trigger.push_back( m_triggerMatching->match( myParticles, "HLT_e26_lhtight_nod0_ivarloose", 0.07, false ) ||
	                          m_triggerMatching->match( myParticles, "HLT_e60_lhmedium_nod0", 0.07, false ) ||  // included for scale factors
	                          m_triggerMatching->match( myParticles, "HLT_e140_lhloose_nod0", 0.07, false ) );  // included for scale factors
	      // ele_trigger.push_back( false );

	      if (filetype == "AOD"){
	          // ele_LHValue.push_back( Accessor_LHValue( *ele ) ); // For EGAM - Gives problems
	          ele_LHLoose.push_back( m_electronLHLoose->accept( ele ) );
	          ele_LHMedium.push_back( m_electronLHMedium->accept( ele ) );
	          ele_LHTight.push_back( m_electronLHTight->accept( ele ) );
	          // ele_LHLooseIsEMValue.push_back( 999 );
	          // ele_LHMediumIsEMValue.push_back( 999 );
	          // ele_LHTightIsEMValue.push_back( 999 );
	      } else if (startsWith(filetype, "EGAM")){
	          ele_LHLoose.push_back( Accessor_LHLoose( *ele ) );
	          ele_LHMedium.push_back( Accessor_LHMedium( *ele ) );
	          ele_LHTight.push_back( Accessor_LHTight( *ele ) );
	          // ele_LHLooseIsEMValue.push_back( Accessor_LHLooseIsEMValue( *ele ) );
	          // ele_LHMediumIsEMValue.push_back( Accessor_LHMediumIsEMValue( *ele ) );
	          // ele_LHTightIsEMValue.push_back( Accessor_LHTightIsEMValue( *ele ) );
	      } else if (filetype == "DAOD"){
	          // ele_LHValue.push_back( Accessor_LHValue( *ele ) );
	          ele_LHLoose.push_back( Accessor_LHLoose( *ele ) );
	          ele_LHMedium.push_back( Accessor_LHMedium( *ele ) );
	          ele_LHTight.push_back( Accessor_LHTight( *ele ) );
	          // ele_LHLooseIsEMValue.push_back( 999 );
	          // ele_LHMediumIsEMValue.push_back( 999 );
	          // ele_LHTightIsEMValue.push_back( 999 );
	      }

	      // Electron - ID variables information "LH variables"
	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: ID variables push back...");}
	      ele_Rhad.push_back( Accessor_Rhad1( *ele ) );
	      ele_Reta.push_back( Accessor_Reta( *ele ) );
	      ele_Rhad1.push_back( Accessor_Rhad( *ele ) );
	      ele_deltaEta1.push_back( Accessor_deltaEta1( *ele ) );
	      ele_Eratio.push_back( Accessor_Eratio( *ele ) );
	      ele_Rphi.push_back( Accessor_Rphi( *ele ) );
	      ele_weta2.push_back( Accessor_weta2( *ele ) );
	      ele_f3.push_back( Accessor_f3( *ele ) );
	      ele_deltaPhiRescaled2.push_back( Accessor_deltaPhiRescaled2( *ele ) );
	      ele_f1.push_back( Accessor_f1( *ele ) );

	      if (filetype != "DAOD"){
	          ele_eProbHT.push_back( ele->trackParticleSummaryFloatValue(xAOD::eProbabilityHT) );
	          ele_dPOverP.push_back( -999.0 );
	          ele_d0.push_back( ele_trackP->d0() );
	          ele_z0.push_back( ele_trackP->z0() );
	      } else if (filetype == "DAOD"){
	          ele_eProbHT.push_back( Accessor_eProbHT( *ele ) );
	          ele_dPOverP.push_back( Accessor_dPOverP( *ele ) );
	          ele_d0.push_back( Accessor_d0( *ele ) );
	          ele_z0.push_back( -999.);
	      }

	      // Electron - Binning vars - Kinematic information
	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: Binning variables push back...");}
	      ele_eta.push_back( ele_cluster->etaBE(2) );

	      // Electron - Selection vars
	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: Selection variables push back...");}
	      ele_wtots1.push_back( Accessor_wtots1( *ele ) );

	      if (filetype != "DAOD"){
	          ele_EptRatio.push_back( -999.0 );
	          ele_numberOfPixelHits.push_back( ele->trackParticleSummaryIntValue(xAOD::numberOfPixelHits) );
	          ele_numberOfSCTHits.push_back( ele->trackParticleSummaryIntValue(xAOD::numberOfSCTHits) );
	          ele_numberOfInnermostPixelHits.push_back( ele->trackParticleSummaryIntValue(xAOD::numberOfInnermostPixelLayerHits) );
	      }else if (filetype == "DAOD"){
	          ele_EptRatio.push_back( Accessor_EptRatio( *ele ) );
	          ele_numberOfPixelHits.push_back( Accessor_numberOfPixelHits( *ele ) );
	          ele_numberOfSCTHits.push_back( Accessor_numberOfSCTHits( *ele ) );
	          ele_numberOfInnermostPixelHits.push_back( Accessor_numberOfInnermostPixelHits( *ele ) );
	      }

	      // Electron - Extra Variables
	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: Extra variables push back...");}
	      if (filetype == "DAOD"){
	          ele_nTracks.push_back( Accessor_nTracks( *ele ) );
	          ele_E7x11_Lr3.push_back( Accessor_E7x11_Lr3( *ele ) );
	          ele_core57cellsEnergyCorrection.push_back( Accessor_core57cellsEnergyCorrection( *ele ) );
	      } else if (filetype == "AOD"){
	          ele_nTracks.push_back( ele->nTrackParticles() );
	          ele_E7x11_Lr3.push_back( -999.0 );
	          ele_core57cellsEnergyCorrection.push_back( -999.0 );
	      } else if (startsWith(filetype, "EGAM")){
	          ele_nTracks.push_back( ele->nTrackParticles() );
	          ele_E7x11_Lr3.push_back( Accessor_E7x11_Lr3( *ele ) );
	          ele_core57cellsEnergyCorrection.push_back( Accessor_core57cellsEnergyCorrection( *ele ) );
	      }

	      // Electron - other variables
	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: Other variables push back...");}
	      if (startsWith(filetype, "EGAM")){
	          ele_d0Sig.push_back( std::sqrt( ele_trackP->definingParametersCovMatrixVec()[0])  );
		      ele_z0Sig.push_back( std::sqrt( ele_trackP->definingParametersCovMatrixVec()[1])  );
	          ele_ECIDSResult.push_back( Accessor_ECIDSResult( *ele ) );
	          ele_author.push_back( Accessor_author( *ele ) );
	          ele_expectInnermostPixelLayerHit.push_back( ele->trackParticleSummaryIntValue(xAOD::expectInnermostPixelLayerHit) );
	          ele_expectNextToInnermostPixelLayerHit.push_back( ele->trackParticleSummaryIntValue(xAOD::expectNextToInnermostPixelLayerHit) );
	          ele_chiSquared.push_back( ele_trackP->chiSquared() );
	          ele_radiusOfFirstHit.push_back( ele_trackP->radiusOfFirstHit() );
	          ele_numberOfTRTHits.push_back( ele->trackParticleSummaryIntValue(xAOD::numberOfTRTHits) );
	          ele_pixeldEdx.push_back( ele_trackP->auxdata<float>("pixeldEdx") );
	      }else if (filetype == "DAOD"){
	          ele_d0Sig.push_back( Accessor_d0Sig( *ele ) );
		      ele_z0Sig.push_back( -999.0  );
	          ele_ECIDSResult.push_back( -999.0 );
	          ele_author.push_back( 999 );
	          ele_expectInnermostPixelLayerHit.push_back( -999 );
	          ele_expectNextToInnermostPixelLayerHit.push_back( -999 );
	          ele_chiSquared.push_back( -999.0 );
			  ele_radiusOfFirstHit.push_back( -999.0 );
			  ele_numberOfTRTHits.push_back( -999.0 );
			  ele_pixeldEdx.push_back( -999.0 );
	      }else if (filetype == "AOD"){
	          ele_d0Sig.push_back( std::sqrt( ele_trackP->definingParametersCovMatrixVec()[0])  );
		      ele_z0Sig.push_back( std::sqrt( ele_trackP->definingParametersCovMatrixVec()[1])  );
	          ele_ECIDSResult.push_back( m_ECIDSToolLoose->calculate( ele ) );
	          // ele_ECIDSResult.push_back( -999.0 );
	          ele_author.push_back( 999 );
	          ele_expectInnermostPixelLayerHit.push_back( -999 );
	          ele_expectNextToInnermostPixelLayerHit.push_back( -999 );
	          ele_chiSquared.push_back( -999.0 );
			  ele_radiusOfFirstHit.push_back( -999.0 );
			  ele_numberOfTRTHits.push_back( -999.0 );
			  ele_pixeldEdx.push_back( -999.0 );
	      }

	      // Electron - other Kinematic information
	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: Other kinematics information push back...");}
	      ele_e.push_back( ele->e() / 1000. ); // Fra cluster
	      ele_et.push_back( ele_p4.Pt() / 1000. );
	      ele_pt_track.push_back( ele_trackP->pt() / 1000. );
	      // ele_pt_obj.push_back( ele->pt() / 1000. );
	      // ele_et_calo.push_back( ele_cluster->pt() / 1000. );
	      ele_phi.push_back( ele_p4.Phi() );
	      ele_m.push_back( ele->m() / 1000.);
	      ele_charge.push_back( ele->charge() );

	      // Electron - Isolation variables
	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: Isolation variables push back...");}
	      ele_GradientIso.push_back( m_isolationSelectionToolLoose->accept( *ele ) );
	      ele_topoetcone20.push_back( Accessor_topoetcone20( *ele ) );
	      ele_topoetcone20ptCorrection.push_back( Accessor_topoetcone20ptCorrection( *ele ) );
	      ele_topoetcone40.push_back( Accessor_topoetcone40( *ele ) );
	      ele_ptvarcone20.push_back( Accessor_ptvarcone20( *ele ) );

	      if (filetype != "AOD"){
	          ele_ptvarcone30_TightTTVA_pt500.push_back( Accessor_ptvarcone30_TightTTVA_pt500( *ele ) );
	          ele_ptvarcone20_TightTTVA_pt1000.push_back( Accessor_ptvarcone20_TightTTVA_pt1000( *ele ) );
	          ele_ptvarcone30_TightTTVA_pt1000.push_back( Accessor_ptvarcone30_TightTTVA_pt1000( *ele ) );

			  double ptvarcone30_TightTTVALooseCone_pt500 = -999.0;
			  double ptvarcone20_TightTTVALooseCone_pt1000 = -999.0;
			  double ptvarcone30_TightTTVALooseCone_pt1000 = -999.0;
			  double ptvarcone40_TightTTVALooseCone_pt1000 = -999.0;
			  double ptcone20_TightTTVALooseCone_pt500 = -999.0;
			  double ptcone20_TightTTVALooseCone_pt1000 = -999.0;

			  if ( Accessor_ptvarcone30_TightTTVALooseCone_pt500.isAvailable( *ele ) ) ptvarcone30_TightTTVALooseCone_pt500 = Accessor_ptvarcone30_TightTTVALooseCone_pt500( *ele );
			  if ( Accessor_ptvarcone20_TightTTVALooseCone_pt1000.isAvailable( *ele ) ) ptvarcone20_TightTTVALooseCone_pt1000 = Accessor_ptvarcone20_TightTTVALooseCone_pt1000( *ele );
			  if ( Accessor_ptvarcone30_TightTTVALooseCone_pt1000.isAvailable( *ele ) ) ptvarcone30_TightTTVALooseCone_pt1000 = Accessor_ptvarcone30_TightTTVALooseCone_pt1000( *ele );
			  if ( Accessor_ptvarcone40_TightTTVALooseCone_pt1000.isAvailable( *ele ) ) ptvarcone40_TightTTVALooseCone_pt1000 = Accessor_ptvarcone40_TightTTVALooseCone_pt1000( *ele );
			  if ( Accessor_ptcone20_TightTTVALooseCone_pt500.isAvailable( *ele ) ) ptcone20_TightTTVALooseCone_pt500 = Accessor_ptcone20_TightTTVALooseCone_pt500( *ele );
			  if ( Accessor_ptcone20_TightTTVALooseCone_pt1000.isAvailable( *ele ) ) ptcone20_TightTTVALooseCone_pt1000 = Accessor_ptcone20_TightTTVALooseCone_pt1000( *ele );

			  ele_ptvarcone30_TightTTVALooseCone_pt500.push_back( ptvarcone30_TightTTVALooseCone_pt500 );
			  ele_ptvarcone20_TightTTVALooseCone_pt1000.push_back( ptvarcone20_TightTTVALooseCone_pt1000 );
			  ele_ptvarcone30_TightTTVALooseCone_pt1000.push_back( ptvarcone30_TightTTVALooseCone_pt1000 );
			  ele_ptvarcone40_TightTTVALooseCone_pt1000.push_back( ptvarcone40_TightTTVALooseCone_pt1000 );
			  ele_ptcone20_TightTTVALooseCone_pt500.push_back( ptcone20_TightTTVALooseCone_pt500 );
			  ele_ptcone20_TightTTVALooseCone_pt1000.push_back( ptcone20_TightTTVALooseCone_pt1000 );

	      } else if (filetype == "AOD"){
	          ele_ptvarcone30_TightTTVA_pt500.push_back( -999.0 );
	          ele_ptvarcone20_TightTTVA_pt1000.push_back( -999.0 );
	          ele_ptvarcone30_TightTTVA_pt1000.push_back( -999.0 );
	          ele_ptvarcone30_TightTTVALooseCone_pt500.push_back( -999.0 );
	          ele_ptvarcone20_TightTTVALooseCone_pt1000.push_back( -999.0 );
	          ele_ptvarcone30_TightTTVALooseCone_pt1000.push_back( -999.0 );
	          ele_ptvarcone40_TightTTVALooseCone_pt1000.push_back( -999.0 );
	          ele_ptcone20_TightTTVALooseCone_pt500.push_back( -999.0 );
	          ele_ptcone20_TightTTVALooseCone_pt1000.push_back( -999.0 );
	      }

	      // Scale factors
	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: Scale factors calculation...");}
	      double recoSf = 1;
	      double IdSf = 1;
	      double IsoSf = 1;

	      if ( event_isMC ){
	          if(!m_electronSFReco->getEfficiencyScaleFactor( *ele, recoSf)){
	              ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
	          }
	          if(!m_electronSFID->getEfficiencyScaleFactor( *ele, IdSf)){
	              ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
	          }
	          if(!m_electronSFIso->getEfficiencyScaleFactor( *ele, IsoSf)){
	              ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
	          }
	      }

	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: Scale factors push back...");}
	      ele_recoSF.push_back( recoSf );
	      ele_idSF.push_back( IdSf );
	      ele_isoSF.push_back( IsoSf );
	      ele_combSF.push_back( recoSf * IdSf * IsoSf );

	      // LGBM
	      // if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: ID information calculation...");}
	      // ele_pdf_score.push_back( m_ele_lgbm_pdfvars->predict( *ele ) );
	      // ele_pdfconv_score.push_back( m_ele_lgbm_pdfcutvars->predict( *ele ) );


	      // Fill kinematics histograms
	      if (debug_FillingElectronVectors) {ANA_MSG_DEBUG("::execute: Electrons: Filling kinematics histograms...");}
	      if ( event_isMC ){
	          hist( "h_eta" )->Fill( ele_cluster->etaBE(2), eventWeight * ele_combSF.back() );
	          hist( "h_phi" )->Fill( ele->phi(), eventWeight * ele_combSF.back() );
	          hist( "h_et" )->Fill( ele_p4.Pt() / 1000., eventWeight * ele_combSF.back() );
	          if (filetype != "AOD"){
	              if ( Accessor_LHLoose( *ele ) && m_isolationSelectionToolLoose->accept( *ele ) ){
	                  hist( "h_eta_LHLoose" )->Fill( ele_cluster->etaBE(2), eventWeight * ele_combSF.back() );
	                  hist( "h_phi_LHLoose" )->Fill( ele->phi(), eventWeight * ele_combSF.back() );
	                  hist( "h_et_LHLoose" )->Fill( ele_p4.Pt() / 1000., eventWeight * ele_combSF.back() );
	              }
	          } else if (filetype != "AOD"){
	              if ( m_electronLHLoose->accept( ele ) && m_isolationSelectionToolLoose->accept( *ele ) ){
	                  hist( "h_eta_LHLoose" )->Fill( ele_cluster->etaBE(2), eventWeight * ele_combSF.back() );
	                  hist( "h_phi_LHLoose" )->Fill( ele->phi(), eventWeight * ele_combSF.back() );
	                  hist( "h_et_LHLoose" )->Fill( ele_p4.Pt() / 1000., eventWeight * ele_combSF.back() );
	              }
	          }
	      }
	      else{
	          hist( "h_eta" )->Fill( ele_cluster->etaBE(2) );
	          hist( "h_phi" )->Fill( ele_p4.Phi() );
	          hist( "h_et" )->Fill( ele_p4.Pt() / 1000. );

	          if (filetype != "AOD"){
	              if ( Accessor_LHLoose( *ele ) && m_isolationSelectionToolLoose->accept( *ele ) ){
	                  hist( "h_eta_LHLoose" )->Fill( ele_cluster->etaBE(2) );
	                  hist( "h_phi_LHLoose" )->Fill( ele_p4.Phi() );
	                  hist( "h_et_LHLoose" )->Fill( ele_p4.Pt() / 1000. );
	              }
	          } else if (filetype == "AOD"){
	              if ( m_electronLHLoose->accept( ele ) && m_isolationSelectionToolLoose->accept( *ele ) ){
	                  hist( "h_eta_LHLoose" )->Fill( ele_cluster->etaBE(2) );
	                  hist( "h_phi_LHLoose" )->Fill( ele_p4.Phi() );
	                  hist( "h_et_LHLoose" )->Fill( ele_p4.Pt() / 1000. );
	              }
	          }
	      }
	  }
  }

  // Fill the photons vectors
  if (includesPho){
	  ANA_MSG_DEBUG("::execute: Filling photon vectors...");
	  bool debug_FillingPhotonVectors = true;
	  for ( const xAOD::Photon* pho : selPhotons ){
	  	  // ANA_MSG_DEBUG("::execute: Photon pointer:");
		  // std::cout << pho << std::endl;

	      const TLorentzVector& pho_p4 = pho->p4();
	      const xAOD::CaloCluster* pho_cluster = pho->caloCluster();

		  // Check for bad trackParticles in verticies that will cause segmentation violation
		  const xAOD::Vertex* vertex = pho->vertex();
		  if ( vertex != nullptr ) {
			  bool badtrack = false;
			  for (size_t tracki = 0; tracki < vertex->nTrackParticles(); ++tracki){
				  if (vertex->trackParticle(tracki) == nullptr){
					  badtrack = true;
					  break;
				  }
			  }
			  if (badtrack) {
				  Warning("execute()","Photon in event %li has a trackParticle()=0 from vertex. Continue to next photon.", m_eventCounter);
				  continue;
			  }
		  }

	      if (filetype == "EGAM1") {
	          pho_maxEcell_time.push_back( -999.0 );
	          pho_maxEcell_energy.push_back( -999.0 );
	      } else if (startsWith(filetype, "EGAM")){
	          pho_maxEcell_time.push_back( Accessor_maxEcell_time( *pho ) );
	          pho_maxEcell_energy.push_back( Accessor_maxEcell_energy( *pho ) );
	      } else if (filetype == "DAOD") {
	          pho_maxEcell_time.push_back( Accessor_maxEcell_time( *pho ) );
	          pho_maxEcell_energy.push_back( Accessor_maxEcell_energy( *pho ) );
	      } else if (filetype == "AOD") {
	          pho_maxEcell_time.push_back( -999.0 );
	          pho_maxEcell_energy.push_back( -999.0 );
	          // pho_maxEcell_energy.push_back( compute_maxEcell_energy( *pho ) );
	          // pho->auxdata< float >( "maxEcell_energy" ) = pho_maxEcell_energy;
	      }

	      // ID information
	      // if (debug_FillingPhotonVectors) {ANA_MSG_DEBUG("::execute: Photon: ID information calculation...");}
	      // double pho_pdf_data     = m_pho_lgbm_pdfvars_data->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
	      // double pho_pdfconv_data = m_pho_lgbm_convvars_data->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
	      // double pho_pdfbin_data  = m_pho_lgbm_binvars_data->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
	      // double pho_pdf_mc     = m_pho_lgbm_pdfvars_mc->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
	      // double pho_pdfconv_mc = m_pho_lgbm_convvars_mc->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
	      // double pho_pdfbin_mc  = m_pho_lgbm_binvars_mc->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
	      // if (filetype != "AOD"){
	      //     pho_pdfnoconv  = m_pho_lgbm_noconvvars->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
	      //     pho_pdfnocell  = m_pho_lgbm_nocellvars->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
	      //     pho_pdfext  =  m_pho_lgbm_extvars->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
	      // }

		  // if (debug_FillingPhotonVectors) {ANA_MSG_DEBUG("::execute: Photon: ID information Push back...");}
	    //   pho_pdf_score_data.push_back( pho_pdf_data );
	    //   pho_pdfconv_score_data.push_back( pho_pdfconv_data );
	    //   pho_pdfbin_score_data.push_back( pho_pdfbin_data );
	    //   pho_pdf_logitscore_data.push_back( -1. / 15. * log( 1. / pho_pdf_data - 1. ) );
	    //   pho_pdfconv_logitscore_data.push_back( -1. / 15. * log( 1. / pho_pdfconv_data - 1. ) );
	    //   pho_pdfbin_logitscore_data.push_back( -1. / 15. * log( 1. / pho_pdfbin_data - 1. ) );
	      // pho_pdf_score_mc.push_back( pho_pdf_mc );
	      // pho_pdfconv_score_mc.push_back( pho_pdfconv_mc );
	      // pho_pdfbin_score_mc.push_back( pho_pdfbin_mc );
	      // pho_pdf_logitscore_mc.push_back( -1. / 15. * log( 1. / pho_pdf_mc - 1. ) );
	      // pho_pdfconv_logitscore_mc.push_back( -1. / 15. * log( 1. / pho_pdfconv_mc - 1. ) );
	      // pho_pdfbin_logitscore_mc.push_back( -1. / 15. * log( 1. / pho_pdfbin_mc - 1. ) );
	      // if (filetype != "AOD"){
	      //     pho_pdfnoconv_score.push_back( pho_pdfnoconv );
	      //     pho_pdfnocell_score.push_back( pho_pdfnocell );
	      //     pho_pdfext_score.push_back( pho_pdfext );
	      //     pho_pdfnoconv_logitscore.push_back( -1. / 15. * log( 1. / pho_pdfnoconv - 1. ) );
	      //     pho_pdfnocell_logitscore.push_back( -1. / 15. * log( 1. / pho_pdfnocell - 1. ) );
	      //     pho_pdfext_logitscore.push_back( -1. / 15. * log( 1. / pho_pdfext - 1. ) );
	      // }

		  pho_isPhotonEMLoose.push_back( m_photonIsEMLoose->accept( pho ) );
		  pho_isPhotonEMTight.push_back( m_photonIsEMTight->accept( pho ) );
		  if (filetype != "AOD"){
			  pho_isPhotonEMLoose.push_back( Accessor_DFCommonPhotonsIsEMLoose( *pho ) );
			  pho_isPhotonEMTight.push_back( Accessor_DFCommonPhotonsIsEMTight( *pho ) );
		  }

	      // Truth information
	      if (debug_FillingPhotonVectors) {ANA_MSG_DEBUG("::execute: Photon: Truth information push back...");}
	      if ( event_isMC ){
			  // EGAMMA truth
			  const xAOD::TruthParticle* tp_egam = nullptr;

			  bool truthEgParticle = false;
			  for ( size_t i = 0; i<pho_TruthPointerIdx.size(); i++){
				  if ( pho->index() == pho_TruthPointerIdx[i] ) {
					  truthEgParticle = true; // photonen er truth matched
					  tp_egam = pho_TruthPointer[i]; // Truthpointer fra egam
					  break;
				  }
			  }
			  pho_egamTruthParticle.push_back( truthEgParticle );

	          if ( tp_egam == nullptr ){
	              pho_truthPdgId_egam.push_back( 0 );
	              pho_truth_eta_egam.push_back( -999.0 );
	              pho_truth_phi_egam.push_back( -999.0 );
	              pho_truth_m_egam.push_back( -999.0 );
	              pho_truth_px_egam.push_back( -999.0 );
	              pho_truth_py_egam.push_back( -999.0 );
	              pho_truth_pz_egam.push_back( -999.0 );
	              pho_truth_E_egam.push_back( 0 );
	            }
	          else{
	              pho_truthPdgId_egam.push_back( tp_egam->pdgId() );
	              pho_truth_eta_egam.push_back( tp_egam->eta() );
	              pho_truth_phi_egam.push_back( tp_egam->phi() );
	              pho_truth_m_egam.push_back( tp_egam->m() / 1000. );
	              pho_truth_px_egam.push_back( tp_egam->px() / 1000. );
	              pho_truth_py_egam.push_back( tp_egam->py() / 1000. );
	              pho_truth_pz_egam.push_back( tp_egam->pz() / 1000. );
	              pho_truth_E_egam.push_back( tp_egam->e() / 1000. );
	            }

			  // ATLAS truth
	          const xAOD::TruthParticle* tp_atlas = xAOD::TruthHelpers::getTruthParticle( *pho );
			  if ( tp_atlas == nullptr ){
			      pho_truthPdgId_atlas.push_back( 0 );
	              pho_truth_eta_atlas.push_back( -999.0 );
	              pho_truth_phi_atlas.push_back( -999.0 );
	              pho_truth_m_atlas.push_back( -999.0 );
	              pho_truth_px_atlas.push_back( -999.0 );
	              pho_truth_py_atlas.push_back( -999.0 );
	              pho_truth_pz_atlas.push_back( -999.0 );
	              pho_truth_E_atlas.push_back( 0 );
		        }
		      else{
		          pho_truthPdgId_atlas.push_back( tp_atlas->pdgId() );
	              pho_truth_eta_atlas.push_back( tp_atlas->eta() );
	              pho_truth_phi_atlas.push_back( tp_atlas->phi() );
	              pho_truth_m_atlas.push_back( tp_atlas->m() / 1000. );
	              pho_truth_px_atlas.push_back( tp_atlas->px() / 1000. );
	              pho_truth_py_atlas.push_back( tp_atlas->py() / 1000. );
	              pho_truth_pz_atlas.push_back( tp_atlas->pz() / 1000. );
	              pho_truth_E_atlas.push_back( tp_atlas->e() / 1000. );
		        }

			  // Photon truth type and origin
	          pho_truthType.push_back( Accessor_truthType( *pho ) );
	          pho_truthOrigin.push_back( Accessor_truthOrigin( *pho ) );

	        }

	      // kinematics
	      if (debug_FillingPhotonVectors) {ANA_MSG_DEBUG("::execute: Photon: Kinematic information push back...");}
	      pho_e.push_back( pho->e() );
	      pho_eta.push_back( pho_cluster->etaBE(2) );
	      pho_phi.push_back( pho_p4.Phi() );
	      pho_et.push_back( pho_p4.Pt() / 1000. );
	      // pho_pt_obj.push_back( pho->pt() / 1000. );
	      // pho_et_calo.push_back( pho_cluster->pt() / 1000. );

	      // // Create and save images
	      // m_images->create_images( *pho );
	      // em_calo.push_back( m_images->em_calo );
	      // h_calo.push_back( m_images->h_calo );

	      // em_calo0.push_back( em_calo.back()[0] );
	      // em_calo1.push_back( em_calo.back()[1] );
	      // em_calo2.push_back( em_calo.back()[2] );
	      // em_calo3.push_back( em_calo.back()[3] );
	      // em_calo4.push_back( em_calo.back()[4] );
	      // em_calo5.push_back( em_calo.back()[5] );
	      // em_calo6.push_back( em_calo.back()[6] );
	      // em_calo7.push_back( em_calo.back()[7] );
	      //
	      // h_calo0.push_back( h_calo.back()[0] );
	      // h_calo1.push_back( h_calo.back()[1] );
	      // h_calo2.push_back( h_calo.back()[2] );
	      // h_calo3.push_back( h_calo.back()[3] );
	      // h_calo4.push_back( h_calo.back()[4] );
	      // h_calo5.push_back( h_calo.back()[5] );
	      // h_calo6.push_back( h_calo.back()[6] );
	      // h_calo7.push_back( h_calo.back()[7] );

	      // Scale factors
	      if (debug_FillingPhotonVectors) {ANA_MSG_DEBUG("::execute: Photon: Scale factors calculation...");}
	      double recoSf = 1;
	      double IdSf = 1;
	      double IsoSf = 1;
	      if ( event_isMC ){
	          if(!m_photonSFReco->getEfficiencyScaleFactor( *pho, recoSf)){
	              ATH_MSG_WARNING( "Couldn't get photon scale factor!" );
	            }
	          if(!m_photonSFID->getEfficiencyScaleFactor( *pho, IdSf)){
	              ATH_MSG_WARNING( "Couldn't get photon scale factor!" );
	            }
	          if(!m_photonSFIso->getEfficiencyScaleFactor( *pho, IsoSf)){
	              ATH_MSG_WARNING( "Couldn't get photon scale factor!" );
	            }
	        }

	      if (debug_FillingPhotonVectors) {ANA_MSG_DEBUG("::execute: Photon: Scale factors push back...");}
	      pho_recoSF.push_back( recoSf );
	      pho_idSF.push_back( IdSf );
	      pho_isoSF.push_back( IsoSf );

	      pho_combSF.push_back( recoSf * IdSf * IsoSf );

	      // Additional variables for ER with CNN

	      // eAccCluster.push_back( cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3) );
	      // double etaCalo;
	      // double phiCalo;
	      // if ( !cluster->retrieveMoment( xAOD::CaloCluster::ETACALOFRAME, etaCalo ) )
	      //     ANA_MSG_WARNING( "cannot find etaCalo" );
	      // if ( !cluster->retrieveMoment( xAOD::CaloCluster::PHICALOFRAME, phiCalo ) )
	      //     ANA_MSG_WARNING( "cannot find phiCalo" );

	      // f0Cluster.push_back( (std::abs((cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3))) >= 0.0001) ? cluster->energyBE(0) / (cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3)) : -999.0f);
	      // R12.push_back( (std::abs(cluster->energyBE(2)) >= 0.0001) ? cluster->energyBE(1) / cluster->energyBE(2) : -999.0f ); // float
	      // fTG3.push_back( (std::abs(cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3)) >= 0.0001) ? (cluster->eSample(CaloSampling::TileGap3) / 1000.)/(cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3)) : -999.0f );
	      // cellIndexCluster.push_back( std::floor( std::abs( etaCalo/0.025 ) ) );
	      // etaModCalo.push_back( std::fmod(std::abs( etaCalo), 0.025 ) );
	      // phiModCalo.push_back( (std::abs(etaCalo) < 1.425) ? std::fmod(phiCalo, TMath::Pi()/512) : std::fmod(phiCalo, TMath::Pi()/384)); // float
	      // dPhiTH3.push_back( std::fmod(2.*TMath::Pi()+phiCalo,TMath::Pi()/32.)-TMath::Pi()/64.0 );
	      // poscs1.push_back( Accessor_poscs1( *pho ) );
	      // poscs2.push_back( Accessor_poscs2( *pho ) );

	      // LH variables
	      if (debug_FillingPhotonVectors) {ANA_MSG_DEBUG("::execute: Photon: LH Variables push back...");}
	      pho_Rhad1.push_back( Accessor_Rhad( *pho ) );
	      pho_Rhad.push_back( Accessor_Rhad1( *pho ) );
	      pho_weta2.push_back( Accessor_weta2( *pho ) );
	      pho_Rphi.push_back( Accessor_Rphi( *pho ) );
	      pho_Reta.push_back( Accessor_Reta( *pho ) );
	      pho_Eratio.push_back( Accessor_Eratio( *pho ) );
	      pho_wtots1.push_back( Accessor_wtots1( *pho ) );
	      pho_DeltaE.push_back( Accessor_DeltaE( *pho ) );
	      pho_weta1.push_back( Accessor_weta1( *pho ) );
	      pho_fracs1.push_back( Accessor_fracs1( *pho ) );
	      pho_f1.push_back( Accessor_f1( *pho ) );

	      pho_nTrackPart.push_back( -999 );
	      pho_ConversionType.push_back( pho->conversionType() );
	      pho_ConversionRadius.push_back( pho->conversionRadius() );
	      pho_VertexConvPtRatio.push_back( compute_convPtRatio(pho) ); // float
	      pho_VertexConvEtOverPt.push_back( compute_convEtOverPt(pho) );

	      // Photon Extra Variables
	      if (debug_FillingPhotonVectors) {ANA_MSG_DEBUG("::execute: Photon: Extra variables push back...");}
	      pho_core57cellsEnergyCorrection.push_back( Accessor_core57cellsEnergyCorrection( *pho ) );
	      if (filetype == "EGAM1"){
	          pho_r33over37allcalo.push_back( -999.0 );
	      } else if (filetype != "EGAM1"){
	          pho_r33over37allcalo.push_back( Accessor_r33over37allcalo( *pho ) );
	      }


	      // Isolation variables
	      if (debug_FillingPhotonVectors) {ANA_MSG_DEBUG("::execute: Photon: Isolation variables push back...");}
	      pho_GradientIso.push_back( m_isolationSelectionToolLoose->accept( *pho ) );
	      pho_topoetcone20.push_back( Accessor_topoetcone20( *pho ) );
	      // pho_topoetcone30.push_back( Accessor_topoetcone30( *pho ) );// Not in EGAM
	      pho_topoetcone40.push_back( Accessor_topoetcone40( *pho ) );
	      if (filetype == "EGAM1"){
	          pho_ptvarcone20.push_back( -999.0 );
	      } else if (filetype != "EGAM1"){
	          pho_ptvarcone20.push_back( Accessor_ptvarcone20( *pho ) );
	      }
	      // pho_ptvarcone30.push_back( Accessor_ptvarcone30( *pho ) );// Not in EGAM
	      // pho_ptvarcone40.push_back( Accessor_ptvarcone40( *pho ) );// Not in EGAM

	      // if (filetype == "EGAM1"){
	      //     // nonexistent aux data item `::topoetcone30'
	      //     pho_LGBM_iso.push_back( -999.0 );
	      //     pho_LGBM_logitiso.push_back( -999.0 );
	      // } else if (filetype != "EGAM1"){
	      //     double pho_iso = m_pho_lgbm_iso->predict( *pho, event_correctedScaledAverageMu, NvtxReco );
	      //     pho_LGBM_iso.push_back( pho_iso );
	      //     pho_LGBM_logitiso.push_back(  -1. / 15. * log( 1. / pho_iso - 1. ) );
	      // }

	      // Fill kinematics histograms
	      if (debug_FillingPhotonVectors) {ANA_MSG_DEBUG("::execute: Photon: Filling kinematics histogram...");}
	      if ( event_isMC ){
	          hist( "h_eta" )->Fill( pho_cluster->etaBE(2), eventWeight * pho_combSF.back() );
	          hist( "h_phi" )->Fill( pho->phi(), eventWeight * pho_combSF.back() );
	          hist( "h_et" )->Fill( pho_p4.Pt() / 1000., eventWeight * pho_combSF.back() );
	          // if ( Accessor_DFCommonPhotonsIsEMLoose( *pho ) && m_isolationSelectionToolLoose->accept( *pho ) ){ // For DAODs
	          if ( m_photonIsEMLoose->accept( pho ) && m_isolationSelectionToolLoose->accept( *pho ) ){
	              hist( "h_eta_LHLoose" )->Fill( pho_cluster->etaBE(2), eventWeight * pho_combSF.back() );
	              hist( "h_phi_LHLoose" )->Fill( pho->phi(), eventWeight * pho_combSF.back() );
	              hist( "h_et_LHLoose" )->Fill( pho_p4.Pt() / 1000., eventWeight * pho_combSF.back() );
	            }
	        }
	      else{
	          hist( "h_eta" )->Fill( pho_cluster->etaBE(2) );
	          hist( "h_phi" )->Fill( pho_p4.Phi() );
	          hist( "h_et" )->Fill( pho_p4.Pt() / 1000. );
	          // if ( Accessor_DFCommonPhotonsIsEMLoose( *pho ) && m_isolationSelectionToolLoose->accept( *pho ) ){ // For DAODs
	          if ( m_photonIsEMLoose->accept( pho ) && m_isolationSelectionToolLoose->accept( *pho ) ){
	              hist( "h_eta_LHLoose" )->Fill( pho_cluster->etaBE(2) );
	              hist( "h_phi_LHLoose" )->Fill( pho_p4.Phi() );
	              hist( "h_et_LHLoose" )->Fill( pho_p4.Pt() / 1000. );
	          }
	        }
	  }
  }



  // Fill the fwd electrons vectors
  if (includesFwd){
	  ANA_MSG_DEBUG("::execute: Filling fwd electron vectors...");
	  bool debug_FillingFwdVectors = true;
      for ( const xAOD::Electron* fel : selFwdElectrons ){
          const TLorentzVector& fel_p4 = fel->p4();
		  const xAOD::CaloCluster* fel_cluster = fel->caloCluster();

		  // Truth information
		  if (debug_FillingFwdVectors) {ANA_MSG_DEBUG("::execute: Fwd electrons: Truth information push back...");}
		  if ( event_isMC ){
			  const xAOD::TruthParticle* tp = xAOD::TruthHelpers::getTruthParticle( *fel );

			  fwd_truthType.push_back( Accessor_truthType( *fel ) );
			  fwd_truthOrigin.push_back( Accessor_truthOrigin( *fel ) );

			  if ( tp == nullptr ){
				  fwd_truthPdgId.push_back( 0 );
				  fwd_truth_eta.push_back( -999 );
				  fwd_truth_phi.push_back( -999 );
				  fwd_truth_E.push_back( 0 );
			  }
			  else{
				  fwd_truthPdgId.push_back( tp->pdgId() );
				  fwd_truth_eta.push_back( tp->eta() );
				  fwd_truth_phi.push_back( tp->phi() );
				  fwd_truth_E.push_back( tp->e() / 1000. );
			  }

			  bool fwd_truthEgParticle = false;
			  for ( size_t i = 0; i<fwd_TruthPointerIdx.size(); i++){
				  if ( fel->index() == fwd_TruthPointerIdx[i] ) fwd_truthEgParticle = true;
			  }
			  fwd_truthParticle.push_back( fwd_truthEgParticle );
		  }

		  // Kinematics
		  if (debug_FillingFwdVectors) {ANA_MSG_DEBUG("::execute: Fwd electrons: Kinematics variables push back...");}
		  fwd_eta.push_back( fel->eta() );
		  fwd_phi.push_back( fel->phi() );
		  fwd_pt.push_back( -999.0 );
		  fwd_et.push_back( fel_p4.Pt() / 1000. );

		  // Isolation variables
		  if (debug_FillingFwdVectors) {ANA_MSG_DEBUG("::execute: Fwd electrons: Isolation variables push back...");}
		  fwd_topoetcone20.push_back( Accessor_topoetcone20( *fel ) );
		  if (filetype == "EGAM8") {
			  fwd_topoetcone30.push_back( Accessor_topoetcone30( *fel ) );
		  } else {
			  fwd_topoetcone30.push_back( -999.0 ); // Not in EGAM3
		  }
		  fwd_topoetcone40.push_back( Accessor_topoetcone40( *fel ) );
		  fwd_topoetconecoreConeEnergyCorrection.push_back( Accessor_topoetconecoreConeEnergyCorrection( *fel ) );

		  // Scale factors
		  if (debug_FillingFwdVectors) {ANA_MSG_DEBUG("::execute: Fwd electrons: Scale factors calculation...");}
		  double recoSf = 1;
		  double IdSf = 1;
		  if ( event_isMC ){
			  if(!m_electronSFRecoFwd->getEfficiencyScaleFactor( *fel, recoSf)){
				  ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
			  }
			  if(!m_electronSFIDFwd->getEfficiencyScaleFactor( *fel, IdSf)){
				  ATH_MSG_WARNING( "Couldn't get electron scale factor!" );
			  }
		  }

		  if (debug_FillingFwdVectors) {ANA_MSG_DEBUG("::execute: Fwd electrons: Scale factors push back...");}
		  fwd_recoSF.push_back( recoSf );
		  fwd_idSF.push_back( IdSf );
		  fwd_combSF.push_back( recoSf * IdSf );

		  // ID information
		  if (filetype != "AOD"){
			  fwd_LHLoose.push_back( Accessor_fwdLHLoose( *fel ) );
			  fwd_LHMedium.push_back( Accessor_fwdLHMedium( *fel ) );
			  fwd_LHTight.push_back( Accessor_fwdLHTight( *fel ) );
		  } else if (filetype == "AOD"){
			  fwd_LHLoose.push_back( m_electronLHLooseFwd->accept( fel ) );
			  fwd_LHMedium.push_back( m_electronLHMediumFwd->accept( fel ) );
			  fwd_LHTight.push_back( m_electronLHTightFwd->accept( fel ) );
		  }

		  // LGBM predictions
		  // fwd_LGBM_pid.push_back( m_fwd_lgbm_pid->predict( *fel ) );
		  // fwd_LGBM_energy.push_back( m_fwd_lgbm_er->predict(*fel, NvtxReco, event_correctedScaledActualMu) );
		  // fwd_LGBM_et.push_back( fwd_LGBM_energy.back() / cosh(fwd_eta.back()) );
		  // fwd_LGBM_iso.push_back( m_fwd_lgbm_iso->predict( *fel ) );

		  // Other variables
		  double secondLambdaCluster;
          double lateralCluster;
          double longitudinalCluster;
          double fracMaxCluster;
          double secondRCluster;
          double centerLambdaCluster;

          if ( !fel_cluster->retrieveMoment( xAOD::CaloCluster::SECOND_LAMBDA, secondLambdaCluster ) )
                  ANA_MSG_WARNING("cannot find SECOND_LAMBDA"); // double
          if ( !fel_cluster->retrieveMoment( xAOD::CaloCluster::LATERAL, lateralCluster ) )
                  ANA_MSG_WARNING("cannot find LATERAL"); // double
          if ( !fel_cluster->retrieveMoment( xAOD::CaloCluster::LONGITUDINAL, longitudinalCluster ) )
                  ANA_MSG_WARNING("cannot find LONGITUDINAL"); // double
          if ( !fel_cluster->retrieveMoment( xAOD::CaloCluster::ENG_FRAC_MAX, fracMaxCluster ) )
                  ANA_MSG_WARNING("cannot find ENG_FRAC_MAX"); // double
          if ( !fel_cluster->retrieveMoment( xAOD::CaloCluster::SECOND_R, secondRCluster ) )
                  ANA_MSG_WARNING("cannot find SECOND_R"); // double
          if ( !fel_cluster->retrieveMoment( xAOD::CaloCluster::CENTER_LAMBDA, centerLambdaCluster ) )
                  ANA_MSG_WARNING("cannot find CENTER_LAMBDA"); // double

		fwd_secondLambdaCluster.push_back( secondLambdaCluster );
		fwd_lateralCluster.push_back( lateralCluster );
		fwd_longitudinalCluster.push_back( longitudinalCluster );
		fwd_fracMaxCluster.push_back( fracMaxCluster );
		fwd_secondRCluster.push_back( secondRCluster );
		fwd_centerLambdaCluster.push_back( centerLambdaCluster );

          // fill kinematic histograms
          if (debug_FillingFwdVectors) {ANA_MSG_DEBUG("::execute: Fwd electrons: Filling kinematic histograms...");}
          if ( event_isMC ){
              hist( "h_fwd_eta" )->Fill( fel->eta(), eventWeight * fwd_combSF.back() );
              hist( "h_fwd_phi" )->Fill( fel->phi(), eventWeight * fwd_combSF.back() );
              hist( "h_fwd_et" )->Fill( fel_p4.Pt() / 1000., eventWeight * fwd_combSF.back() );
              // if ( Accessor_fwdLHMedium( *fel ) ){ // For DAODs
              if ( m_electronLHMediumFwd->accept( fel ) ){ // For AODs
                  hist( "h_fwd_eta_LHMedium" )->Fill( fel->eta(), eventWeight * fwd_combSF.back() );
                  hist( "h_fwd_phi_LHMedium" )->Fill( fel->phi(), eventWeight * fwd_combSF.back() );
                  hist( "h_fwd_et_LHMedium" )->Fill( fel_p4.Pt() / 1000., eventWeight * fwd_combSF.back() );
              }
          }
          else{
              hist( "h_fwd_eta" )->Fill( fel->eta() );
              hist( "h_fwd_phi" )->Fill( fel->phi() );
              hist( "h_fwd_et" )->Fill( fel_p4.Pt() / 1000. );
              // if ( Accessor_fwdLHMedium( *fel ) ){ // For DAODs
              if ( m_electronLHMediumFwd->accept( fel ) ){ // For AODs
                  hist( "h_fwd_eta_LHMedium" )->Fill( fel->eta() );
                  hist( "h_fwd_phi_LHMedium" )->Fill( fel->phi() );
                  hist( "h_fwd_et_LHMedium" )->Fill( fel_p4.Pt() / 1000. );
              }
          }
      }
  }

  // Fill the muon vectors
  if (includesMuo){
	  ANA_MSG_DEBUG("::execute: Filling muon vectors...");
	  bool debug_FillingMuonVectors = true;
	  for ( const xAOD::Muon* muo : selMuons ){
		  // Muon track particle: https://acode-browser2.usatlas.bnl.gov/lxr/source/AthAna/atlas/Event/xAOD/xAODMuon/xAODMuon/versions/Muon_v1.h
	      const xAOD::TrackParticle* muo_priTrackP = muo->primaryTrackParticle();
		      const xAOD::TrackParticle* muo_IDTrackP = muo->trackParticle( xAOD::Muon_v1::TrackParticleType::InnerDetectorTrackParticle);

		  if (muo_priTrackP == nullptr){
			  Warning("execute()","A muon in event %li has primaryTrackParticle()=0. Continue to next muon.", m_eventCounter);
			  Warning("execute()","Total muons in event: %i", nMuons);
			  continue;
		  }

	      // Truth information
	      if (debug_FillingMuonVectors) {ANA_MSG_DEBUG("::execute: Muon: Truth information push back...");}
	      if ( event_isMC ){
			  // Truth pointer
		  	  const xAOD::TruthParticle* tp = xAOD::TruthHelpers::getTruthParticle( *muo );

	          if ( tp == nullptr ){
	              muo_truthPdgId.push_back( 0 );
	              muo_truth_eta.push_back( -999.0 );
	              muo_truth_phi.push_back( -999.0 );
	              muo_truth_m.push_back( -999.0 );
	              muo_truth_px.push_back( -999.0 );
	              muo_truth_py.push_back( -999.0 );
	              muo_truth_pz.push_back( -999.0 );
	              muo_truth_E.push_back( 0 );
	            }
	          else{
	              muo_truthPdgId.push_back( tp->pdgId() );
	              muo_truth_eta.push_back( tp->eta() );
	              muo_truth_phi.push_back( tp->phi() );
	              muo_truth_m.push_back( tp->m() / 1000. );
	              muo_truth_px.push_back( tp->px() / 1000. );
	              muo_truth_py.push_back( tp->py() / 1000. );
	              muo_truth_pz.push_back( tp->pz() / 1000. );
	              muo_truth_E.push_back( tp->e() / 1000. );
	            }

			  // Photon truth type and origin
	          muo_truthType.push_back( Accessor_truthType( *muo ) );
	          muo_truthOrigin.push_back( Accessor_truthOrigin( *muo ) );

	        }

		  // Isolation variables
	      if (debug_FillingMuonVectors) {ANA_MSG_DEBUG("::execute: Muon: Isolation information push back...");}
		  muo_etcone20.push_back( Accessor_etcone20( *muo ) );
		  muo_etcone30.push_back( Accessor_etcone30( *muo ) );
		  muo_etcone40.push_back( Accessor_etcone40( *muo ) );
		  muo_ptcone20.push_back( Accessor_ptcone20( *muo ) );
		  muo_ptcone30.push_back( Accessor_ptcone30( *muo ) );
		  muo_ptcone40.push_back( Accessor_ptcone40( *muo ) );
		  muo_ptvarcone20.push_back( Accessor_ptvarcone20( *muo ) );
		  muo_ptvarcone30.push_back( Accessor_ptvarcone30( *muo ) );
		  muo_ptvarcone40.push_back( Accessor_ptvarcone40( *muo ) );

		  // Kinematic variables
		  if (debug_FillingMuonVectors) {ANA_MSG_DEBUG("::execute: Muon: Kinematic information push back...");}
		  muo_pt.push_back( Accessor_pt( *muo ) );
	      muo_eta.push_back( Accessor_eta( *muo ) );
	      muo_phi.push_back( Accessor_phi( *muo ) );
	      muo_charge.push_back( Accessor_charge( *muo ) );

		  // Hits variables
		  if (debug_FillingMuonVectors) {ANA_MSG_DEBUG("::execute: Muon: Hits information push back...");}
	      muo_innerSmallHits.push_back( Accessor_innerSmallHits( *muo ) );
	      muo_innerLargeHits.push_back( Accessor_innerLargeHits( *muo ) );
	      muo_middleSmallHits.push_back( Accessor_middleSmallHits( *muo ) );
	      muo_middleLargeHits.push_back( Accessor_middleLargeHits( *muo ) );
	      muo_outerSmallHits.push_back( Accessor_outerSmallHits( *muo ) );
	      muo_outerLargeHits.push_back( Accessor_outerLargeHits( *muo ) );
	      muo_extendedSmallHits.push_back( Accessor_extendedSmallHits( *muo ) );
	      muo_extendedLargeHits.push_back( Accessor_extendedLargeHits( *muo ) );
		  muo_cscEtaHits.push_back( Accessor_cscEtaHits( *muo ) );
		  muo_cscUnspoiledEtaHits.push_back( Accessor_cscUnspoiledEtaHits( *muo ) );

		  // Holes variables
		  if (debug_FillingMuonVectors) {ANA_MSG_DEBUG("::execute: Muon: Holes information push back...");}
	      muo_innerSmallHoles.push_back( Accessor_innerSmallHoles( *muo ) );
	      muo_innerLargeHoles.push_back( Accessor_innerLargeHoles( *muo ) );
	      muo_middleSmallHoles.push_back( Accessor_middleSmallHoles( *muo ) );
	      muo_middleLargeHoles.push_back( Accessor_middleLargeHoles( *muo ) );
	      muo_outerSmallHoles.push_back( Accessor_outerSmallHoles( *muo ) );
	      muo_outerLargeHoles.push_back( Accessor_outerLargeHoles( *muo ) );
	      muo_extendedSmallHoles.push_back( Accessor_extendedSmallHoles( *muo ) );
	      muo_extendedLargeHoles.push_back( Accessor_extendedLargeHoles( *muo ) );

		  // Other variables
		  if (debug_FillingMuonVectors) {ANA_MSG_DEBUG("::execute: Muon: Other information push back...");}
		  muo_author.push_back( Accessor_author( *muo ) );
		  muo_allAuthors.push_back( Accessor_allAuthors( *muo ) );
		  muo_muonType.push_back( Accessor_muonType( *muo ) );
		  muo_numberOfPrecisionLayers.push_back( Accessor_numberOfPrecisionLayers( *muo ) );
		  muo_numberOfPrecisionHoleLayers.push_back( Accessor_numberOfPrecisionHoleLayers( *muo ) );
		  muo_quality.push_back( Accessor_quality( *muo ) );
		  muo_energyLossType.push_back( Accessor_energyLossType( *muo ) );
		  muo_spectrometerFieldIntegral.push_back( Accessor_spectrometerFieldIntegral( *muo ) );
	   	  muo_scatteringCurvatureSignificance.push_back( Accessor_scatteringCurvatureSignificance( *muo ) );
	   	  muo_scatteringNeighbourSignificance.push_back( Accessor_scatteringNeighbourSignificance( *muo ) );
	   	  muo_momentumBalanceSignificance.push_back( Accessor_momentumBalanceSignificance( *muo ) );
	   	  muo_segmentDeltaEta.push_back( Accessor_segmentDeltaEta( *muo ) );
	   	  muo_CaloLRLikelihood.push_back( Accessor_CaloLRLikelihood( *muo ) );
	   	  muo_EnergyLoss.push_back( Accessor_EnergyLoss( *muo ) );
	   	  muo_CaloMuonIDTag.push_back( Accessor_CaloMuonIDTag( *muo ) );

				// Loose, medium and tight
        muo_LHLoose.push_back( m_muonSelectionToolLoose->accept( *muo ) );
        muo_LHMedium.push_back( m_muonSelectionToolMedium->accept( *muo ) );
        muo_LHTight.push_back( m_muonSelectionToolTight->accept( *muo ) );


				// Triger information
	      std::vector<const xAOD::IParticle*> myParticles;
	      myParticles.push_back( muo );
	      muo_trigger.push_back( m_triggerMatching->match( myParticles, "HLT_mu26_imedium", 0.07, false ) ||
	                          m_triggerMatching->match( myParticles, "HLT_mu50", 0.07, false ) ||  // included for scale factors
	                          m_triggerMatching->match( myParticles, "HLT_mu60_msonly_0eta105", 0.07, false ) );  // included for scale factors
	      // muo_trigger.push_back( false );




		  double DFCommonGoodMuon = -999.0;
		  double DFCommonMuonsPreselection = -999.0;

		  if ( Accessor_DFCommonGoodMuon.isAvailable( *muo ) ) DFCommonGoodMuon = Accessor_DFCommonGoodMuon( *muo );
		  if ( Accessor_DFCommonMuonsPreselection.isAvailable( *muo ) ) DFCommonMuonsPreselection = Accessor_DFCommonMuonsPreselection( *muo );

	   	  muo_DFCommonGoodMuon.push_back( DFCommonGoodMuon );
	   	  muo_DFCommonMuonsPreselection.push_back( DFCommonMuonsPreselection );

		  // Primary track particle
		  if (debug_FillingMuonVectors) {ANA_MSG_DEBUG("::execute: Muon: Primary track particle push back...");}
		  muo_priTrack_d0.push_back( muo_priTrackP->d0() );
		  muo_priTrack_z0.push_back( muo_priTrackP->z0() );
		  muo_priTrack_d0Sig.push_back( std::sqrt( muo_priTrackP->definingParametersCovMatrixVec()[0]) );
		  muo_priTrack_z0Sig.push_back( std::sqrt( muo_priTrackP->definingParametersCovMatrixVec()[1]) );
		  muo_priTrack_theta.push_back( muo_priTrackP->theta() );
		  muo_priTrack_qOverP.push_back( muo_priTrackP->qOverP() );
		  muo_priTrack_vx.push_back( muo_priTrackP->vx() );
		  muo_priTrack_vy.push_back( muo_priTrackP->vy() );
		  muo_priTrack_vz.push_back( muo_priTrackP->vz() );
		  muo_priTrack_phi.push_back( muo_priTrackP->phi() );
		  muo_priTrack_chiSquared.push_back( muo_priTrackP->chiSquared() );
		  muo_priTrack_numberDoF.push_back( muo_priTrackP->numberDoF() );
		  muo_priTrack_radiusOfFirstHit.push_back( muo_priTrackP->radiusOfFirstHit() );
		  muo_priTrack_trackFitter.push_back( muo_priTrackP->trackFitter() );
		  muo_priTrack_particleHypothesis.push_back( muo_priTrackP->particleHypothesis() );
		  muo_priTrack_numberOfUsedHitsdEdx.push_back( muo_priTrackP->numberOfUsedHitsdEdx() );
		  muo_priTrack_numberOfContribPixelLayers.push_back( Accessor_numberOfContribPixelLayers( *muo_priTrackP ) );
		  muo_priTrack_numberOfInnermostPixelLayerHits.push_back( Accessor_numberOfInnermostPixelLayerHits( *muo_priTrackP ) );
		  muo_priTrack_expectInnermostPixelLayerHit.push_back( Accessor_expectInnermostPixelLayerHit( *muo_priTrackP ) );
		  muo_priTrack_numberOfNextToInnermostPixelLayerHits.push_back( Accessor_numberOfNextToInnermostPixelLayerHits( *muo_priTrackP ) );
		  muo_priTrack_expectNextToInnermostPixelLayerHit.push_back( Accessor_expectNextToInnermostPixelLayerHit( *muo_priTrackP ) );
		  muo_priTrack_numberOfPixelHits.push_back( Accessor_numberOfPixelHits( *muo_priTrackP ) );
		  muo_priTrack_numberOfGangedPixels.push_back( Accessor_numberOfGangedPixels( *muo_priTrackP ) );
		  muo_priTrack_numberOfGangedFlaggedFakes.push_back( Accessor_numberOfGangedFlaggedFakes( *muo_priTrackP ) );
		  muo_priTrack_numberOfPixelSpoiltHits.push_back( Accessor_numberOfPixelSpoiltHits( *muo_priTrackP ) );
		  muo_priTrack_numberOfDBMHits.push_back( Accessor_numberOfDBMHits( *muo_priTrackP ) );
		  muo_priTrack_numberOfSCTHits.push_back( Accessor_numberOfSCTHits( *muo_priTrackP ) );
		  muo_priTrack_numberOfTRTHits.push_back( Accessor_numberOfTRTHits( *muo_priTrackP ) );
		  muo_priTrack_numberOfOutliersOnTrack.push_back( Accessor_numberOfOutliersOnTrack( *muo_priTrackP ) );
		  muo_priTrack_standardDeviationOfChi2OS.push_back( Accessor_standardDeviationOfChi2OS( *muo_priTrackP ) );
		  muo_priTrack_pixeldEdx.push_back( Accessor_pixeldEdx( *muo_priTrackP ) );

		  // Inner detector track particle
		  if (muo_IDTrackP != nullptr){
			  if (debug_FillingMuonVectors) {ANA_MSG_DEBUG("::execute: Muon: Inner detector track particle push back...");}
			  muo_IDTrack_d0.push_back( muo_IDTrackP->d0() );
			  muo_IDTrack_z0.push_back( muo_IDTrackP->z0() );
			  muo_IDTrack_d0Sig.push_back( std::sqrt( muo_IDTrackP->definingParametersCovMatrixVec()[0]) );
			  muo_IDTrack_z0Sig.push_back( std::sqrt( muo_IDTrackP->definingParametersCovMatrixVec()[1]) );
			  muo_IDTrack_theta.push_back( muo_IDTrackP->theta() );
			  muo_IDTrack_qOverP.push_back( muo_IDTrackP->qOverP() );
			  muo_IDTrack_vx.push_back( muo_IDTrackP->vx() );
			  muo_IDTrack_vy.push_back( muo_IDTrackP->vy() );
			  muo_IDTrack_vz.push_back( muo_IDTrackP->vz() );
			  muo_IDTrack_phi.push_back( muo_IDTrackP->phi() );
			  muo_IDTrack_chiSquared.push_back( muo_IDTrackP->chiSquared() );
			  muo_IDTrack_numberDoF.push_back( muo_IDTrackP->numberDoF() );
			  muo_IDTrack_radiusOfFirstHit.push_back( muo_IDTrackP->radiusOfFirstHit() );
			  muo_IDTrack_trackFitter.push_back( muo_IDTrackP->trackFitter() );
			  muo_IDTrack_particleHypothesis.push_back( muo_IDTrackP->particleHypothesis() );
			  muo_IDTrack_numberOfUsedHitsdEdx.push_back( muo_IDTrackP->numberOfUsedHitsdEdx() );
		  } else {
			  if (debug_FillingMuonVectors) {ANA_MSG_DEBUG("::execute: Muon: Inner detector track particle is nullptr - push back -999.0...");}
			  muo_IDTrack_d0.push_back( -999.0 );
			  muo_IDTrack_z0.push_back( -999.0 );
			  muo_IDTrack_d0Sig.push_back( -999.0 );
			  muo_IDTrack_z0Sig.push_back( -999.0 );
			  muo_IDTrack_theta.push_back( -999.0 );
			  muo_IDTrack_qOverP.push_back( -999.0 );
			  muo_IDTrack_vx.push_back( -999.0 );
			  muo_IDTrack_vy.push_back( -999.0 );
			  muo_IDTrack_vz.push_back( -999.0 );
			  muo_IDTrack_phi.push_back( -999.0 );
			  muo_IDTrack_chiSquared.push_back( -999.0 );
			  muo_IDTrack_numberDoF.push_back( -999.0 );
			  muo_IDTrack_radiusOfFirstHit.push_back( -999.0 );
			  muo_IDTrack_trackFitter.push_back( -999.0 );
			  muo_IDTrack_particleHypothesis.push_back( -999.0 );
			  muo_IDTrack_numberOfUsedHitsdEdx.push_back( -999.0 );
		  }



		  // Equal values
		  if (debug_FillingMuonVectors) {ANA_MSG_DEBUG("::execute: Muon: Values equal to Higgs...");}
		  muo_ET_Core.push_back( Accessor_ET_Core( *muo ) );
		  muo_ET_EMCore.push_back( Accessor_ET_EMCore( *muo ) );
		  muo_ET_HECCore.push_back( Accessor_ET_HECCore( *muo ) );
		  muo_ET_TileCore.push_back( Accessor_ET_TileCore( *muo ) );
		  muo_FSR_CandidateEnergy.push_back( Accessor_FSR_CandidateEnergy( *muo ) );
		  muo_InnerDetectorPt.push_back( Accessor_InnerDetectorPt( *muo ) );
		  muo_MuonSpectrometerPt.push_back( Accessor_MuonSpectrometerPt( *muo ) );
		  muo_combinedTrackOutBoundsPrecisionHits.push_back( Accessor_combinedTrackOutBoundsPrecisionHits( *muo ) );
		  muo_coreMuonEnergyCorrection.push_back( Accessor_coreMuonEnergyCorrection( *muo ) );
		  muo_deltaphi_0.push_back( Accessor_deltaphi_0( *muo ) );
		  muo_deltaphi_1.push_back( Accessor_deltaphi_1( *muo ) );
		  muo_deltatheta_0.push_back( Accessor_deltatheta_0( *muo ) );
		  muo_deltatheta_1.push_back( Accessor_deltatheta_1( *muo ) );
		  muo_extendedClosePrecisionHits.push_back( Accessor_extendedClosePrecisionHits( *muo ) );
		  muo_extendedOutBoundsPrecisionHits.push_back( Accessor_extendedOutBoundsPrecisionHits( *muo ) );
		  muo_innerClosePrecisionHits.push_back( Accessor_innerClosePrecisionHits( *muo ) );
		  muo_innerOutBoundsPrecisionHits.push_back( Accessor_innerOutBoundsPrecisionHits( *muo ) );
		  muo_isEndcapGoodLayers.push_back( Accessor_isEndcapGoodLayers( *muo ) );
		  muo_isSmallGoodSectors.push_back( Accessor_isSmallGoodSectors( *muo ) );
		  muo_middleClosePrecisionHits.push_back( Accessor_middleClosePrecisionHits( *muo ) );
		  muo_middleOutBoundsPrecisionHits.push_back( Accessor_middleOutBoundsPrecisionHits( *muo ) );
		  muo_numEnergyLossPerTrack.push_back( Accessor_numEnergyLossPerTrack( *muo ) );
		  muo_numberOfGoodPrecisionLayers.push_back( Accessor_numberOfGoodPrecisionLayers( *muo ) );
		  muo_outerClosePrecisionHits.push_back( Accessor_outerClosePrecisionHits( *muo ) );
		  muo_outerOutBoundsPrecisionHits.push_back( Accessor_outerOutBoundsPrecisionHits( *muo ) );
		  muo_sigmadeltaphi_0.push_back( Accessor_sigmadeltaphi_0( *muo ) );
		  muo_sigmadeltaphi_1.push_back( Accessor_sigmadeltaphi_1( *muo ) );
		  muo_sigmadeltatheta_0.push_back( Accessor_sigmadeltatheta_0( *muo ) );
		  muo_sigmadeltatheta_1.push_back( Accessor_sigmadeltatheta_1( *muo ) );
		  muo_neflowisol20.push_back( Accessor_neflowisol20( *muo ) );
		  muo_neflowisol30.push_back( Accessor_neflowisol30( *muo ) );
		  muo_neflowisol40.push_back( Accessor_neflowisol40( *muo ) );
		  muo_neflowisolCorrBitset.push_back( Accessor_neflowisolCorrBitset( *muo ) );
		  muo_neflowisolcoreConeEnergyCorrection.push_back( Accessor_neflowisolcoreConeEnergyCorrection( *muo ) );
		  muo_ptconeCorrBitset.push_back( Accessor_ptconeCorrBitset( *muo ) );
		  muo_ptconecoreTrackPtrCorrection.push_back( Accessor_ptconecoreTrackPtrCorrection( *muo ) );
		  muo_topoetconeCorrBitset.push_back( Accessor_topoetconeCorrBitset( *muo ) );
		  muo_topoetconecoreConeEnergyCorrection.push_back( Accessor_topoetconecoreConeEnergyCorrection( *muo ) );

		  double etconeCorrBitset = -999.0;
		  double etconecoreConeEnergyCorrection = -999.0;
		  double CT_EL_Type = -999.0;
		  double CT_ET_Core = -999.0;
		  double CT_ET_FSRCandidateEnergy = -999.0;
		  double CT_ET_LRLikelihood = -999.0;
		  double d0_staco = -999.0;
		  double z0_staco = -999.0;
		  double phi0_staco = -999.0;
		  double qOverPErr_staco = -999.0;
		  double qOverP_staco = -999.0;
		  double theta_staco = -999.0;

		  if ( Accessor_etconeCorrBitset.isAvailable( *muo ) ) etconeCorrBitset = Accessor_etconeCorrBitset( *muo );
		  if ( Accessor_etconecoreConeEnergyCorrection.isAvailable( *muo ) ) etconecoreConeEnergyCorrection = Accessor_etconecoreConeEnergyCorrection( *muo );
		  if ( Accessor_CT_EL_Type.isAvailable( *muo ) ) CT_EL_Type = Accessor_CT_EL_Type( *muo );
		  if ( Accessor_CT_ET_Core.isAvailable( *muo ) ) CT_ET_Core = Accessor_CT_ET_Core( *muo );
		  if ( Accessor_CT_ET_FSRCandidateEnergy.isAvailable( *muo ) ) CT_ET_FSRCandidateEnergy = Accessor_CT_ET_FSRCandidateEnergy( *muo );
		  if ( Accessor_CT_ET_LRLikelihood.isAvailable( *muo ) ) CT_ET_LRLikelihood = Accessor_CT_ET_LRLikelihood( *muo );
		  if ( Accessor_d0_staco.isAvailable( *muo ) ) d0_staco = Accessor_d0_staco( *muo );
		  if ( Accessor_z0_staco.isAvailable( *muo ) ) z0_staco = Accessor_z0_staco( *muo );
		  if ( Accessor_phi0_staco.isAvailable( *muo ) ) phi0_staco = Accessor_phi0_staco( *muo );
		  if ( Accessor_qOverPErr_staco.isAvailable( *muo ) ) qOverPErr_staco = Accessor_qOverPErr_staco( *muo );
		  if ( Accessor_qOverP_staco.isAvailable( *muo ) ) qOverP_staco = Accessor_qOverP_staco( *muo );
		  if ( Accessor_theta_staco.isAvailable( *muo ) ) theta_staco = Accessor_theta_staco( *muo );

		  muo_etconeCorrBitset.push_back( etconeCorrBitset );
		  muo_etconecoreConeEnergyCorrection.push_back( etconecoreConeEnergyCorrection );
		  muo_CT_EL_Type.push_back( CT_EL_Type );
		  muo_CT_ET_Core.push_back( CT_ET_Core );
		  muo_CT_ET_FSRCandidateEnergy.push_back( CT_ET_FSRCandidateEnergy );
		  muo_CT_ET_LRLikelihood.push_back( CT_ET_LRLikelihood );
		  muo_d0_staco.push_back( d0_staco );
		  muo_phi0_staco.push_back( phi0_staco );
		  muo_qOverPErr_staco.push_back( qOverPErr_staco );
		  muo_qOverP_staco.push_back( qOverP_staco );
		  muo_theta_staco.push_back( theta_staco );
		  muo_z0_staco.push_back( z0_staco );

		  // muo_nphiMatchedHitsPerChamberLayer.push_back( Accessor_nphiMatchedHitsPerChamberLayer( *muo ) ); // Vectors
		  // muo_nprecMatchedHitsPerChamberLayer.push_back( Accessor_nprecMatchedHitsPerChamberLayer( *muo ) ); // Vectors
		  // muo_ntrigEtaMatchedHitsPerChamberLayer.push_back( Accessor_ntrigEtaMatchedHitsPerChamberLayer( *muo ) ); // Vectors
		  // muo_segmentsOnTrack.push_back( Accessor_segmentsOnTrack( *muo ) ); // Vectors
	  }
  }

  // write everything into the tree
  ANA_MSG_DEBUG("::execute: Write everything into the tree...");
  tree ("analysis")->Fill();
  ANA_MSG_DEBUG("::execute: After write everything into the tree...");

  return StatusCode::SUCCESS;
}

// Function to calculat delta R between two particles
float MyxAODAnalysis::deltaR( const float eta1, const float phi1, const float eta2, const float phi2 ) {
	const float deta = fabs(eta1 - eta2);

	float dphi;
	if ( fabs(phi1 - phi2 - 2.0*3.14159265359) < fabs(phi1 - phi2) ) {
		dphi = fabs(phi1 - phi2 - 2.0*3.14159265359) ;
	} else {
		dphi = fabs(phi1 - phi2);
	}

	return sqrt( deta*deta + dphi*dphi );
}

// Clear the vectors before filling the information of the new event
void MyxAODAnalysis::clearVectors(){
    ANA_MSG_DEBUG("::clearVectors: Clearing vectors...");

    ANA_MSG_DEBUG("::clearVectors: Clearing electron vectors...");
    // Truth information
    ele_truthPdgId_egam.clear();
    ele_truth_eta_egam.clear();
    ele_truth_phi_egam.clear();
    ele_truth_m_egam.clear();
    ele_truth_px_egam.clear();
    ele_truth_py_egam.clear();
    ele_truth_pz_egam.clear();
    ele_truth_E_egam.clear();
    ele_truthPdgId_atlas.clear();
    ele_truth_eta_atlas.clear();
    ele_truth_phi_atlas.clear();
    ele_truth_m_atlas.clear();
    ele_truth_px_atlas.clear();
    ele_truth_py_atlas.clear();
    ele_truth_pz_atlas.clear();
    ele_truth_E_atlas.clear();
    ele_egamTruthParticle.clear();
    ele_truthType.clear();
    ele_truthOrigin.clear();
    ele_TruthPointerIdx.clear();
    ele_TruthPointer.clear();
    // ID scores
    ele_trigger.clear();
    // ele_LHValue.clear();
    ele_LHLoose.clear();
    ele_LHMedium.clear();
    ele_LHTight.clear();
    // ele_LHLooseIsEMValue.clear();
    // ele_LHMediumIsEMValue.clear();
    // ele_LHTightIsEMValue.clear();
    // ID variables
    ele_Rhad.clear();
    ele_Reta.clear();
    ele_Rhad1.clear();
    ele_deltaEta1.clear();
    ele_Eratio.clear();
    ele_Rphi.clear();
    ele_eProbHT.clear();
    ele_weta2.clear();
    ele_dPOverP.clear();
    ele_f3.clear();
    ele_d0.clear();
    ele_z0.clear();
    ele_deltaPhiRescaled2.clear();
    ele_f1.clear();
    // Binning variables
    ele_eta.clear();
    // Selection vars
    ele_wtots1.clear();
    ele_EptRatio.clear();
    ele_numberOfPixelHits.clear();
    ele_numberOfSCTHits.clear();
    ele_numberOfInnermostPixelHits.clear();
    // Extra variables
    ele_E7x11_Lr3.clear();
    ele_nTracks.clear();
    ele_core57cellsEnergyCorrection.clear();
    // Other variables
    ele_d0Sig.clear();
    ele_z0Sig.clear();
    ele_ECIDSResult.clear();
    ele_author.clear();
    ele_expectInnermostPixelLayerHit.clear();
    ele_expectNextToInnermostPixelLayerHit.clear();
    ele_chiSquared.clear();
	ele_radiusOfFirstHit.clear();
	ele_numberOfTRTHits.clear();
	ele_pixeldEdx.clear();
    // Other kinematic information
    ele_e.clear();
    ele_et.clear();
    ele_pt_track.clear();
    ele_phi.clear();
    ele_m.clear();
    ele_charge.clear();
    //Isolation variables
    ele_GradientIso.clear();
    ele_topoetcone20.clear();
    ele_topoetcone20ptCorrection.clear();
    ele_topoetcone40.clear();
    ele_ptvarcone20.clear();
    ele_ptvarcone30_TightTTVA_pt500.clear();
    ele_ptvarcone20_TightTTVA_pt1000.clear();
    ele_ptvarcone30_TightTTVA_pt1000.clear();
    ele_ptvarcone30_TightTTVALooseCone_pt500.clear();
    ele_ptvarcone20_TightTTVALooseCone_pt1000.clear();
    ele_ptvarcone30_TightTTVALooseCone_pt1000.clear();
    ele_ptvarcone40_TightTTVALooseCone_pt1000.clear();
    ele_ptcone20_TightTTVALooseCone_pt500.clear();
    ele_ptcone20_TightTTVALooseCone_pt1000.clear();
    // Scale factors
    ele_recoSF.clear();
    ele_idSF.clear();
    ele_isoSF.clear();
    ele_combSF.clear();
    // LGBM
    // ele_pdf_score.clear();
    // ele_pdfconv_score.clear();


    ANA_MSG_DEBUG("::clearVectors: Clearing fwd electron vectors...");
    // Truth information
    fwd_truthPdgId.clear();
    fwd_truthType.clear();
    fwd_truthOrigin.clear();
    fwd_truthParticle.clear();
    fwd_truth_eta.clear();
    fwd_truth_phi.clear();
    fwd_truth_E.clear();
    fwd_TruthPointerIdx.clear();
    // ID scores
    fwd_LHLoose.clear();
    fwd_LHMedium.clear();
    fwd_LHTight.clear();
    // LGBM
  //   fwd_LGBM_pid.clear();
  //   fwd_LGBM_energy.clear();
  //   fwd_LGBM_et.clear();
	// fwd_LGBM_iso.clear();
    // Kinematics
    fwd_eta.clear();
    fwd_phi.clear();
    fwd_pt.clear();
    fwd_et.clear();
    // Isolation variables
    fwd_topoetcone20.clear();
    fwd_topoetcone30.clear();
    fwd_topoetcone40.clear();
    fwd_topoetconecoreConeEnergyCorrection.clear();
    // Scale factors
    fwd_recoSF.clear();
    fwd_idSF.clear();
    fwd_combSF.clear();
	// Other variables
	fwd_secondLambdaCluster.clear();
	fwd_lateralCluster.clear();
	fwd_longitudinalCluster.clear();
	fwd_fracMaxCluster.clear();
	fwd_secondRCluster.clear();
	fwd_centerLambdaCluster.clear();


    ANA_MSG_DEBUG("::clearVectors: Clearing photon vectors...");
    // Truth information
    pho_truthPdgId_egam.clear();
    pho_truth_m_egam.clear();
    pho_truth_px_egam.clear();
    pho_truth_py_egam.clear();
    pho_truth_pz_egam.clear();
    pho_truth_eta_egam.clear();
    pho_truth_phi_egam.clear();
    pho_truth_E_egam.clear();
    pho_truthPdgId_atlas.clear();
    pho_truth_m_atlas.clear();
    pho_truth_px_atlas.clear();
    pho_truth_py_atlas.clear();
    pho_truth_pz_atlas.clear();
    pho_truth_eta_atlas.clear();
    pho_truth_phi_atlas.clear();
    pho_truth_E_atlas.clear();
	pho_egamTruthParticle.clear();
    pho_truthType.clear();
    pho_truthOrigin.clear();
    pho_TruthPointerIdx.clear();
    pho_TruthPointer.clear();
    // ID scores
    pho_isPhotonEMLoose.clear();
    pho_isPhotonEMTight.clear();
    // LGBM scores
    // pho_pdf_score_data.clear();
    // pho_pdfconv_score_data.clear();
    // pho_pdfbin_score_data.clear();
    // pho_pdf_logitscore_data.clear();
    // pho_pdfconv_logitscore_data.clear();
    // pho_pdfbin_logitscore_data.clear();
    // pho_pdf_score_mc.clear();
    // pho_pdfconv_score_mc.clear();
    // pho_pdfbin_score_mc.clear();
    // pho_pdf_logitscore_mc.clear();
    // pho_pdfconv_logitscore_mc.clear();
    // pho_pdfbin_logitscore_mc.clear();
	// pho_pdfnoconv_score.clear();
	// pho_pdfnocell_score.clear();
	// pho_pdfext_score.clear();
    // pho_pdfnoconv_logitscore.clear();
    // pho_pdfnocell_logitscore.clear();
    // pho_pdfext_logitscore.clear();
    // Kinematics
    pho_e.clear();
    pho_m.clear();
    pho_eta.clear();
    pho_phi.clear();
    pho_et.clear();
    // LH variables
    pho_Rhad1.clear();
    pho_Rhad.clear();
    pho_weta2.clear();
    pho_Rphi.clear();
    pho_Reta.clear();
    pho_Eratio.clear();
    pho_f1.clear();
    pho_wtots1.clear();
    pho_DeltaE.clear();
    pho_weta1.clear();
    pho_fracs1.clear();
    // Conversion variables
    pho_nTrackPart.clear();
    pho_ConversionType.clear();
    pho_ConversionRadius.clear();
    pho_VertexConvEtOverPt.clear();
    pho_VertexConvPtRatio.clear();
    // Extra variables
    pho_maxEcell_time.clear();
    pho_maxEcell_energy.clear();
    pho_core57cellsEnergyCorrection.clear();
    pho_r33over37allcalo.clear();
    // Isolation vriables
    pho_GradientIso.clear();
    pho_topoetcone20.clear();
    pho_topoetcone40.clear();
    pho_ptvarcone20.clear();
    // pho_LGBM_iso.clear();
    // pho_LGBM_logitiso.clear();
    // Scale factors
    pho_recoSF.clear();
    pho_idSF.clear();
    pho_isoSF.clear();
    pho_combSF.clear();
    // // Additional variables for ER with CNN
    // f0Cluster.clear();
    // R12.clear();
    // fTG3.clear();
    // eAccCluster.clear();
    // cellIndexCluster.clear();
    // etaModCalo.clear();
    // pho_phiModCalo.clear();
    // dPhiTH3.clear();
    // poscs1.clear();
    // poscs2.clear();
    // // Images
    // em_calo.clear();
    // h_calo.clear();
    // em_calo0.clear();
    // em_calo1.clear();
    // em_calo2.clear();
    // em_calo3.clear();
    // em_calo4.clear();
    // em_calo5.clear();
    // em_calo6.clear();
    // em_calo7.clear();
    // h_calo0.clear();
    // h_calo1.clear();
    // h_calo2.clear();
    // h_calo3.clear();
    // h_calo4.clear();
    // h_calo5.clear();
    // h_calo6.clear();
    // h_calo7.clear();

	ANA_MSG_DEBUG("::clearVectors: Clearing muon vectors...");
    // Truth information
    muo_truthPdgId.clear();
    muo_truthType.clear();
    muo_truthOrigin.clear();
    muo_truth_m.clear();
    muo_truth_px.clear();
    muo_truth_py.clear();
    muo_truth_pz.clear();
    muo_truth_eta.clear();
    muo_truth_phi.clear();
    muo_truth_E.clear();
	// kinematics
	muo_author.clear();
	muo_etcone20.clear();
	muo_etcone30.clear();
	muo_etcone40.clear();
	muo_ptcone20.clear();
	muo_ptcone30.clear();
	muo_ptcone40.clear();
	muo_ptvarcone20.clear();
	muo_pt.clear();
	muo_eta.clear();
	muo_phi.clear();
	muo_charge.clear();
	muo_numberOfPrecisionLayers.clear();
	muo_numberOfPrecisionHoleLayers.clear();
	muo_ptvarcone30.clear();
	muo_ptvarcone40.clear();
	muo_innerSmallHits.clear();
	muo_innerLargeHits.clear();
	muo_middleSmallHits.clear();
	muo_middleLargeHits.clear();
	muo_outerSmallHits.clear();
	muo_outerLargeHits.clear();
	muo_extendedSmallHits.clear();
	muo_extendedLargeHits.clear();
	muo_allAuthors.clear();
	muo_muonType.clear();
	muo_quality.clear();
	muo_energyLossType.clear();
	muo_innerSmallHoles.clear();
	muo_innerLargeHoles.clear();
	muo_middleSmallHoles.clear();
	muo_middleLargeHoles.clear();
	muo_outerSmallHoles.clear();
	muo_outerLargeHoles.clear();
	muo_extendedSmallHoles.clear();
	muo_extendedLargeHoles.clear();
	muo_cscEtaHits.clear();
	muo_cscUnspoiledEtaHits.clear();
	muo_spectrometerFieldIntegral.clear();
	muo_scatteringCurvatureSignificance.clear();
	muo_scatteringNeighbourSignificance.clear();
	muo_momentumBalanceSignificance.clear();
	muo_segmentDeltaEta.clear();
	muo_CaloLRLikelihood.clear();
	muo_EnergyLoss.clear();
	muo_CaloMuonIDTag.clear();
	muo_DFCommonGoodMuon.clear();
	muo_DFCommonMuonsPreselection.clear();
	muo_priTrack_d0.clear();
	muo_priTrack_z0.clear();
	muo_priTrack_d0Sig.clear();
	muo_priTrack_z0Sig.clear();
	muo_priTrack_theta.clear();
	muo_priTrack_qOverP.clear();
	muo_priTrack_vx.clear();
	muo_priTrack_vy.clear();
	muo_priTrack_vz.clear();
	muo_priTrack_phi.clear();
	muo_priTrack_chiSquared.clear();
	muo_priTrack_numberDoF.clear();
	muo_priTrack_radiusOfFirstHit.clear();
	muo_priTrack_trackFitter.clear();
	muo_priTrack_particleHypothesis.clear();
	muo_priTrack_numberOfUsedHitsdEdx.clear();
	muo_priTrack_numberOfContribPixelLayers.clear();
	muo_priTrack_numberOfInnermostPixelLayerHits.clear();
	muo_priTrack_expectInnermostPixelLayerHit.clear();
	muo_priTrack_numberOfNextToInnermostPixelLayerHits.clear();
	muo_priTrack_expectNextToInnermostPixelLayerHit.clear();
	muo_priTrack_numberOfPixelHits.clear();
	muo_priTrack_numberOfGangedPixels.clear();
	muo_priTrack_numberOfGangedFlaggedFakes.clear();
	muo_priTrack_numberOfPixelSpoiltHits.clear();
	muo_priTrack_numberOfDBMHits.clear();
	muo_priTrack_numberOfSCTHits.clear();
	muo_priTrack_numberOfTRTHits.clear();
	muo_priTrack_numberOfOutliersOnTrack.clear();
	muo_priTrack_standardDeviationOfChi2OS.clear();
	muo_priTrack_pixeldEdx.clear();
	muo_IDTrack_d0.clear();
	muo_IDTrack_z0.clear();
	muo_IDTrack_d0Sig.clear();
	muo_IDTrack_z0Sig.clear();
	muo_IDTrack_theta.clear();
	muo_IDTrack_qOverP.clear();
	muo_IDTrack_vx.clear();
	muo_IDTrack_vy.clear();
	muo_IDTrack_vz.clear();
	muo_IDTrack_phi.clear();
	muo_IDTrack_chiSquared.clear();
	muo_IDTrack_numberDoF.clear();
	muo_IDTrack_radiusOfFirstHit.clear();
	muo_IDTrack_trackFitter.clear();
	muo_IDTrack_particleHypothesis.clear();
	muo_IDTrack_numberOfUsedHitsdEdx.clear();
	muo_ET_Core.clear();
	muo_ET_EMCore.clear();
	muo_ET_HECCore.clear();
	muo_ET_TileCore.clear();
	muo_FSR_CandidateEnergy.clear();
	muo_InnerDetectorPt.clear();
	muo_MuonSpectrometerPt.clear();
	muo_combinedTrackOutBoundsPrecisionHits.clear();
	muo_coreMuonEnergyCorrection.clear();
	muo_deltaphi_0.clear();
	muo_deltaphi_1.clear();
	muo_deltatheta_0.clear();
	muo_deltatheta_1.clear();
	muo_etconecoreConeEnergyCorrection.clear();
	muo_extendedClosePrecisionHits.clear();
	muo_extendedOutBoundsPrecisionHits.clear();
	muo_innerClosePrecisionHits.clear();
	muo_innerOutBoundsPrecisionHits.clear();
	muo_isEndcapGoodLayers.clear();
	muo_isSmallGoodSectors.clear();
	muo_middleClosePrecisionHits.clear();
	muo_middleOutBoundsPrecisionHits.clear();
	muo_numEnergyLossPerTrack.clear();
	muo_numberOfGoodPrecisionLayers.clear();
	muo_outerClosePrecisionHits.clear();
	muo_outerOutBoundsPrecisionHits.clear();
	muo_sigmadeltaphi_0.clear();
	muo_sigmadeltaphi_1.clear();
	muo_sigmadeltatheta_0.clear();
	muo_sigmadeltatheta_1.clear();
	muo_etconeCorrBitset.clear();
	muo_neflowisol20.clear();
	muo_neflowisol30.clear();
	muo_neflowisol40.clear();
	muo_neflowisolCorrBitset.clear();
	muo_neflowisolcoreConeEnergyCorrection.clear();
	muo_ptconeCorrBitset.clear();
	muo_ptconecoreTrackPtrCorrection.clear();
	muo_topoetconeCorrBitset.clear();
	muo_topoetconecoreConeEnergyCorrection.clear();
  muo_LHLoose.clear();
  muo_LHMedium.clear();
  muo_LHTight.clear();

	muo_trigger.clear();

	muo_CT_EL_Type.clear();
	muo_CT_ET_Core.clear();
	muo_CT_ET_FSRCandidateEnergy.clear();
	muo_CT_ET_LRLikelihood.clear();

	muo_d0_staco.clear();
	muo_phi0_staco.clear();
	muo_qOverPErr_staco.clear();
	muo_qOverP_staco.clear();
	muo_theta_staco.clear();
	muo_z0_staco.clear();

	// muo_nphiMatchedHitsPerChamberLayer.clear();
	// muo_nprecMatchedHitsPerChamberLayer.clear();
	// muo_ntrigEtaMatchedHitsPerChamberLayer.clear();
	// muo_segmentsOnTrack.clear();
}



void MyxAODAnalysis::resetCutFlow( std::map< TString, multiplicityCutFlow* > map ){
    for ( auto& cutFlow : map ){
        cutFlow.second->indiv = 0;
        cutFlow.second->acc = 0;
        cutFlow.second->truthIndiv = 0;
        cutFlow.second->truthAcc = 0;
    }
}


StatusCode MyxAODAnalysis::createCutFlowHists( std::map<TString, multiplicityCutFlow* > map ){
    for ( const auto& string : map ){
        ANA_CHECK( book( TH1D ( "h_acc_" + string.first, "h_acc_" + string.first, 20, 0, 20) ) ) ;
        ANA_CHECK( book( TH1D ( "h_indiv_" + string.first, "h_indiv_" + string.first, 20, 0, 20) ) ) ;
        ANA_CHECK( book( TH1D ( "h_truthAcc_" + string.first, "h_truthAcc_" + string.first, 20, 0, 20) ) ) ;
        ANA_CHECK( book( TH1D ( "h_truthIndiv_" + string.first, "h_truthIndiv_" + string.first, 20, 0, 20) ) ) ;
    }



    return StatusCode::SUCCESS;
}

void MyxAODAnalysis::fillCutFlowHists( std::map<TString, multiplicityCutFlow* > map ){
    for ( const auto& string : map ){
        std::string type = string.first.Data();
        hist( "h_indiv_" + type )->Fill(string.second->indiv);
        hist( "h_acc_" + type )->Fill(string.second->acc);
        hist( "h_truthIndiv_" + type )->Fill(string.second->truthIndiv);
        hist( "h_truthAcc_" + type )->Fill(string.second->truthAcc);
    }
}

void MyxAODAnalysis::increaseCutFlow( multiplicityCutFlow* cutFlow, bool pass, bool &passAll, bool truth_el, int idx, int electron){
    // Electron
    if ( electron == 0 ){
        if ( pass ) {
            cutFlow->indiv++;
            hist( "h_ElectronIndivCutFlow" )->Fill(idx);
        }
        passAll = pass && passAll;
        if ( passAll ) {
            cutFlow->acc++;
            hist( "h_ElectronCutFlow" )->Fill(idx);

        }
        if ( truth_el ){
            if ( pass ) {
                cutFlow->truthIndiv++;
                hist( "h_ElectronIndivTruthCutFlow" )->Fill(idx);
            }
            if ( passAll ) {
                cutFlow->truthAcc++;
                hist( "h_ElectronTruthCutFlow" )->Fill(idx);
            }
        }
    }

    // Muon
    else if ( electron == 1){
        if ( pass ) {
            cutFlow->indiv++;
            hist( "h_MuonIndivCutFlow" )->Fill(idx);
        }
        passAll = pass && passAll;
        if ( passAll ) {
            cutFlow->acc++;
            hist( "h_MuonCutFlow" )->Fill(idx);

        }
        if ( truth_el ){
            if ( pass ) {
                cutFlow->truthIndiv++;
                hist( "h_MuonIndivTruthCutFlow" )->Fill(idx);
            }
            if ( passAll ) {
                cutFlow->truthAcc++;
                hist( "h_MuonTruthCutFlow" )->Fill(idx);
            }
        }
    }

    // Forward electron
    else if ( electron == 2){
        if ( pass ) {
            cutFlow->indiv++;
            hist( "h_FwdIndivCutFlow" )->Fill(idx);
        }
        passAll = pass && passAll;
        if ( passAll ) {
            cutFlow->acc++;
            hist( "h_FwdCutFlow" )->Fill(idx);

        }
        if ( truth_el ){
            if ( pass ) {
                cutFlow->truthIndiv++;
                hist( "h_FwdIndivTruthCutFlow" )->Fill(idx);
            }
            if ( passAll ) {
                cutFlow->truthAcc++;
                hist( "h_FwdTruthCutFlow" )->Fill(idx);
            }
        }
    }

    //Photon
    else if ( electron == 3){
        if ( pass ) {
            cutFlow->indiv++;
            hist( "h_PhotonIndivCutFlow" )->Fill(idx);
        }
        passAll = pass && passAll;
        if ( passAll ) {
            cutFlow->acc++;
            hist( "h_PhotonCutFlow" )->Fill(idx);

        }
        if ( truth_el ){
            if ( pass ) {
                cutFlow->truthIndiv++;
                hist( "h_PhotonIndivTruthCutFlow" )->Fill(idx);
            }
            if ( passAll ) {
                cutFlow->truthAcc++;
                hist( "h_PhotonTruthCutFlow" )->Fill(idx);
            }
        }
    }

    return;
}

std::vector < const xAOD::Electron* > MyxAODAnalysis::selectFwdElectronCandidates(){
    std::vector < const xAOD::Electron* > electrons_out;

    resetCutFlow( m_FwdCutFlows );

    fwd_TruthPointerIdx.clear();
    if ( event_isMC ){
        for ( const xAOD::TruthParticle* tp : *m_egammaTruthContainer ){
            if ( xAOD::EgammaHelpers::getRecoElectron( tp ) != nullptr){
                if ( xAOD::EgammaHelpers::getRecoElectron( tp )->author( xAOD::EgammaParameters::AuthorFwdElectron ) ) {
                    fwd_TruthPointerIdx.push_back(xAOD::EgammaHelpers::getRecoElectron( tp )->index());
                }
            }
        }
    }

    for ( const auto& fel : *m_fwdElectrons ){

        bool truth_el = false;
        if ( event_isMC ){
            for ( size_t i = 0; i<fwd_TruthPointerIdx.size(); i++){
                if ( fel->index() == fwd_TruthPointerIdx[i] ) truth_el = true;
            }
        }

        bool passAll = true;
        increaseCutFlow( m_FwdCutFlows["Init"], true, passAll, truth_el, 0, 2);

        if ( passAll ) electrons_out.push_back( fel );

        // No cuts are needed
        // // transverse momentum cut
        // increaseCutFlow( m_FwdCutFlows["Pt"], fel->pt() > 20e3, passAll, truth_el, 1, 2);
        //
        // // Eta cut
        // increaseCutFlow( m_FwdCutFlows["Eta"], abs( fel->eta() ) < 4.9, passAll, truth_el, 2, 2);
        //
        // // Object Quality
        // increaseCutFlow( m_FwdCutFlows["OQ"], fel->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON), passAll, truth_el, 3, 2);
        //
        // // // Medium Likelihood
        // // increaseCutFlow( m_FwdCutFlows["LHMedium"], bool(Accessor_fwdLHMedium( *fel )) , passAll, truth_el, 4, 2); // For DAODs
        // increaseCutFlow( m_FwdCutFlows["LHMedium"], bool(m_electronLHMediumFwd->accept( fel )) , passAll, truth_el, 4, 2); // For AODs

    }

    fillCutFlowHists(m_FwdCutFlows);

    return electrons_out;
}

std::vector < const xAOD::Photon* > MyxAODAnalysis::selectPhotonCandidates(){
    std::vector < const xAOD::Photon* > photons_out  ;

    resetCutFlow( m_PhotonCutFlows );

    pho_TruthPointerIdx.clear();
	pho_TruthPointer.clear();
    if ( event_isMC ){
        for ( const xAOD::TruthParticle* tp : *m_egammaTruthContainer ){
            if ( xAOD::EgammaHelpers::getRecoPhoton( tp ) != nullptr){ // Ikke null pointer
                if ( xAOD::EgammaHelpers::getRecoPhoton( tp )->author( xAOD::EgammaParameters::AuthorFwdElectron ) == false ){ // Ikke FWD
                    pho_TruthPointerIdx.push_back(xAOD::EgammaHelpers::getRecoPhoton( tp )->index()); // Alle rekonstruerede photoner som er matchet med truth photoner
					pho_TruthPointer.push_back( tp );
                }
            }
        }
    }

    for ( const xAOD::Photon* pho : *m_photons ){

        bool truth_pho = false;
        if ( event_isMC ){
            for ( size_t i = 0; i<pho_TruthPointerIdx.size(); i++){
                if ( pho->index() == pho_TruthPointerIdx[i] ) truth_pho = true;
            }
        }

        bool passAll = true;
        increaseCutFlow( m_PhotonCutFlows["Init"], true, passAll, truth_pho, 0, 3);

        // Overlap removal
        const ort::inputAccessor_t selectAcc("overlaps");
        increaseCutFlow( m_PhotonCutFlows["OR"], !selectAcc( *pho ), passAll, truth_pho, 1, 3);

        if ( passAll ) photons_out.push_back( pho );

        // We are not interested in cuts in this file:
        // // transverse momentum cut
        // increaseCutFlow( m_PhotonCutFlows["Pt"], pho->pt() > 4.5e3, passAll, truth_pho, 2, 3);
        //
        // // Isolation
        // increaseCutFlow( m_PhotonCutFlows["Iso"], m_isolationSelectionToolLoose->accept( *pho ), passAll, truth_pho, 5, 3);
        //
        // // Loose Likelihood
        // // increaseCutFlow( m_PhotonCutFlows["isPhotonEMLoose"], bool(Accessor_DFCommonPhotonsIsEMLoose( *pho )), passAll, truth_pho, 10, 3); // For DAODs
        // increaseCutFlow( m_PhotonCutFlows["isPhotonEMLoose"], bool( m_photonIsEMLoose->accept( pho ) ), passAll, truth_pho, 6, 3);
    }

    fillCutFlowHists(m_PhotonCutFlows);

    return photons_out;
}


std::vector < const xAOD::Electron* > MyxAODAnalysis::selectElectronCandidates(){
    std::vector < const xAOD::Electron* > electrons_out;

    resetCutFlow( m_ElectronCutFlows );

    ele_TruthPointerIdx.clear();
	ele_TruthPointer.clear();
    if ( event_isMC ){
        for ( const xAOD::TruthParticle* tp : *m_egammaTruthContainer ){
            if ( xAOD::EgammaHelpers::getRecoElectron( tp ) != nullptr){
                if (! xAOD::EgammaHelpers::getRecoElectron( tp )->author( xAOD::EgammaParameters::AuthorFwdElectron ) ) { // Ikke FWD (There is a !)
                    ele_TruthPointerIdx.push_back(xAOD::EgammaHelpers::getRecoElectron( tp )->index());
					ele_TruthPointer.push_back( tp );
                }
            }
        }
    }

    for ( const auto& ele : *m_electrons ){

        bool truth_el = false;
        if ( event_isMC ){
            for ( size_t i = 0; i<ele_TruthPointerIdx.size(); i++){
                if ( ele->index() == ele_TruthPointerIdx[i] ) truth_el = true;
            }
        }

        bool passAll = true;
        increaseCutFlow( m_ElectronCutFlows["Init"], true, passAll, truth_el, 0, 0);

        // Overlap removal
        const ort::inputAccessor_t selectAcc("overlaps");
        increaseCutFlow( m_ElectronCutFlows["OR"], !selectAcc( *ele ), passAll, truth_el, 1, 0);

        if ( passAll ) electrons_out.push_back( ele );

        // // transverse momentum cut
        // // increaseCutFlow( m_ElectronCutFlows["Pt"], ele->pt() >  20e3, passAll, truth_el, 1, 0); // Inspired by forward
        // increaseCutFlow( m_ElectronCutFlows["Pt"], ele->pt() > 4.5e3, passAll, truth_el, 2, 0); // Inspired by photon
        //
        // // Eta cut
        // // increaseCutFlow( m_ElectronCutFlows["Eta"], abs( ele->eta() ) < 4.9, passAll, truth_el, 2, 0);
        //
        // // Isolation
        // increaseCutFlow( m_ElectronCutFlows["Iso"], m_isolationSelectionToolLoose->accept( *ele ), passAll, truth_el, 4, 0);
        //
        // // // Medium Likelihood
        // // increaseCutFlow( m_ElectronCutFlows["LHLoose"], bool(Accessor_LHLoose( *ele )) , passAll, truth_el, 4, 0); // For DAODs
        // increaseCutFlow( m_ElectronCutFlows["LHLoose"], bool(m_electronLHLoose->accept( ele )) , passAll, truth_el, 5, 0); // For AODs
    }

    fillCutFlowHists(m_ElectronCutFlows);

    return electrons_out;
}

std::vector < const xAOD::Muon* > MyxAODAnalysis::selectMuonCandidates(){
    std::vector < const xAOD::Muon* > muons_out;

    resetCutFlow( m_MuonCutFlows );

    // muo_TruthPointerIdx.clear();
	// muo_TruthPointer.clear();
    // if ( event_isMC ){
    //     for ( const xAOD::TruthParticle* tp : *m_egammaTruthContainer ){
    //         if ( xAOD::EgammaHelpers::getRecoMuon( tp ) != nullptr){
    //             if (! xAOD::EgammaHelpers::getRecoMuon( tp )->author( xAOD::EgammaParameters::AuthorFwdElectron ) == false ){ // Ikke FWD
    //                 muo_TruthPointerIdx.push_back(xAOD::EgammaHelpers::getRecoMuon( tp )->index());
	// 				muo_TruthPointer.push_back( tp );
    //             }
    //         }
    //     }
    // }

    for ( const auto& muon : *m_muons ){

        bool truth_muon = false;
        // if ( event_isMC ){
        //     for ( size_t i = 0; i<muo_TruthPointerIdx.size(); i++){
        //         if ( muon->index() == muo_TruthPointerIdx[i] ) truth_muon = true;
        //     }
        // }

        bool passAll = true;
        increaseCutFlow( m_MuonCutFlows["Init"], true, passAll, truth_muon, 0, 1);

        // Overlap removal
        const ort::inputAccessor_t selectAcc("overlaps");
        increaseCutFlow( m_MuonCutFlows["OR"], !selectAcc( *muon ), passAll, truth_muon, 1, 1);

        if ( passAll ) muons_out.push_back( muon );

    }

    fillCutFlowHists(m_MuonCutFlows);

    return muons_out;
}

MyxAODAnalysis :: ~MyxAODAnalysis () {

}

StatusCode MyxAODAnalysis :: finalize ()
{
  ANA_MSG_DEBUG("::finalize: Finalizing...");
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

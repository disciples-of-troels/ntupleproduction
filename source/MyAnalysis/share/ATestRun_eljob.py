#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
parser.add_option( '-f', '--input-file', dest = 'input_file',
                   action = 'store', type = 'string', default = 'sample',
                   help = 'Input directory for EventLoop' )
parser.add_option( '-e', '--events', dest = 'events',
                   action = 'store', type = 'int', default = -1,
                   help = 'Number of events to run' )
parser.add_option( '-n', '--nFiles', dest = 'nFiles',
                   action = 'store', type = 'int', default = -1,
                   help = 'Number of files to run on (only interesting for the grid)' )
parser.add_option( '-g', '--grid', dest = 'grid',
                   action = 'store', type = 'int', default = 0,
                   help = 'Run on grid' )
parser.add_option( '-o', '--outputName', dest = 'outputName',
                   action = 'store', type = 'string', default = "user.lehrke.test",
                   help = 'Name of the outputFile' )


( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
if not options.grid:
    ROOT.SH.ScanDir().scan( sh, options.input_file )
else:
    ROOT.SH.scanRucio (sh, options.input_file )
if ( "mc16_13TeV" in options.input_file ):
    isData = False
else:
    isData = True
sh.printContent()


# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, options.events )
# Skip events used for debugging
# job.options().setDouble( ROOT.EL.Job.optSkipEvents, 295)
job.outputAdd(ROOT.EL.OutputStream('ANALYSIS'))

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm, addPrivateTool
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

alg.OutputLevel = 3
# Set output level to debug
# alg.OutputLevel = ROOT.MSG.DEBUG


# ============================================================================
# Setting up tools
# ============================================================================


# ============================================================================
# Event
# ============================================================================
# GRL
addPrivateTool( alg, 'grlTool', 'GoodRunsListSelectionTool' )
# GRLFilePath = "GoodRunsLists/data17_13TeV/20190708/data17_13TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
GRLFilePath = "GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
# GRLFilePath = "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"

alg.grlTool.GoodRunsListVec = [ GRLFilePath ]
alg.grlTool.PassThrough = 0 # if true (default) will ignore result of GRL and will just pass all events

# PRW
addPrivateTool( alg, 'pileupReweighting', 'CP::PileupReweightingTool')
# # OLD
# listOfLumicalcFiles = ["GoodRunsLists/data17_13TeV/20190708/ilumicalc_histograms_None_325713-340453_OflLumi-13TeV-010.root"]
listOfLumicalcFiles = ["GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"]
# # Current
# listOfLumicalcFiles = ["GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"]

# # OLD
# PRWConfigFiles = [ 'MyAnalysis/NTUP_PILEUP.12973660._000001.pool.root.1',"GoodRunsLists/data17_13TeV/20190708/purw.actualMu.2017.root"]
# PRWConfigFiles = [ 'MyAnalysis/combined_prw_2017.root',"GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"]"GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"
# PRWConfigFiles = ['MyAnalysis/NTUP_PILEUP.13182909._000001.pool.root.1'] # old Zee - mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_e5984_s3126_r10201_r10210_p3918

# # MC_HZllgam
# PRWConfigFiles = [ 'MyAnalysis/NTUP_PILEUP.14204493._000004.pool.root.1'] # For mc16_13TeV.344303.MPIOFF_ggH125_Zllgam.merge.AOD.e5145_e5984_s3126_r10201_r10210_v0

# # MC_modelFebruary2020
# [Wminusenu, Wplusenu, Zee, ttbar]
# PRWConfigFiles = ['MyAnalysis/NTUP_PILEUP.13182880._000001.pool.root.1', 'MyAnalysis/NTUP_PILEUP.13182843._000001.pool.root.1','MyAnalysis/Zee_pileup_merge.root', 'MyAnalysis/ttbar_pileup_merge.root' ]
# DYee
# PRWConfigFiles = ['MyAnalysis/NTUP_PILEUP.12974783._000001.pool.root.1'] # DYee

# # MC_otherEGAM
# PRWConfigFiles = [ 'MyAnalysis/NTUP_PILEUP.12911415._000001.pool.root.1'] # For mc16_13TeV.301535.eegammaPt10_35.deriv.DAOD_EGAM3.e3952_s3126_r10201_r10210_p3956_v0

# # dataMuon (Sara)
# PRWConfigFiles = ['MyAnalysis/Zmumu_pileup_merge.root'] # Zmumu
# PRWConfigFiles = ['MyAnalysis/NTUP_PILEUP.12911423._000001.pool.root.1'] # mumugammaPt10_35 EGAM4
# PRWConfigFiles = ['MyAnalysis/NTUP_PILEUP.17252366._000001.pool.root.1'] # Zmumu test 13.10.20
PRWConfigFiles = ['MyAnalysis/NTUP_PILEUP.18373650._000003.pool.root.1'] # Zmumu test2 13.10.20


alg.pileupReweighting.ConfigFiles = PRWConfigFiles
alg.pileupReweighting.LumiCalcFiles = listOfLumicalcFiles


# ============================================================================
# Electron
# ============================================================================

# electron/photon calibration and smearing
addPrivateTool( alg, 'egammaCalibrationAndSmearingTool', 'CP::EgammaCalibrationAndSmearingTool')
alg.egammaCalibrationAndSmearingTool.ESModel = "es2018_R21_v0"
alg.egammaCalibrationAndSmearingTool.randomRunNumber = 123456
alg.egammaCalibrationAndSmearingTool.doSmearing = 0

# electron/photon shifting
addPrivateTool( alg, 'MCShifterTool', 'ElectronPhotonShowerShapeFudgeTool')
alg.MCShifterTool.Preselection = 22
alg.MCShifterTool.FFCalibFile = "ElectronPhotonShowerShapeFudgeTool/v2/PhotonFudgeFactors.root"

# electron LH selector
addPrivateTool( alg, 'electronLHLoose', 'AsgElectronLikelihoodTool')
alg.electronLHLoose.WorkingPoint = "LooseLHElectron"

addPrivateTool( alg, 'electronLHMedium', 'AsgElectronLikelihoodTool')
alg.electronLHMedium.WorkingPoint = "MediumLHElectron"

addPrivateTool( alg, 'electronLHTight', 'AsgElectronLikelihoodTool')
alg.electronLHTight.WorkingPoint = "TightLHElectron"

# ECIDS tool
addPrivateTool( alg, 'ECIDSToolLoose', 'AsgElectronChargeIDSelectorTool')
ECIDSFilePath = "ElectronPhotonSelectorTools/ChargeID/ECIDS_20180731rel21Summer2018.root"
alg.ECIDSToolLoose.TrainingFile = ECIDSFilePath

# Electron SF Reco
addPrivateTool( alg, 'electronSFReco', 'AsgElectronEfficiencyCorrectionTool')
alg.electronSFReco.RecoKey = "Reconstruction"

# Electron SF ID
addPrivateTool( alg, 'electronSFID', 'AsgElectronEfficiencyCorrectionTool')
alg.electronSFID.IdKey = "Medium"

# Electron SF Iso
addPrivateTool( alg, 'electronSFIso', 'AsgElectronEfficiencyCorrectionTool')
alg.electronSFIso.IdKey = "Medium"
alg.electronSFIso.IsoKey = "Gradient"

# ============================================================================
# Photon
# ============================================================================

# photon EM selector
addPrivateTool( alg, 'photonIsEMLoose', 'AsgPhotonIsEMSelector')
alg.photonIsEMLoose.isEMMask = 31745
alg.photonIsEMLoose.ConfigFile = "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf"

addPrivateTool( alg, 'photonIsEMTight', 'AsgPhotonIsEMSelector')
alg.photonIsEMTight.isEMMask = 4193281
alg.photonIsEMTight.ConfigFile = "ElectronPhotonSelectorTools/offline/20180825/PhotonIsEMTightSelectorCutDefs.conf"

# Photon SF Reco
addPrivateTool( alg, 'photonSFReco', 'AsgPhotonEfficiencyCorrectionTool')
alg.photonSFReco.ForceDataType = 1

# Photon SF ID
addPrivateTool( alg, 'photonSFID', 'AsgPhotonEfficiencyCorrectionTool')
alg.photonSFID.ForceDataType = 1

# Photon SF Iso
addPrivateTool( alg, 'photonSFIso', 'AsgPhotonEfficiencyCorrectionTool')
alg.photonSFIso.IsoKey = "Loose"
alg.photonSFIso.ForceDataType = 1

# ============================================================================
# Forward electron
# ============================================================================

# Forward Electron SF Reco
addPrivateTool( alg, 'electronSFRecoFwd', 'AsgElectronEfficiencyCorrectionTool')
alg.electronSFRecoFwd.RecoKey = "Reconstruction"

# Forward Electron SF ID
addPrivateTool( alg, 'electronSFIDFwd', 'AsgElectronEfficiencyCorrectionTool')
alg.electronSFIDFwd.IdKey = "FwdMedium"

# electron LH selector
addPrivateTool( alg, 'electronLHLooseFwd', 'AsgForwardElectronLikelihoodTool')
alg.electronLHLooseFwd.WorkingPoint = "LooseLHForwardElectron"

addPrivateTool( alg, 'electronLHMediumFwd', 'AsgForwardElectronLikelihoodTool')
alg.electronLHMediumFwd.WorkingPoint = "MediumLHForwardElectron"

addPrivateTool( alg, 'electronLHTightFwd', 'AsgForwardElectronLikelihoodTool')
alg.electronLHTightFwd.WorkingPoint = "TightLHForwardElectron"

# ============================================================================
# Muon
# ============================================================================
# muon calibration and smearing
addPrivateTool( alg, 'muonCalibrationSmearingTool', 'CP::MuonCalibrationAndSmearingTool')
alg.muonCalibrationSmearingTool.Year = "Data18"

# muon selection
addPrivateTool( alg, 'muonSelectionToolLoose', 'CP::MuonSelectionTool')
alg.muonSelectionToolLoose.MaxEta = 2.7
alg.muonSelectionToolLoose.MuQuality = 2

addPrivateTool( alg, 'muonSelectionToolMedium', 'CP::MuonSelectionTool')
alg.muonSelectionToolMedium.MaxEta = 2.7
alg.muonSelectionToolMedium.MuQuality = 1

addPrivateTool( alg, 'muonSelectionToolTight', 'CP::MuonSelectionTool')
alg.muonSelectionToolTight.MaxEta = 2.7
alg.muonSelectionToolTight.MuQuality = 0

addPrivateTool( alg, 'muonSF', 'CP::MuonEfficiencyScaleFactors')
alg.muonSF.WorkingPoint = 'Medium'


# addPrivateTool( alg, 'muonSFIso', 'CP::MuonEfficiencyScaleFactors')
# alg.muonSFIso.WorkingPoint = 'GradientIso'

# ============================================================================
# Isolation
# ============================================================================
# isolation electron/muon
addPrivateTool( alg, 'isolationSelectionToolLoose', 'CP::IsolationSelectionTool')
alg.isolationSelectionToolLoose.ElectronWP = "Gradient"
alg.isolationSelectionToolLoose.MuonWP = "FCLoose"  # CHANGED! https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RecommendedIsolationWPs?fbclid=IwAR1Ujvfvn_B7Ff_LTBNzn8haAWYYqNug3_ygmoTzua4FLZWUKtM8twIkavk


# ============================================================================
# Jets
# ============================================================================
# jet calibration
addPrivateTool( alg, "jetCalibrationTool", "JetCalibrationTool")
# alg.jetCalibrationTool.JetCollection = "AntiKt4EMPFlow" # For AODs and DAODs
# alg.jetCalibrationTool.ConfigFile = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config" # For AODs and DAODs
alg.jetCalibrationTool.JetCollection = "AntiKt4EMTopo"
alg.jetCalibrationTool.ConfigFile = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config"
if ( isData ):
	calibSequence = "JetArea_Residual_EtaJES_GSC_Insitu"
else:
	calibSequence = "JetArea_Residual_EtaJES_GSC_Smear"
alg.jetCalibrationTool.CalibSequence = calibSequence
alg.jetCalibrationTool.CalibArea = "00-04-82"
alg.jetCalibrationTool.IsData = isData

# jet cleaning - AODs
addPrivateTool( alg, "jetCleaningTool", "JetCleaningTool")
alg.jetCleaningTool.CutLevel = "LooseBad"
alg.jetCleaningTool.DoUgly = False


# ============================================================================
# MET
# ============================================================================
# met maker
addPrivateTool( alg, "metutil", "met::METMaker")
alg.metutil.DoMuonEloss = False
alg.metutil.DoRemoveMuonJets = True
alg.metutil.DoSetMuonJetEMScale = True


# ============================================================================
# LightGBM
# ============================================================================
#
# # Electron
# addPrivateTool( alg, "ele_lgbmPdfVar", "LightGBMPredictor")
# alg.ele_lgbmPdfVar.ConfigFile = "2019_05_16_14_1463528748"
#
# addPrivateTool( alg, "ele_lgbmPdfCutVar", "LightGBMPredictor")
# alg.ele_lgbmPdfCutVar.ConfigFile = "2019_05_16_19_2015088566"
#
#
# # Photon
# # old
# # addPrivateTool( alg, "pho_lgbmPdfVar", "LightGBMPredictor")
# # alg.pho_lgbmPdfVar.ConfigFile = "2019_08_15_11_47234812"
# # addPrivateTool( alg, "pho_lgbmPdfConvVar", "LightGBMPredictor")
# # alg.pho_lgbmPdfConvVar.ConfigFile = "2019_08_15_15_2437115810"
# # addPrivateTool( alg, "pho_lgbmPdfBinVar", "LightGBMPredictor")
# # alg.pho_lgbmPdfBinVar.ConfigFile = "2019_08_15_18_956659904"
#
# # Trained on data
# addPrivateTool( alg, "pho_lgbmPdfVar_data", "LightGBMPredictor")
# alg.pho_lgbmPdfVar_data.ConfigFile = "PhotonDATA_pdf_model_phoPdf01"
# addPrivateTool( alg, "pho_lgbmPdfConvVar_data", "LightGBMPredictor")
# alg.pho_lgbmPdfConvVar_data.ConfigFile = "PhotonDATA_conv_model_phoConv01"
# addPrivateTool( alg, "pho_lgbmPdfBinVar_data", "LightGBMPredictor")
# alg.pho_lgbmPdfBinVar_data.ConfigFile = "PhotonDATA_bin_model_phoBin01"
#
# # Trained on mc
# addPrivateTool( alg, "pho_lgbmPdfVar_mc", "LightGBMPredictor")
# alg.pho_lgbmPdfVar_mc.ConfigFile = "PhotonMC_pdf_model_phoPdf02"
# addPrivateTool( alg, "pho_lgbmPdfConvVar_mc", "LightGBMPredictor")
# alg.pho_lgbmPdfConvVar_mc.ConfigFile = "PhotonMC_conv_model_phoConv02"
# addPrivateTool( alg, "pho_lgbmPdfBinVar_mc", "LightGBMPredictor")
# alg.pho_lgbmPdfBinVar_mc.ConfigFile = "PhotonMC_bin_model_phoBin02"
#
# # Other
# # addPrivateTool( alg, "pho_lgbmPdfNoConvVar", "LightGBMPredictor") # Uses maxEcell_energy that is not available for AODs
# # alg.pho_lgbmPdfNoConvVar.ConfigFile = "2019_08_21_19_3307376757"
# # addPrivateTool( alg, "pho_lgbmPdfNoCellVar", "LightGBMPredictor")
# # alg.pho_lgbmPdfNoCellVar.ConfigFile = "2019_23_08_20_4210130482"
# # addPrivateTool( alg, "pho_lgbmPdfExtVar", "LightGBMPredictor") # Uses maxEcell_energy that is not available for AODs
# # alg.pho_lgbmPdfExtVar.ConfigFile = "2019_08_15_22_896011677"
# addPrivateTool( alg, "pho_lgbmIso", "LightGBMPredictor")
# alg.pho_lgbmIso.ConfigFile = "2019_08_15_9_3898251691"
#
#
# # Forward electron
# addPrivateTool( alg, "fwd_lgbmPid", "LightGBMPredictor")
# alg.fwd_lgbmPid.ConfigFile = "2019_06_24_6_3697977657"
# # alg.fwd_lgbmPid.ConfigFile = "fwd_PID_model_6ATvars"
# # alg.fwd_lgbmPid.ConfigFile = "fwd_PID_model_6vars" // Not completely implemented
#
# addPrivateTool( alg, "fwd_lgbmEr", "LightGBMPredictor")
# alg.fwd_lgbmEr.ConfigFile = "2019_06_24_9_1305298504"
# # alg.fwd_lgbmEr.ConfigFile = "fwd_ER_model_6vars" // Not implemented
#
# addPrivateTool( alg, "fwd_lgbmIso", "LightGBMPredictor")
# alg.fwd_lgbmIso.ConfigFile = "2019_06_24_4_2590916238"
# # alg.fwd_lgbmIso.ConfigFile = "fwd_ISO_model_7vars" // Not implemented

# ============================================================================
# Images
# ============================================================================
# addPrivateTool( alg, "imageCreation", "ImageCreation")
# alg.imageCreation.Time = True


# Add our algorithm to the job
job.algsAdd( alg )


# Run the job using the direct driver.
if ( not options.grid ):
    driver = ROOT.EL.DirectDriver()
    driver.submit( job, options.submission_dir )
else:
    driver = ROOT.EL.PrunDriver()
    driver.options().setString("nc_outputSampleName", options.outputName )
    driver.options().setDouble( ROOT.EL.Job.optGridNFiles, options.nFiles )
    driver.submitOnly( job, options.submission_dir)

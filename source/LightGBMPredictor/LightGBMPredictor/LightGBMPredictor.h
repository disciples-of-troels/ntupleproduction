#ifndef LIGHTGBMPREDICTOR_LIGHTGBMPREDICTORDICT_H_
#define LIGHTGBMPREDICTOR_LIGHTGBMPREDICTORDICT_H_

#include <string>
#include <set>

#include "AsgTools/AsgTool.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "LightGBMPredictor/ILightGBMPredictor.h"

// A bunch of includes
#include <LightGBM/config.h>
#include <LightGBM/dataset_loader.h>
#include <LightGBM/boosting.h>
#include <LightGBM/prediction_early_stop.h>
#include <LightGBM/objective_function.h>
#include <LightGBM/metric.h>
#include <LightGBM/utils/common.h>

#include <fstream>
#include <TSystem.h>
#include <array>
#include <string>


#ifndef XAOD_ANALYSIS
#include "GaudiKernel/ToolHandle.h"
#endif



class LightGBMPredictor :
virtual public ILightGBMPredictor, public asg::AsgTool {
  ASG_TOOL_CLASS2(LightGBMPredictor, ILightGBMPredictor, IAsgTool)

 public:
  explicit LightGBMPredictor(const std::string& type);
  virtual ~LightGBMPredictor() { delete m_booster; }


  StatusCode initialize() override ;
  StatusCode finalize()
#ifndef XAOD_ANALYSIS
    override
#endif
    ;

  double predict(const xAOD::Electron& electron) const;
  double predict(const xAOD::Photon& photon, float averageMu, int NvtxReco) const;
  double predict(const xAOD::Electron& electron, int NvtxReco, float actualMu ) const;


 private:

  LightGBM::Boosting* m_booster = nullptr;
  LightGBM::PredictionEarlyStopInstance m_early_stop;


  TString m_configFile = "";
  TString m_hash = "";
  const SG::AuxElement::ConstAccessor< float > deltaPhiRescaled2;
  const SG::AuxElement::ConstAccessor< float > f3;
  const SG::AuxElement::ConstAccessor< float > deltaEta1;
  const SG::AuxElement::ConstAccessor< UChar_t > numberOfInnermostPixelHits;
  const SG::AuxElement::ConstAccessor< UChar_t > numberOfPixelHits;
  const SG::AuxElement::ConstAccessor< UChar_t > numberOfSCTHits;
  const SG::AuxElement::ConstAccessor< UChar_t > numberOfPixelDeadSensors;
  const SG::AuxElement::ConstAccessor< UChar_t > numberOfSCTDeadSensors;

  const SG::AuxElement::ConstAccessor< float > topoetcone20;
  const SG::AuxElement::ConstAccessor< float > topoetcone30;
  const SG::AuxElement::ConstAccessor< float > topoetcone40;
  const SG::AuxElement::ConstAccessor< float > ptvarcone20;
  const SG::AuxElement::ConstAccessor< float > ptvarcone30;
  const SG::AuxElement::ConstAccessor< float > ptvarcone40;
  const SG::AuxElement::ConstAccessor< float > topoetconecoreConeEnergyCorrection;
  const SG::AuxElement::ConstAccessor< float > core57cellsEnergyCorrection;
  const SG::AuxElement::ConstAccessor< float > DeltaE;
  const SG::AuxElement::ConstAccessor< float > Eratio;
  const SG::AuxElement::ConstAccessor< float > eta;
  const SG::AuxElement::ConstAccessor< float > f1;
  const SG::AuxElement::ConstAccessor< float > fracs1;
  const SG::AuxElement::ConstAccessor< float > maxEcell_energy;
  const SG::AuxElement::ConstAccessor< float > maxEcell_time;
  const SG::AuxElement::ConstAccessor< float > r33over37allcalo;
  const SG::AuxElement::ConstAccessor< float > Reta;
  const SG::AuxElement::ConstAccessor< float > Rhad;
  const SG::AuxElement::ConstAccessor< float > Rhad1;
  const SG::AuxElement::ConstAccessor< float > Rphi;
  const SG::AuxElement::ConstAccessor< float > weta1;
  const SG::AuxElement::ConstAccessor< float > weta2;
  const SG::AuxElement::ConstAccessor< float > wtots1;

  const SG::AuxElement::ConstAccessor< float > Accessor_pt1{ "pt1" } ; //!
  const SG::AuxElement::ConstAccessor< float > Accessor_pt2{ "pt2" } ; //!

  std::tuple< float, float, float, float > conversionVars( const xAOD::Photon photon) const ;
  float et_calo(const xAOD::Photon photon) const ;
  float d0(const xAOD::TrackParticle track) const ;
  float d0sigma(const xAOD::TrackParticle track) const ;
  float d0Sig( const xAOD::TrackParticle track ) const ;
  float dPOverP(const xAOD::TrackParticle track ) const ;
  float TRTPID( const xAOD::TrackParticle track ) const ;
  UChar_t numberOfPixelHitsPlusDead( const xAOD::TrackParticle track ) const ;
  UChar_t numberOfSCTHitsPlusDead( const xAOD::TrackParticle track ) const ;
  float EptRatio( const xAOD::Electron electron ) const ;
  float convEtOverPt(const xAOD::Photon* ph) const ;
  float convPtRatio(const xAOD::Photon* ph) const ;

  float deltaR( const float eta1, const float phi1, const float eta2, const float phi2 ) const ;


};

#endif  // IFFTRUTHCLASSIFIER_IFFTRUTHCLASSIFIER_H_

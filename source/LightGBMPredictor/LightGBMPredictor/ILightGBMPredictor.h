#ifndef LIGHTGBMPREDICTOR_ILIGHTGBMPREDICTORDICT_H_
#define LIGHTGBMPREDICTOR_ILIGHTGBMPREDICTORDICT_H_

#include "AsgTools/AsgTool.h"
#include "xAODEgamma/Electron.h"


class ILightGBMPredictor : virtual public asg::IAsgTool {
  ASG_TOOL_INTERFACE(ILightGBMPredictor)
 public:
  virtual ~ILightGBMPredictor() {}
  
  virtual double predict( const xAOD::Electron& electron) const=0;
  virtual double predict( const xAOD::Photon& photon, float averageMu, int NvtxReco) const=0;
  virtual double predict( const xAOD::Electron& electron, int NvtxReco, float actualMu ) const=0;

};

#endif  // LIGHTGBMPREDICTOR_ILIGHTGBMPREDICTORDICT_H_

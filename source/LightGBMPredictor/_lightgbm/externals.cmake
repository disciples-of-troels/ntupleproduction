# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building lightgbm
#

set( _buildDir
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/_lightgbmBuild )

# Set up the variables that the users can pick up RestFrame with:
set( LIGHTGBM_INCLUDE_DIRS
  $<BUILD_INTERFACE:${_buildDir}/include>
  $<INSTALL_INTERFACE:include>
  ${ROOT_INCLUDE_DIRS} )
set( LIGHTGBM_LIBRARIES
   ${_buildDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}_lightgbm${CMAKE_SHARED_LIBRARY_SUFFIX} )

# If you need to debug code and want to keep line numbers in the stacktrace
#set( CMAKE_CXX_FLAGS "-g -Og -rdynamic" )

# Set up the build of lightgbm for the build area:
ExternalProject_Add( _lightgbm
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   GIT_REPOSITORY https://github.com/Microsoft/LightGBM.git
   GIT_TAG stable
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   CMAKE_ARGS -DUSE_OPENMP=OFF -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
   LOG_CONFIGURE 1
   BUILD_IN_SOURCE 1
   INSTALL_COMMAND COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR>
   BUILD_BYPRODUCTS ${RESTFRAMES_LIBRARIES} )

# Install
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS )

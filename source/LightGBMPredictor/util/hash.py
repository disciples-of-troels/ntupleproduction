import zlib

iso =       ['p_topoetcone20','p_topoetcone30','p_topoetcone40','p_topoetconecoreConeEnergyCorrection']


example_vars = ['p_Rhad1',
                'p_Rhad',
                'p_f3',
                'p_weta2',
                'p_Rphi',
                'p_Reta',
                'p_Eratio',
                'p_f1',
                'p_numberOfInnermostPixelHits',
                'p_numberOfPixelHits',
                'p_numberOfSCTHits',
                'p_d0',
                'p_d0Sig',
                'p_dPOverP',
                'p_deltaEta1',
                'p_deltaPhiRescaled2',
                'p_EptRatio',
                'p_eProbHT',
                'pX_wtots1']

example_vars.sort(key=str.lower)
varhash = f"{len(example_vars)}_{zlib.crc32(bytes(':'.join(example_vars), 'utf-8'))}"
print('example_vars', varhash)


iso.sort(key=str.lower)
varhash = f"{len(iso)}_{zlib.crc32(bytes(':'.join(iso), 'utf-8'))}"
print('iso', varhash)


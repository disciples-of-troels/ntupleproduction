# About 
This tool gives a transformed prediction of being a signal electron by LightGBM

# Implementation
To use this tool clone this repository into your source folder. 

In your CMakeLists.txt inside the package folder ( e.g. MyAnalysis )
~~~shell
atlas_add_library ([...]
    PRIVATE_LINK_LIBRARIES [...] LightGBMPredictorLib )


atlas_add_dictionary ([...]
    LINK_LIBRARIES [...] LightGBMPredictorLib)


atlas_add_component ([...]
    LINK_LIBRARIES [...] LightGBMPredictorLib)
~~~

In the CMakeLists.txt in the source folder ( you only add the middle part, the rest is to specify the position where to put it )
~~~shell
# Find the AnalysisBase project. This is what, amongst other things, pulls
# in the definition of all of the "atlas_" prefixed functions/macros.
find_package( ${ATLAS_PROJECT} REQUIRED )

# Include the externals configuration:
include( LightGBMPredictor/_lightgbm/externals.cmake )

# Set up CTest. This makes sure that per-package build log files can be
# created if the user so chooses.
atlas_ctest_setup()
~~~


If your run script/executable (after compiling) complains about: `YourAnalysisExe: error while loading shared libraries: lib_lightgbm.so: cannot open shared object file: No such file or directory`, see https://gitlab.cern.ch/dnielsen/_lightgbm

# Usage in AnalysisBase ( as an AnaToolHandle )

In your header file
~~~c++
#include "LightGBMPredictor/LightGBMPredictor.h"

~

asg::AnaToolHandle<ILightGBMPredictor> m_lgbm;

~~~

In your source file ( this follows the algorithm structure in the Analysis Software Tutorial https://atlassoftwaredocs.web.cern.ch/ABtutorial/ )
~~~c++
MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator),
    m_lgbm ("LightGBMPredictor", this)

~

ANA_CHECK( m_lgbm->setProperty("ConfigFile", "") ); // This is one possible config file. There needs to be a config file set, otherwise the Tool will fail to run
ANA_CHECK( m_lgbm->initialize() );

~~~


#include <set>
#include <string>

#include "LightGBMPredictor/LightGBMPredictor.h"
#include "xAODTracking/Vertex.h"
#include "xAODCaloEvent/CaloCluster.h"

LightGBMPredictor::LightGBMPredictor(const std::string& type) :
    asg::AsgTool(type),
    deltaPhiRescaled2("deltaPhiRescaled2"),
    f3("f3"),
    deltaEta1("deltaEta1"),
    numberOfInnermostPixelHits("numberOfInnermostPixelLayerHits"),
    numberOfPixelHits("numberOfPixelHits"),
    numberOfSCTHits("numberOfSCTHits"),
    numberOfPixelDeadSensors("numberOfPixelDeadSensors"),
    numberOfSCTDeadSensors("numberOfSCTDeadSensors"),
    topoetcone20("topoetcone20"),
    topoetcone30("topoetcone30"),
    topoetcone40("topoetcone40"),
    ptvarcone20("ptvarcone20"),
    ptvarcone30("ptvarcone30"),
    ptvarcone40("ptvarcone40"),
    topoetconecoreConeEnergyCorrection("topoetconecoreConeEnergyCorrection"),
    core57cellsEnergyCorrection("core57cellsEnergyCorrection"),
    DeltaE("DeltaE"),
    Eratio("Eratio"),
    eta("eta"),
    f1("f1"),
    fracs1("fracs1"),
    maxEcell_energy("maxEcell_energy"),
    maxEcell_time("maxEcell_time"),
    r33over37allcalo("r33over37allcalo"),
    Reta("Reta"),
    Rhad("Rhad"),
    Rhad1("Rhad1"),
    Rphi("Rphi"),
    weta1("weta1"),
    weta2("weta2"),
    wtots1("wtots1")
    {
        declareProperty("ConfigFile",m_configFile="","The config file to use");
    }



StatusCode LightGBMPredictor::initialize(){
    ATH_MSG_INFO("Initialize LightGBMPredictor");
    if ( m_configFile.Length() > 0 ){

        // Make an input file object
        std::ifstream input_file;

        // Open your trained model
        std::string filename = gSystem->Getenv("WorkDir_DIR"); //

        filename += "/data/LightGBMPredictor/";
        filename += m_configFile ;
        filename += ".txt";
        input_file.open( filename, std::ios::in | std::ios::binary );
        // Make an std::string from the file
        std::string model_str((std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());

        // Get length as well since that is needed when passing the raw string
        size_t len = std::strlen(model_str.data());

        if ( len == 0 ){
            ANA_MSG_ERROR("Config file could not be loaded. It is not located in the data directory");
            return StatusCode::FAILURE;
        }

        // Create a gbdt and not from file (since that apparently freezes)
        m_booster = LightGBM::Boosting::CreateBoosting("gbdt", nullptr);
        // Load model from the string from your model file
        m_booster->LoadModelFromString(model_str.data(), len);

        // close input file ( not necessary, but to be explicit )
        input_file.close();

        // create early stopping instance, this is needed for the prediction function of LGBM
        m_early_stop = CreatePredictionEarlyStopInstance("none", LightGBM::PredictionEarlyStopConfig());

        // get hash of the config file, such that it knows which variables to use in which order
        TObjArray *tx = m_configFile.Tokenize("_");
        m_hash = ((TObjString *)(tx->Last()))->String();
        return StatusCode::SUCCESS;
    }
    else{
        ATH_MSG_ERROR("No config file set. Please specify one");
        return StatusCode::FAILURE;
    }
}

StatusCode LightGBMPredictor::finalize() {
  ATH_MSG_INFO("Finalize LightGBMPredictor");
  return StatusCode::SUCCESS;
}

float LightGBMPredictor::d0(const xAOD::TrackParticle track) const{
    return track.d0();
}

float LightGBMPredictor::d0sigma(const xAOD::TrackParticle track) const{
    return sqrt( ( track.definingParametersCovMatrix() )( 0, 0 ) );;
}

float LightGBMPredictor::d0Sig( const xAOD::TrackParticle track ) const{
    float d0 = LightGBMPredictor::d0(track);
    float d0sigma = LightGBMPredictor::d0sigma(track);
    return ( d0sigma > 0.0f ) ? d0 / d0sigma : -999.f;
}

float LightGBMPredictor::dPOverP(const xAOD::TrackParticle track ) const{
    float dPOverP = -999;
    unsigned int index;
    if ( track.indexOfParameterAtPosition( index, xAOD::LastMeasurement ) ) {
        const double refittedTrack_LMqoverp = double( track.charge() ) / sqrt( std::pow( track.parameterPX( index ), 2 ) +
                std::pow( track.parameterPY( index ), 2 ) +
                std::pow( track.parameterPZ( index ), 2 ));
        dPOverP = 1.0 - track.qOverP()/refittedTrack_LMqoverp;
        if ( dPOverP < -4.0 ) dPOverP = -4.0;
    } else {
        dPOverP = -999.f;
    }

    return dPOverP;
}

float LightGBMPredictor::TRTPID( const xAOD::TrackParticle track ) const{
    float eProbHT;
    bool transTRTPID = true;
    if ( transTRTPID == true ) {
        constexpr double tau = 15.0;
        constexpr double fEpsilon = 1.0e-30;  // to avoid zero division
        //Transform the TRT PID output for use in the LH tool.
        track.summaryValue( eProbHT, ( xAOD::SummaryType )48 ); // type 48 = eProbabilityHT
        double pid_tmp = eProbHT;
        if ( pid_tmp >= 1.0 ) pid_tmp = 1.0 - 1.0e-15;  //this number comes from TMVA
        else if ( pid_tmp <= fEpsilon ) pid_tmp = fEpsilon;
        eProbHT = -log( 1.0/pid_tmp - 1.0 )*( 1./double( tau ));
    }
    else {
        track.summaryValue( eProbHT, ( xAOD::SummaryType )48 ); // type 48 = eProbabilityHT
    }

    return eProbHT;
}

UChar_t LightGBMPredictor::numberOfPixelHitsPlusDead( const xAOD::TrackParticle track ) const {
    return numberOfPixelHits( track ) + numberOfPixelDeadSensors( track );
}

UChar_t LightGBMPredictor::numberOfSCTHitsPlusDead( const xAOD::TrackParticle track ) const {
    return numberOfSCTHits( track ) + numberOfSCTDeadSensors( track );
}

float LightGBMPredictor::EptRatio( const xAOD::Electron electron ) const{
    const xAOD::TrackParticle track = *(electron.trackParticle());
    const TLorentzVector& el_p4 = electron.p4();

    return ( track.pt() > 0.0f ) ? el_p4.Pt()  / track.pt() : -999.f;
}

float LightGBMPredictor::deltaR( const float eta1, const float phi1, const float eta2, const float phi2 ) const{
	const float deta = fabs(eta1 - eta2);

	float dphi;
	if ( fabs(phi1 - phi2 - 2.0*3.14159265359) < fabs(phi1 - phi2) ) {
		dphi = fabs(phi1 - phi2 - 2.0*3.14159265359) ;
	} else {
		dphi = fabs(phi1 - phi2);
	}

	return sqrt( deta*deta + dphi*dphi );
}

float getPtAtFirstMeasurement( const xAOD::TrackParticle* tp ) {
    if ( tp == nullptr ) return 0.0f;
    unsigned int index;
    if ( tp->indexOfParameterAtPosition( index, xAOD::FirstMeasurement ) )
        return std::hypot(tp->parameterPX(index), tp->parameterPY(index)); // hypotenuse with overflow safety
    return tp->pt();
}

std::tuple< float, float, float, float > LightGBMPredictor::conversionVars( const xAOD::Photon photon ) const{
    const xAOD::Vertex* vertex = photon.vertex();
    const xAOD::CaloCluster* cluster = photon.caloCluster();

    float photonConversionType, photonConversionRadius, photonVertexConvPtRatio, photonVertexConvEtOverPt, vertexPtConv, vertexPt1, vertexPt2, eAccCluster;

    eAccCluster = cluster->energyBE(1) + cluster->energyBE(2) + cluster->energyBE(3);

    photonConversionType = photon.conversionType();
    photonConversionRadius = photon.conversionRadius();

    if ( vertex != nullptr ) {

        // alternative vertex pt
        if ( vertex->nTrackParticles() == 1 ) {
            vertexPtConv = vertex->trackParticle(0)->p4().Perp(); // float
        } else if ( vertex->nTrackParticles() == 2 ) {
            vertexPtConv = (vertex->trackParticle(0)->p4()+vertex->trackParticle(1)->p4()).Perp(); // float
        }

        // Pt of electrons from photon conversion
        vertexPt1 = (Accessor_pt1.isAvailable(*vertex)) ? Accessor_pt1(*vertex) : getPtAtFirstMeasurement(vertex->trackParticle(0)); // float
        vertexPt2 = (Accessor_pt2.isAvailable(*vertex)) ? Accessor_pt2(*vertex) : getPtAtFirstMeasurement(vertex->trackParticle(1)); // float

        if (xAOD::EgammaHelpers::numberOfSiTracks(vertex) == 2) {
            // pt ratio between electrons from converted photon
            photonVertexConvPtRatio = std::max(vertexPt1, vertexPt2)/(vertexPt1+vertexPt2); // float
            // et over pt for conversions
            photonVertexConvEtOverPt = std::min( std::max( 0.0f, float(eAccCluster/(std::cosh(cluster->eta())*vertexPtConv) )), 2.0f ); // float
        }
    }

    // const float vars[4] = { photonConversionType, photonConversionRadius, photonVertexConvPtRatio, photonVertexConvEtOverPt};
    // return vars;

    return {photonConversionType, photonConversionRadius, photonVertexConvPtRatio, photonVertexConvEtOverPt};
}


/// This ptconv is the old one used by MVACalib
float compute_ptconv(const xAOD::Photon* ph) {
    auto vx = ph->vertex();
    if (!vx) return 0.0;

    TLorentzVector sum;
    if (vx->trackParticle(0)) sum += vx->trackParticle(0)->p4();
    if (vx->trackParticle(1)) sum += vx->trackParticle(1)->p4();
    return sum.Perp();
}

float compute_pt1conv(const xAOD::Photon* ph) {
    static const SG::AuxElement::Accessor<float> accPt1("pt1");
    const xAOD::Vertex* vx = ph->vertex();

    if (!vx) return 0.0;
    if (accPt1.isAvailable(*vx)) {
        return accPt1(*vx);
    } else {
        return getPtAtFirstMeasurement(vx->trackParticle(0));
    }
}

float compute_pt2conv(const xAOD::Photon* ph) {
    static const SG::AuxElement::Accessor<float> accPt2("pt2");
    const xAOD::Vertex* vx = ph->vertex();

    if (!vx) return 0.0;
    if (accPt2.isAvailable(*vx)) {
    return accPt2(*vx);
    } else {
    return getPtAtFirstMeasurement(vx->trackParticle(1));
    }
}

float LightGBMPredictor::convEtOverPt(const xAOD::Photon* ph) const{
    auto cl = ph->caloCluster();

    float Eacc = cl->energyBE(1) + cl->energyBE(2) + cl->energyBE(3);
    float cl_eta = cl->eta();

    float rv = 0.0;
    if (xAOD::EgammaHelpers::numberOfSiTracks(ph) == 2) {
        rv = std::max(0.0f, Eacc/(std::cosh(cl_eta)*compute_ptconv(ph)));
    }
    return std::min(rv, 2.0f);
}

float LightGBMPredictor::convPtRatio(const xAOD::Photon* ph) const{
    if (xAOD::EgammaHelpers::numberOfSiTracks(ph) == 2) {
        auto pt1 = compute_pt1conv(ph);
        auto pt2 = compute_pt2conv(ph);
        return std::max(pt1, pt2)/(pt1+pt2);
    } else {
        return 1.0f;
    }
}

float LightGBMPredictor::et_calo(const xAOD::Photon photon) const{
    const TLorentzVector& pho_p4 = photon.p4();
    float et;
    et = pho_p4.Pt();
    return et / 1000. ;
}

double LightGBMPredictor::predict( const xAOD::Electron& electron ) const{
    // Make score variable
    double score;

    if ( m_hash == "1463528748" ){
        const xAOD::TrackParticle tt = *(electron.trackParticle());

        const double features[14] = { double(Rhad1(electron)), double(Rhad(electron)), double(f3(electron)), double(weta2(electron)), double(Rphi(electron)), double(Reta(electron)),
                                    double(Eratio(electron)), double(f1(electron)), double(d0(tt)), double(d0Sig(tt)), double(dPOverP(tt)),
                                    double(deltaEta1(electron)), double(deltaPhiRescaled2(electron)), double(TRTPID(tt)) };

        // Predict on features, save in score
        m_booster->Predict(features, &score, &m_early_stop);
    }

    else if ( m_hash == "2015088566" ){
        const xAOD::TrackParticle tt = *(electron.trackParticle());

        const double features[19] = { double(Rhad1(electron)), double(Rhad(electron)), double(f3(electron)), double(weta2(electron)), double(Rphi(electron)), double(Reta(electron)),
                                    double(Eratio(electron)), double(f1(electron)), double(numberOfInnermostPixelHits(tt)), double(numberOfPixelHitsPlusDead(tt)),
                                    double(numberOfSCTHitsPlusDead(tt)), double(d0(tt)), double(d0Sig(tt)), double(dPOverP(tt)),
                                    double(deltaEta1(electron)), double(deltaPhiRescaled2(electron)), double(EptRatio(electron)), double(TRTPID(tt)), double(wtots1(electron)) };

        // Predict on features, save in score
        m_booster->Predict(features, &score, &m_early_stop);

    }

    else if ( m_hash == "3697977657" ){
        // fwd_lgbmPid
        const xAOD::CaloCluster* cluster = electron.caloCluster();

        double secondLambdaCluster;
        double lateralCluster;
        double longitudinalCluster;
        double fracMaxCluster;
        double secondRCluster;
        double centerLambdaCluster;

        if ( !cluster->retrieveMoment( xAOD::CaloCluster::SECOND_LAMBDA, secondLambdaCluster ) )
                ANA_MSG_WARNING("cannot find SECOND_LAMBDA"); // double
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::LATERAL, lateralCluster ) )
                ANA_MSG_WARNING("cannot find LATERAL"); // double
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::LONGITUDINAL, longitudinalCluster ) )
                ANA_MSG_WARNING("cannot find LONGITUDINAL"); // double
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::ENG_FRAC_MAX, fracMaxCluster ) )
                ANA_MSG_WARNING("cannot find ENG_FRAC_MAX"); // double
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::SECOND_R, secondRCluster ) )
                ANA_MSG_WARNING("cannot find SECOND_R"); // double
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::CENTER_LAMBDA, centerLambdaCluster ) )
                ANA_MSG_WARNING("cannot find CENTER_LAMBDA"); // double

        const double features[6] = { centerLambdaCluster, fracMaxCluster, lateralCluster, longitudinalCluster, secondRCluster, secondLambdaCluster };

        m_booster->Predict(features, &score, &m_early_stop);

    }

    else if ( m_hash == "6ATvars" ){
        // fwd_lgbmPid from lwentzel/work/FinalForwardModels/PID_model_6ATvars.txt
        const xAOD::CaloCluster* cluster = electron.caloCluster();

        double secondLambdaCluster;
        double lateralCluster;
        double longitudinalCluster;
        double fracMaxCluster;
        double secondRCluster;
        double centerLambdaCluster;

        if ( !cluster->retrieveMoment( xAOD::CaloCluster::SECOND_LAMBDA, secondLambdaCluster ) )
                ANA_MSG_WARNING("cannot find SECOND_LAMBDA"); // double
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::LATERAL, lateralCluster ) )
                ANA_MSG_WARNING("cannot find LATERAL"); // double
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::LONGITUDINAL, longitudinalCluster ) )
                ANA_MSG_WARNING("cannot find LONGITUDINAL"); // double
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::ENG_FRAC_MAX, fracMaxCluster ) )
                ANA_MSG_WARNING("cannot find ENG_FRAC_MAX"); // double
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::SECOND_R, secondRCluster ) )
                ANA_MSG_WARNING("cannot find SECOND_R"); // double
        if ( !cluster->retrieveMoment( xAOD::CaloCluster::CENTER_LAMBDA, centerLambdaCluster ) )
                ANA_MSG_WARNING("cannot find CENTER_LAMBDA"); // double

        const double features[6] = { centerLambdaCluster, fracMaxCluster, lateralCluster, longitudinalCluster, secondRCluster, secondLambdaCluster };

        m_booster->Predict(features, &score, &m_early_stop);

    }

    else if ( m_hash == "2590916238" ){

        const double feature[4] = { double(topoetcone20(electron)), double(topoetcone30(electron)), double(topoetcone40(electron)), double(topoetconecoreConeEnergyCorrection(electron))};

        m_booster->Predict(feature, &score, &m_early_stop);
    }

    else{
        score = -10.0;
    }

    return -1. / 15. * log( 1. / score - 1. );

}

double LightGBMPredictor::predict( const xAOD::Photon& photon, float averageMu, int NvtxReco ) const{
    // Make score variable
    double score;

    // const float vars[4] = conversionVars(photon);
    float photonConversionType, photonConversionRadius, photonVertexConvPtRatio, photonVertexConvEtOverPt;
    std::tie(photonConversionType, photonConversionRadius, photonVertexConvPtRatio, photonVertexConvEtOverPt) = conversionVars(photon);

    if ( m_hash == "47234812" ){ // PDF Photon Vars

        const double features[11] = { double(DeltaE(photon)), double(Eratio(photon)), double(f1(photon)), double(fracs1(photon)), double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)),
                                      double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        // Predict on features, save in score
        m_booster->Predict(features, &score, &m_early_stop);
    }

    else if ( m_hash == "phoPdf01" ){
        // PDF Photon vars trained on data from /groups/hep/bhenckel/FinalPhotonModelsBKH/PhotonDATA_pdf_model.txt

        const double features[11] = { double(DeltaE(photon)), double(Eratio(photon)), double(f1(photon)), double(fracs1(photon)), double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)),
                                      double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        m_booster->Predict(features, &score, &m_early_stop);

    }

    else if ( m_hash == "phoPdf02" ){
        // PDF Photon vars trained on MC from /groups/hep/bhenckel/FinalPhotonModelsBKH/PhotonMC_pdf_model.txt

        const double features[11] = { double(DeltaE(photon)), double(Eratio(photon)), double(f1(photon)), double(fracs1(photon)), double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)),
                                      double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        // Predict on features, save in score
        m_booster->Predict(features, &score, &m_early_stop);

    }

    else if ( m_hash == "2437115810" ){

        const double features[15] = { double(DeltaE(photon)), double(Eratio(photon)), double(f1(photon)), double(fracs1(photon)),
                                      double(photonConversionRadius), double(photonConversionType), double(convEtOverPt(&photon)), double(convPtRatio(&photon)),
                                      double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)), double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        // Predict on features, save in score
        m_booster->Predict(features, &score, &m_early_stop);

    }

    else if ( m_hash == "phoConv01" ){
        // PDF Photon vars trained on data from /groups/hep/bhenckel/FinalPhotonModelsBKH/PhotonDATA_conv_model.txt

        const double features[15] = { double(DeltaE(photon)), double(Eratio(photon)), double(f1(photon)), double(fracs1(photon)),
                                      double(photonConversionRadius), double(photonConversionType), double(convEtOverPt(&photon)), double(convPtRatio(&photon)),
                                      double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)), double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        // Predict on features, save in score
        m_booster->Predict(features, &score, &m_early_stop);

    }

    else if ( m_hash == "phoConv02" ){
        // PDF Photon vars trained on MC from /groups/hep/bhenckel/FinalPhotonModelsBKH/PhotonMC_conv_model.txt

        const double features[15] = { double(DeltaE(photon)), double(Eratio(photon)), double(f1(photon)), double(fracs1(photon)),
                                      double(photonConversionRadius), double(photonConversionType), double(convEtOverPt(&photon)), double(convPtRatio(&photon)),
                                      double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)), double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        // Predict on features, save in score
        m_booster->Predict(features, &score, &m_early_stop);

    }

    else if ( m_hash == "956659904" ){

        const double features[18] = { double(averageMu), double(DeltaE(photon)), double(Eratio(photon)), double(et_calo(photon)), double(eta(photon)), double(f1(photon)), double(fracs1(photon)),
                                      double(photonConversionRadius), double(photonConversionType),double(convEtOverPt(&photon)), double(convPtRatio(&photon)),
                                      double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)), double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        m_booster->Predict(features, &score, &m_early_stop);

    }

    else if ( m_hash == "phoBin01" ){
        // PDF Photon vars trained on data from /groups/hep/bhenckel/FinalPhotonModelsBKH/PhotonDATA_bin_model.txt

        const double features[18] = { double(averageMu), double(DeltaE(photon)), double(Eratio(photon)), double(et_calo(photon)), double(eta(photon)), double(f1(photon)), double(fracs1(photon)),
                                      double(photonConversionRadius), double(photonConversionType),double(convEtOverPt(&photon)), double(convPtRatio(&photon)),
                                      double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)), double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        m_booster->Predict(features, &score, &m_early_stop);

    }

    else if ( m_hash == "phoBin02" ){
        // PDF Photon vars trained on MC from /groups/hep/bhenckel/FinalPhotonModelsBKH/PhotonMC_bin_model.txt

        const double features[18] = { double(averageMu), double(DeltaE(photon)), double(Eratio(photon)), double(et_calo(photon)), double(eta(photon)), double(f1(photon)), double(fracs1(photon)),
                                      double(photonConversionRadius), double(photonConversionType),double(convEtOverPt(&photon)), double(convPtRatio(&photon)),
                                      double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)), double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        m_booster->Predict(features, &score, &m_early_stop);

    }

    else if ( m_hash == "896011677" ){

        const double feature[22] = { double(averageMu), double(core57cellsEnergyCorrection(photon)), double(DeltaE(photon)), double(Eratio(photon)), double(et_calo(photon)),
                                     double(eta(photon)), double(f1(photon)), double(fracs1(photon)), double(maxEcell_energy(photon)), double(maxEcell_time(photon)), double(photonConversionRadius),
                                     double(photonConversionType), double(convEtOverPt(&photon)), double(convPtRatio(&photon)), double(r33over37allcalo(photon)),
                                     double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)), double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        m_booster->Predict(feature, &score, &m_early_stop);
    }

    else if ( m_hash == "3307376757" ){

        const double feature[19] = { double(averageMu), double(core57cellsEnergyCorrection(photon)), double(DeltaE(photon)), double(Eratio(photon)), double(et_calo(photon)), double(eta(photon)),
                                     double(f1(photon)), double(fracs1(photon)), double(maxEcell_energy(photon)), double(maxEcell_time(photon)), double(photonConversionType), double(r33over37allcalo(photon)),
                                     double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)), double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        m_booster->Predict(feature, &score, &m_early_stop);

    }

    else if ( m_hash == "4210130482" ){

        const double feature[20] = { double(averageMu), double(core57cellsEnergyCorrection(photon)), double(DeltaE(photon)), double(Eratio(photon)), double(et_calo(photon)),
                                     double(eta(photon)), double(f1(photon)), double(fracs1(photon)), double(photonConversionRadius),
                                     double(photonConversionType), double(convEtOverPt(&photon)), double(convPtRatio(&photon)), double(r33over37allcalo(photon)),
                                     double(Reta(photon)), double(Rhad(photon)), double(Rhad1(photon)), double(Rphi(photon)), double(weta1(photon)), double(weta2(photon)), double(wtots1(photon)) };

        m_booster->Predict(feature, &score, &m_early_stop);

    }

    else if ( m_hash == "3898251691" ){

        const double feature[9] = { double(averageMu), double(NvtxReco), double(et_calo(photon)), double(topoetcone20(photon)), double(topoetcone30(photon)), double(topoetcone40(photon)),
                                    double(ptvarcone20(photon)), double(ptvarcone30(photon)), double(ptvarcone40(photon)) };

        m_booster->Predict(feature, &score, &m_early_stop);
    }

    else{
        score = -10.0;
    }

    return score;

}


double LightGBMPredictor::predict( const xAOD::Electron& electron, int NvtxReco, float actualMu ) const{
    // Make score variable
    double score;

    if ( m_hash == "1305298504" ){
        const xAOD::CaloCluster* cluster = electron.caloCluster();
        const TLorentzVector& el_p4 = electron.p4();

        const double features[9] = { double(cluster->etaBE(2)), el_p4.Eta(), double( cluster->energyBE(1)+cluster->energyBE(2)+cluster->energyBE(3) ),
                                     double(NvtxReco), double(actualMu),
                                     double(cluster->energyBE(3)), double(cluster->energyBE(2)), el_p4.Phi(), double(cluster->energyBE(1))
                                    };

        m_booster->Predict(features, &score, &m_early_stop);

        return score;
    }

    else{
        score = 0.0;
    }

    return score;

}

// double LightGBMPredictor::predict( const xAOD::Electron& electron, int NvtxReco ) const{
//     // Make score variable
//     double score;
//
//     if ( m_hash == "6vars" ){
//         // fwd_lgbmPid
//         const xAOD::CaloCluster* cluster = electron.caloCluster();
//         // const TLorentzVector& el_p4 = electron.p4();
//
//         double lateralCluster;
//         double fracMaxCluster;
//         double centerLambdaCluster;
//         double topoetconecoreConeEnergyCorrection = double(topoetconecoreConeEnergyCorrection(electron));
//
//         if ( !cluster->retrieveMoment( xAOD::CaloCluster::LATERAL, lateralCluster ) )
//                 ANA_MSG_WARNING("cannot find LATERAL"); // double
//         if ( !cluster->retrieveMoment( xAOD::CaloCluster::ENG_FRAC_MAX, fracMaxCluster ) )
//                 ANA_MSG_WARNING("cannot find ENG_FRAC_MAX"); // double
//         if ( !cluster->retrieveMoment( xAOD::CaloCluster::CENTER_LAMBDA, centerLambdaCluster ) )
//                 ANA_MSG_WARNING("cannot find CENTER_LAMBDA"); // double
//
//         const double features[6] = { centerLambdaCluster, fracMaxCluster, lateralCluster, topoetconecoreConeEnergyCorrection,
//                                      double(NvtxReco)
//                                     };
//
//         m_booster->Predict(features, &score, &m_early_stop);
//
//         return score;
//     }
//
//     else{
//         score = 0.0;
//     }
//
//     return score;
//
// }

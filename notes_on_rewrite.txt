Notes on changes from DAOD processing to AOD processing:

----------
Get event_isMC
----------
Changed using the following link:
https://atlassoftwaredocs.web.cern.ch/ABtutorial/basic_ana_tool_handle/#using-the-tool-itself



----------
From Accessor_DFCommonPhotonsIsEMLoose to AsgPhotonIsEMSelector (named: m_photonsIsEMLoose)
----------
The following link was used:
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2?fbclid=IwAR1mdll8uNZv2k-SxcRtn1qqfqNdSngHvfpoiOCTSiDiJYFApg3d5oQMeks#Recomputing_the_photon_ID_flags

AsgPhotonIsEMSelector header:
https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools/ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h

AsgPhotonIsEMSelector isEMMask:
Done using bit numbering. Bitnumbers on variables are shown here:
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2?fbclid=IwAR1mdll8uNZv2k-SxcRtn1qqfqNdSngHvfpoiOCTSiDiJYFApg3d5oQMeks#Photon_isEM_word
Calculation in Root gives:
Tight: 4193281 = (1<<0) | (1<<10) | (1<<11) | (1<<12) | (1<<13) | (1<<14) | (1<<15) | (1<<16) | (1<<17) | (1<<18) | (1<<19) | (1<<20) | (1<<21)
Loose: 31745   = (1<<0) | (1<<10) | (1<<11) | (1<<12) | (1<<13) | (1<<14)



----------
From DFCommonElectronsLHLoose to AsgElectronLikelihoodTool
----------
The following link was used:
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EGammaIdentificationRun2?fbclid=IwAR1mdll8uNZv2k-SxcRtn1qqfqNdSngHvfpoiOCTSiDiJYFApg3d5oQMeks#Usage_of_the_Electron_ID_tool
https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools/trunk/util/testEGIdentificationPoints.cxx



----------
From DFCommonForwardElectronsLHLoose to AsgForwardElectronLikelihoodTool
----------
The following link was used:
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2?fbclid=IwAR1mdll8uNZv2k-SxcRtn1qqfqNdSngHvfpoiOCTSiDiJYFApg3d5oQMeks#LH_based_forward_electron_identi



----------
From DFCommonJets_eventClean_LooseBad to JetCleaningTool.h
----------
The following link was used:
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HowToCleanJets2017?fbclid=IwAR2MRCi-fGSD1g1fM5aztdQNr_WcX0_b23pW-ACQo1Jmp6Lp0RGjHXUQvxg#Jet_Cleaning_Tool
Remember to add the tool to source/MyAnalysis/CMakeLists.txt



----------
Missing electron variables
----------
For: ele_d0, ele_d0sig ele_numberOfInnermostPixelHits, ele_numberOfPixelHits & ele_numberOfSCTHits,
TrackParticle variables found here:
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaD3PDtoxAOD?fbclid=IwAR2bdap3_oTFxtv6fTCP8xktJjihXAskiup2335TiUx_6AMWzG7bJEqh1Zk#TrackParticle



----------
Pileup Reweighting
----------
Inspiration:
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ExtendedPileupReweighting?fbclid=IwAR0_NZnR7kamw9QvLwW08KJ7gGqrwAS-4qWYqq9uKKfthsV2aXaeXl3yg-4#Tool_Properties
Get correct filetype with correct AMI tag, and get the files they refer to.

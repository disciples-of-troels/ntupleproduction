Potential good signal for low energy electrons (and photons) in MC:

rucio ls mc16_13TeV.*DAOD_EGAM3.*r10210_p3956 | grep CONT | sort
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/rucio-clients/1.20.8/lib/python2.7/site-packages/requests/__init__.py:91: RequestsDependencyWarning: urllib3 (1.24.3) or chardet (2.2.1) doesn't match a supported version!
  RequestsDependencyWarning)
| mc16_13TeV:mc16_13TeV.301535.Sherpa_CT10_eegammaPt10_35.deriv.DAOD_EGAM3.e3952_s3126_r10201_r10210_p3956                     | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301899.Sherpa_CT10_eegammaPt35_70.deriv.DAOD_EGAM3.e3952_s3126_r10201_r10210_p3956                     | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301900.Sherpa_CT10_eegammaPt70_140.deriv.DAOD_EGAM3.e3952_s3126_r10201_r10210_p3956                    | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301901.Sherpa_CT10_eegammaPt140.deriv.DAOD_EGAM3.e3952_s3126_r10201_r10210_p3956                       | CONTAINER    |
| mc16_13TeV:mc16_13TeV.366140.Sh_224_NN30NNLO_eegamma_LO_pty_7_15.deriv.DAOD_EGAM3.e7006_e5984_s3126_r10201_r10210_p3956      | CONTAINER    |
| mc16_13TeV:mc16_13TeV.366141.Sh_224_NN30NNLO_eegamma_LO_pty_15_35.deriv.DAOD_EGAM3.e7006_e5984_s3126_r10201_r10210_p3956     | CONTAINER    |
| mc16_13TeV:mc16_13TeV.366142.Sh_224_NN30NNLO_eegamma_LO_pty_35_70.deriv.DAOD_EGAM3.e7006_e5984_s3126_r10201_r10210_p3956     | CONTAINER    |
| mc16_13TeV:mc16_13TeV.366143.Sh_224_NN30NNLO_eegamma_LO_pty_70_140.deriv.DAOD_EGAM3.e7006_e5984_s3126_r10201_r10210_p3956    | CONTAINER    |
| mc16_13TeV:mc16_13TeV.366144.Sh_224_NN30NNLO_eegamma_LO_pty_140_E_CMS.deriv.DAOD_EGAM3.e7006_e5984_s3126_r10201_r10210_p3956 | CONTAINER    |


For bkg:
| mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM3.e3601_e5984_s3126_r10201_r10210_p3954                        | CONTAINER    |
| mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM3.e3601_s3126_r10201_r10210_p3954                              | CONTAINER    |


Signal in data:
rucio ls data17_13TeV.*DAOD_EGAM3.*r10258* | grep CONT | sort
| data17_13TeV:data17_13TeV.00334842.physics_Main.deriv.DAOD_EGAM3.r10258_p3399_p3518                | CONTAINER    |
| data17_13TeV:data17_13TeV.00334842.physics_Main.deriv.DAOD_EGAM3.r10258_p3399_p3948                | CONTAINER    |
| data17_13TeV:data17_13TeV.00334849.physics_Main.deriv.DAOD_EGAM3.r10258_p3399_p3518                | CONTAINER    |
| data17_13TeV:data17_13TeV.00334849.physics_Main.deriv.DAOD_EGAM3.r10258_p3399_p3948                | CONTAINER    |
| data17_13TeV:data17_13TeV.00334890.physics_Main.deriv.DAOD_EGAM3.r10258_p3399_p3518                | CONTAINER    |
| data17_13TeV:data17_13TeV.00334890.physics_Main.deriv.DAOD_EGAM3.r10258_p3399_p3948                | CONTAINER    |




-------------------------------------------------------------------------------------------------------
Electrons from Zee range:

MC:

rucio ls mc16_13TeV.*DAOD_EGAM1.*r9778_p324* | grep CONT | sort
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/rucio-clients/1.20.8/lib/python2.7/site-packages/requests/__init__.py:91: RequestsDependencyWarning: urllib3 (1.24.3) or chardet (2.2.1) doesn't match a supported version!
  RequestsDependencyWarning)
| mc16_13TeV:mc16_13TeV.301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241                 | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241                 | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241                 | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241                 | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241                 | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241                | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301006.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1000M1250.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301007.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1250M1500.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301008.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1500M1750.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301009.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1750M2000.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301010.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2000M2250.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301011.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2250M2500.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301012.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2500M2750.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301013.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2750M3000.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301014.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3000M3500.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301015.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3500M4000.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301016.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4000M4500.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301017.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4500M5000.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241               | CONTAINER    |
| mc16_13TeV:mc16_13TeV.301018.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_5000M.deriv.DAOD_EGAM1.e3649_s3126_r9781_r9778_p3241                   | CONTAINER    |
| mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_s3126_r9781_r9778_p3241                          | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EGAM1.e5299_s3126_r9781_r9778_p3241            | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EGAM1.e5299_e5984_s3126_r9781_r9778_p3241    | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.deriv.DAOD_EGAM1.e5299_e5984_s3126_r9781_r9778_p3241         | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EGAM1.e5299_e5984_s3126_r9781_r9778_p3241    | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EGAM1.e5299_e5984_s3126_r9781_r9778_p3241  | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.deriv.DAOD_EGAM1.e5299_e5984_s3126_r9781_r9778_p3241       | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EGAM1.e5299_e5984_s3126_r9781_r9778_p3241   | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EGAM1.e5299_e5984_s3126_r9781_r9778_p3241 | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.deriv.DAOD_EGAM1.e5299_e5984_s3126_r9781_r9778_p3241      | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EGAM1.e5299_e5984_s3126_r9781_r9778_p3241   | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EGAM1.e5299_e5984_s3126_r9781_r9778_p3241 | CONTAINER    |
| mc16_13TeV:mc16_13TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.deriv.DAOD_EGAM1.e5299_e5984_s3126_r9781_r9778_p3241      | CONTAINER    |


rucio ls mc16_13TeV.*DAOD_EGAM1.*r10726* | grep CONT | sort
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/rucio-clients/1.20.8/lib/python2.7/site-packages/requests/__init__.py:91: RequestsDependencyWarning: urllib3 (1.24.3) or chardet (2.2.1) doesn't match a supported version!
  RequestsDependencyWarning)
| mc16_13TeV:mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.deriv.DAOD_EGAM1.e3601_e5984_s3126_r10724_r10726_p3916                     | CONTAINER    |
| mc16_13TeV:mc16_13TeV.361103.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusenu.deriv.DAOD_EGAM1.e3601_e5984_s3126_r10724_r10726_p3916                    | CONTAINER    |
| mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_e5984_a875_r10724_r10726_p3638                           | CONTAINER    |
| mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_e5984_a875_r10724_r10726_p3918                           | CONTAINER    |
| mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_e5984_s3126_r10724_r10726_p3638                          | CONTAINER    |
| mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_e5984_s3126_r10724_r10726_p3918                          | CONTAINER    |
| mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_e5984_s3458_r11451_r10726_p3956                          | CONTAINER    |
| mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_e5984_s3459_r11452_r10726_p3956                          | CONTAINER    |
| mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_e5984_s3460_r11453_r10726_p3956                          | CONTAINER    |
| mc16_13TeV:mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_EGAM1.e3601_s3126_r11450_r10726_p3956

Background in MC:

rucio ls mc16_13TeV.*DAOD_EGAM7.*36*r10726* | grep CONT | sort
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/rucio-clients/1.20.8/lib/python2.7/site-packages/requests/__init__.py:91: RequestsDependencyWarning: urllib3 (1.24.3) or chardet (2.2.1) doesn't match a supported version!
  RequestsDependencyWarning)
| mc16_13TeV:mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_EGAM7.e6337_e5984_s3126_s3136_r10724_r10726_p3613                | CONTAINER    |
| mc16_13TeV:mc16_13TeV.423300.Pythia8EvtGen_A14NNPDF23LO_perf_JF17.deriv.DAOD_EGAM7.e3848_e5984_s3126_s3136_r10724_r10726_p3648                | CONTAINER    |
| mc16_13TeV:mc16_13TeV.423301.Pythia8EvtGen_A14NNPDF23LO_perf_JF23.deriv.DAOD_EGAM7.e3848_e5984_s3126_s3136_r10724_r10726_p3648                | CONTAINER    |
| mc16_13TeV:mc16_13TeV.423302.Pythia8EvtGen_A14NNPDF23LO_perf_JF35.deriv.DAOD_EGAM7.e3848_e5984_s3126_s3136_r10724_r10726_p3648                | CONTAINER    |
| mc16_13TeV:mc16_13TeV.423303.Pythia8EvtGen_A14NNPDF23LO_perf_JF50.deriv.DAOD_EGAM7.e3848_e5984_s3126_s3136_r10724_r10726_p3648                | CONTAINER    |
